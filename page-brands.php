<?php
/* Template Name: Brands */

// get taxonomy brand.
$terms = get_terms([
    'taxonomy'      => 'brand',
    'hide_empty'    => false,
    'orderby'       => 'name',
    'order'         => 'ASC',
]);

$last_char  = '';
$brands     = [];

// grouping by first letter of brand.
if( ! isset( $terms->errors ) && $terms ) {

    foreach( $terms as $term ) {
        $title = $term->name;
        $char  = $title[0];

        if( $char !== $last_char ) {
            $brands[$char] = [];
            $last_char = $char;
        }

        if( isset( $brands[$char] ) ) {
            $brands[$char][$term->term_id] = $title;
        }
    }

}
?>

<?php get_header(); ?>

    <div id="site-content" class="site-content site-content-about">
        <div class="site-content-inner">
            <main class="main" role="main">
                <div class="page-single wrapper-about">
                    <div class="brand">
                        <section class="section brand-header intro">
                            <div class="brand-header-inner">
                                <div class="text">
                                    <h3 class="text">POPULAR BRANDS</h3>
                                </div>
                                <div class="search">
                                    <input type="text" class="search-brand" id="search-brand" placeholder="Search">
                                </div>
                            </div>
                        </section>
                        <section class="section brand-content intro">
                            <?php if( $brands ) : ?>
                                <div class="grid">
                                    <?php foreach( $brands as $brand_char => $brand_list ) : ?>
                                        <div class="grid-sizer">
                                            <div class="grid-item">
                                                <div class="brand-inside">
                                                    <div class="brand-inside-items js-brand-expand">
                                                        <div class="brand-inside-head js-brand-expand">
                                                            <p class="text"><?php echo $brand_char ?></p>
                                                        </div>
                                                        <div class="brand-items-head js-brand-expand">
                                                            <span class="sub-label text-hide"><svg class="icon js-icon" role="img"><use xlink:href="<?= library_url() ?>/images/svg-symbols.svg#icon-chevron-down" /></svg></span>
                                                            <span class="sub-label text-view"><svg class="icon js-icon" role="img"><use xlink:href="<?= library_url() ?>/images/svg-symbols.svg#icon-chevron-down" /></svg></span>
                                                        </div>
                                                    </div>
                                                    <div class="brand-inside-content js-itembrand-expand">
                                                        <ul class="list">
                                                            <?php if( $brand_list ) : ?>
                                                                <?php foreach( $brand_list as $brand_id => $brand_name ) : ?>
                                                                    <li><a href="<?php echo get_term_link( (int) $brand_id ) ?>" class="link-brand"><?php echo $brand_name ?></a></li>
                                                                <?php endforeach; ?>
                                                            <?php endif; ?>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            <?php else : ?>
                                No brand found.
                            <?php endif; ?>
                        </section>
                    </div>
                </div>
            </main>
        </div>
    </div>

<?php get_footer(); ?>
