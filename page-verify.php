<?php
/* Template Name: Verify */

global $post;
?>

<?php get_header(); ?>

<div id="site-content" class="site-content site-content-about">
    <div class="site-content-inner">
        <main class="main" role="main">
            <div class="page-single wrapper-about">
                <div class="contact">
                    <div class="contact-page">
                        <div class="contact-page-content intro">
                            <section class="section page-content intro">
                                <div class="page-content-inner">
                                    <?php if( is_user_logged_in() ) : ?>
                                        <h2 class="text">Your account is verified</h2>
                                    <?php else : ?>
                                        <h2 class="text">Verify your account</h2>
                                    <?php endif; ?>
                                    <div class="page-content-wrapper wrapper">
                                        <div style="margin-top: 24px; margin-bottom: 24px">
                                            <?php wc_print_notices(); ?>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
</div>

<?php get_footer(); ?>