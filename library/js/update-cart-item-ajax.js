// initialize when document is ready
jQuery(document).ready(function($) {

    let body = $('body');

    body.on('change', '.cart-shipping-insurance', function () {

        // tidak dipakai, tp biarkan utk referensi project lain.
        //return;

        let cart_id = $(this).attr('data-cart-id'),
            item = $(this).closest('.item'),
            redirect_url = $(this).attr('data-redirect-url');

        $.ajax({
            method: "post",
            url: vars.ajaxurl,
            data: {
                action: 'update_cart_item',
                security: $('#woocommerce-cart-nonce').val(),
                shipping_insurance: $(this).prop('checked') ? 1 : 0,
                cart_id: cart_id
            },
            beforeSend: function () {
                item.addClass('is-loading');
            }
        }).done(function (resp) {
            if( resp ) {

                location.href = redirect_url;

            }

            item.removeClass('is-loading');
        }).fail(function (data) {
            item.removeClass('is-loading');
        });
    });

    body.on('change', '.cart-wood-packaging', function () {

        // tidak dipakai, tp biarkan utk referensi project lain.
        return;

        let cart_id = $(this).attr('data-cart-id'),
            item = $(this).closest('.item'),
            redirect_url = $(this).attr('data-redirect-url');

        $.ajax({
            method: "post",
            url: vars.ajaxurl,
            data: {
                action: 'update_cart_item',
                security: $('#woocommerce-cart-nonce').val(),
                wood_packaging: $(this).prop('checked') ? 1 : 0,
                cart_id: cart_id
            },
            beforeSend: function () {
                item.addClass('is-loading');
            }
        }).done(function (resp) {
            if( resp ) {

                location.href = redirect_url;

            }

            item.removeClass('is-loading');
        }).fail(function (data) {
            item.removeClass('is-loading');
        });
    })

});
