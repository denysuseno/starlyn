// initialize when document is ready
jQuery(document).ready(function($) {

    let body = $('body');

    body.on('change', '.shipping-packaging-flag', function () {

        let formCheckout = body.find('form.checkout'),
            wrapper = $('.shipping-packaging-flag-wrapper'),
            summary = $('.checkout-page-summary .content');

        $.ajax({
            method: "post",
            url: shipping_packaging_vars.ajaxurl,
            data: {
                action: 'update_shipping_packaging_flag',
                security: $('#woocommerce-cart-nonce').val(),
                shipping_packaging_flag: $(this).prop('checked') ? 1 : 0
            },
            beforeSend: function () {
                wrapper.addClass('is-loading');
                summary.addClass('is-loading');
            }
        }).done(function (resp) {
            if( resp ) {
                formCheckout.trigger('update');
                // location.href = shipping_insurance_vars.redirect_url;
            }
            wrapper.removeClass('is-loading');
            summary.removeClass('is-loading');
        }).fail(function (data) {
            wrapper.removeClass('is-loading');
            summary.removeClass('is-loading');
        });
    });

});
