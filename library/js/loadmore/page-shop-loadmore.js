// initialize when document is ready
jQuery(document).ready(function($) {

    let ajax_posts  = $( '#ajax-posts' ),
        btn_more    = $( '#more-posts-shop' );

    function load_posts() {

        $.ajax({
            url: page_shop_loadmore_vars.ajaxurl,
            data: {
                'action': 'page_shop_more_post_ajax',
                'query': page_shop_loadmore_vars.posts,
                'page': page_shop_loadmore_vars.current_page,
            },
            type: "POST",
            beforeSend: function () {
                btn_more.hide();
                ajax_posts.addClass( 'is-loading' );
            },
            success: function ( posts ) {
                btn_more.show();
                if( posts ){
                    ajax_posts.append( posts );
                    page_shop_loadmore_vars.current_page++;

                    if( page_shop_loadmore_vars.current_page == page_shop_loadmore_vars.max_page ) {
                        btn_more.hide(); // if last page, HIDE the button
                    }
                } else{
                    btn_more.hide(); // / if no data, HIDE the button as well
                }
                ajax_posts.removeClass( 'is-loading' );
            },
            error: function ( jqXHR, textStatus, errorThrown ) {
                console.log( jqXHR + " :: " + textStatus + " :: " + errorThrown );
                ajax_posts.removeClass( 'is-loading' );
            }
        });
        return false;
    }

    btn_more.on( 'click', function () {
        $(this).hide();
        load_posts();
    });

});