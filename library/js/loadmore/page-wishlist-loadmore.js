// initialize when document is ready
jQuery(document).ready(function($) {

    let ajax_posts  = $( '#ajax-posts-wishlist' ),
        btn_more    = $( '#more-posts-wishlist' ),
        params      = page_wishlist_loadmore_vars,
        page_number = 1;

    function load_posts() {
        page_number++;

        let str = '&page_number=' + parseInt( page_number ) + '&action=page_wishlist_more_post_ajax';

        $.ajax({
            type: "POST",
            dataType: "html",
            url: params.ajaxurl,
            data: str,
            beforeSend: function () {
                btn_more.hide();
                ajax_posts.addClass( 'is-loading' );
            },
            success: function ( data ) {
                if(data.length){
                    ajax_posts.append(data);

                    if( page_number < params.max_page ) {
                        btn_more.show();
                    }
                } else{
                    btn_more.hide();
                }
                ajax_posts.removeClass( 'is-loading' );
            },
            error: function ( jqXHR, textStatus, errorThrown ) {
                console.log( jqXHR + " :: " + textStatus + " :: " + errorThrown );
                ajax_posts.removeClass( 'is-loading' );
            }
        });
        return false;
    }

    btn_more.on( 'click', function () {
        $(this).hide();
        load_posts();
    });

});