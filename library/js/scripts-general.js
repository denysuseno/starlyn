// initialize when document is ready
jQuery(document).ready(function($) {

    let body = $('body');

    body.on('change', '.ajax-province', function () {
        let val = $(this).find('option:selected').text(),
            form = $(this).closest('form');
    });

    // Numeric only control handler
    jQuery.fn.ForceNumericOnly =
        function () {
            return this.each(function () {
                $(this).keydown(function (e) {
                    var key = e.charCode || e.keyCode || 0;
                    // allow backspace, tab, delete, enter, arrows, numbers and keypad numbers ONLY
                    // home, end, period, and numpad decimal
                    return (
                        key == 8 ||
                        key == 9 ||
                        key == 13 ||
                        key == 46 ||
                        key == 110 ||
                        key == 190 ||
                        (key >= 35 && key <= 40) ||
                        (key >= 48 && key <= 57) ||
                        (key >= 96 && key <= 105));
                });
            });
        };

    $(".force-number").ForceNumericOnly();

    function FErenderZoneOptions(data, selected){
        let html = '';
        let i=0;

        $.each(data, function(key, val) {

            let selectedStr = '';
            if( selected && selected === key ){
                selectedStr = 'selected="selected"';
            }

            html += '<option value="'+key+'" '+ selectedStr +'>'+val+'</option>';
        });
        return html;
    }

    function FErenderZoneDistrictOptions(data, selected){
        let html = '';
        let i=0;

        $.each(data, function(key, val) {
            let selectedStr = '';
            if( selected && selected === val.district_name ){
                selectedStr = 'selected="selected"';
            }

            html += '<option value="'+val.district_name+'" '+ selectedStr +' data-postcode="'+val.postcode+'">'+val.district_name+'</option>';
        });
        return html;
    }

    function FErenderZonePostcodeOptions(data, selected){
        let html = '<option value="">- Pilih Kode Pos -</option>';
        let i=0;
        $.each(data, function(key, val) {
            /*console.log([
                selected,
                val,
                selected && selected === val
        ]);*/
            let selectedStr = '';
            if( selected && selected === val ){
                selectedStr = 'selected="selected"';
            }

            html += '<option value="'+val+'" '+ selectedStr +' >'+val+'</option>';
        });
        return html;
    }

    let formCheckout = body.find('form.checkout');

    if( $('.form-address-book').length > 0 ) {

        let hasProvince         = body.find('.province-ajax'),
            hasRegency          = body.find('.regency-ajax'),
            hasDistrict         = body.find('.district-ajax'),
            hasPostcode         = body.find('.postcode-ajax'),
            form                = hasProvince.closest('form'),
            resRegency          = "",
            resDistrict         = "",
            resPostcode         = "";

        function getRegency() {
            form.addClass('is-loading');

            return $.ajax({
                method: "get",
                url: $(hasProvince).attr('data-url'),
                data: { province: $(hasProvince).val() }
            }).done(function (resp) {
                form.removeClass('is-loading');
                let resp_json = jQuery.parseJSON( resp );
                if( resp_json.length !== 0 ) {
                    resRegency = FErenderZoneOptions( resp_json , $(hasRegency).attr('data-val'));
                } else {
                    resRegency = '<option value="">- Pilih Kabupaten / Kota -</option>';
                }
            }).fail(function (data) {
                form.removeClass('is-loading');
            });
        }

        function getDistrict() {
            form.addClass('is-loading');

            return $.ajax({
                method: "get",
                url: $(hasRegency).attr('data-url'),
                data: { regency: $(hasRegency).val() }
            }).done(function (resp) {
                form.removeClass('is-loading');
                let resp_json = jQuery.parseJSON( resp );
                if( resp_json.length !== 0 ) {
                    resDistrict = FErenderZoneDistrictOptions( resp_json , $(hasDistrict).attr('data-val'));
                } else {
                    resDistrict = '<option value="">- Pilih Kecamatan -</option>';
                }
            }).fail(function (data) {
                form.removeClass('is-loading');
            });
        }

        function getPostcode() {
            form.addClass('is-loading');

            return $.ajax({
                method: "get",
                url: $(hasDistrict).attr('data-url'),
                data: { regency: $(hasRegency).val(), district: $(hasDistrict).val() }
            }).done(function (resp) {
                form.removeClass('is-loading');
                let resp_json = jQuery.parseJSON( resp ),
                    valid_postcode = jQuery.inArray( "0", resp_json ) !== 0; // cek apakah ada value 0 di dalam array, apabila ada maka bernilai 0, apabila tidak ada maka bernilai -1

                resPostcode = FErenderZonePostcodeOptions( resp_json , $(hasPostcode).attr('data-val'));

                if( sn.is_production ) {
                    if( resp_json.length !== 0 && valid_postcode ) {

                    } else {
                        resPostcode = '<option value="">- Pilih Kode Pos -</option>';
                    }
                }
            }).fail(function (data) {
                form.removeClass('is-loading');
            });
        }

        $.when(
            getRegency()
        ).then(function () {
            $(hasRegency).html(resRegency);
            $.when(
                getDistrict()
            ).then(function () {
                $(hasDistrict).html(resDistrict);
                $.when(
                    getPostcode()
                ).then(function () {
                    $(hasPostcode).html(resPostcode);
                });
            });
        });

        $(hasProvince).on('change', function () {

            getRegency().done(function () {
                $(hasRegency).html(resRegency).change();
            });

        });

        $(hasRegency).on('change', function () {

            getDistrict().done(function () {
                $(hasDistrict).html(resDistrict).change();
            });

        });

        $(hasDistrict).on('change', function () {

            getPostcode().done(function () {
                $(hasPostcode).html(resPostcode).change();
            });

        });

        //formCheckout.trigger('update');
        formCheckout.find('#billing_state').trigger('change');

    }

    body.on('click', '.address-book-remove', function () {
        if( confirm('Are you sure?') ) {
            let contentWrapper = $('.account-main-content');

            contentWrapper.addClass('is-loading');
            return $.ajax({
                method: "get",
                url: $(this).attr('data-url'),
                data: { id: parseInt($(this).attr('data-address-id')) }
            }).done(function (resp) {
                if( resp ) {
                    location.href = $('.set-main-address').attr('data-redirect-url');
                }
                contentWrapper.removeClass('is-loading');
            }).fail(function (data) {
                contentWrapper.removeClass('is-loading');
            });
        }

        return false;
    });

    body.on('click', '.set-main-address', function () {
        if( confirm('Are you sure?') ) {
            let contentWrapper = $('.account-main-content');
            
            contentWrapper.addClass('is-loading');
            return $.ajax({
                method: "post",
                url: $(this).attr('data-url'),
                data: {
                    address_id                  : parseInt($(this).attr('data-address-id')) ,
                    address_name                : $(this).attr('data-address-name') ,
                    address_address_name        : $(this).attr('data-address-address-name') ,
                    address_country             : $(this).attr('data-address-country') ,
                    address_province            : $(this).attr('data-address-province') ,
                    address_regency             : $(this).attr('data-address-regency') ,
                    address_district            : $(this).attr('data-address-district') ,
                    address_postcode            : $(this).attr('data-address-postcode') ,
                    address_address             : $(this).attr('data-address-address') ,
                    address_phone               : $(this).attr('data-address-phone') ,
                    as_default_billing_address  : 'yes'
                }
            }).done(function (resp) {
                if( resp ) {
                    location.href = $('.set-main-address').attr('data-redirect-url');
                }
                contentWrapper.removeClass('is-loading');
            }).fail(function ( jqXHR, textStatus, errorThrown ) {
                console.log( jqXHR + " :: " + textStatus + " :: " + errorThrown );
                contentWrapper.removeClass('is-loading');
            });
        }

        return false;
    });

    $(".checkout-select-address").on('click', function () {
        let address_name                = $(this).attr('data-address-name') ,
            address_address_name        = $(this).attr('data-address-address-name') ,
            address_country             = $(this).attr('data-address-country') ,
            address_province            = $(this).attr('data-address-province') ,
            address_regency             = $(this).attr('data-address-regency') ,
            address_district            = $(this).attr('data-address-district') ,
            address_postcode            = $(this).attr('data-address-postcode') ,
            address_address             = $(this).attr('data-address-address') ,
            address_phone               = $(this).attr('data-address-phone'),
            address_is_main             = $(this).attr('data-address-is-main'),
            shippingList                = body.find('.shipping-list'),
            checkoutSummary             = $('.checkout-page-summary'),
            popupAddress                = body.find('#popup-address'),
            btnChooseAnotherAddress     = body.find('.btn-checkout-choose-another-address');

        shippingList.addClass('is-loading');
        checkoutSummary.addClass('is-loading');

        formCheckout.find('#billing_first_name').val(address_name);
        formCheckout.find('#billing_country').val(address_country);
        formCheckout.find('#billing_state').val(address_province);
        formCheckout.find('#billing_city').val(address_regency + ', ' + address_district);
        formCheckout.find('#billing_district').val(address_district);
        formCheckout.find('#billing_postcode').val(address_postcode);
        formCheckout.find('#billing_address_1').val(address_address);
        formCheckout.find('#billing_phone').val(address_phone);
        formCheckout.find('#billing_address_2').val(address_address_name);

        formCheckout.trigger('update');
        //formCheckout.find('#billing_state').trigger('change');

        if( $('#billing_first_name').val().length !== 0 ) {
            let shippingAddressPreview = $('.shipping-address-preview'),
                addressDetail = shippingAddressPreview.find('.address');

            addressDetail.removeClass('is-hidden');
        }

        setTimeout(function() {
            // close the popup
            popupAddress.removeClass('is-popped');
            body.removeClass();
            btnChooseAnotherAddress.removeClass('popup-address-toggler is-popped');

            $('#checkout-selected-address-name').html(formCheckout.find('#billing_first_name').val());
            $('#checkout-selected-address-salutation').html(formCheckout.find('#billing_address_2').val());
            $('#checkout-selected-address-phone').html(formCheckout.find('#billing_phone').val());
            $('#checkout-selected-address-address').html(formCheckout.find('#billing_address_1').val());
            $('#checkout-selected-address-district').html(formCheckout.find('#billing_district').val());
            $('#checkout-selected-address-city').html(formCheckout.find('#billing_city').val());
            $('#checkout-selected-address-postcode').html(formCheckout.find('#billing_postcode').val());

            if( parseInt( address_is_main ) !== 0 ) {
                $('#checkout-selected-address-is-main').html( '<span class="selected">Utama</span>' );
            } else {
                $('#checkout-selected-address-is-main').html( '' );
            }

            $('.checkout-selected-address-name').html( formCheckout.find('#billing_first_name').val() );
            $('.checkout-selected-address-salutation').html( '(' + formCheckout.find('#billing_address_2').val() + ')' );
            $('.checkout-selected-address-phone').html( formCheckout.find('#billing_phone').val() );
            $('.checkout-selected-address-address').html( formCheckout.find('#billing_address_1').val() );
            $('.checkout-selected-address-district').html( formCheckout.find('#billing_district').val() );
            $('.checkout-selected-address-city').html( formCheckout.find('#billing_city').val() );
            $('.checkout-selected-address-postcode').html( formCheckout.find('#billing_postcode').val() );

            if( parseInt( address_is_main ) !== 0 ) {
                $('.checkout-selected-address-is-main').html( '<span class="selected">Utama</span>' );
            } else {
                $('.checkout-selected-address-is-main').html( '' );
            }
        }, 1 ); // 1 second delay, half a second (500) seems comfortable too

        setTimeout( function () {
            shippingList.removeClass('is-loading');
            checkoutSummary.removeClass('is-loading');
        }, 2000);
    });

    formCheckout.find('#billing_state').trigger('change');

    body.on('click', '.show_payment_method_wrapping', function () {
        let $this               = body.find('.show_payment_method_wrapping'),
            paymentMethodDiv    = body.find('.checkout-payment-wrapper'),
            shippingMethodDiv   = body.find('.checkout-shipping-wrapper'),
            summaryPayment      = body.find('.summary-payment-wrapper'),
            summaryShipping     = body.find('.summary-shipping-wrapper'),
            btnPaymentShipping  = body.find('.btn-payment-fragments-shipping'),
            btnPaymentPayment   = body.find('.btn-payment-fragments-payment');

        body.addClass('is-loading');

        $("html, body").animate({scrollTop: 0}, "slow");

        setTimeout(function() {
            paymentMethodDiv.removeClass('is-hidden');
            shippingMethodDiv.addClass('is-hidden');

            summaryPayment.removeClass('is-hidden');
            summaryShipping.addClass('is-hidden');

            $this.removeClass('show_payment_method_wrapping');
            $this.addClass('btn-checkout-place-order');

            btnPaymentShipping.addClass('is-hidden');
            btnPaymentPayment.removeClass('is-hidden');

            body.removeClass('is-loading');

        }, 1000 ); // 1 second delay, half a second (500) seems comfortable too
    });

    body.on('click', '.btn-checkout-place-order', function () {
        $('button[name="woocommerce_checkout_place_order"]').trigger('click');
    });

    if( sn.is_checkout_page && sn.is_user_logged_in ) {

        if( ! sn.user_logged_in_empty_address_book ) {
            $.ajax({
                method: "get",
                url: sn.get_main_address_book_ajax_url,
                data: {
                    user_id : parseInt( sn.current_user_id ),
                },
                beforeSend: function () {
                    body.addClass('is-loading');
                }
            }).done(function (resp) {
                if( resp ) {
                    let data = jQuery.parseJSON( resp );

                    formCheckout.find('#billing_first_name').val( data.name );
                    formCheckout.find('#billing_country').val( data.country );
                    formCheckout.find('#billing_state').val( data.province );
                    formCheckout.find('#billing_city').val( data.regency + ', ' + data.district );
                    formCheckout.find('#billing_district').val( data.district );
                    formCheckout.find('#billing_postcode').val( data.postcode );
                    formCheckout.find('#billing_address_1').val( data.address );
                    formCheckout.find('#billing_phone').val( data.phone );
                    formCheckout.find('#billing_address_2').val( data.address_name );

                    $('#checkout-selected-address-name').html( data.name );
                    $('#checkout-selected-address-salutation').html( data.address_name );
                    $('#checkout-selected-address-phone').html( data.phone );
                    $('#checkout-selected-address-address').html( data.address );
                    $('#checkout-selected-address-district').html( data.district );
                    $('#checkout-selected-address-city').html( data.regency );
                    $('#checkout-selected-address-postcode').html( data.postcode );

                    if( data.as_default_billing_address ) {
                        $('#checkout-selected-address-is-main').html( '<span class="selected">Utama</span>' );
                    } else {
                        $('#checkout-selected-address-is-main').html( '' );
                    }

                    $('.checkout-selected-address-name').html( data.name );
                    $('.checkout-selected-address-salutation').html( '(' + data.address_name + ')' );
                    $('.checkout-selected-address-phone').html( data.phone );
                    $('.checkout-selected-address-address').html( data.address );
                    $('.checkout-selected-address-district').html( data.district );
                    $('.checkout-selected-address-city').html( data.regency );
                    $('.checkout-selected-address-postcode').html( data.postcode );

                    if( data.as_default_billing_address ) {
                        $('.checkout-selected-address-is-main').html( '<span class="selected">Utama</span>' );
                    } else {
                        $('.checkout-selected-address-is-main').html( '' );
                    }

                    // WAJIB! bug fix setelah selesai order & ingin order lagi.
                    formCheckout.find('#billing_state').trigger('change');

                }
                body.removeClass('is-loading');
            }).fail(function (data) {
                body.removeClass('is-loading');
            });
        } else {
            console.log( 'User ini BELUM memiliki addressbook!' );
        }

    }

    if( sn.is_cart_page ) {
        let inputQty = $('.woocommerce').find('input.qty');

        if( inputQty.length > 0 ) {

            $('.btn-cart-minus, .btn-cart-plus').on('click', function () {
                inputQty.change();
            });

            $('.woocommerce').on('change', 'input.qty', function () {
                body.addClass('is-loading');

                setTimeout(function () {
                    $("[name='update_cart']").trigger("click");
                }, 500); // 1 second delay, half a second (500) seems comfortable too
            });

        }
    }

    let myAccountOrderFilterDateFrom = $('.myaccount-orders-filter-date-from');
    let myAccountOrderFilterDateTo   = $('.myaccount-orders-filter-date-to');

    if( myAccountOrderFilterDateFrom.length > 0 && myAccountOrderFilterDateTo.length > 0 ) {
        myAccountOrderFilterDateFrom.on('change', function () {
            let form = myAccountOrderFilterDateFrom.closest('form');

            form.submit();
        });

        myAccountOrderFilterDateTo.on('change', function () {
            let form = myAccountOrderFilterDateTo.closest('form');

            form.submit();
        });
    }

    /*if( sn.is_checkout_page ) {
        let btnCheckoutPlaceOrder = $('.btn-checkout-place-order');

        if (btnCheckoutPlaceOrder.length > 0) {
            btnCheckoutPlaceOrder.on('click', function () {
                $('body').addClass('is-loading');
                $('button[name="woocommerce_checkout_place_order"]').trigger('click');
            })
        }

        let shippingAddressPreview = $('.shipping-address-preview'),
            addressDetail = shippingAddressPreview.find('.address');

        if ($('#billing_first_name').length > 0) {
            if ($('#billing_first_name').val().length === 0) {
                addressDetail.addClass('is-hidden');
            }
        }
    }*/

    $(".choose-payment-method-va").on('click', function () {

        let selectedBankCd = $(this).data('bank-code'),
            bankCd = $('select[name="bankCdVAV2"]');

        if( typeof selectedBankCd !== "undefined" ) {
            bankCd.val( '' );
            bankCd.val( selectedBankCd );
        }

    });

    $(".choose-payment-method-e-wallet").on('click', function () {

        let selectedMitraCd = $(this).data('mitra-code'),
            mitraCd = $('select[name="mitraCdEWalletV2"]');

        if( typeof selectedMitraCd !== "undefined" ) {
            mitraCd.val( '' );
            mitraCd.val( selectedMitraCd );
            $('#nicephone').focus();
        }

    });

    $(".choose-payment-method-payloan").on('click', function () {

        let selectedMitraCd = $(this).data('mitra-code'),
            mitraCd = $('select[name="mitraCdPayLoan"]');

        if( typeof selectedMitraCd !== "undefined" ) {
            mitraCd.val( '' );
            mitraCd.val( selectedMitraCd );
        }

    });


    body.on('click', '.js-payment-expand', function () {
        let li              = $(this).closest('li'),
            choosenPayment  = li.find('.checkout-select-payment-method').first(),
            va              = $(".choose-payment-method-va").first(),
            eWallet         = $(".choose-payment-method-e-wallet").first(),
            payloan         = $(".choose-payment-method-payloan").first();

        if( choosenPayment.length ) {
            if( choosenPayment.hasClass('choose-payment-method-va') && va.length ) {
                va.trigger('click');
            }
            if( choosenPayment.hasClass('choose-payment-method-e-wallet') && eWallet.length ) {
                eWallet.trigger('click');
            }
            if( choosenPayment.hasClass('choose-payment-method-payloan') && payloan.length ) {
                payloan.trigger('click');
            }
            choosenPayment.trigger('change');
        }
    });

    body.on('change', '.checkout-select-payment-method', function () {
        let gateway_id          = $(this).val(),
            checkoutPageMain    = body.find('.checkout-page-main'),
            checkoutPageSummary = body.find('.checkout-page-summary'),
            bankCd              = $('select[name="bankCdVAV2"]'),
            mitraCdEwallet      = $('select[name="mitraCdEWalletV2"]'),
            mitraCdPayloan      = $('select[name="mitraCdPayLoan"]');

        if( gateway_id !== 'nicepay_vav2' && bankCd.length > 0 ) {
            bankCd.val( '' );
        }

        if( gateway_id !== 'nicepay_ewalletv2' && mitraCdEwallet.length > 0 ) {
            mitraCdEwallet.val( '' );
        }

        if( gateway_id !== 'nicepay_payloan' && mitraCdPayloan.length > 0 ) {
            mitraCdPayloan.val( '' );
        }

        formCheckout.trigger('update');

        checkoutPageMain.addClass('is-loading');
        checkoutPageSummary.addClass('is-loading');

        setTimeout(function() {
            checkoutPageMain.removeClass('is-loading');
            checkoutPageSummary.removeClass('is-loading');

            if( gateway_id !== 'nicepay_ewalletv2' ) {
                $('.woocommerce-NoticeGroup-checkout').addClass( 'is-hidden' );
                $("html, body").animate({scrollTop: 0}, "slow");
            }
        }, 1000);
    });

    /** TEMPORARY DISABLED **/
    /*body.on("adding_to_cart", function( e, btn, data ) {
        let woodPackaging = $("#wood_packaging-" + btn.attr("data-product_id"));
        let shippingInsurance = $("#shipping_insurance-" + btn.attr("data-product_id"));

        data["wood_packaging"] = woodPackaging.prop('checked') ? 1 : 0;
        data["shipping_insurance"] = shippingInsurance.prop('checked') ? 1 : 0;

        return true;
    });*/

    body.on( 'click', '.homepage-product-nav', function () {
        $( '.home-product-display' ).addClass( 'is-loading' );

        setTimeout(function() {
            $( '.home-product-display' ).removeClass('is-loading');
        }, 250);
    } );

    body.on( 'change', '#billing_email_alias', function () {
        let val                 = $(this).val(),
            billing_email       = $('#billing_email'),
            checkout_guest_form = $('.checkout-guest-form'),
            hint_login          = $('.hint-login'),
            hint_register       = $('.hint-register'),
            hint_failed         = $('.hint-failed'),
            btn_payment         = $('.show_payment_method_wrapping').addClass('is-disabled');

        billing_email.val( val );

        $.ajax({
            method: "post",
            url: sn.check_existing_user_ajax_url,
            data: {
                user_email : val
            },
            beforeSend: function () {
                $(this).addClass('is-loading');
                checkout_guest_form.addClass( 'is-disabled' );
                hint_login.addClass('is-hidden');
                hint_register.addClass('is-hidden');
                hint_failed.addClass('is-hidden');
                btn_payment.addClass('is-disabled');
            }
        }).done(function (resp) {
            if( resp.success ) {
                if( resp.is_exists ) {
                    hint_login.removeClass('is-hidden');

                    //formCheckout.find('#billing_first_name').val( '' );
                    //formCheckout.find('#billing_country').val( '' );
                    //formCheckout.find('#billing_state').val( '' );
                    formCheckout.find('#billing_city').val( '' );
                    formCheckout.find('#billing_district').val( '' );
                    formCheckout.find('#billing_postcode').val( '' );
                    //formCheckout.find('#billing_address_1').val( '' );
                    //formCheckout.find('#billing_phone').val( '' );
                    //formCheckout.find('#billing_address_2').val( '' );

                    formCheckout.find('#billing_state_alias').val( '' );
                    formCheckout.find('#billing_regency_alias').val( '' );
                    formCheckout.find('#billing_district_alias').val( '' );
                    formCheckout.find('#billing_postcode_alias').val( '' );
                } else {
                    hint_register.removeClass('is-hidden');
                    checkout_guest_form.removeClass( 'is-disabled' );
                    btn_payment.removeClass('is-disabled');
                }
            } else {
                hint_failed.html( resp.message );
                hint_failed.removeClass('is-hidden');

                //formCheckout.find('#billing_first_name').val( '' );
                //formCheckout.find('#billing_country').val( '' );
                formCheckout.find('#billing_state').val( '' );
                formCheckout.find('#billing_city').val( '' );
                formCheckout.find('#billing_district').val( '' );
                formCheckout.find('#billing_postcode').val( '' );
                //formCheckout.find('#billing_address_1').val( '' );
                //formCheckout.find('#billing_phone').val( '' );
                //formCheckout.find('#billing_address_2').val( '' );

                formCheckout.find('#billing_state_alias').val( '' );
                formCheckout.find('#billing_regency_alias').val( '' );
                formCheckout.find('#billing_district_alias').val( '' );
                formCheckout.find('#billing_postcode_alias').val( '' );
            }

            formCheckout.trigger('update');
            $(this).removeClass('is-loading');
        }).fail(function (data) {
            $(this).removeClass('is-loading');
        });
    });

    let kotamadya;

    body.on( 'change', '#billing_first_name_alias', function () {
        let val = $(this).val();

        formCheckout.find('#billing_first_name').val( val );

        $('.checkout-selected-address-name').html( val );
    });

    body.on( 'change', '#billing_phone_alias', function () {
        let val = $(this).val();

        formCheckout.find('#billing_phone').val( val );

        $('.checkout-selected-address-phone').html( val );
    });

    body.on( 'change', '#billing_address_2_alias', function () {
        let val = $(this).val();

        formCheckout.find('#billing_address_2').val( val );

        $('.checkout-selected-address-salutation').html( '(' + val + ')' );
    });

    body.on( 'change', '#billing_address_1_alias', function () {
        let val = $(this).val();

        formCheckout.find('#billing_address_1').val( val );

        $('.checkout-selected-address-address').html( val );
    });

    body.on( 'change', '#billing_state_alias', function () {
        let val = $(this).val();

        formCheckout.find('#billing_state').val( val );

        formCheckout.trigger('update');
    });

    body.on( 'change', '#billing_regency_alias', function () {
        let val = $(this).val();

        kotamadya = '';
        kotamadya = val;

        formCheckout.find('#billing_city').val( val );

        $('.checkout-selected-address-city').html( val );

        formCheckout.trigger('update');
    });

    body.on( 'change', '#billing_district_alias', function () {
        let val = $(this).val();

        formCheckout.find('#billing_city').val( kotamadya + ', ' + val );
        formCheckout.find('#billing_district').val( val );

        $('.checkout-selected-address-district').html( val );

        formCheckout.trigger('update');
    });

    body.on( 'change', '#billing_postcode_alias', function () {
        let val = $(this).val();

        formCheckout.find('#billing_postcode').val( val );

        $('.checkout-selected-address-postcode').html( val );
    });

    body.on( 'change', '#order_comment_alias', function () {
        let val = $(this).val();

        formCheckout.find('#order_comments').val( val );
    });

    if( sn.is_checkout_page && ! sn.is_user_logged_in ) {

        formCheckout.find('#billing_email').val( '' );
        formCheckout.find('#billing_first_name').val( '' );
        formCheckout.find('#billing_phone').val( '' );
        formCheckout.find('#billing_address_1').val( '' );
        formCheckout.find('#billing_address_2').val( '' );
        formCheckout.find('#billing_state').val( '' );
        formCheckout.find('#billing_city').val( '' );
        formCheckout.find('#billing_district').val( '' );
        formCheckout.find('#billing_postcode').val( '' );

        $('.checkout-selected-address-name').html( formCheckout.find('#billing_first_name_alias').val() );
        $('.checkout-selected-address-salutation').html( '(' + formCheckout.find('#billing_address_2_alias').val() + ')' );
        $('.checkout-selected-address-phone').html( formCheckout.find('#billing_phone_alias').val() );
        $('.checkout-selected-address-address').html( formCheckout.find('#billing_address_1_alias').val() );
        $('.checkout-selected-address-district').html( formCheckout.find('#billing_district_alias').val() );
        $('.checkout-selected-address-city').html( formCheckout.find('#billing_regency_alias').val() );
        $('.checkout-selected-address-postcode').html( formCheckout.find('#billing_postcode_alias').val() );

        $( "#billing_email_alias" ).keyup(function() {
            $('.show_payment_method_wrapping').addClass('is-disabled');
        });

        if( formCheckout.find('#billing_email_alias').length > 0 ) {
            if( formCheckout.find('#billing_email_alias').val() != '' ) {
                formCheckout.find('#billing_email_alias').trigger('change');
            }
        }

    }
});
