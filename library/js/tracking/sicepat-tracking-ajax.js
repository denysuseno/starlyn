// initialize when document is ready
jQuery(document).ready(function($) {

    let body = $('body');

    body.on('click', '.sicepat-ajax-tracking', function () {
        let awb_no  = $(this).attr('data-awb-no'),
            nonce   = $(this).attr('data-nonce'),
            popup   = $('.popup-inline-wrapper');

        $.ajax({
            method: "post",
            url: vars.ajaxurl,
            data: {
                action: 'sicepat_tracking_ajax',
                security: nonce,
                awb_no: awb_no
            },
            beforeSend: function () {
                popup.addClass('is-loading');
            }
        }).done(function (resp) {
            if( resp.success ) {
                popup.html( resp.response_html );
            } else {
                popup.html( resp.errors );
            }
            popup.removeClass('is-loading');
        }).fail(function (data) {
            popup.removeClass('is-loading');
        });
    });

    body.on('change', '.cart-wood-packaging', function () {
        let cart_id = $(this).attr('data-cart-id'),
            item = $(this).closest('.item'),
            redirect_url = $(this).attr('data-redirect-url');

        $.ajax({
            method: "post",
            url: vars.ajaxurl,
            data: {
                action: 'update_cart_item',
                security: $('#woocommerce-cart-nonce').val(),
                wood_packaging: $(this).prop('checked') ? 1 : 0,
                cart_id: cart_id
            },
            beforeSend: function () {
                item.addClass('is-loading');
            }
        }).done(function (resp) {
            if( resp ) {

                location.href = redirect_url;

            }

            item.removeClass('is-loading');
        }).fail(function (data) {
            item.removeClass('is-loading');
        });
    })

});
