<?php
/**
 * Display single product reviews (comments)
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product-reviews.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 4.3.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

if ( ! comments_open() ) {
	return;
}
$ratingAvg  = $product->get_average_rating();
$ratingCount= $product->get_rating_count();
?>

<div class="review">
    <p class="product-name"><?php echo $product->get_name(); // WPCS: XSS ok. ?></p>
    <div class="review-rating">
        <div class="review-rating-average">
            <div class="rating-count">
                <span class="highligth"><?= $ratingAvg ?></span>/5
            </div>
            <div class="rating-star">
                <div class="rating-star-space" title="Rated <?= $ratingAvg ?> out of 5">
                    <span class="fill" style="width:<?= ( ( $ratingAvg / 5 ) * 100 ) ?>%"></span>
                </div>
            </div>
        </div>
        <div class="review-rating-detail">
            <ul class="items">
                <?php for( $stars=5; $stars>=1; $stars-- ) : ?>
                    <?php
                    $bar_fill_width = $ratingCount ? ( ( $product->get_rating_count( $stars ) / $ratingCount ) * 100 ) : 0;
                    ?>
                    <li class="item">
                        <div class="star">
                            <span class="number"><?= $stars ?></span>
                            <svg class="icon" role="img"><use xlink:href="<?= library_url() ?>/images/svg-symbols.svg#icon-star" /></svg>
                        </div>
                        <div class="bar">
                            <?php echo sprintf( '<span class="fill" style="width: %d%%"></span>', $bar_fill_width ) ?>
                        </div>
                        <div class="count"><?= $product->get_rating_count( $stars ) ?></div>
                    </li>
                <?php endfor; ?>
            </ul>
        </div>
    </div>
    <div class="review-list">
        <ul class="items">
            <?php if ( have_comments() ) : ?>

                <?php wp_list_comments( apply_filters( 'woocommerce_product_review_list_args', array( 'callback' => 'woocommerce_comments' ) ) ); ?>

                <?php
                if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) :
                    echo '<nav class="woocommerce-pagination">';
                    paginate_comments_links(
                        apply_filters(
                            'woocommerce_comment_pagination_args',
                            array(
                                'prev_text' => '&larr;',
                                'next_text' => '&rarr;',
                                'type'      => 'list',
                            )
                        )
                    );
                    echo '</nav>';
                endif;
                ?>
            <?php else : ?>
                <p class="woocommerce-noreviews" style="margin-top: 20px; margin-bottom: 20px"><?php esc_html_e( 'There are no reviews yet.', 'woocommerce' ); ?></p>
            <?php endif; ?>
        </ul>
        <div class="action">
            <?php if ( get_option( 'woocommerce_review_rating_verification_required' ) === 'no' || wc_customer_bought_product( '', get_current_user_id(), $product->get_id() ) ) : ?>
                <?php if( is_user_logged_in() ) : ?>
                    <?php
                    $product_id = $product->get_id();
                    $review_key = get_current_user_id() . '-' . $product_id . '-product';
                    $comments   = get_comments( ['meta_key' => 'review_key', 'meta_value' => $review_key] );
                    ?>

                    <?php if( ! $comments ) : ?>
                        <?php if( 1 == 2 ) : ?>
                            <a href="#popup-review-<?php echo $product->get_id() ?>" class="button button-primary js-popup-inline">Beri Ulasan</a>
                        <?php endif; ?>
                    <?php else : ?>
                        <a href="#popup-review-<?php echo $product->get_id() ?>" class="button button-gray is-disabled js-popup-inline">Ulasan diberikan</a>
                    <?php endif; ?>
                <?php endif; ?>
            <?php else : ?>
                <p class="woocommerce-verification-required">
                    <?php esc_html_e( 'Only logged in customers who have purchased this product may leave a review.', 'woocommerce' ); ?>
                    <br>
                    <?php if( ! is_user_logged_in() ) : ?>
                        <a href="<?php echo wc_get_page_permalink( 'myaccount' ) ?>" class="button button-primary" style="margin-top: 12px;">Login to Review</a>
                    <?php endif; ?>
                </p>
            <?php endif; ?>

            <?php if ( have_comments() && 1 == 2 ) : ?>
                <a href="#" class="link see-all">See all</a>
            <?php endif; ?>
        </div>
    </div>
    <div class="share">
    <ul class="items">
        <li class="item">
            <a href="https://www.facebook.com/sharer/sharer.php?u=<?= get_the_permalink() ?>" class="button button-primary facebook">
                <svg class="icon" role="img"><use xlink:href="<?= library_url() ?>/images/svg-symbols.svg#icon-facebook-o" /></svg>
                Share
            </a>
        </li>
        <li class="item">
            <a href="https://api.whatsapp.com/send?text=<?= get_the_permalink() ?>" class="button button-primary whatsapp">
                <svg class="icon" role="img"><use xlink:href="<?= library_url() ?>/images/svg-symbols.svg#icon-whatsapp" /></svg>
                Share
            </a>
        </li>
    </ul>
</div>
</div>

<!-- popup-review -->
<?php if ( get_option( 'woocommerce_review_rating_verification_required' ) === 'no' || wc_customer_bought_product( '', get_current_user_id(), $product->get_id() ) ) : ?>
    <div id="popup-review-<?php echo $product->get_id() ?>" class="popup-review popup-inline">

        <?php do_action( 'woocommerce_custom_product_review_form' ); ?>

    </div>
<?php endif; ?>
