<?php
/**
 * Single Product Rating
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/rating.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

global $product;

if ( ! wc_review_ratings_enabled() ) {
	return;
}

$ratingCount = $product->get_rating_count();
$ratingAvg   = $product->get_average_rating();

if ( $ratingCount > 0 ) : ?>

    <div class="rating">
        <div class="rating-star">
            <div class="rating-star-space" title="Rated <?= $ratingAvg ?> out of 5">
                <span class="fill" style="width: <?= ( ( $ratingAvg / 5 ) * 100 ) ?>%"></span>
            </div>
        </div>
        <span class="count">(<?= $ratingAvg ?>)</span>
    </div>

<?php endif; ?>
