<?php
/**
 * Single Product Meta
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/meta.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce/Templates
 * @version     3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $product;
if ( $product->has_dimensions() && ! $product->is_type('variable') ) {
    $weight = $product->get_weight() ? $product->get_weight() * 1000 : '';
} elseif( $product->has_dimensions() && $product->is_type('variable') ) {
    $children_ids       = $product->get_children();
    $variation_weight   = [];
    foreach ( $children_ids as $children_id ) {
        $variation          = new WC_Product_Variation($children_id);
        $variation_weight[] = $variation->get_weight() ? $variation->get_weight() * 1000 : '';
    }
    $weight = $variation_weight ? max( $variation_weight ) : 0;
}

$weight_unit = 'gr'; //get_option('woocommerce_weight_unit');
$condition   = get_field( 'product_conditions', $product->get_id() );
?>
<div class="info">

	<?php do_action( 'woocommerce_product_meta_start' ); ?>

	<?php /*if ( wc_product_sku_enabled() && ( $product->get_sku() || $product->is_type( 'variable' ) ) ) : */?><!--

		<span class="sku_wrapper"><?php /*esc_html_e( 'SKU:', 'woocommerce' ); */?> <span class="sku"><?php /*echo ( $sku = $product->get_sku() ) ? $sku : esc_html__( 'N/A', 'woocommerce' ); */?></span></span>

	--><?php /*endif; */?>

	<?php /*echo wc_get_product_category_list( $product->get_id(), ', ', '<span class="posted_in">' . _n( 'Category:', 'Categories:', count( $product->get_category_ids() ), 'woocommerce' ) . ' ', '</span>' ); */?><!--

	--><?php /*echo wc_get_product_tag_list( $product->get_id(), ', ', '<span class="tagged_as">' . _n( 'Tag:', 'Tags:', count( $product->get_tag_ids() ), 'woocommerce' ) . ' ', '</span>' ); */?>

    <ul class="items">
        <li class="item">
            <span class="label">Berat</span>
            <span class="value"><?php echo sprintf( '%d%s', $weight, $weight_unit ) ?></span>
        </li>
        <li class="item">
            <span class="label">Kondisi</span>
            <span class="value"><?php echo $condition ?></span>
        </li>
        <li class="item">
            <span class="label">Category</span>
            <span class="value"><?php echo wc_get_product_category_list( $product->get_id() ) ?></span>
        </li>
    </ul>

	<?php do_action( 'woocommerce_product_meta_end' ); ?>

</div>

<?php if ( wc_product_sku_enabled() && ( $product->get_sku() || $product->is_type( 'variable' ) ) ) : ?>
    <?php if( $product->get_sku() ) : ?>
        <div class="info" style="margin-top: 24px">
            <span class="sku_wrapper"><?php esc_html_e( 'Product SKU:', 'woocommerce' ); ?> <span class="sku"><?php echo ( $sku = $product->get_sku() ) ? $sku : esc_html__( 'N/A', 'woocommerce' ); ?></span></span>
        </div>
    <?php endif; ?>
<?php endif; ?>