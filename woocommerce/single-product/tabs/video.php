<?php
/**
 * Recommendation Tab
 *
 */

defined( 'ABSPATH' ) || exit;

global $product;

//$heading = apply_filters( 'woocommerce_product_video_heading', __( 'Recommendation', 'woocommerce' ) );
$youtube_channel_thumbnail_url      = get_channel_thumbnail_url( 'default' );
$youtube_channel_name               = get_channel_name();
$youtube_channel_subscribers_count  = get_channel_subscribers_count();
$youtube_channel_url                = get_channel_url();
$youtube_video_url                  = get_field( 'product_video_url', $product->get_id() );
$youtube_video_iframe               = get_field( 'product_video_iframe', $product->get_id() );
$youtube_video_content_title        = get_transient( 'product_video_content_title' );
if( ! $youtube_video_content_title ) {
    $youtube_video_content_title    = get_video_content_title( $youtube_video_url );
    set_transient( 'product_video_content_title', $youtube_video_content_title, DAY_IN_SECONDS );
}
$youtube_video_viewer_count         = get_transient( 'product_video_viewer_count' );
if( ! $youtube_video_viewer_count ) {
    $youtube_video_viewer_count     = get_video_content_viewer_count( $youtube_video_url );
    set_transient( 'product_video_viewer_count', $youtube_video_viewer_count, DAY_IN_SECONDS );
}
?>

<div class="channel-container">
    <div class="channel-logo">
        <img src="<?= library_url() ?>/images/logo-youtube.png" alt="Logo Youtube" class="image">
    </div>
    <div class="channel-header">
        <div class="avatar">
            <img src="<?php echo $youtube_channel_thumbnail_url ?>" alt="Channel Avatar Youtube" class="image">
        </div>
        <div class="meta">
            <h5 class="title">
                <a href="<?php echo $youtube_channel_url ?>" class="link" target="_blank"><?php echo $youtube_channel_name ?></a>
            </h5>
            <p class="subs"><?php echo $youtube_channel_subscribers_count ?> subscribers</p>
        </div>
    </div>
    <div class="channel-content">
        <?php if( $youtube_video_iframe ) : ?>
            <div class="channel-content-video">
                <?php echo $youtube_video_iframe ?>
            </div>
        <?php endif; ?>

        <?php if( $youtube_video_url ) : ?>
            <div class="channel-content-meta">
                <h4 class="title">
                    <a href="<?php echo $youtube_video_url ?>" target="_blank" class="link"><?php echo $youtube_video_content_title ?></a>
                </h4>
                <p class="detail"><?php echo number_format( $youtube_video_viewer_count, 0, ',', '.' ) . ' views' ?></p>
            </div>
        <?php endif; ?>
    </div>
</div>