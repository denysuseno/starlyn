<?php
/**
 * Recommendation Tab
 *
 */

defined( 'ABSPATH' ) || exit;

global $product;

//$heading = apply_filters( 'woocommerce_product_recommendation_heading', __( 'Recommendation', 'woocommerce' ) );
$key_specs = get_field('key_specs', $product->get_id() );
?>

<div class="recomendation">
    <div class="header">
        <h4 class="title">Key Specs</h4>
    </div>
    <div class="content">
        <?php if( $key_specs ) : ?>
            <ul class="items">
                <?php foreach ( $key_specs as $spec ) : ?>
                    <li class="item">
                        <span class="label"><?php echo $spec['label'] ?></span>
                        <span class="value"><?php echo $spec['value'] ?></span>
                    </li>
                <?php endforeach; ?>
            </ul>
        <?php endif; ?>
    </div>
</div>
