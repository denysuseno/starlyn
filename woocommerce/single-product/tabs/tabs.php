<?php
/**
 * Single Product tabs
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/tabs/tabs.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.8.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Filter tabs and allow third parties to add their own.
 *
 * Each tab is an array containing title, callback and priority.
 *
 * @see woocommerce_default_product_tabs()
 */
$product_tabs = apply_filters( 'woocommerce_product_tabs', array() );

if ( ! empty( $product_tabs ) ) : ?>

	<!--div class="woocommerce-tabs wc-tabs-wrapper">-->
        <div class="product-tabs-list" role="tablist" aria-label="Programming Languages">
			<?php foreach ( $product_tabs as $key => $product_tab ) : ?>
                <button id="tab-<?php echo esc_attr( $key ); ?>" class="tab" role="tab"><?php echo wp_kses_post( apply_filters( 'woocommerce_product_' . $key . '_tab_title', $product_tab['title'], $key ) ); ?></button>
			<?php endforeach; ?>
        </div>
        <div class="product-tabs-content">
            <div class="tab-content-items">
                <?php foreach ( $product_tabs as $key => $product_tab ) : ?>
                    <div class="tab-content-item">
                        <div class="tab-content-dropdown js-tab-content-dropdown">
                            <?php echo wp_kses_post( apply_filters( 'woocommerce_product_' . $key . '_tab_title', $product_tab['title'], $key ) ); ?>
                            <svg class="icon" role="img"><use xlink:href="<?= library_url() ?>/images/svg-symbols.svg#icon-angle-down" /></svg>
                        </div>
                        <div class="tab-content" aria-labelledby="tab-<?php echo esc_attr( $key ); ?>" role="tabpanel">
                            <?php
                            if ( isset( $product_tab['callback'] ) ) {
                                call_user_func( $product_tab['callback'], $key, $product_tab );
                            }
                            ?>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>

		<?php do_action( 'woocommerce_product_after_tabs' ); ?>
	<!--</div>-->

<?php endif; ?>
