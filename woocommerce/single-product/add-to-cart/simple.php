<?php
/**
 * Simple product add to cart
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/add-to-cart/simple.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

if ( ! $product->is_purchasable() ) {
	return;
}

echo wc_get_stock_html( $product ); // WPCS: XSS ok.

if ( $product->is_in_stock() ) : ?>

	<?php do_action( 'woocommerce_before_add_to_cart_form' ); ?>

	<form class="cart" action="<?php echo esc_url( apply_filters( 'woocommerce_add_to_cart_form_action', $product->get_permalink() ) ); ?>" method="post" enctype='multipart/form-data'>
		<?php do_action( 'woocommerce_before_add_to_cart_button' ); ?>

        <?php do_action( 'woocommerce_before_add_to_cart_quantity' ); ?>

            <?php
            woocommerce_quantity_input(
                array(
                    'min_value'   => apply_filters( 'woocommerce_quantity_input_min', $product->get_min_purchase_quantity(), $product ),
                    'max_value'   => apply_filters( 'woocommerce_quantity_input_max', $product->get_max_purchase_quantity(), $product ),
                    'input_value' => isset( $_POST['quantity'] ) ? wc_stock_amount( wp_unslash( $_POST['quantity'] ) ) : $product->get_min_purchase_quantity(), // WPCS: CSRF ok, input var ok.
                )
            );
            ?>

        <?php do_action( 'woocommerce_after_add_to_cart_quantity' ); ?>

        <?php
            $force_wood_packaging       = get_field( 'force_wood_packaging', $product->get_id() );
            $force_shipping_insurance   = get_field( 'force_shipping_insurance', $product->get_id() );
        ?>

        <div class="is-hidden">
            <?php echo sprintf( '<input type="checkbox" name="wood_packaging" %s value="1" id="wood_packaging-%d">', $force_wood_packaging == 'yes' ? 'checked' : '', $product->get_id() ) ?> Packing Kayu <br>
            <?php echo sprintf( '<input type="checkbox" name="shipping_insurance" %s value="1" id="shipping-insurance-packing-%d">',$force_shipping_insurance == 'yes' ? 'checked' : '', $product->get_id() ) ?> Asuransi
        </div>

        <button type="submit" name="add-to-cart" value="<?php echo esc_attr( $product->get_id() ); ?>" class="single_add_to_cart_button button alt button button-gray is-hidden" id="btn-add-to-cart"><?php echo esc_html( $product->single_add_to_cart_text() ); ?></button>

		<?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>
	</form>

	<?php do_action( 'woocommerce_after_add_to_cart_form' ); ?>


<?php endif; ?>
