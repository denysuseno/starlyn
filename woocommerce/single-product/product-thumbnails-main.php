<?php

defined( 'ABSPATH' ) || exit;

// Note: `wc_get_gallery_image_html` was added in WC 3.3.2 and did not exist prior. This check protects against theme overrides being used on older versions of WC.
if ( ! function_exists( 'wc_get_gallery_image_html' ) ) {
    return;
}

global $product;

$thumbnailId    = $product->get_image_id();
$thumbnailUrl   = wp_get_attachment_image_url( $thumbnailId, 'original' );
$attachment_ids = $product->get_gallery_image_ids();

?>

<div class="slides product-main-slider">
    <div class="slide">
        <img src="<?= $thumbnailUrl ?>" alt="<?php echo $product->get_title() ?>" class="slide-image">
    </div>
    <?php
    if ( $attachment_ids && $product->get_image_id() ) :
        foreach ( $attachment_ids as $attachment_id ) :
    ?>
            <div class="slide">
                <img src="<?= wp_get_attachment_image_url( $attachment_id, 'original' ) ?>" alt="<?php echo $product->get_title() ?>" class="slide-image">
            </div>
    <?php
        endforeach;
    endif;
    ?>
</div>