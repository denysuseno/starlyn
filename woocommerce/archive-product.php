<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

get_header( 'shop' );

global $wp_query;
?>

    <div id="site-content" class="site-content site-content-product">
        <div class="site-content-inner">
            <main class="main" role="main">
                <div class="product-page">
                    <div class="wrapper">
                        <div class="product-page-header">
                            <h3 class="title">All Products</h3>
                        </div>
                        <div class="product-page-content">
                            <div class="aside">
                                <?php do_action( 'starlyn_archive_aside_desktop' ); ?>
                            </div>
                            <div class="content">

                                <?php if ( woocommerce_product_loop() ) : ?>

                                    <?php
                                    /**
                                     * Hook: woocommerce_before_shop_loop.
                                     *
                                     * @hooked woocommerce_output_all_notices - 10
                                     * @hooked woocommerce_result_count - 20
                                     * @hooked woocommerce_catalog_ordering - 30
                                     */
                                    do_action( 'woocommerce_before_shop_loop' );
                                    ?>

                                    <div class="notices" style="margin-bottom: 32px">
                                        <?php do_action( 'starlyn_product_archives_output_all_notices' ); ?>
                                    </div>

                                    <?php
                                    woocommerce_product_loop_start();

                                    if ( wc_get_loop_prop( 'total' ) ) {
                                        while ( have_posts() ) {
                                            the_post();

                                            /**
                                             * Hook: woocommerce_shop_loop.
                                             */
                                            do_action( 'woocommerce_shop_loop' );

                                            wc_get_template_part( 'content', 'product' );
                                        }
                                    }
                                    ?>

                                    <?php
                                    woocommerce_product_loop_end();

                                    /**
                                     * Hook: woocommerce_after_shop_loop.
                                     *
                                     * @hooked woocommerce_pagination - 10
                                     */
                                    do_action( 'woocommerce_after_shop_loop' );
                                    ?>

                                    <?php if( $wp_query->max_num_pages > 1 ) : ?>
                                        <div class="view-more">
                                            <a href="javascript:void(0)" id="more-posts-shop" class="link">View More</a>
                                        </div>
                                    <?php endif; ?>

                                    <?php
                                    /**
                                     * Hook: woocommerce_after_shop_loop.
                                     *
                                     * @hooked woocommerce_pagination - 10
                                     */
                                    do_action( 'woocommerce_after_shop_loop' );
                                    ?>

                                <?php else : ?>

                                    <?php
                                    /**
                                     * Hook: woocommerce_no_products_found.
                                     *
                                     * @hooked wc_no_products_found - 10
                                     */
                                    do_action( 'woocommerce_no_products_found' );
                                    ?>

                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>
    </div>

<?php
get_footer( 'shop' );
