<?php
/**
 * Loop Add to Cart
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/add-to-cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce/Templates
 * @version     3.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

global $product;
$args['class'] = ! $product->is_in_stock() ? 'is-disabled' : $args['class'];

$force_wood_packaging       = get_field( 'force_wood_packaging', $product->get_id() );
$force_shipping_insurance   = get_field( 'force_shipping_insurance', $product->get_id() );
?>
<div class="is-hidden">
    <?php echo sprintf( '<input type="checkbox" name="wood_packaging" %s value="1" id="wood_packaging-%d">', $force_wood_packaging == 'yes' ? 'checked' : '', $product->get_id() ) ?> Packing Kayu <br>
    <?php echo sprintf( '<input type="checkbox" name="shipping_insurance" %s value="1" id="shipping_insurance-%d">',$force_shipping_insurance == 'yes' ? 'checked' : '', $product->get_id() ) ?> Asuransi
</div>

<div class="action">
    <?php
    echo apply_filters(
        'woocommerce_loop_add_to_cart_link', // WPCS: XSS ok.
        sprintf(
            '<a href="%s" data-quantity="%s" class="%s button button-gray" %s><svg class="icon" role="img"><use xlink:href="'.library_url().'/images/svg-symbols.svg#icon-cart" /></svg> <span class="text">%s</span></a>',
            $product->is_in_stock() ? esc_url( $product->add_to_cart_url() ) : 'javascript:void(0)',
            esc_attr( isset( $args['quantity'] ) ? $args['quantity'] : 1 ),
            esc_attr( isset( $args['class'] ) ? $args['class'] : 'button' ),
            isset( $args['attributes'] ) ? wc_implode_html_attributes( $args['attributes'] ) : '',
            "Add to Cart",
        ),
        $product,
        $args
    );
    ?>
</div>
