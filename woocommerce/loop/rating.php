<?php
/**
 * Loop Rating
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/rating.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $product;

if ( ! wc_review_ratings_enabled() ) {
	return;
}
$ratingAvg = $product->get_average_rating();
?>

<div class="rating">

    <div class="rating-star">
        <div class="rating-star-space" title="Rated <?= $ratingAvg ?> out of 5">
            <span class="fill" style="width: <?= ( ( $ratingAvg / 5 ) * 100 ) ?>%"></span>
        </div>
    </div>
    <span class="count">(<?= $ratingAvg ?>)</span>

    <?php /*echo wc_get_rating_html( $product->get_average_rating() ); // WordPress.XSS.EscapeOutput.OutputNotEscaped. */?>

</div>
