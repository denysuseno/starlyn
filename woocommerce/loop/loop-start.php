<?php
/**
 * Product Loop Start
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/loop/loop-start.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce/Templates
 * @version     3.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>
<div class="product-display">
    <?php if( is_page( 'deals' ) ) : ?>
        <ul id="ajax-posts-deals" class="items">
    <?php elseif( is_page( 'wishlist' ) ) : ?>
        <ul id="ajax-posts-wishlist" class="items">
    <?php else : ?>
        <ul id="ajax-posts" class="items">
    <?php endif; ?>