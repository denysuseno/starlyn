<?php
/**
 * Order Item Details
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/order/order-details-item.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.7.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! apply_filters( 'woocommerce_order_item_visible', true, $item ) ) {
	return;
}

$is_visible = $product && $product->is_visible();
$product_permalink = apply_filters('woocommerce_order_item_permalink', $is_visible ? $product->get_permalink($item) : '', $item, $order);
?>

<div class="item-image">
    <a href="<?= $product_permalink ? $product_permalink : 'javascript:void(0)' ?>" class="link">
        <img src="<?= get_the_post_thumbnail_url( $item->get_product_id(), 'thumbnail' ) ?>" alt="<?= $product->get_name() ?>" class="image">
    </a>
</div>
<div class="item-main">
    <div class="detail">
        <div class="meta">
            <div class="meta-info">
                <?php
                $qty          = $item->get_quantity();
                $refunded_qty = $order->get_qty_refunded_for_item( $item_id );

                if ( $refunded_qty ) {
                    $qty_display = '<del>' . esc_html( $qty ) . '</del> <ins>' . esc_html( $qty - ( $refunded_qty * -1 ) ) . '</ins>';
                } else {
                    $qty_display = esc_html( $qty );
                }
                ?>
                <h5 class="title">
                    <?php echo apply_filters( 'woocommerce_order_item_name', $product_permalink ? sprintf( '<a href="%s" class="link">%s</a>', $product_permalink, $item->get_name() ) : $item->get_name(), $item, $is_visible ); ?>
                    <?php echo apply_filters( 'woocommerce_order_item_quantity_html', ' <strong class="product-quantity">' . sprintf( '&times;&nbsp;%s', $qty_display ) . '</strong>', $item ); ?>
                </h5>
                <div class="info">
                    <p class="info-label">Product Info:</p>
                    <div class="info-items">
                        <div class="info-item">
                            <?php
                            $weight      = $product->get_weight() ? $product->get_weight() * 1000 : '';
                            $weight_unit = 'gr'; //get_option('woocommerce_weight_unit');
                            ?>
                            <span class="label">Berat</span>
                            <span class="value"><?php echo sprintf( '%d%s', $weight, $weight_unit ) ?></span>
                        </div>
                        <div class="info-item">
                            <?php if ( $product->has_dimensions() && ! $product->is_type('variable') ) : ?>
                                <?php
                                $length = ! empty( $product->get_length() ) ? $product->get_length() : '';
                                $width  = ! empty( $product->get_width() ) ? $product->get_width() : '';
                                $height = ! empty( $product->get_height() ) ? $product->get_height() : '';
                                ?>
                                <span class="label">Dimensi</span>
                                <span class="value"><?php echo sprintf( '%dx%dx%d cm', $length, $width, $height ) ?></span>
                            <?php elseif( $product->has_dimensions() && $product->is_type('variable') ) : ?>
                                <span class="label">Dimensi</span>
                                <?php echo esc_html( wc_format_dimensions( $product->get_dimensions( false ) ) ); ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="total">
            <?php echo $order->get_formatted_line_subtotal( $item ) ?>
            <!--Rp 1.784.000
            <div class="discount">
                <span class="highligth">Save 15%</span> <span class="text">was Rp  2.099.000</span>
            </div>-->
        </div>
    </div>
</div>
