<?php
/**
 * Order details
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/order/order-details.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.7.0
 */

defined( 'ABSPATH' ) || exit;

$order = wc_get_order( $order_id ); // phpcs:ignore WordPress.WP.GlobalVariablesOverride.OverrideProhibited

if ( ! $order ) {
	return;
}

$order_items           = $order->get_items( apply_filters( 'woocommerce_purchase_order_item_types', 'line_item' ) );
$show_purchase_note    = $order->has_status( apply_filters( 'woocommerce_purchase_note_order_statuses', array( 'completed', 'processing' ) ) );
$show_customer_details = is_user_logged_in() && $order->get_user_id() === get_current_user_id();
$downloads             = $order->get_downloadable_items();
$show_downloads        = $order->has_downloadable_item() && $order->is_download_permitted();

if ( $show_downloads ) {
	wc_get_template(
		'order/order-downloads.php',
		array(
			'downloads'  => $downloads,
			'show_title' => true,
		)
	);
}
?>
<div class="transaction">
	<?php do_action( 'woocommerce_order_details_before_order_table', $order ); ?>

    <div class="transaction-meta">
        <h4 class="transaction-meta-id">Order #<?= $order->get_order_number() ?></h4>
        <div class="transaction-meta-items">
            <div class="transaction-meta-item">
                <div class="label">Order status:</div>
                <div class="value"><?= wc_get_order_status_name( $order->get_status() ) ?></div>
            </div>
            <div class="transaction-meta-item">
                <div class="label">Order date:</div>
                <div class="value"><?= wc_format_datetime( $order->get_date_created() ) ?></div>
            </div>
        </div>
    </div>

    <?php
        $awb_no             = get_post_meta( $order->get_id(), '_awb_no', true ); // '0114541900204500'; '000667702188';
        $shipping_service   = get_post_meta( $order->get_id(), '_shipping_service', true );
    ?>

    <div class="transaction-summary">
        <?php if( in_array( $order->get_status(), ['shipped'] ) && $awb_no ) : ?>
            <?php if( $shipping_service == 'sicepat' ) : ?>
                <a href="#popup-tracking" class="button button-primary button-tracking js-popup-inline sicepat-ajax-tracking" data-awb-no="<?php echo $awb_no ?>" data-nonce="<?php echo wp_create_nonce('sicepat-tracking-nonce') ?>">Track order</a>
            <?php elseif( $shipping_service == 'jne' ) : ?>
                <a href="#popup-tracking" class="button button-primary button-tracking js-popup-inline jne-ajax-tracking" data-awb-no="<?php echo $awb_no ?>" data-nonce="<?php echo wp_create_nonce('jne-tracking-nonce') ?>">Track order</a>
            <?php endif; ?>
        <?php endif; ?>

        <div class="container">
            <h4 class="heading">Order Summary</h4>
            <div class="summary-items">
                <ul class="items">
                    <?php do_action( 'woocommerce_order_details_before_order_table_items', $order ); ?>

                    <?php foreach ( $order_items as $item_id => $item ) : $product = $item->get_product(); ?>
                        <li class="item">

                            <?php
                            wc_get_template(
                                'order/order-details-item.php',
                                array(
                                    'order'              => $order,
                                    'item_id'            => $item_id,
                                    'item'               => $item,
                                    'show_purchase_note' => $show_purchase_note,
                                    'purchase_note'      => $product ? $product->get_purchase_note() : '',
                                    'product'            => $product,
                                )
                            );
                            ?>

                        </li>
                    <?php endforeach; ?>

                    <?php do_action( 'woocommerce_order_details_after_order_table_items', $order ); ?>
                </ul>
            </div>
            <div class="summary-detail">
                <ul class="items">
                    <?php
                    $totalRows = $order->get_order_item_totals();
                    $orderTotal = $totalRows['order_total'];
                    unset( $totalRows['order_total'] );

                    foreach ( $totalRows as $key => $total ) {
                        ?>
                        <p class="row">
                            <span class="text"><?php echo esc_html( $total['label'] ); ?></span>
                            <span class="value <?= $key == 'discount' ? 'value-reduced' : ''; ?>"><?php echo ( 'payment_method' === $key ) ? esc_html( $total['value'] ) : wp_kses_post( $total['value'] ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></span>
                        </p>
                        <?php
                    }
                    ?>

                    <?php if ( $order->get_customer_note() ) : ?>
                        <p class="row">
                            <span class="text"><?php esc_html_e( 'Note:', 'woocommerce' ); ?></span>
                            <span class="value"><?php echo wp_kses_post( nl2br( wptexturize( $order->get_customer_note() ) ) ); ?></span>
                        </p>
                    <?php endif; ?>
                </ul>
            </div>
            <div class="summary-total">
                <p class="row">
                    <span class="text">Total</span>
                    <span class="value"><?= $orderTotal['value'] ?></span>
                </p>
            </div>
        </div>
        <div class="transaction-summary-container">
            <div class="transaction-summary-items"></div>
        </div>
    </div>
    <div class="transaction-shipping">
        <div class="container">
            <div class="section shipping-address">
                <?php
                if ( $show_customer_details ) {
                    wc_get_template( 'order/order-details-customer.php', array( 'order' => $order ) );
                }
                ?>
            </div>
            <div class="section shipping-method">
                <h4 class="heading">Shipping Method</h4>
                <?php foreach( $order->get_shipping_methods() as $shipping_method ) : ?>
                    <p><?= $shipping_method->get_name() ?></p>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
    <div class="transaction-action">
        <ul class="items">
            <?php if( 1 == 2 ) : ?>
                <li class="item">
                    <a href="#popup-complain" class="button button-gray js-popup-inline">Complain</a>
                </li>
            <?php endif; ?>
            <?php if( $order->get_status() == 'completed' ) : ?>

                <li class="item">

                    <a href="#popup-review-<?php echo $order->get_order_number() ?>" class="button button-primary js-popup-inline">Review</a>

                </li>

            <?php endif; ?>
        </ul>
    </div>

    <!-- DENSUS UNDER DEVELOPMENT -->
    <!-- popup tracking -->
    <div id="popup-tracking" class="popup-tracking popup-inline">
        <div class="popup-inline-wrapper">
            <h4 class="heading">Tracking Details</h4>
            <span class="js-button-close js-popup-inline" data-popper-target="#popup-tracking"><svg class="icon" role="img"><use xlink:href="<?= library_url() ?>/images/svg-symbols.svg#icon-close" /></svg></span>
            <div class="table-responsive">
                <table class="table-responsive-table">
                    <thead>
                    <tr>
                        <th>No. Tracking</th>
                        <th>Jenis pengiriman</th>
                        <th>Tanggal Pengiriman</th>
                        <th>Pengiriman dari</th>
                        <th>Tujuan pengiriman</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                        <td>-</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="history">
                <ul class="items">
                    <li class="item">
                        <span class="date">-</span>
                        <span class="location">-</span>
                        <span class="notes">-</span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- popup complain -->
    <div id="popup-complain" class="popup-complain popup-inline">
        <div class="popup-inline-wrapper">
            <h4 class="heading">Komplain</h4>
            <span class="js-button-close js-popup-inline" data-popper-target="#popup-complain"><svg class="icon" role="img"><use xlink:href="<?= library_url() ?>/images/svg-symbols.svg#icon-close" /></svg></span>
            <form class="form">
                <div class="form-input">
                    <div class="form-input-field">
                        <label class="label">Order no.</label>
                        <input type="text" class="input" value="#237912" readonly>
                    </div>
                    <div class="form-input-field">
                        <label class="label">Tanggal</label>
                        <input type="date" class="input" value="2020-01-09">
                    </div>
                    <div class="form-input-field">
                        <label class="label">Perihal Keluhan</label>
                        <textarea class="input"></textarea>
                    </div>
                </div>
                <div class="form-submit">
                    <button class="button button-gray">Send</button>
                </div>
            </form>
        </div>
    </div>
    <!-- popup review -->
    <div id="popup-review-<?php echo $order->get_order_number() ?>" class="popup-review popup-inline">

        <?php foreach ( $order_items as $item ) : ?>

            <?php
            $product_id = $item->get_product_id();
            $review_key = get_current_user_id() . '-' . $product_id . '-' . $order->get_id();
            $comments   = get_comments( ['meta_key' => 'review_key', 'meta_value' => $review_key] );
            ?>

            <?php if( ! $comments ) : ?>

                <?php
                $commenter    = wp_get_current_commenter();
                $comment_form = array(
                    /* translators: %s is product title */
                    'title_reply'         => sprintf( esc_html__( 'Tambah ulasan untuk produk &ldquo;%s&rdquo;', 'woocommerce' ), $item->get_name() ),
                    /* translators: %s is product title */
                    'title_reply_to'      => esc_html__( 'Leave a Reply to %s', 'woocommerce' ),
                    'title_reply_before'  => '<h4 class="heading">',
                    'title_reply_after'   => '</h4> <span class="js-button-close js-popup-inline" data-popper-target="#popup-review-'.$order->get_order_number().'"><svg class="icon" role="img"><use xlink:href="'.library_url().'/images/svg-symbols.svg#icon-close" /></svg></span>',
                    'comment_notes_after' => '',
                    'label_submit'        => esc_html__( 'Submit', 'woocommerce' ),
                    'logged_in_as'        => '',
                    'comment_field'       => '',
                    'id_form'               => 'commentform-' . $order->get_order_number() . '-' . $product_id,
                    'class_form'            => 'form',
                    'class_container'       => 'popup-inline-wrapper',
                    'submit_field'          => '<div class="form-submit">%1$s %2$s</div>',
                    'class_submit'          => 'button button-gray',
                );

                $name_email_required = (bool) get_option( 'require_name_email', 1 );
                $fields              = array(
                    'author' => array(
                        'label'    => __( 'Name', 'woocommerce' ),
                        'type'     => 'text',
                        'value'    => $commenter['comment_author'],
                        'required' => $name_email_required,
                    ),
                    'email'  => array(
                        'label'    => __( 'Email', 'woocommerce' ),
                        'type'     => 'email',
                        'value'    => $commenter['comment_author_email'],
                        'required' => $name_email_required,
                    ),
                );

                $comment_form['fields'] = array();

                foreach ( $fields as $key => $field ) {
                    $field_html  = '<p class="comment-form-' . esc_attr( $key ) . '">';
                    $field_html .= '<label for="' . esc_attr( $key ) . '">' . esc_html( $field['label'] );

                    if ( $field['required'] ) {
                        $field_html .= '&nbsp;<span class="required">*</span>';
                    }

                    $field_html .= '</label><input id="' . esc_attr( $key ) . '" name="' . esc_attr( $key ) . '" type="' . esc_attr( $field['type'] ) . '" value="' . esc_attr( $field['value'] ) . '" size="30" ' . ( $field['required'] ? 'required' : '' ) . ' /></p>';

                    $comment_form['fields'][ $key ] = $field_html;
                }

                $account_page_url = wc_get_page_permalink( 'myaccount' );
                if ( $account_page_url ) {
                    /* translators: %s opening and closing link tags respectively */
                    $comment_form['must_log_in'] = '<p class="must-log-in">' . sprintf( esc_html__( 'You must be %1$slogged in%2$s to post a review.', 'woocommerce' ), '<a href="' . esc_url( $account_page_url ) . '">', '</a>' ) . '</p>';
                }

                if ( wc_review_ratings_enabled() ) {
                    ob_start();
                    include '_comment-field.php';
                    $comment_form['comment_field'] = ob_get_contents();
                    ob_get_clean();
                }

                comment_form( apply_filters( 'woocommerce_product_review_comment_form_args', $comment_form ), $product_id );
                ?>

            <?php else : ?>

                <div class="popup-inline-wrapper">
                    <h4 class="heading"><?php echo sprintf( esc_html__( 'Ulasan untuk produk &ldquo;%s&rdquo;', 'woocommerce' ), $item->get_name() ) ?></h4>
                    <span class="js-button-close js-popup-inline" data-popper-target="#popup-review-1"><svg class="icon" role="img"><use xlink:href="<?php echo library_url() ?>/images/svg-symbols.svg#icon-close" /></svg></span>
                    <p>
                        Ulasan telah diberikan untuk produk <a href="<?php echo add_query_arg( ['tab' => 'tab-reviews'], get_permalink( $product_id ) ) ?>"><?php echo $item->get_name() ?></a>
                    </p>
                </div>

            <?php endif; ?>

        <?php endforeach; ?>

    </div>
    <!-- DENSUS UNDER DEVELOPMENT -->

</div>
