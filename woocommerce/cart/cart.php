<?php
/**
 * Cart Page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.8.0
 */

defined( 'ABSPATH' ) || exit;

do_action( 'woocommerce_before_cart' ); ?>

<div class="cart-page-main">

    <?php if( wc_notice_count() ) : ?>
    <div style="margin-bottom: 24px">
        <?php do_action( 'woo_cart_output_all_notices' ); ?>
    </div>
    <?php endif; ?>

    <div class="header">
        <h3 class="title">Your Cart</h3>
    </div>
    <div class="content">
        <form class="" action="<?php echo esc_url( wc_get_cart_url() ); ?>" method="post">
            <?php do_action( 'woocommerce_before_cart_table' ); ?>

            <div class="cart-products">
                <?php do_action( 'woocommerce_before_cart_contents' ); ?>

                <ul class="items">
                    <?php
                    foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) :
                        $_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
                        $product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

                        if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) :
                            $product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
                            $force_wood_packaging       = get_field( 'force_wood_packaging', $_product->get_id() );
                            $force_shipping_insurance   = get_field( 'force_shipping_insurance', $_product->get_id() );
                            ?>
                            <li class="item">
                                <div class="item-image">
                                    <?php $thumbnailId = $_product->get_image_id(); $thumbnail = wp_get_attachment_image_url( $thumbnailId ); ?>
                                    <a href="<?= $product_permalink ?>" class="link">
                                        <img src="<?= $thumbnail ?>" alt="<?= $_product->get_name() ?>" class="image">
                                    </a>
                                </div>
                                <div class="item-main">
                                    <div class="detail">
                                        <div class="meta">
                                            <div class="meta-info">
                                                <h5 class="title"><a href="<?= $product_permalink ?>" class="link"><?= $_product->get_name() ?></a></h5>
                                                <div class="info">
                                                    <p class="info-label">Product Info:</p>
                                                    <div class="info-items">
                                                        <div class="info-item">
                                                            <?php
                                                                $weight      = $_product->get_weight() ? $_product->get_weight() * 1000 : '';
                                                                $weight_unit = 'gr'; //get_option('woocommerce_weight_unit');
                                                            ?>
                                                            <span class="label">Berat</span>
                                                            <span class="value"><?php echo sprintf( '%d%s', $weight, $weight_unit ) ?></span>
                                                        </div>
                                                        <div class="info-item">
                                                            <?php if ( $_product->has_dimensions() && ! $_product->is_type('variable') ) : ?>
                                                                <?php
                                                                    $length = ! empty( $_product->get_length() ) ? $_product->get_length() : '';
                                                                    $width  = ! empty( $_product->get_width() ) ? $_product->get_width() : '';
                                                                    $height = ! empty( $_product->get_height() ) ? $_product->get_height() : '';
                                                                ?>
                                                                <span class="label">Dimensi</span>
                                                                <span class="value"><?php echo sprintf( '%dx%dx%d cm', $length, $width, $height ) ?></span>
                                                            <?php elseif( $_product->has_dimensions() && $_product->is_type('variable') ) : ?>
                                                                <span class="label">Dimensi</span>
                                                                <?php echo esc_html( wc_format_dimensions( $_product->get_dimensions( false ) ) ); ?>
                                                            <?php endif; ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php /*echo wc_get_formatted_cart_item_data( $cart_item ); // PHPCS: XSS ok. */?>
                                            </div>
                                        </div>
                                        <div class="quantity">
                                            <div class="quantity-input js-qty-input">
                                                <svg class="icon minus btn-cart-minus" role="img">
                                                    <use xlink:href="<?= library_url() ?>/images/svg-symbols.svg#icon-minus" />
                                                </svg>

                                                <?php
                                                woocommerce_quantity_input(
                                                    array(
                                                        'input_name'   => "cart[{$cart_item_key}][qty]",
                                                        'input_value'  => $cart_item['quantity'],
                                                        'max_value'    => $_product->get_max_purchase_quantity(),
                                                        'min_value'    => '0',
                                                        'product_name' => $_product->get_name(),
                                                    ),
                                                    $_product,
                                                    true);
                                                ?>

                                                <svg class="icon plus btn-cart-plus" role="img">
                                                    <use xlink:href="<?= library_url() ?>/images/svg-symbols.svg#icon-plus" />
                                                </svg>
                                            </div>
                                            <?php
                                            echo apply_filters( // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
                                                'woocommerce_cart_item_remove_link',
                                                sprintf(
                                                    '<a href="%s" class="remove" aria-label="%s" data-product_id="%s" data-product_sku="%s" onclick="%s">Remove</a>',
                                                    esc_url( wc_get_cart_remove_url( $cart_item_key ) ),
                                                    esc_html__( 'Remove', 'woocommerce' ),
                                                    esc_attr( $product_id ),
                                                    esc_attr( $_product->get_sku() ),
                                                    "return confirm('Are you sure?')"
                                                ),
                                                $cart_item_key
                                            );
                                            ?>
                                        </div>
                                        <div class="total">
                                            <?php echo WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ); ?>
                                        </div>
                                    </div>
                                    <div class="shipping is-hidden">
                                        <div class="shipping-option">
                                            <ul class="shipping-option-items">
                                                <li class="shipping-option-item <?php echo $force_wood_packaging == 'yes' ? 'is-disabled' : '' ?>">
                                                    <label class="label">
                                                        <?php $wood_packaging_checked = ( isset( $cart_item['wood_packaging'] ) && $cart_item['wood_packaging'] == 1 ) || $force_wood_packaging == 'yes' ? 'checked' : '' ?>
                                                        <?php echo sprintf( '<input type="checkbox" name="cart[%s][wood_packaging]" %s value="1" data-cart-id="%s" data-redirect-url="%s" class="cart-wood-packaging">', $cart_item_key, $wood_packaging_checked, $cart_item_key, wc_get_cart_url() ) ?>
                                                        <span class="box">
                                                            <svg class="icon" role="img"><use xlink:href="<?= library_url() ?>/images/svg-symbols.svg#icon-check" /></svg>
                                                        </span>
                                                        <span class="text">Packing kayu</span>
                                                    </label>
                                                </li>
                                                <li class="shipping-option-item <?php echo $force_shipping_insurance == 'yes' ? 'is-disabled' : '' ?>">
                                                    <label class="label">
                                                        <?php $shipping_insurance_checked = ( isset( $cart_item['shipping_insurance'] ) && $cart_item['shipping_insurance'] == 1 ) || $force_shipping_insurance == 'yes' ? 'checked' : '' ?>
                                                        <?php echo sprintf( '<input type="checkbox" name="cart[%s][shipping_insurance]" %s value="1" data-cart-id="%s" data-redirect-url="%s" class="cart-shipping-insurance">', $cart_item_key, $shipping_insurance_checked, $cart_item_key, wc_get_cart_url() ) ?>
                                                        <span class="box">
                                                            <svg class="icon" role="img"><use xlink:href="<?= library_url() ?>/images/svg-symbols.svg#icon-check" /></svg>
                                                        </span>
                                                        <span class="text">Asuransi</span>
                                                    </label>
                                                </li>
                                            </ul>
                                            <?php if( $force_wood_packaging == 'yes' && $force_shipping_insurance == 'yes' ) : ?>
                                                <div class="shipping-option-notes">Pengiriman produk ini harus menggunakan packing kayu & asuransi</div>
                                            <?php elseif( $force_wood_packaging == 'yes' && $force_shipping_insurance == 'no' ) : ?>
                                                <div class="shipping-option-notes">Pengiriman produk ini harus menggunakan packing kayu</div>
                                            <?php elseif( $force_wood_packaging == 'no' && $force_shipping_insurance == 'yes' ) : ?>
                                                <div class="shipping-option-notes">Pengiriman produk ini harus menggunakan asuransi</div>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        <?php
                        endif;
                    endforeach;
                    ?>
                </ul>

                <?php do_action( 'woocommerce_cart_contents' ); ?>

                <?php do_action( 'woocommerce_after_cart_contents' ); ?>
            </div>

            <div class="cart-coupon">
                <?php if ( wc_coupons_enabled() ) : ?>
                    <input type="text" name="coupon_code" class="input-text input input-coupon" id="coupon_code" value="" placeholder="<?php esc_attr_e( 'Masukkan Kode Voucher', 'woocommerce' ); ?>" />
                    <button type="submit" class="button button-gray" name="apply_coupon" value="<?php esc_attr_e( 'Apply coupon', 'woocommerce' ); ?>"><?php esc_attr_e( 'Pakai Voucher', 'woocommerce' ); ?></button>
                    <?php do_action( 'woocommerce_cart_coupon' ); ?>
                <?php endif; ?>

                <button type="submit" class="button button-gray is-hidden" name="update_cart" value="<?php esc_attr_e( 'Update cart', 'woocommerce' ); ?>"><?php esc_html_e( 'Update cart', 'woocommerce' ); ?></button>

                <?php do_action( 'woocommerce_cart_actions' ); ?>

                <?php wp_nonce_field( 'woocommerce-cart', 'woocommerce-cart-nonce' ); ?>
            </div>

            <?php do_action( 'woocommerce_after_cart_table' ); ?>
        </form>
    </div>
</div>

<?php do_action( 'woocommerce_before_cart_collaterals' ); ?>

<div class="cart-page-summary">
    <div class="inner">
        <div class="header">
            <h3 class="title">Order Summary</h3>
        </div>
        <?php
        /**
         * Cart collaterals hook.
         *
         * @hooked woocommerce_cross_sell_display
         * @hooked woocommerce_cart_totals - 10
         */
        do_action( 'woocommerce_cart_collaterals' );
        ?>
        <div class="action">
            <?php do_action( 'woocommerce_proceed_to_checkout' ); ?>
        </div>
    </div>
</div>

<?php do_action( 'woocommerce_after_cart' ); ?>
