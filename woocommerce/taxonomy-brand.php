<?php
/**
 * The Template for displaying product archives of a brand.
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

$current_brand              = get_queried_object();

$overview_image             = get_field( 'overview_image', $current_brand );
$top_category               = get_field( 'top_category', $current_brand );
$top_category_items         = $top_category['category_item'];
$highlight_category         = get_field( 'highlight_category', $current_brand );
$highlight_category_items   = $highlight_category['highlight_category_item'];

get_header();
?>

<div id="site-content" class="site-content site-content-about">
    <div class="site-content-inner">
        <main class="main" role="main">
            <div class="page-single wrapper-about">
                <div class="brand-over">
                    <section class="section brand-overview">
                        <div class="brand-overview-header intro">
                            <div class="breadcrumb">
                                <?php if( ! is_user_logged_in() ) : ?>
                                    <a href="<?php echo get_permalink( get_option( 'woocommerce_myaccount_page_id' ) ) ?>" class="a">Login</a>
                                    <span class="separator">
                                          /
                                        </span>
                                <?php endif; ?>
                                <a href="<?php echo get_permalink( get_page_by_path( 'brands' ) ) ?>" class="a">Brands</a>
                                <span class="separator">
                                      /
                                    </span>
                                <span class="a"><?php echo $current_brand->name ?></span>
                            </div>
                            <h1 class="text"><?php echo $current_brand->name ?></h1>
                        </div>
                        <div class="brand-overview-wrapper">
                            <img src="<?php echo $overview_image ?>" alt="<?php echo $current_brand->name . ' Overview Image' ?>" class="image">
                        </div>
                    </section>
                    <?php if( $top_category_items ) : ?>
                        <section class="section brand-content intro">
                            <div class="brand-content-wrapper">
                                <div class="brand-content-header">
                                    <p class="text">Shop All <?php echo $current_brand->name ?></p>
                                </div>
                                <div class="brand-content-item">
                                    <div class="item">
                                        <?php foreach ( $top_category_items as $item ) : ?>
                                            <div class="items">
                                                <div class="items-img">
                                                    <img src="<?php echo $item['category_image'] ?>" alt="<?php echo $item['category_image'] ?>" class="image">
                                                </div>
                                                <div class="items-text">
                                                    <p class="text"><?php echo $item['category_title'] ?></p>
                                                    <?php if( $item['category_child'] ) : ?>
                                                        <div class="sub-text">
                                                            <?php foreach ( $item['category_child'] as $child ) : ?>
                                                                <p class="value"><a href="<?php echo add_query_arg( ['brand' => $current_brand->name], get_category_link( $child['category_child_category_product'] ) ) ?>"><?php echo $child['category_child_title'] ?></a></p>
                                                            <?php endforeach; ?>
                                                        </div>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            </div>
                        </section>
                    <?php endif; ?>
                    <?php if( $highlight_category_items ) : ?>
                        <section class="section brand-wrapper intro">
                            <div class="brand-wrapper-item">
                                <?php $i=1; ?>
                                <?php foreach( $highlight_category_items as $item ) : ?>
                                    <?php
                                        $loop_class = $i % 2 == 0 ? 'footer' : 'items';
                                        $product_cat = [];
                                        if( isset( $item['category'] ) && $item['category'] ) {
                                            foreach ( $item['category'] as $cat ) {
                                                $product_cat[$cat->slug] = $cat->slug;
                                            }
                                        }
                                    ?>
                                        <div class="brand-wrapper-<?php echo $loop_class ?>">
                                            <!-- <div class="item"> -->
                                            <div class="items">
                                                <img src="<?php echo $item['image'] ?>" alt="<?php echo $item['title'] ?>" class="image">
                                            </div>
                                            <div class="description">
                                                <h2 class="text"><?php echo $item['title'] ?></h2>
                                                <p class="value"><?php echo $item['description'] ?></p>
                                                <?php if( $product_cat ) : ?>
                                                    <a href="<?php echo add_query_arg( ['product_cat' => implode( ',', $product_cat )], wc_get_page_permalink('shop') ) ?>" class="link button button-primary"><?php echo $item['title'] ?></a>
                                                <?php endif; ?>
                                            </div>
                                            <!-- </div> -->
                                        </div>
                                    <?php $i++; ?>
                                <?php endforeach; ?>
                            </div>
                        </section>
                    <?php endif; ?>
                </div>
            </div>
        </main>
    </div>
</div>

<?php get_footer(); ?>