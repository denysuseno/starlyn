<?php
/**
 * My Account navigation
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/navigation.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$gravatar_url = get_avatar_img_url( 160 );

do_action( 'woocommerce_before_account_navigation' ); ?>

    <div class="preview">
        <div class="preview-image">
            <img src="<?php echo $gravatar_url ?>" alt="Gravatar" class="image">
        </div>
        <div class="preview-meta">
            <h5 class="name"><?= wp_get_current_user()->display_name ?></h5>
            <a href="#" class="email"><?= wp_get_current_user()->user_email ?></a>
        </div>
    </div>
    <div class="nav">
        <ul class="items">
            <?php global $wp; $currentUrlPath = str_replace('my-account/', '', $wp->request); ?>
            <?php foreach ( wc_get_account_menu_items() as $endpoint => $label ) : ?>
                <?php $isViewOrderPage = strpos( $wp->request, 'my-account/view-order' ) !== false; ?>
                <?php $toggledTransactionMenu = in_array($currentUrlPath, ['transaction', 'orders', 'dispute-center']) || ( strpos( $currentUrlPath, 'orders' ) !== false ) || $isViewOrderPage ? true : false; ?>
                <li class="<?php echo wc_get_account_menu_item_classes( $endpoint ); ?> <?= $toggledTransactionMenu ? 'is-toggled' : '' ?>">
                    <?php if( $endpoint == 'transaction' ) : ?>
                        <span class="link js-account-nav-toggle">
                            Transaksi
                            <svg class="icon" role="img"><use xlink:href="<?= library_url() ?>/images/svg-symbols.svg#icon-angle-down" /></svg>
                        </span>
                        <ul class="sub-items">
                            <li class="sub-item <!-- @nav-account-transaction-history --> <?= ( strpos( $currentUrlPath, 'orders' ) !== false ) || $isViewOrderPage ? 'item-current' : ''; ?>">
                                <a href="<?php echo esc_url( wc_get_account_endpoint_url( 'orders' ) ); ?>" class="link">Histori Transaksi</a>
                            </li>
                            <!-- Hide menu complain sementara -->
                            <?php if( 1 == 2 ) : ?>
                                <li class="sub-item <!-- @nav-account-dispute --> <?= $currentUrlPath == 'dispute-center' ? 'item-current' : ''; ?>">
                                    <a href="<?php echo esc_url( wc_get_account_endpoint_url( 'dispute-center' ) ); ?>" class="link">Komplain</a>
                                </li>
                            <?php endif; ?>
                        </ul>
                    <?php else : ?>
                        <a href="<?php echo esc_url( wc_get_account_endpoint_url( $endpoint ) ); ?>" class="link"><?php echo esc_html( $label ); ?></a>
                    <?php endif; ?>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
    <div class="action">
        <ul class="items">
            <li class="item <!-- @nav-account-profile --> <?= $currentUrlPath == 'edit-account' ? 'item-current' : ''; ?>">
                <a href="<?php echo esc_url( wc_get_account_endpoint_url( 'edit-account' ) ); ?>" class="link">
                    <svg class="icon" role="img"><use xlink:href="<?= library_url() ?>/images/svg-symbols.svg#icon-gear" /></svg>
                    Profile
                </a>
            </li>
            <li class="item">
                <a href="<?= wc_get_account_endpoint_url( 'customer-logout' ) ?>" class="link">
                    <svg class="icon" role="img"><use xlink:href="<?= library_url() ?>/images/svg-symbols.svg#icon-signout" /></svg>
                    Log Out
                </a>
            </li>
        </ul>
    </div>

<?php do_action( 'woocommerce_after_account_navigation' ); ?>
