<h4 class="heading">Tracking Details</h4>
<span class="js-button-close js-popup-inline" data-popper-target="#popup-tracking"><svg class="icon" role="img"><use xlink:href="<?= library_url() ?>/images/svg-symbols.svg#icon-close" /></svg></span>
<div class="table-responsive">
    <table class="table-responsive-table">
        <thead>
        <tr>
            <th>No. Tracking</th>
            <th>Jenis pengiriman</th>
            <th>Tanggal Pengiriman</th>
            <th>Pengiriman dari</th>
            <th>Tujuan pengiriman</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td><?php echo $args->awb_no ?></td>
            <td><?php echo $args->shipping ?></td>
            <td><?php echo $args->send_at ?></td>
            <td><?php echo $args->sender_address ?></td>
            <td><?php echo $args->recipient_address ?></td>
        </tr>
        </tbody>
    </table>
</div>
<div class="history">
    <?php if( $args->track_history ) : ?>
        <ul class="items">
            <?php foreach ( $args->track_history as $history ) : ?>
                <li class="item">
                    <span class="date"><?php echo $history['date'] ?></span>
                    <span class="notes"><?php echo $history['desc'] ?></span>
                </li>
            <?php endforeach; ?>
        </ul>
    <?php endif; ?>
</div>