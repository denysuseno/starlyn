<?php
/**
 * Orders
 *
 * Shows orders on the account page.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/orders.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.7.0
 */

defined( 'ABSPATH' ) || exit;

do_action( 'woocommerce_before_account_orders', $has_orders ); ?>

<div class="account-main-header">
    <h3 class="title">Histori Transaksi</h3>
</div>

<div class="account-main-content">

    <div class="data">

        <div class="data-filter">
            <p class="heading">See Transaction History</p>
            <form class="form">
                <div class="data-filter-fields">
                    <div class="data-filter-field form-input-date">
                        <textarea name="from" class="input myaccount-orders-filter-date-from" placeholder="Start Date"><?= isset( $_GET['from'] ) && $_GET['from'] ? $_GET['from'] : '' ?></textarea>
                    </div>
                    <div class="data-filter-field form-input-date">
                        <textarea name="to" class="input myaccount-orders-filter-date-to" placeholder="Start Date"><?= isset( $_GET['to'] ) && $_GET['to'] ? $_GET['to'] : '' ?></textarea>
                    </div>
                </div>
                <div class="data-filter-search">
                    <input type="text" class="input" name="order_no" placeholder="Search order number" value="<?= isset( $_GET['order_no'] ) && $_GET['order_no'] ? $_GET['order_no'] : '' ?>">
                </div>
            </form>
        </div>
        <div class="data-table">
            <div class="data-table-content">
                <div class="table-responsive">

                    <table class="table table-responsive-table">
                        <thead>
                        <tr>
                            <?php foreach ( wc_get_account_orders_columns() as $column_id => $column_name ) : ?>
                                <th class="woocommerce-orders-table__header woocommerce-orders-table__header-<?php echo esc_attr( $column_id ); ?>"><span class="nobr"><?php echo esc_html( $column_name ); ?></span></th>
                            <?php endforeach; ?>
                        </tr>
                        </thead>

                        <tbody>
                        <?php
                        if( $has_orders ) :
                            foreach ( $customer_orders->orders as $customer_order ) {
                                $order          = wc_get_order( $customer_order ); // phpcs:ignore WordPress.WP.GlobalVariablesOverride.OverrideProhibited
                                $item_count     = $order->get_item_count() - $order->get_item_count_refunded();
                                $order_status   = $order->get_status();
                                $has_pay_confirm= get_field( 'has_submit_pay_confirm', $order->get_id() );
                                $needPay        = in_array( $order_status, ['pending', 'on-hold'] ) && ! $has_pay_confirm ? true : false;

                                if( ( isset( $_GET['order_no'] ) && $_GET['order_no'] == $order->get_order_number() ) || ( ! isset( $_GET['order_no'] ) || ( is_null( $_GET['order_no'] ) || empty( $_GET['order_no'] ) ) ) ) :
                                ?>
                                <tr class="woocommerce-orders-table__row woocommerce-orders-table__row--status-<?php echo esc_attr( $order->get_status() ); ?> order">
                                    <?php foreach ( wc_get_account_orders_columns() as $column_id => $column_name ) : ?>
                                        <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-<?php echo esc_attr( $column_id ); ?> <?= 'order-action-pay' === $column_id && $needPay ? 'action' : '' ?>" data-title="<?php echo esc_attr( $column_name ); ?>">
                                            <?php if ( has_action( 'woocommerce_my_account_my_orders_column_' . $column_id ) ) : ?>
                                                <?php do_action( 'woocommerce_my_account_my_orders_column_' . $column_id, $order ); ?>

                                            <?php elseif ( 'order-number' === $column_id ) : ?>

                                                <?php echo esc_html( _x( '#', 'hash before order number', 'woocommerce' ) . $order->get_order_number() ); ?>

                                            <?php elseif ( 'order-ship-to' === $column_id ) : ?>
                                                <?php echo $order->get_formatted_shipping_full_name(); ?>

                                            <?php elseif ( 'order-date' === $column_id ) : ?>
                                                <time datetime="<?php echo esc_attr( $order->get_date_created()->date( 'c' ) ); ?>"><?php echo esc_html( wc_format_datetime( $order->get_date_created() ) ); ?></time>

                                            <?php elseif ( 'order-status' === $column_id ) : ?>
                                                <?php echo esc_html( wc_get_order_status_name( $order->get_status() ) ); ?>

                                            <?php elseif ( 'order-total' === $column_id ) : ?>
                                                <?php
                                                /* translators: 1: formatted order total 2: total order items */
                                                echo wp_kses_post( sprintf( _n( '%1$s (%2$s item)', '%1$s (%2$s items)', $item_count, 'woocommerce' ), $order->get_formatted_order_total(), $item_count ) );
                                                ?>

                                            <?php elseif ( 'order-actions' === $column_id ) : ?>
                                                <?php
                                                /*$actions = wc_get_account_orders_actions( $order );

                                                if ( ! empty( $actions ) ) {
                                                    foreach ( $actions as $key => $action ) { // phpcs:ignore WordPress.WP.GlobalVariablesOverride.OverrideProhibited
                                                        echo '<a href="' . esc_url( $action['url'] ) . '" class="' . sanitize_html_class( $key ) . ' link">' . esc_html( $action['name'] ) . '</a>';
                                                    }
                                                }*/
                                                ?>

                                                <a href="<?= $order->get_view_order_url() ?>" class="link">Lihat order</a>

                                            <?php elseif ( 'order-action-pay' === $column_id ) : ?>

                                                <?php if( $needPay ) : ?>
                                                        <a href="<?= add_query_arg( ['order_no' => $order->get_order_number()], get_permalink( get_page_by_path( 'payment-confirmation' ) ) ) ?>" class="button button-primary button-payment-confirmation">Konfirmasi</a>
                                                <?php endif; ?>

                                            <?php endif; ?>
                                        </td>
                                    <?php endforeach; ?>
                                </tr>
                                <?php
                                endif;
                            }
                        else :
                        ?>
                            <tr class="order">
                                <td colspan="7">
                                    <div class="woocommerce-message woocommerce-message--info woocommerce-Message woocommerce-Message--info woocommerce-info" style="text-align: center">
                                        <a class="link" href="<?php echo esc_url( apply_filters( 'woocommerce_return_to_shop_redirect', wc_get_page_permalink( 'shop' ) ) ); ?>">
                                            <?php esc_html_e( 'Browse products', 'woocommerce' ); ?>
                                        </a>
                                        <?php esc_html_e( 'No order has been made yet.', 'woocommerce' ); ?>
                                    </div>
                                </td>
                            </tr>
                        <?php
                        endif;
                        ?>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>

        <?php if ( $has_orders ) : ?>

            <?php do_action( 'woocommerce_before_account_orders_pagination' ); ?>

            <?php if ( 1 < $customer_orders->max_num_pages ) : ?>
                <div class="woocommerce-pagination woocommerce-pagination--without-numbers woocommerce-Pagination" style="margin-top: 12px;">
                    <?php if ( 1 !== $current_page ) : ?>
                        <a class="woocommerce-button woocommerce-button--previous woocommerce-Button woocommerce-Button--previous button" href="<?php echo esc_url( add_query_arg( $_SERVER['QUERY_STRING'], '', wc_get_endpoint_url( 'orders', $current_page - 1 ) ) ); ?>"><?php esc_html_e( 'Previous', 'woocommerce' ); ?></a>
                    <?php endif; ?>

                    <?php if ( intval( $customer_orders->max_num_pages ) !== $current_page ) : ?>
                        <a class="woocommerce-button woocommerce-button--next woocommerce-Button woocommerce-Button--next button" href="<?php echo esc_url( add_query_arg( $_SERVER['QUERY_STRING'], '', wc_get_endpoint_url( 'orders', $current_page + 1 ) ) ); ?>"><?php esc_html_e( 'Next', 'woocommerce' ); ?></a>
                    <?php endif; ?>
                </div>
            <?php endif; ?>

        <?php endif; ?>

    </div>

</div>

<?php do_action( 'woocommerce_after_account_orders', $has_orders ); ?>
