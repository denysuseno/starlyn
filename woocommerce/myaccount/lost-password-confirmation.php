<?php
/**
 * Lost password confirmation text.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/lost-password-confirmation.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.9.0
 */

defined( 'ABSPATH' ) || exit;
?>

<?php do_action( 'woocommerce_before_lost_password_confirmation_message' ); ?>

<div class="page-single wrapper-about">
    <div class="forget-password">
        <section class="section forget-password-header intro">
            <div class="breadcrumb">
                <a href="" class="a">Login</a>
                <span class="separator">
                /
              </span>
                <span class="a">Reset Password</span>
            </div>
            <div class="forget-password-call">
                <p class="text">Call: <span><?php echo call_support(); ?></span></p>
            </div>
        </section>
        <section class="section forget-password-content intro">
            <div class="forget-password-wrapper">
                <h3 class="reset">Reset Password</h3>
                <div class="forget-email-reset">
                    <?php $email = !isset($_GET['email']) ? '' : $_GET['email']; ?>
                    <p class="text">
                        <?php if( $email != '' ) : ?>
                            <?php echo esc_html( apply_filters( 'woocommerce_lost_password_confirmation_message', esc_html__( 'Link email untuk reset password telah di kirim ke '.$email.'. Tolong ikuti instruksi di email tersebut untuk reset email anda.', 'woocommerce' ) ) ); ?>
                        <?php else : ?>
                            <?php echo esc_html( apply_filters( 'woocommerce_lost_password_confirmation_message', esc_html__( 'Link email untuk reset password telah di kirim. Tolong ikuti instruksi di email tersebut untuk reset email anda.', 'woocommerce' ) ) ); ?>
                        <?php endif; ?>
                    </p>
                    <p class="title">Belum menerima email juga? <span class="sub-title"><a href="<?= wp_lostpassword_url(); ?>">coba cek kembali email anda.</a></span></p>
                </div>
            </div>
        </section>
    </div>
</div>

<?php do_action( 'woocommerce_after_lost_password_confirmation_message' ); ?>
