<?php
/**
 * Lost password form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-lost-password.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.2
 */

defined( 'ABSPATH' ) || exit;
?>

<?php do_action( 'woocommerce_before_lost_password_form' ); ?>

    <div class="page-single wrapper-about">
        <div class="forget-password">
            <section class="section forget-password-header intro">
                <div class="breadcrumb">
                    <a href="" class="a">Login</a>
                    <span class="separator">
                /
              </span>
                    <span class="a">Reset Password</span>
                </div>
                <div class="forget-password-call">
                    <p class="text">Call: <span><?php echo call_support(); ?></span></p>
                </div>
            </section>
            <section class="section forget-password-content intro">
                <div class="forget-password-wrapper">
                    <h3 class="reset">Reset Password</h3>
                    <p class="text">Isi kolom berikut ini dan tekan tombol <span>"Continue"</span> untuk mendapatkan password baru</p>

                    <?php do_action( 'woocommerce_lost_password_form_message' ); ?>

                    <form method="post" class="woocommerce-ResetPassword lost_reset_password page-forget">
                        <div class="input-forget">
                            <label for="" class="label-input"><?php esc_html_e( 'Your Email', 'woocommerce' ); ?></label> <br>
                            <input class="woocommerce-Input woocommerce-Input--text input-text input-form" type="text" name="user_login" id="user_login" autocomplete="username" />
                        </div>
                        <div class="forms-button">
                            <input type="hidden" name="wc_reset_password" value="true" />

                            <button type="button" class="button button-line" onclick="location.href='<?= get_permalink( get_option('woocommerce_myaccount_page_id') ) ?>'">Back</button>
                            <button type="submit" class="button button-gray" value="<?php esc_attr_e( 'Reset password', 'woocommerce' ); ?>"><?php esc_html_e( 'Continue', 'woocommerce' ); ?></button>
                        </div>

                        <?php wp_nonce_field( 'lost_password', 'woocommerce-lost-password-nonce' ); ?>
                    </form>
                </div>
            </section>
        </div>
    </div>

<?php do_action( 'woocommerce_after_lost_password_form' ); ?>