<?php
$query_args = query_page_wishlist( 1 );
$loop       = new WP_Query( $query_args );
?>

<?php if ( $loop->have_posts() ) : ?>

    <?php woocommerce_product_loop_start(); ?>

    <?php
    if ( $loop->have_posts() ) {
        while ( $loop->have_posts() ) {
            $loop->the_post();

            /**
             * Hook: woocommerce_shop_loop.
             */
            do_action( 'woocommerce_shop_loop' );

            wc_get_template_part( 'content', 'product' );
        }
    }
    wp_reset_postdata();
    ?>

    <?php woocommerce_product_loop_end(); ?>

    <?php if( $loop->found_posts > PAGE_WISHLIST_PPP ) : ?>
        <div class="view-more">
            <a href="<?php echo get_permalink( get_page_by_path( 'wishlist' ) ) ?>" class="link">View All</a>
        </div>
    <?php endif; ?>

<?php else : ?>

    <p>No wishlist found.</p>

<?php endif; ?>