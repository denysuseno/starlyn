<?php
/**
 * My Account Dashboard
 *
 * Shows the first intro screen on the account dashboard.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/dashboard.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce/Templates
 * @version     2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post;
?>

	<div class="account-main-content">
		<div class="account-slider js-account-slider">
            <?php
                $banners = get_field( 'banner', $post->ID );
            ?>
            <?php if( $banners ) : ?>
                <div class="slides">
                    <?php foreach ( $banners as $banner ) : ?>
                        <?php
                        $image = $banner['banner_image'];
                        $link  = $banner['banner_link'];
                        ?>
                        <div class="slide">
                            <?php if( isset( $link['url'] ) && $link['url'] ) : ?>
                                <a href="<?php echo $link['url'] ?>" target="<?php echo $link['target'] ?>">
                            <?php endif; ?>
                                    <img src="<?php echo $image ?>" alt="Banner Image" class="image">
                            <?php if( isset( $link['url'] ) && $link['url'] ) : ?>
                                </a>
                            <?php endif; ?>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
		</div>
		<div class="account-tabs js-tab" data-tab-deeplink>
			<div class="account-tabs-list" role="tablist" aria-label="Programming Languages">
				<button id="tab-recent" class="tab" role="tab">
					<svg class="icon" role="img"><use xlink:href="<?= library_url() ?>/images/svg-symbols.svg#icon-eye" /></svg>
					Terakhir dilihat
				</button>
				<button id="tab-wishlist" class="tab" role="tab">
					<svg class="icon" role="img"><use xlink:href="<?= library_url() ?>/images/svg-symbols.svg#icon-love-filled" /></svg>
					Wishlist
				</button>
			</div>
			<div class="account-tabs-content">
				<div class="tab-content" aria-labelledby="tab-recent" role="tabpanel">
                    <?php wc_get_template( 'myaccount/dashboard-recently-viewed-product.php' ); ?>
				</div>
				<div class="tab-content" aria-labelledby="tab-wishlist" role="tabpanel">
					<?php wc_get_template( 'myaccount/dashboard-wishlist.php' ); ?>
				</div>
			</div>
		</div>
	</div>

<?php
	/**
	 * My Account dashboard.
	 *
	 * @since 2.6.0
	 */
	do_action( 'woocommerce_account_dashboard' );

	/**
	 * Deprecated woocommerce_before_my_account action.
	 *
	 * @deprecated 2.6.0
	 */
	do_action( 'woocommerce_before_my_account' );

	/**
	 * Deprecated woocommerce_after_my_account action.
	 *
	 * @deprecated 2.6.0
	 */
	do_action( 'woocommerce_after_my_account' );

/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */
