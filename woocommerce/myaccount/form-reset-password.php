<?php
/**
 * Lost password reset form.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-reset-password.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.5
 */

defined( 'ABSPATH' ) || exit;
?>

<?php do_action( 'woocommerce_before_reset_password_form' ); ?>

    <div class="page-single wrapper-about">
        <div class="forget-password">
            <section class="section forget-password-header intro">
                <div class="breadcrumb">
                    <a href="" class="a">Login</a>
                    <span class="separator">
                /
              </span>
                    <span class="a">Reset Password</span>
                </div>
                <div class="forget-password-call">
                    <p class="text">Call: <span><?php echo call_support(); ?></span></p>
                </div>
            </section>
            <section class="section forget-password-content intro">
                <div class="forget-password-wrapper">
                    <h3 class="reset">Reset Password</h3>

                    <?php do_action( 'woocommerce_reset_password_form_message' ); ?>

                    <form method="post" class="woocommerce-ResetPassword lost_reset_password page-forget" action="">
                        <div class="input-forget">
                            <label for="password_1" class="label-input">Input New Password</label> <br>
                            <input type="password" class="woocommerce-Input woocommerce-Input--text input-text input-form" name="password_1" id="password_1" autocomplete="new-password" />
                        </div>
                        <div class="input-forget">
                            <label for="password_2" class="label-input">Re_type New Password</label> <br>
                            <input type="password" class="woocommerce-Input woocommerce-Input--text input-text input-form" name="password_2" id="password_2" autocomplete="new-password" />
                        </div>
                        <div class="forms-button">
                            <input type="hidden" name="reset_key" value="<?php echo esc_attr( $args['key'] ); ?>" />
                            <input type="hidden" name="reset_login" value="<?php echo esc_attr( $args['login'] ); ?>" />
                            <input type="hidden" name="wc_reset_password" value="true" />

                            <button type="button" class="button button-line" onclick="location.href='<?= home_url() ?>';">Back</button>
                            <button type="submit" class="woocommerce-Button button button-gray" value="<?php esc_attr_e( 'Save', 'woocommerce' ); ?>"><?php esc_html_e( 'Continue', 'woocommerce' ); ?></button>
                        </div>

                        <?php wp_nonce_field( 'reset_password', 'woocommerce-reset-password-nonce' ); ?>
                    </form>
                </div>
            </section>
        </div>
    </div>

<?php do_action( 'woocommerce_after_reset_password_form' ); ?>

