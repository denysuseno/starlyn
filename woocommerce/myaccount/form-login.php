<?php
/**
 * Login Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-login.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 4.1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

do_action( 'woocommerce_before_customer_login_form' ); ?>

<div class="page-single wrapper-about">

    <?php if( isset( $_GET['register'] ) && $_GET['register'] == 1 ) : ?>
        <div class="forget-password">
            <section class="section forget-password-header intro">
                <div class="breadcrumb">
                    <a href="<?php echo get_permalink( get_option( 'woocommerce_myaccount_page_id' ) ) ?>" class="a">Login</a>
                    <span class="separator">
                    /
                  </span>
                    <span class="a">Sign Up</span>
                </div>
            </section>
            <section class="section forget-password-content intro">
                <div class="forget-password-wrapper">
                    <div style="margin-top: 24px; margin-bottom: 24px">
                        <?php wc_print_notices(); ?>
                    </div>

                    <h3 class="reset">Sign Up</h3>
                    <p class="text">Isi kolom berikut ini dan tekan tombol <span>"Sign Up"</span> untuk mendapatkan password baru.</p>

                    <?php do_action( 'woocommerce_customer_register_form_message' ); ?>

                    <form method="post" class="woocommerce-form woocommerce-form-register register page-forget" <?php do_action( 'woocommerce_register_form_tag' ); ?> action="" >

                        <?php do_action( 'woocommerce_register_form_start' ); ?>

                        <div class="input-forget">
                            <label for="reg_email" class="label-input"><?php esc_html_e( 'Your Email', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
                            <input type="email" class="woocommerce-Input woocommerce-Input--text input-text input-form" name="email" id="reg_email" autocomplete="email" value="<?php echo ( ! empty( $_POST['email'] ) ) ? esc_attr( wp_unslash( $_POST['email'] ) ) : ''; ?>" /><?php // @codingStandardsIgnoreLine ?>
                        </div>
                        <?php if ( 'no' === get_option( 'woocommerce_registration_generate_password' ) ) : ?>
                            <div class="input-forget">
                                <label for="reg_password" class="label-input"><?php esc_html_e( 'Password', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
                                <input type="password" class="woocommerce-Input woocommerce-Input--text input-text input-form" name="password" id="reg_password" autocomplete="new-password" />
                            </div>
                            <div class="input-forget">
                                <label for="confirm-password" class="label-input">Confirm Password <span class="required">*</span></label> <br>
                                <input type="password" class="input-form" name="confirm_password" id="confirm-password">
                            </div>
                        <?php else : ?>

                            <p><?php esc_html_e( 'Password akan dikirim ke email kamu.', 'woocommerce' ); ?></p>

                        <?php endif; ?>

                        <?php do_action( 'woocommerce_register_form' ); ?>

                        <div class="forms-button">
                            <?php wp_nonce_field( 'woocommerce-register', 'woocommerce-register-nonce' ); ?>
                            <button type="submit" class="woocommerce-Button woocommerce-button woocommerce-form-register__submit button button-gray" name="register" disabled value="<?php esc_attr_e( 'Register', 'woocommerce' ); ?>"><?php esc_html_e( 'Sign Up', 'woocommerce' ); ?></button>
                        </div>

                        <?php do_action( 'woocommerce_register_form_end' ); ?>

                    </form>
                </div>
            </section>
        </div>
    <?php else : ?>
        <div class="forget-password">
            <section class="section forget-password-header intro">
                <div class="breadcrumb">
                    <a href="<?php echo esc_url( get_permalink( get_option( 'woocommerce_myaccount_page_id' ) ) ) ?>" class="a">Login</a>
                </div>
                <div class="forget-password-call">
                    <p class="text">Call: <span><?php echo call_support(); ?></span></p>
                </div>
            </section>
            <section class="section forget-password-content intro">
                <div class="forget-password-wrapper">
                    <h3 class="reset">Login</h3>

                    <div style="margin-top: 24px; margin-bottom: 24px">
                        <?php wc_print_notices(); ?>
                        <?php
                        if( isset( $_GET['login'] ) && $_GET['login'] == 'failed' ) {
                            if( isset( $_GET['error_code'] ) && $_GET['error_code'] == 'register_confirmation_error' ) {
                                $email = isset( $_GET['user_email'] ) ? $_GET['user_email'] : '';
                                $user = get_user_by( 'email', $email );
                                if( $user ) {
                                    $user_id = $user->ID;
                                    $location = add_query_arg( ['u' => $user_id], get_permalink( get_page_by_path( 'verify' ) ) );
                                    ?>
                                    <strong>Error:</strong> Your account has to be activated before you can login. Please click the link in the activation email that has been sent to you.<br /> If you do not receive the activation email within a few minutes, check your spam folder or <a href="<?php echo $location ?>">click here to resend it</a>.
                                    <?php
                                }
                            } else {
                                echo sprintf(
                                        __( '<strong>Error</strong>: The password you entered is incorrect.' )
                                    ) .
                                    ' <a href="' . wp_lostpassword_url() . '">' .
                                    __( 'Lost your password?' ) .
                                    '</a>';
                            }
                        }
                        ?>
                    </div>

                    <form method="post" class="form">
                        <div class="form-input">
                            <div class="label">
                                <label class="label-text" for="username">Email:</label>
                            </div>
                            <input type="email" class="input" name="username" id="username" value="<?php echo ( ! empty( $_POST['username'] ) ) ? esc_attr( $_POST['username'] ) : ''; ?>" /><?php //@codingStandardsIgnoreLine ?>
                        </div>
                        <div class="form-input">
                            <div class="label">
                                <span class="label-text" for="">Password:</span>
                                <div class="remember">
                                    <input type="checkbox" name="rememberme" id="remember-login" value="forever" style="margin-right: 4px">
                                    <label for="remember-login" class="remember-label">Ingat saya</label>
                                </div>
                            </div>
                            <input class="input" type="password" name="password" id="password" />
                        </div>
                        <div class="form-action">
                            <?php wp_nonce_field( 'woocommerce-login', 'woocommerce-login-nonce' ); ?>
                            <a href="<?php echo esc_url( wp_lostpassword_url() ); ?>" class="forgot-password">Forgot your password?</a>
                            <button type="submit" class="button button-primary button-login" name="login" value="<?php esc_attr_e( 'Login', 'woocommerce' ); ?>"><?php esc_html_e( 'Login', 'woocommerce' ); ?></button>
                            <?php
                                $redirect = esc_url( get_permalink( get_option( 'woocommerce_myaccount_page_id' ) ) );
                                if( isset( $_GET['redirect'] ) && $_GET['redirect'] ) {
                                    $redirect = $_GET['redirect'];
                                }
                            ?>
                            <input type="hidden" name="redirect" value="<?php echo $redirect; ?>" />
                        </div>
                    </form>
                </div>
            </section>
        </div>
    <?php endif; ?>
</div>

<?php do_action( 'woocommerce_after_customer_login_form' ); ?>
