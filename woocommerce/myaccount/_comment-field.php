<div class="form-input">
    <div class="form-input-field">
        <?php
        // format: userId-productId-orderId
        $review_key = get_current_user_id() . '-' . $product_id . '-' . $order->get_id();
        ?>
        <input type="hidden" name="review_key" class="input" id="review-key" value="<?php echo $review_key ?>">
    </div>
</div>

<div class="form-input">
    <!-- Rating -->
    <div class="form-input-field">
        <div class="rating-input">
            <span class="rating-input-label">Berikan Rating :</span>
            <div class="rating-input-select">
                <label class="star-select">
                    <input type="radio" name="rating" value="1" />
                    <svg class="icon" role="img"><use xlink:href="<?php echo library_url() ?>/images/svg-symbols.svg#icon-star" /></svg>
                </label>
                <label class="star-select">
                    <input type="radio" name="rating" value="2" />
                    <svg class="icon" role="img"><use xlink:href="<?php echo library_url() ?>/images/svg-symbols.svg#icon-star" /></svg>
                    <svg class="icon" role="img"><use xlink:href="<?php echo library_url() ?>/images/svg-symbols.svg#icon-star" /></svg>
                </label>
                <label class="star-select">
                    <input type="radio" name="rating" value="3" />
                    <svg class="icon" role="img"><use xlink:href="<?php echo library_url() ?>/images/svg-symbols.svg#icon-star" /></svg>
                    <svg class="icon" role="img"><use xlink:href="<?php echo library_url() ?>/images/svg-symbols.svg#icon-star" /></svg>
                    <svg class="icon" role="img"><use xlink:href="<?php echo library_url() ?>/images/svg-symbols.svg#icon-star" /></svg>
                </label>
                <label class="star-select">
                    <input type="radio" name="rating" value="4" />
                    <svg class="icon" role="img"><use xlink:href="<?php echo library_url() ?>/images/svg-symbols.svg#icon-star" /></svg>
                    <svg class="icon" role="img"><use xlink:href="<?php echo library_url() ?>/images/svg-symbols.svg#icon-star" /></svg>
                    <svg class="icon" role="img"><use xlink:href="<?php echo library_url() ?>/images/svg-symbols.svg#icon-star" /></svg>
                    <svg class="icon" role="img"><use xlink:href="<?php echo library_url() ?>/images/svg-symbols.svg#icon-star" /></svg>
                </label>
                <label class="star-select">
                    <input type="radio" name="rating" value="5" />
                    <svg class="icon" role="img"><use xlink:href="<?php echo library_url() ?>/images/svg-symbols.svg#icon-star" /></svg>
                    <svg class="icon" role="img"><use xlink:href="<?php echo library_url() ?>/images/svg-symbols.svg#icon-star" /></svg>
                    <svg class="icon" role="img"><use xlink:href="<?php echo library_url() ?>/images/svg-symbols.svg#icon-star" /></svg>
                    <svg class="icon" role="img"><use xlink:href="<?php echo library_url() ?>/images/svg-symbols.svg#icon-star" /></svg>
                    <svg class="icon" role="img"><use xlink:href="<?php echo library_url() ?>/images/svg-symbols.svg#icon-star" /></svg>
                </label>
            </div>
        </div>
    </div>

    <!-- Review Content -->
    <div class="form-input-field">
        <label class="label" for="comment">Tulis review anda</label>
        <textarea id="comment" class="input" name="comment" cols="45" rows="5" required></textarea>
    </div>
</div>