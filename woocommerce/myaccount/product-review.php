<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}

$user_id            = get_current_user_id();
$current_user       = wp_get_current_user();
$customer_email     = $current_user->user_email;

$from_date          = isset( $_GET['from'] ) ? $_GET['from'] : '';
$to_date            = isset( $_GET['to'] ) ? $_GET['to'] : '';
$order_no           = isset( $_GET['order_no'] ) ? $_GET['order_no'] : '';
$current_page       = isset( $_GET['current_page'] ) ? absint( $_GET['current_page'] ) : 1;

$args               = array(
    'posts_per_page'=> 10,
    'customer'      => get_current_user_id(),
    'page'          => $current_page,
    'paginate'      => true,
    'status'        => ['completed'],
);
$customer_orders    = wc_get_orders(
    apply_filters(
        'woocommerce_my_account_my_orders_review_query',
        $args
    )
);
$has_orders = 0 < $customer_orders->total;
?>

<div class="account-main-header">
    <h3 class="title">Ulasan Produk</h3>
</div>
<div class="account-main-content">
    <div class="data">
        <div class="data-filter">
            <form class="form">
                <div class="data-filter-fields">
                    <div class="data-filter-field form-input-date">
                        <textarea name="from" class="input myaccount-orders-filter-date-from" placeholder="Start Date"><?= isset( $_GET['from'] ) && $_GET['from'] ? $_GET['from'] : '' ?></textarea>
                    </div>
                    <div class="data-filter-field form-input-date">
                        <textarea name="to" class="input myaccount-orders-filter-date-to" placeholder="Start Date"><?= isset( $_GET['to'] ) && $_GET['to'] ? $_GET['to'] : '' ?></textarea>
                    </div>
                </div>
                <div class="data-filter-search">
                    <input type="text" class="input" name="order_no" placeholder="Search order number" value="<?= isset( $_GET['order_no'] ) && $_GET['order_no'] ? $_GET['order_no'] : '' ?>">
                </div>
            </form>
        </div>
        <div class="data-table">
            <div class="data-table-content">
                <div class="table-responsive">
                    <table class="table table-responsive-table table-review">
                        <thead>
                        <tr>
                            <th>Order#</th>
                            <th>Tanggal</th>
                            <th>Total Belanja</th>
                            <!--<th>Rating</th>-->
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php if( $has_orders ) : ?>
                                <?php foreach( $customer_orders->orders as $customer_order ) : ?>
                                    <?php
                                    $order          = wc_get_order( $customer_order ); // phpcs:ignore WordPress.WP.GlobalVariablesOverride.OverrideProhibited
                                    $item_count     = $order->get_item_count() - $order->get_item_count_refunded();
                                    $items          = $order->get_items();
                                    ?>
                                    <tr>
                                        <td><?php echo sprintf( '#%s', $order->get_order_number() ) ?></td>
                                        <td><time datetime="<?php echo esc_attr( $order->get_date_created()->date( 'c' ) ); ?>"><?php echo esc_html( wc_format_datetime( $order->get_date_created() ) ); ?></time></td>
                                        <td>
                                            <?php
                                            /* translators: 1: formatted order total 2: total order items */
                                            echo wp_kses_post( sprintf( _n( '%1$s (%2$s item)', '%1$s (%2$s items)', $item_count, 'woocommerce' ), $order->get_formatted_order_total(), $item_count ) );
                                            ?>
                                        </td><!--
                                        <td>
                                            <div class="rating-star">
                                                <div class="rating-star-space" title="Rated 0 out of 5">
                                                    <span class="fill" style="width:0%"></span>
                                                </div>
                                            </div>
                                        </td>-->
                                        <td class="action">
                                            <a href="#popup-review-<?php echo $order->get_order_number() ?>" class="button button-primary js-popup-inline">Beri Ulasan</a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            <?php else : ?>
                                <tr>
                                    <td colspan="5">
                                        <div class="woocommerce-message woocommerce-message--info woocommerce-Message woocommerce-Message--info woocommerce-info" style="text-align: center">
                                            <a class="link" href="<?php echo esc_url( apply_filters( 'woocommerce_return_to_shop_redirect', wc_get_page_permalink( 'shop' ) ) ); ?>">
                                                <?php esc_html_e( 'Browse products', 'woocommerce' ); ?>
                                            </a>
                                            <?php esc_html_e( 'No order has been made yet.', 'woocommerce' ); ?>
                                        </div>
                                    </td>
                                </tr>
                            <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <?php if ( $has_orders ) : ?>

            <?php if ( 1 < $customer_orders->max_num_pages ) : ?>
                <div class="woocommerce-pagination woocommerce-pagination--without-numbers woocommerce-Pagination" style="margin-top: 12px;">
                    <?php if ( 1 !== $current_page ) : ?>
                        <a class="woocommerce-button woocommerce-button--previous woocommerce-Button woocommerce-Button--previous button" href="<?php echo esc_url( add_query_arg( ['from' => $from_date, 'to' => $to_date, 'order_no' => $order_no, 'current_page' => $current_page - 1], '', wc_get_endpoint_url( 'product-review' ) ) ); ?>"><?php esc_html_e( 'Previous', 'woocommerce' ); ?></a>
                    <?php endif; ?>

                    <?php if ( intval( $customer_orders->max_num_pages ) !== $current_page ) : ?>
                        <a class="woocommerce-button woocommerce-button--next woocommerce-Button woocommerce-Button--next button" href="<?php echo esc_url( add_query_arg( ['from' => $from_date, 'to' => $to_date, 'order_no' => $order_no, 'current_page' => $current_page + 1], '', wc_get_endpoint_url( 'product-review' ) ) ); ?>"><?php esc_html_e( 'Next', 'woocommerce' ); ?></a>
                    <?php endif; ?>
                </div>
            <?php endif; ?>

        <?php endif; ?>
    </div>
</div>

<?php if( $has_orders ) : ?>
    <?php foreach( $customer_orders->orders as $customer_order ) : ?>
        <?php
        $order          = wc_get_order( $customer_order ); // phpcs:ignore WordPress.WP.GlobalVariablesOverride.OverrideProhibited
        $item_count     = $order->get_item_count() - $order->get_item_count_refunded();
        $items          = $order->get_items();
        ?>
        <!-- popup review -->
        <div id="popup-review-<?php echo $order->get_order_number() ?>" class="popup-review popup-inline">
            <?php foreach ( $items as $item ) : ?>

                <?php
                $product_id = $item->get_product_id();
                $review_key = get_current_user_id() . '-' . $product_id . '-' . $order->get_id();
                $comments   = get_comments( ['meta_key' => 'review_key', 'meta_value' => $review_key] );
                ?>

                <?php if( ! $comments ) : ?>

                    <?php
                    $commenter    = wp_get_current_commenter();
                    $comment_form = array(
                        /* translators: %s is product title */
                        'title_reply'         => sprintf( esc_html__( 'Tambah ulasan untuk produk &ldquo;%s&rdquo;', 'woocommerce' ), $item->get_name() ),
                        /* translators: %s is product title */
                        'title_reply_to'      => esc_html__( 'Leave a Reply to %s', 'woocommerce' ),
                        'title_reply_before'  => '<h4 class="heading">',
                        'title_reply_after'   => '</h4> <span class="js-button-close js-popup-inline" data-popper-target="#popup-review-'.$order->get_order_number().'"><svg class="icon" role="img"><use xlink:href="'.library_url().'/images/svg-symbols.svg#icon-close" /></svg></span>',
                        'comment_notes_after' => '',
                        'label_submit'        => esc_html__( 'Submit', 'woocommerce' ),
                        'logged_in_as'        => '',
                        'comment_field'       => '',
                        'id_form'               => 'commentform-' . $order->get_order_number() . '-' . $product_id,
                        'class_form'            => 'form',
                        'class_container'       => 'popup-inline-wrapper',
                        'submit_field'          => '<div class="form-submit">%1$s %2$s</div>',
                        'class_submit'          => 'button button-gray',
                    );

                    $name_email_required = (bool) get_option( 'require_name_email', 1 );
                    $fields              = array(
                        'author' => array(
                            'label'    => __( 'Name', 'woocommerce' ),
                            'type'     => 'text',
                            'value'    => $commenter['comment_author'],
                            'required' => $name_email_required,
                        ),
                        'email'  => array(
                            'label'    => __( 'Email', 'woocommerce' ),
                            'type'     => 'email',
                            'value'    => $commenter['comment_author_email'],
                            'required' => $name_email_required,
                        ),
                    );

                    $comment_form['fields'] = array();

                    foreach ( $fields as $key => $field ) {
                        $field_html  = '<p class="comment-form-' . esc_attr( $key ) . '">';
                        $field_html .= '<label for="' . esc_attr( $key ) . '">' . esc_html( $field['label'] );

                        if ( $field['required'] ) {
                            $field_html .= '&nbsp;<span class="required">*</span>';
                        }

                        $field_html .= '</label><input id="' . esc_attr( $key ) . '" name="' . esc_attr( $key ) . '" type="' . esc_attr( $field['type'] ) . '" value="' . esc_attr( $field['value'] ) . '" size="30" ' . ( $field['required'] ? 'required' : '' ) . ' /></p>';

                        $comment_form['fields'][ $key ] = $field_html;
                    }

                    $account_page_url = wc_get_page_permalink( 'myaccount' );
                    if ( $account_page_url ) {
                        /* translators: %s opening and closing link tags respectively */
                        $comment_form['must_log_in'] = '<p class="must-log-in">' . sprintf( esc_html__( 'You must be %1$slogged in%2$s to post a review.', 'woocommerce' ), '<a href="' . esc_url( $account_page_url ) . '">', '</a>' ) . '</p>';
                    }

                    if ( wc_review_ratings_enabled() ) {
                        ob_start();
                        include '_comment-field.php';
                        $comment_form['comment_field'] = ob_get_contents();
                        ob_get_clean();
                    }

                    comment_form( apply_filters( 'woocommerce_product_review_comment_form_args', $comment_form ), $product_id );
                    ?>

                <?php else : ?>

                    <div class="popup-inline-wrapper">
                        <h4 class="heading"><?php echo sprintf( esc_html__( 'Ulasan untuk produk &ldquo;%s&rdquo;', 'woocommerce' ), $item->get_name() ) ?></h4>
                        <span class="js-button-close js-popup-inline" data-popper-target="#popup-review-1"><svg class="icon" role="img"><use xlink:href="<?php echo library_url() ?>/images/svg-symbols.svg#icon-close" /></svg></span>
                        <p>
                            Ulasan telah diberikan untuk produk <a href="<?php echo add_query_arg( ['tab' => 'tab-reviews'], get_permalink( $product_id ) ) ?>"><?php echo $item->get_name() ?></a>
                        </p>
                    </div>

                <?php endif; ?>

            <?php endforeach; ?>
        </div>
    <?php endforeach; ?>
<?php endif; ?>
