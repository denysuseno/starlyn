<?php
/**
 * Edit account form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-edit-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.0
 */

defined( 'ABSPATH' ) || exit;

do_action( 'woocommerce_before_edit_account_form' );

$accountBirthdate   = esc_attr( get_user_meta( get_current_user_id(), 'account_birthdate', true ) );
$accountGender      = esc_attr( get_user_meta( get_current_user_id(), 'account_gender', true ) );
$accountPhone       = esc_attr( get_user_meta( get_current_user_id(), 'account_phone', true ) );

$displayChangePassword = isset($_GET['change_password']) && $_GET['change_password'] ? true : false;
?>

<?php if( $displayChangePassword ) : ?>

    <div class="profile">
        <div class="profile-header">
            <h3 class="title">Ganti Password Anda</h3>
        </div>
    </div>
    <form class="woocommerce-EditAccountForm edit-account form" action="" method="post" <?php do_action( 'woocommerce_edit_account_form_tag' ); ?> >
        <div class="form-input">
            <div class="form-input-field">
                <label for="password_current" class="label"><?php esc_html_e( 'Password lama', 'woocommerce' ); ?><span class="required">*</span></label>
                <input type="password" class="woocommerce-Input woocommerce-Input--password input-text input" name="password_current" id="password_current" autocomplete="off" />
            </div>
            <div class="form-input-field">
                <label for="password_1" class="label"><?php esc_html_e( 'Password baru', 'woocommerce' ); ?><span class="required">*</span></label>
                <input type="password" class="woocommerce-Input woocommerce-Input--password input-text input" name="password_1" id="password_1" autocomplete="off" />
            </div>
            <div class="form-input-field">
                <label for="password_2" class="label"><?php esc_html_e( 'Ulangi password baru', 'woocommerce' ); ?><span class="required">*</span></label>
                <input type="password" class="woocommerce-Input woocommerce-Input--password input-text input" name="password_2" id="password_2" autocomplete="off" />
            </div>
        </div>
        <div class="form-submit">
            <?php wp_nonce_field( 'save_account_details', 'save-account-details-nonce' ); ?>
            <button type="submit" class="woocommerce-Button button button-gray" name="save_account_details" value="<?php esc_attr_e( 'Save changes', 'woocommerce' ); ?>"><?php esc_html_e( 'Save Password', 'woocommerce' ); ?></button>
            <input type="hidden" name="action" value="save_account_details" />
            <input type="hidden" name="account_display_name" class="input" value="<?php echo esc_attr( $user->display_name ); ?>">
            <input type="hidden" name="account_birthdate" class="input" value="<?php echo esc_attr( $accountBirthdate ); ?>">
            <input type="hidden" name="account_gender" class="input" value="<?php echo esc_attr( $accountGender ); ?>">
            <input type="hidden" name="account_email" class="input" value="<?php echo esc_attr( $user->user_email ); ?>">
            <input type="hidden" name="account_phone" class="input" value="<?php echo esc_attr( $accountPhone ); ?>">
            <input type="hidden" name="change_password_page" class="input" value="1">
        </div>
    </form>

<?php else : ?>

    <div class="profile">
        <div class="profile-header">
            <h3 class="title">Profil Anda</h3>
        </div>
    </div>
    <div class="profile-content">
        <div class="profile-preview">
            <div class="profile-preview-image">
                <?php
                $gravatar_url = get_avatar_img_url( 205 );
                ?>
                <div class="image">
                    <img src="<?php echo $gravatar_url ?>" alt="Gravatar">
                </div>
                <div class="file">
                    <a href="<?php echo esc_url( 'gravatar.com' ) ?>" target="_blank" class="button button-ligth-gray">Change Profile Picture</a>
                </div>
            </div>
            <div class="profile-preview-password">
                <a href="<?php echo esc_url( add_query_arg(['change_password' => true], wc_get_account_endpoint_url( 'edit-account' )) ); ?>" class="button button-ligth-gray">Change Password</a>
            </div>
        </div>
        <div class="profile-meta">
            <section class="section">
                <h5 class="heading">PROFILE <a href="#popup-profile" class="link js-popup-inline">Edit</a></h5>
                <ul class="items">
                    <li class="item">
                        <div class="label"><?php esc_html_e( 'Name', 'woocommerce' ); ?></div>
                        <div class="value"><?php echo esc_attr( $user->display_name ); ?></div>
                    </li>
                    <li class="item">
                        <div class="label"><?php esc_html_e( 'Birthdate', 'woocommerce' ); ?></div>
                        <div class="value"><?php echo $accountBirthdate ? $accountBirthdate : '-'; ?></div>
                    </li>
                    <li class="item">
                        <div class="label"><?php esc_html_e( 'Gender', 'woocommerce' ); ?></div>
                        <div class="value"><?php echo $accountGender ? $accountGender : '-'; ?></div>
                    </li>
                </ul>
            </section>
            <section class="section">
                <h5 class="heading">Contact</h5>
                <ul class="items">
                    <li class="item">
                        <div class="label"><?php esc_html_e( 'Email', 'woocommerce' ); ?></div>
                        <div class="value"><?php echo esc_attr( $user->user_email ); ?></div>
                    </li>
                    <li class="item">
                        <div class="label"><?php esc_html_e( 'No. Handphone', 'woocommerce' ); ?></div>
                        <div class="value"><?php echo $accountPhone ? $accountPhone : '-' ?></div>
                    </li>
                </ul>
            </section>
        </div>
    </div>

    <!-- popup profile -->
    <div id="popup-profile" class="popup-profile popup-inline">
        <div class="popup-inline-wrapper">
            <h4 class="heading">Ubah Profil</h4>
            <span class="js-button-close js-popup-inline" data-popper-target="#popup-review-1"><svg class="icon" role="img"><use xlink:href="<?= library_url() ?>/images/svg-symbols.svg#icon-close" /></svg></span>
            <form class="woocommerce-EditAccountForm edit-account form" action="" method="post" <?php do_action( 'woocommerce_edit_account_form_tag' ); ?> >
                <div class="form-input">
                    <div class="form-input-field">
                        <label class="label" for="account_display_name">Nama <span class="required">*</span></label>
                        <input type="text" name="account_display_name" id="account_display_name" class="input" value="<?php echo esc_attr( $user->display_name ); ?>">
                    </div>
                    <div class="form-input-field">
                        <label class="label" for="account_birthdate">Tanggal Lahir</label>
                        <input type="date" name="account_birthdate" id="account_birthdate" class="input" value="<?php echo esc_attr( $accountBirthdate ); ?>">
                    </div>
                    <div class="form-input-field">
                        <label class="label" for="account_gender">Jenis Kelamin</label>
                        <select name="account_gender" class="input" id="account_gender">
                            <option value="" <?= is_null($accountGender) || empty($accountGender) || $accountGender == "" ? 'selected="selected"' : '' ?>>Select</option>
                            <option value="Male" <?= $accountGender == "Male" ? 'selected="selected"' : '' ?>>Male</option>
                            <option value="Female" <?= $accountGender == "Female" ? 'selected="selected"' : '' ?>>Female</option>
                        </select>
                    </div>
                    <div class="form-input-field">
                        <label class="label" for="account_email">Alamat Email <span class="required">*</span></label>
                        <input type="text" name="account_email" id="account_email" class="input" value="<?php echo esc_attr( $user->user_email ); ?>">
                    </div>
                    <div class="form-input-field">
                        <label class="label" for="account_phone">Nomor Telepon <span class="required">*</span></label>
                        <input type="text" name="account_phone" id="account_phone" class="input force-number" value="<?php echo esc_attr( $accountPhone ); ?>">
                    </div>
                </div>
                <div class="form-submit">
                    <?php wp_nonce_field( 'save_account_details', 'save-account-details-nonce' ); ?>
                    <button type="submit" class="woocommerce-Button button button-gray" name="save_account_details" value="<?php esc_attr_e( 'Save changes', 'woocommerce' ); ?>"><?php esc_html_e( 'Save Profile', 'woocommerce' ); ?></button>
                    <input type="hidden" name="action" value="save_account_details" />
                </div>
            </form>
        </div>
    </div>

<?php endif; ?>

<?php do_action( 'woocommerce_after_edit_account_form' ); ?>