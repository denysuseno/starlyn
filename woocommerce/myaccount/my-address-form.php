<?php
    if( isset( $_GET['address'] ) && $_GET['address'] ) {
        $address        = get_address_by_id( $_GET['address'] );

        $editAddressUrl = wc_get_account_endpoint_url( 'edit-address' );

        if( !$address ) {
        ?>
            <p>Data alamat tidak ditemukan. <br> <a href="<?= esc_url( $editAddressUrl ) ?>">Kembali ke daftar alamat</a></p>
        <?php
            return;
        }

        if( $address->userid != get_current_user_id() ) {
        ?>
            <p>Anda tidak dapat mengubah alamat yang dimaksud. <br> <a href="<?= esc_url( $editAddressUrl ) ?>">Kembali ke daftar alamat</a></p>
        <?php
            return;
        }

        $addrID         = $address->id;
        $addrName       = $address->name;
        $addrPhone      = $address->phone;
        $addrLabel      = $address->address_name;
        $addrAddress    = $address->address;
        $addrProvince   = $address->province;
        $addrRegency    = $address->regency;
        $addrDistrict   = $address->district;
        $addrPostcode   = $address->postcode;
        $addrIsMain     = $address->as_default_billing_address;

        $isEdit         = true;
    } else {
        $addrID         = '';
        $addrName       = isset( $_GET['address_name'] ) && $_GET['address_name'] ? $_GET['address_name'] : '';
        $addrPhone      = isset( $_GET['address_phone'] ) && $_GET['address_phone'] ? $_GET['address_phone'] : '';
        $addrLabel      = isset( $_GET['address_label'] ) && $_GET['address_label'] ? $_GET['address_label'] : '';
        $addrAddress    = isset( $_GET['address_address'] ) && $_GET['address_address'] ? $_GET['address_address'] : '';
        $addrProvince   = isset( $_GET['address_province'] ) && $_GET['address_province'] ? $_GET['address_province'] : '';
        $addrRegency    = isset( $_GET['address_regency'] ) && $_GET['address_regency'] ? $_GET['address_regency'] : '';
        $addrDistrict   = isset( $_GET['address_district'] ) && $_GET['address_district'] ? $_GET['address_district'] : '';
        $addrPostcode   = isset( $_GET['address_postcode'] ) && $_GET['address_postcode'] ? $_GET['address_postcode'] : '';
        $addrIsMain     = isset( $_GET['address_is_main'] ) && $_GET['address_is_main'] ? $_GET['address_is_main'] : '';

        $isEdit         = false;
    }
?>

<div class="account-main-header">
    <h3 class="title"><?= isset( $_GET['address'] ) && $_GET['address'] ? 'Ubah Alamat' : 'Tambah Alamat Baru' ?></h3>
</div>
<div class="account-main-content">
    <div class="addressbook-form">
        <form class="form form-address-book" method="post">
            <div class="form-input">
                <input type="hidden" name="address_id" value="<?= $addrID ?>">
                <input type="hidden" name="address_country" value="ID">
                <input type="hidden" name="redirect_after_success" value="<?= wc_get_account_endpoint_url( 'edit-address' ) ?>">
                <input type="hidden" name="redirect_after_failed" value="<?= wc_get_account_endpoint_url( 'edit-address/shipping' ) ?>">
                <div class="form-input-field">
                    <label class="label" for="address-label">Label<span class="required">*</span></label>
                    <input type="text" class="input" id="address-label" name="address_address_name" value="<?= $addrLabel ?>" placeholder="Contoh: Rumah, Toko" required>
                </div>
                <div class="form-input-field">
                    <label class="label" for="address-name">Nama<span class="required">*</span></label>
                    <input type="text" class="input" id="address-name" name="address_name" value="<?= $addrName ?>" required>
                </div>
                <div class="form-input-field">
                    <label class="label" for="address-phone">Nomor telepon<span class="required">*</span></label>
                    <input type="text" class="input force-number" id="address-phone" name="address_phone" value="<?= $addrPhone ?>" required>
                </div>
                <div class="form-input-field">
                    <label class="label" for="address-address">Alamat<span class="required">*</span></label>
                    <textarea rows="5" class="input" id="address-address" name="address_address" required><?= $addrAddress ?></textarea>
                </div>
                <div class="form-input-fields">
                    <div class="form-input-field">
                        <?php
                            $province = get_province();
                            $regencyNonce = wp_create_nonce( 'get_regency_nonce' );
                        ?>
                        <label class="label" for="address-province">Provinsi<span class="required">*</span></label>
                        <select class="input province-ajax" id="address-province" name="address_province" data-url="<?= admin_url( 'admin-ajax.php?action=get_regency&nonce='.$regencyNonce ) ?>" required>
                            <option value="">- Pilih Provinsi -</option>
                            <?php if( $province ) : ?>
                                <?php foreach( $province as $item ) : ?>
                                    <?php $selected = $addrProvince == $item->province_name ? 'selected="selected"' : ''; ?>
                                    <option value="<?= $item->province_name ?>" <?= $selected ?>><?= $item->province_name ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                    </div>
                    <div class="form-input-field">
                        <?php $districtNonce = wp_create_nonce( 'get_district_nonce' ); ?>
                        <label class="label" for="address-regency">Kabupaten / Kota<span class="required">*</span></label>
                        <select class="input regency-ajax" id="address-regency" name="address_regency" data-val="<?= $addrRegency ?>" data-url="<?= admin_url( 'admin-ajax.php?action=get_district&nonce='.$districtNonce ) ?>" required>
                            <option value="">- Pilih Kabupaten / Kota -</option>
                        </select>
                    </div>
                </div>
                <div class="form-input-fields">
                    <?php $postcodeNonce = wp_create_nonce( 'get_postcode_nonce' ); ?>
                    <div class="form-input-field">
                        <label class="label" for="address-district">Kecamatan<span class="required">*</span></label>
                        <select class="input district-ajax" id="address-district" name="address_district" data-val="<?= $addrDistrict ?>" data-url="<?= admin_url( 'admin-ajax.php?action=get_postcode&nonce='.$postcodeNonce ) ?>" required>
                            <option value="">- Pilih Kecamatan -</option>
                        </select>
                    </div>
                    <div class="form-input-field">
                        <label class="label" for="address-postcode">Kode Pos<span class="required">*</span></label>
                        <select class="input postcode-ajax" id="address-postcode" name="address_postcode" data-val="<?= $addrPostcode ?>" required>
                            <option value="">- Pilih Kode Pos -</option>
                        </select>
                        <!--<input type="text" class="input" id="address-postcode" name="address_postcode" value="<?/*= $addrPostcode */?>" minlength="5" maxlength="5">-->
                    </div>
                </div>
                <div class="form-input-field">
                    <label class="label label-main-address">
                        <input type="checkbox" name="as_default_billing_address" <?= $addrIsMain ? 'checked' : '' ?> value="yes">
                        <span class="checkbox">
                            <svg class="icon" role="img"><use xlink:href="<?= library_url() ?>/images/svg-symbols.svg#icon-check" /></svg>
                        </span>
                        <span class="text">Set as my main address</span>
                    </label>
                </div>
            </div>
            <div class="form-submit">
                <?php if( isset( $_GET['address'] ) && $_GET['address'] ) : ?>
                    <?php wp_nonce_field( 'update_address_book' ); ?>
                    <input type="hidden" name="action" value="update_address_book" />
                    <button class="button button-gray" type="submit" name="update_address_book">Save Changes</button>
                <?php else : ?>
                    <?php wp_nonce_field( 'save_address_book' ); ?>
                    <input type="hidden" name="action" value="save_address_book" />
                    <button class="button button-gray" type="submit" name="save_address_book">Save Address</button>
                <?php endif; ?>
            </div>
        </form>
    </div>
</div>