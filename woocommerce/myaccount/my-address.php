<?php
/**
 * My Addresses
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/my-address.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 2.6.0
 */

defined( 'ABSPATH' ) || exit;

$customer_id = get_current_user_id();
$addresses = get_all_my_address_book();
?>

<div class="account-main-header">
    <h3 class="title">Alamat</h3>
    <?php if( count( $addresses ) < MAX_ADDRESS_BOOK ) : ?>
        <a href="<?php echo esc_url( wc_get_endpoint_url( 'edit-address', 'shipping' ) ); ?>" class="button button-gray">+ Add New Address</a>
    <?php endif; ?>
</div>
<div class="account-main-content">
    <div class="data">

        <?php if( $addresses ) : ?>

            <div class="data-address">
                <ul class="items">

                    <?php foreach ( $addresses as $address ) : ?>

                        <?php
                            $isMain = $address->as_default_billing_address;
                        ?>

                        <li class="item">
                            <div class="item-meta">
                                <div class="item-meta-detail">
                                    <div class="row">
                                        <div class="label">Name</div>
                                        <div class="value"><?= $address->name ?> <?= $isMain ? '<span class="main-address">Utama</span>' : '' ?></div>
                                    </div>
                                    <div class="row">
                                        <div class="label">Address</div>
                                        <div class="value"><?= $address->address ?></div>
                                    </div>
                                    <div class="row">
                                        <div class="label">Telephone</div>
                                        <div class="value"><?= $address->phone ?></div>
                                    </div>
                                </div>
                                <div class="item-meta-action">
                                    <?php
                                        $removeNonce = wp_create_nonce('ajax_delete_address_book');
                                    ?>
                                    <a href="<?php echo add_query_arg( ['address' => $address->id], esc_url( wc_get_endpoint_url( 'edit-address', 'shipping' ) ) ) ?>" class="link link-edit">Edit</a>
                                    <?php if( !$isMain ) : ?>
                                        <a href="javascript:void(0)" class="link link-remove address-book-remove" data-url="<?= admin_url( 'admin-ajax.php?action=ajax_delete_address_book&nonce='.$removeNonce ) ?>" data-address-id="<?= $address->id ?>" data-nonce="<?= $removeNonce ?>" data-redirect-url="<?= esc_url( wc_get_account_endpoint_url( 'edit-address' ) ) ?>">
                                            <svg class="icon" role="img"><use xlink:href="<?= library_url() ?>/images/svg-symbols.svg#icon-trash" /></svg>
                                        </a>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="item-select">
                                <?php
                                    $setMainAddrNonce = wp_create_nonce('ajax_set_main_address_book');
                                ?>
                                <label class="label">
                                    <input type="radio" name="main_address" <?= $isMain ? 'checked' : '' ?> class="<?= !$isMain ? 'set-main-address' : ''; ?>"
                                           data-url="<?= admin_url( 'admin-ajax.php?action=set_main_address_book&nonce='.$setMainAddrNonce ) ?>"
                                           data-address-id="<?= $address->id ?>"
                                           data-address-name="<?= $address->name ?>"
                                           data-address-address-name="<?= $address->address_name ?>"
                                           data-address-country="<?= $address->country ?>"
                                           data-address-province="<?= $address->province ?>"
                                           data-address-regency="<?= $address->regency ?>"
                                           data-address-district="<?= $address->district ?>"
                                           data-address-postcode="<?= $address->postcode ?>"
                                           data-address-address="<?= $address->address ?>"
                                           data-address-phone="<?= $address->phone ?>"
                                           data-nonce="<?= $setMainAddrNonce ?>"
                                           data-redirect-url="<?= esc_url( wc_get_account_endpoint_url( 'edit-address' ) ) ?>"
                                    >
                                    <span class="checkbox">
                                        <svg class="icon" role="img"><use xlink:href="<?= library_url() ?>/images/svg-symbols.svg#icon-check" /></svg>
                                    </span>
                                    <span class="text"><?= $isMain ? 'Main Address' : 'Set as Main Address' ?></span>
                                </label>
                            </div>
                        </li>

                    <?php endforeach; ?>

                </ul>
            </div>

        <?php else : ?>

            <p>Anda belum memiliki data alamat.</p>

        <?php endif; ?>

    </div>
</div>