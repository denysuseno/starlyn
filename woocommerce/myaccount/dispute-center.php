<?php

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly.
}
?>

<div class="account-main-header">
    <h3 class="title">Komplain</h3>
</div>
<div class="account-main-content">
    <div class="data">
        <div class="data-filter">
            <form class="form">
                <div class="data-filter-fields">
                    <div class="data-filter-field">
                        <input type="text" class="input" placeholder="Invoice">
                    </div>
                    <div class="data-filter-field">
                        <select class="input">
                            <option value="">Komplain Terbaru</option>
                        </select>
                    </div>
                    <div class="data-filter-field">
                        <select class="input">
                            <option value="">Tipe Pengajuan</option>
                        </select>
                    </div>
                    <div class="data-filter-field">
                        <select class="input">
                            <option value="">Asal Pengiriman</option>
                        </select>
                    </div>
                </div>
            </form>
        </div>
        <div class="data-table">
            <div class="data-table-content">
                <div class="table-responsive">
                    <table class="table table-responsive-table">
                        <thead>
                        <tr>
                            <th>Order#</th>
                            <th>Tanggal</th>
                            <th>Dikirim ke</th>
                            <th>Total Belanja</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>#237912</td>
                            <td>Januari 9, 2020</td>
                            <td>Icubic</td>
                            <td>Rp . 12.899.000 ( 3 items)</td>
                            <td>Proccesing</td>
                        </tr>
                        <tr>
                            <td>#243562</td>
                            <td>Januari 9, 2020</td>
                            <td>Icubic</td>
                            <td>Rp . 12.899.000 ( 3 items)</td>
                            <td>On Hold</td>
                        </tr>
                        <tr>
                            <td>#237912</td>
                            <td>Januari 9, 2020</td>
                            <td>Icubic</td>
                            <td>Rp . 12.899.000 ( 3 items)</td>
                            <td>Proccesing</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
