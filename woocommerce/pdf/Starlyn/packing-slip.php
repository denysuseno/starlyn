<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>
<?php do_action( 'wpo_wcpdf_before_document', $this->type, $this->order ); ?>

<table style="width: 100%; border-collapse: collapse; border: 1px solid #333;">
    <!-- Header -->
    <tr>
        <td style="width: 100%; padding: 8px 12px; border: 1px solid #333;">
            <table style="width: 100%; border-collapse: collapse;">
                <tr>
                    <td style="width: 50%; padding-right: 8px;">
                        <?php
                        if( $this->has_header_logo() ) {
                            $this->header_logo();
                        } else {
                            echo $this->get_title();
                        }
                        ?>
                    </td>
                    <td style="width: 50%; padding-left: 8px; text-align: right;">
                        <?php do_action( 'wpo_wcpdf_shipping_partner_logo', $this->order ) ?>
                    </td>
                </tr>
            </table>
        </td>
    </tr>

    <!-- AWB Barcode -->
    <tr>
        <td style="width: 100%; padding: 12px; border: 1px solid #333; text-align: center;">
            <?php do_action( 'wpo_wcpdf_generate_barcode_waybill', $this->order ) ?>
        </td>
    </tr>

    <!-- Service Code -->
    <tr>
        <td style="width: 100%; padding: 8px 12px; border: 1px solid #333; text-align: center; font-size: 12px;">Jenis Layanan: <strong><?php do_action( 'wp_wcpd_display_shipping_sub_service', $this->order ); ?></strong></td>
    </tr>

    <!-- Package Details & Barcode Order -->
    <tr>
        <td>
            <table style="width: 100%; border-collapse: collapse; font-size: 12px;">
                <tr>
                    <td rowspan="3" style="border-right: 1px solid #333; width: 140px; text-align: center; ">
                        <?php do_action( 'wpo_wcpdf_generate_barcode_order', $this->order ) ?>
                    </td>
                    <td style="border-bottom: 1px solid #333; padding: 6px 0 6px 8px; width: 32px;">Asuransi</td>
                    <td style="border-bottom: 1px solid #333; padding: 6px 8px;">: <strong><?php do_action( 'wpo_wcpdf_shipping_insurance_rate', $this->order ) ?></strong></td>
                </tr>
                <tr>
                    <td style="border-bottom: 1px solid #333; padding: 6px 0 6px 8px;">Berat</td>
                    <td style="border-bottom: 1px solid #333; padding: 6px 8px;">: <strong><?php do_action( 'wpo_wcpdf_shipping_total_weight', $this->order ) ?></strong></td>
                </tr>
                <tr>
                    <td style="padding: 6px 0 6px 8px;">Quantity</td>
                    <td style="padding: 6px 8px;">: <strong><?php do_action( 'wpo_wcpdf_shipping_total_quantity', $this->order ) ?></strong></td>
                </tr>
            </table>
        </td>
    </tr>

    <!-- Shipping Address -->
    <tr>
        <td style="width: 100%; padding: 6px 8px; border: 1px solid #333; font-size: 10px;">
            <table style="width: 100%; border-collapse: collapse;">
                <?php
                $billing_address            = $order->get_billing_address_1() . ' (' . $order->get_billing_address_2() . ')';
                $billing_address_city       = $order->get_billing_city();
                $billing_address_state      = $order->get_billing_state();
                $billing_address_postcode   = $order->get_billing_postcode();
                $billing_address_full       = $billing_address . ', ' . $billing_address_city . ', ' . $billing_address_state . ', ' . $billing_address_postcode;

                $shipper_address_opt        = get_field( 'shipper_address', 'option' );
                $shipper_name               = ! isset( $shipper_address_opt['shipper_name'] ) ? '' : $shipper_address_opt['shipper_name'];
                $shipper_address_1          = ! isset( $shipper_address_opt['shipper_address_1'] ) ? '' : $shipper_address_opt['shipper_address_1'];
                $shipper_address_2          = ! isset( $shipper_address_opt['shipper_address_2'] ) ? '' : $shipper_address_opt['shipper_address_2'];
                $shipper_address_3          = ! isset( $shipper_address_opt['shipper_address_3'] ) ? '' : $shipper_address_opt['shipper_address_3'];
                $shipper_district           = ! isset( $shipper_address_opt['shipper_district'] ) ? '' : $shipper_address_opt['shipper_district'];
                $shipper_city               = ! isset( $shipper_address_opt['shipper_city'] ) ? '' : $shipper_address_opt['shipper_city'];
                $shipper_postcode           = ! isset( $shipper_address_opt['shipper_postcode'] ) ? '' : $shipper_address_opt['shipper_postcode'];
                $shipper_address_full       = $shipper_address_1 . ' ' . $shipper_address_2 . ' ' . $shipper_address_3 . ', ' . $shipper_district . ', ' . $shipper_city . ', ' . $shipper_postcode;
                $shipper_phone              = ! isset( $shipper_address_opt['shipper_phone'] ) ? '' : $shipper_address_opt['shipper_phone'];
                ?>
                <tr>
                    <td>
                        <p style="margin: 0 0 24px;">Penerima: <br>Nama: <strong><?php echo $order->get_billing_first_name() ?></strong></p>
                        <p style="margin: 0 0 24px;">Alamat Lengkap: <br><strong><?php echo $billing_address_full ?></strong></p>
                        <p style="margin: 0;">No. Telepon: <br><strong>089628826333</strong></p>
                    </td>
                    <td>
                        <p style="margin: 0 0 24px;">Pengirim <br>Nama: <strong><?php echo $shipper_name ?></strong></p>
                        <p style="margin: 0 0 24px;">Alamat Lengkap: <br> <strong><?php echo $shipper_address_full ?></strong></p>
                        <p style="margin: 0;">No. Telepon: <br><strong><?php echo $shipper_phone ?></strong></p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <?php if ( $this->get_shipping_notes() ) : ?>
        <tr>
            <td style="width: 100%; height: 70px; padding: 6px 8px; border: 1px solid #333; font-size: 10px; vertical-align: top;">
                <p style="margin: 0 0 6px;"><strong>Catatan: <br><?php $this->shipping_notes(); ?></strong></p>
            </td>
        </tr>
    <?php endif; ?>
</table>

<div style="display: none">

    <table class="head container">
        <tr>
            <td class="header">
            <?php
            if( $this->has_header_logo() ) {
                $this->header_logo();
            } else {
                echo $this->get_title();
            }
            ?>
            </td>
            <td class="shop-info">
                <?php do_action( 'wpo_wcpdf_before_shop_name', $this->type, $this->order ); ?>
                <div class="shop-name"><h3><?php $this->shop_name(); ?></h3></div>
                <?php do_action( 'wpo_wcpdf_after_shop_name', $this->type, $this->order ); ?>
                <?php do_action( 'wpo_wcpdf_before_shop_address', $this->type, $this->order ); ?>
                <div class="shop-address"><?php $this->shop_address(); ?></div>
                <?php do_action( 'wpo_wcpdf_after_shop_address', $this->type, $this->order ); ?>
            </td>
        </tr>
    </table>

    <h1 class="document-type-label">
    <?php if( $this->has_header_logo() ) echo $this->get_title(); ?>
    </h1>

    <?php do_action( 'wpo_wcpdf_after_document_label', $this->type, $this->order ); ?>

    <table class="order-data-addresses">
        <tr>
            <td class="address shipping-address">
                <!-- <h3><?php _e( 'Shipping Address:', 'woocommerce-pdf-invoices-packing-slips' ); ?></h3> -->
                <?php do_action( 'wpo_wcpdf_before_shipping_address', $this->type, $this->order ); ?>
                <?php $this->shipping_address(); ?>
                <?php do_action( 'wpo_wcpdf_after_shipping_address', $this->type, $this->order ); ?>
                <?php if ( isset($this->settings['display_email']) ) { ?>
                <div class="billing-email"><?php $this->billing_email(); ?></div>
                <?php } ?>
                <?php if ( isset($this->settings['display_phone']) ) { ?>
                <div class="billing-phone"><?php $this->billing_phone(); ?></div>
                <?php } ?>
            </td>
            <td class="address billing-address">
                <?php if ( !empty($this->settings['display_billing_address']) && ( $this->ships_to_different_address() || $this->settings['display_billing_address'] == 'always' ) ) { ?>
                <h3><?php _e( 'Billing Address:', 'woocommerce-pdf-invoices-packing-slips' ); ?></h3>
                <?php do_action( 'wpo_wcpdf_before_billing_address', $this->type, $this->order ); ?>
                <?php $this->billing_address(); ?>
                <?php do_action( 'wpo_wcpdf_after_billing_address', $this->type, $this->order ); ?>
                <?php } ?>
            </td>
            <td class="order-data">
                <table>
                    <?php do_action( 'wpo_wcpdf_before_order_data', $this->type, $this->order ); ?>
                    <tr class="order-number">
                        <th><?php _e( 'Order Number:', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
                        <td><?php $this->order_number(); ?></td>
                    </tr>
                    <tr class="order-date">
                        <th><?php _e( 'Order Date:', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
                        <td><?php $this->order_date(); ?></td>
                    </tr>
                    <tr class="shipping-method">
                        <th><?php _e( 'Shipping Method:', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
                        <td><?php $this->shipping_method(); ?></td>
                    </tr>
                    <?php do_action( 'wpo_wcpdf_after_order_data', $this->type, $this->order ); ?>
                </table>
            </td>
        </tr>
    </table>

    <?php do_action( 'wpo_wcpdf_before_order_details', $this->type, $this->order ); ?>

    <table class="order-details">
        <thead>
            <tr>
                <th class="product"><?php _e('Product', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
                <th class="quantity"><?php _e('Quantity', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
            </tr>
        </thead>
        <tbody>
            <?php $items = $this->get_order_items(); if( sizeof( $items ) > 0 ) : foreach( $items as $item_id => $item ) : ?>
            <tr class="<?php echo apply_filters( 'wpo_wcpdf_item_row_class', $item_id, $this->type, $this->order, $item_id ); ?>">
                <td class="product">
                    <?php $description_label = __( 'Description', 'woocommerce-pdf-invoices-packing-slips' ); // registering alternate label translation ?>
                    <span class="item-name"><?php echo $item['name']; ?></span>
                    <?php do_action( 'wpo_wcpdf_before_item_meta', $this->type, $item, $this->order  ); ?>
                    <span class="item-meta"><?php echo $item['meta']; ?></span>
                    <dl class="meta">
                        <?php $description_label = __( 'SKU', 'woocommerce-pdf-invoices-packing-slips' ); // registering alternate label translation ?>
                        <?php if( !empty( $item['sku'] ) ) : ?><dt class="sku"><?php _e( 'SKU:', 'woocommerce-pdf-invoices-packing-slips' ); ?></dt><dd class="sku"><?php echo $item['sku']; ?></dd><?php endif; ?>
                        <?php if( !empty( $item['weight'] ) ) : ?><dt class="weight"><?php _e( 'Weight:', 'woocommerce-pdf-invoices-packing-slips' ); ?></dt><dd class="weight"><?php echo $item['weight']; ?><?php echo get_option('woocommerce_weight_unit'); ?></dd><?php endif; ?>
                    </dl>
                    <?php do_action( 'wpo_wcpdf_after_item_meta', $this->type, $item, $this->order  ); ?>
                </td>
                <td class="quantity"><?php echo $item['quantity']; ?></td>
            </tr>
            <?php endforeach; endif; ?>
        </tbody>
    </table>

    <div class="bottom-spacer"></div>

    <?php do_action( 'wpo_wcpdf_after_order_details', $this->type, $this->order ); ?>

    <?php do_action( 'wpo_wcpdf_before_customer_notes', $this->type, $this->order ); ?>
    <div class="customer-notes">
        <?php if ( $this->get_shipping_notes() ) : ?>
            <h3><?php _e( 'Customer Notes', 'woocommerce-pdf-invoices-packing-slips' ); ?></h3>
            <?php $this->shipping_notes(); ?>
        <?php endif; ?>
    </div>
    <?php do_action( 'wpo_wcpdf_after_customer_notes', $this->type, $this->order ); ?>

    <?php if ( $this->get_footer() ): ?>
    <div id="footer">
        <?php $this->footer(); ?>
    </div><!-- #letter-footer -->
    <?php endif; ?>

</div>

<?php do_action( 'wpo_wcpdf_after_document', $this->type, $this->order ); ?>