<?php
/**
 * Customer new account email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-new-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates/Emails
 * @version 3.7.0
 */

defined( 'ABSPATH' ) || exit; ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width">
    <title>Starlyn Email - Confirm Email</title>
    <!-- <style> -->
    <style type="text/css">
        * {
            margin: 0;
            padding: 0;
        }
        table {
            border-spacing: 0;
            border-collapse: collapse;
        }
        table thead th {
            border-bottom: 1px solid #DBDBDB;
        }
    </style>
</head>
<body style="color: #141414;font-family: sans-serif;font-weight: normal;padding: 0;margin: 0;text-align: left;font-size: 16px;line-height: 1.5;width: 100%;border-spacing: 0;border-collapse: collapse;background-color: #F5F5F5;">
<table style="width: 100%;">
    <tr>
        <td height="24px" style="font-size:24px;line-height:24px;">&#xA0;</td>
    </tr>
    <tr>
        <td valign="top">
            <table style="width: 100%; max-width: 800px; margin: 0 auto;">
                <tr>
                    <td>
                        <table align="center" style="width: 100%; background-color: #FFFFFF;">
                            <tbody>
                            <tr>
                                <td>
                                    <table style="width: 100%;">
                                        <!-- header -->
                                        <tr>
                                            <td style="text-align: center; padding: 32px; border-bottom: 2px solid #ffc700;box-shadow: 0px 4px 10px 8px rgba(44, 44, 44, 0.08), 0px 4px 40px 20px rgba(136, 135, 135, 0.02);background-color: #f7f7f7; ">
                                                <?php if ( $img = get_option( 'woocommerce_email_header_image' ) ) : ?>
                                                    <img src="<?php echo esc_url( $img ) ?>" alt="<?php echo get_bloginfo( 'name', 'display' ) ?>" style="max-width: 152px; height: auto;">
                                                <?php endif; ?>
                                            </td>
                                        </tr>
                                        <!-- header -->
                                        <!-- body -->
                                        <tr>
                                            <td style="padding: 38px 32px;">
                                                <p><strong style="color: #5A5A5A;">Confirm Your Email Address</strong></p>
                                                <br>
                                                <p style="color: #6B6B6B;">There’s one quick step you need to complete before creating your account. Let’s make sure this is the right email address for you - please confirm this is the right address to use for your new account by clicking the button below.</p>
                                                <p style="margin-top: 38px;">
                                                    <a href="<?php echo esc_url( $url ) ?>" style="font-weight: 500; text-align: center; transition: all .2s ease-out; border: 1px solid transparent; background: #808080; box-shadow: 0px 2px 10px rgba(0, 0, 0, 0.08); border-radius: 2px; color: #fff;padding: 10px 22px;text-decoration: none; text-transform: uppercase;">
                                                        CONFIRM MY EMAIL
                                                    </a>
                                                </p>
                                                <br><br>
                                                <p style="color: #6B6B6B; margin-bottom: 22px; font-size: 14px;">Thank You, <br>Starlyn</p>
                                                <p style="color: #6B6B6B;">Try to use this link below, if the button is not working:</p>
                                                <p>
                                                    <?php echo esc_url( $url ) ?>
                                                </p>
                                            </td>
                                        </tr>
                                        <!-- body -->
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding: 24px 32px; background: #FFC700; box-shadow: 0px 0px 6px 6px rgba(105, 101, 101, 0.08), 0px 0px 30px 20px rgba(136, 135, 135, 0.02); border-radius: 4px;" >
                                    <table style="width: 100%">
                                        <tr>
                                            <td style="vertical-align: top;">
                                                <p style="font-size: 14px; color: #ffffff; font-weight: 700; display: inline-block; margin-top: 3px;">
                                                    © 2020 PT Starlyn Indonesia</p>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td height="24px" style="font-size:24px;line-height:24px;">&#xA0;</td>
    </tr>
</table>
<!-- prevent Gmail on iOS font size manipulation -->
<div style="display:none; white-space:nowrap; font:15px courier; line-height:0;"> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </div>
</body>
</html>