<?php
/**
 * Order details table shown in emails.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/email-order-details.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates/Emails
 * @version 3.7.0
 */

defined( 'ABSPATH' ) || exit;

$text_align = is_rtl() ? 'right' : 'left';
?>

<?php if( ! $sent_to_admin && in_array( $order->get_payment_method(), ['bacs', 'cod', 'cheque'] ) && $order->has_status( 'on-hold' ) ) : ?>
    <tr>
        <td style="padding: 0 38px 32px 32px;">
            <?php do_action( 'woocommerce_email_before_order_table', $order, $sent_to_admin, $plain_text, $email ); ?>
        </td>
    </tr>
<?php endif; ?>

<h2>
	<?php
	if ( $sent_to_admin ) {
		$before = '<a class="link" href="' . esc_url( $order->get_edit_order_url() ) . '">';
		$after  = '</a>';
	} else {
		$before = '';
		$after  = '';
	}
	/* translators: %s: Order ID. */
	// echo wp_kses_post( $before . sprintf( __( '[Order #%s]', 'woocommerce' ) . $after . ' (<time datetime="%s">%s</time>)', $order->get_order_number(), $order->get_date_created()->format( 'c' ), wc_format_datetime( $order->get_date_created() ) ) );
	?>
</h2>

<tr>
    <td style="padding: 0 38px 0 32px;">
        <div style="border: 1px solid #808080; padding: 18px">
            <!-- Summary Items -->
            <div style="width: 100%; overflow-x: auto; ">
                <div style="">
                    <p style="text-transform: capitalize; color: #333333; font-weight: 600;">Order Summary</p>
                    <span style="border-bottom: 1px solid #636363; display: block; margin-top: 14px"></span>

                    <!-- Items -->
                    <table style="width: 100%; margin-top: 15px;">
                        <?php
                        echo wc_get_email_order_items( // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
                            $order,
                            array(
                                'show_sku'      => $sent_to_admin,
                                'show_image'    => true,
                                'image_size'    => array( 120, 108 ),
                                'plain_text'    => $plain_text,
                                'sent_to_admin' => $sent_to_admin,
                            )
                        );
                        ?>
                        <!-- dark line -->
                        <td colspan="3">
                            <p style="margin: 14px 0 14px 0; border-bottom: 1px solid #636363"></p>
                        </td>
                    </table>
                </div>
            </div>

            <!-- Summary Totals -->
            <table style="width: 100%;">
                <?php
                $item_totals = $order->get_order_item_totals();
                $total_amount= $item_totals['order_total'];
                unset( $item_totals['order_total'] );
                $count = count( $item_totals );

                    if ( $item_totals ) {
                        $i = 0;
                        foreach ( $item_totals as $key => $total ) {
                            $i++;
                ?>
                            <tr>
                                <td style="text-align: right;">
                                    <p style="<?php echo $i > 1 ? 'margin-top: 6px;' : '' ?> <?php echo $i == $count-1 ? '' : 'margin-bottom: 6px;' ?>"><?php echo wp_kses_post( $total['label'] ); ?></p>
                                </td>
                                <td style="text-align: right">
                                    <p style="<?php echo $i > 1 ? 'margin-top: 6px;' : '' ?> <?php echo $i == $count-1 ? '' : 'margin-bottom: 6px;' ?> <?php echo $key == 'discount' ? 'color:#F92424;' : '' ?>"><?php echo wp_kses_post( $total['value'] ); ?></p>
                                </td>
                            </tr>
                <?php
                        }
                    }
                ?>
                <tr>
                    <td><p></p></td>
                    <td style="padding-top: 15px; padding-bottom: 15px;"> <p  style="border-top: 2px solid #FFC700;"></p></td>
                </tr>
                <tr>
                    <td style="text-align: right"><p style="margin-bottom: 6px;">TOTAL</p></td>
                    <td>
                        <p style="text-align: right;"><?php echo $total_amount['value'] ?></p>
                    </td>
                </tr>
            </table>
        </div>
    </td>
</tr>

<div style="margin-bottom: 40px; display: none">
	<table class="td" cellspacing="0" cellpadding="6" style="width: 100%; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;" border="1">
		<thead>
			<tr>
				<th class="td" scope="col" style="text-align:<?php echo esc_attr( $text_align ); ?>;"><?php esc_html_e( 'Product', 'woocommerce' ); ?></th>
				<th class="td" scope="col" style="text-align:<?php echo esc_attr( $text_align ); ?>;"><?php esc_html_e( 'Quantity', 'woocommerce' ); ?></th>
				<th class="td" scope="col" style="text-align:<?php echo esc_attr( $text_align ); ?>;"><?php esc_html_e( 'Price', 'woocommerce' ); ?></th>
			</tr>
		</thead>
		<tbody>
			<?php
			echo wc_get_email_order_items( // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
				$order,
				array(
					'show_sku'      => $sent_to_admin,
					'show_image'    => false,
					'image_size'    => array( 32, 32 ),
					'plain_text'    => $plain_text,
					'sent_to_admin' => $sent_to_admin,
				)
			);
			?>
		</tbody>
		<tfoot>
			<?php
			$item_totals = $order->get_order_item_totals();

			if ( $item_totals ) {
				$i = 0;
				foreach ( $item_totals as $total ) {
					$i++;
					?>
					<tr>
						<th class="td" scope="row" colspan="2" style="text-align:<?php echo esc_attr( $text_align ); ?>; <?php echo ( 1 === $i ) ? 'border-top-width: 4px;' : ''; ?>"><?php echo wp_kses_post( $total['label'] ); ?></th>
						<td class="td" style="text-align:<?php echo esc_attr( $text_align ); ?>; <?php echo ( 1 === $i ) ? 'border-top-width: 4px;' : ''; ?>"><?php echo wp_kses_post( $total['value'] ); ?></td>
					</tr>
					<?php
				}
			}
			if ( $order->get_customer_note() ) {
				?>
				<tr>
					<th class="td" scope="row" colspan="2" style="text-align:<?php echo esc_attr( $text_align ); ?>;"><?php esc_html_e( 'Note:', 'woocommerce' ); ?></th>
					<td class="td" style="text-align:<?php echo esc_attr( $text_align ); ?>;"><?php echo wp_kses_post( nl2br( wptexturize( $order->get_customer_note() ) ) ); ?></td>
				</tr>
				<?php
			}
			?>
		</tfoot>
	</table>
</div>

<?php do_action( 'woocommerce_email_after_order_table', $order, $sent_to_admin, $plain_text, $email ); ?>
