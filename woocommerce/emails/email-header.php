<?php
/**
 * Email Header
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/email-header.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates/Emails
 * @version 4.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Starlyn Email - Confirm Email</title>
    <!-- <style> -->
    <style type="text/css">
        * {
            margin: 0;
            padding: 0;
        }
        table {
            border-spacing: 0;
            border-collapse: collapse;
        }
        table thead th {
            border-bottom: 1px solid #DBDBDB;
        }
    </style>
</head>
<body style="color: #141414;font-family: sans-serif;font-weight: normal;padding: 0;margin: 0;text-align: left;font-size: 16px;line-height: 1.5;width: 100%;border-spacing: 0;border-collapse: collapse;background-color: #F5F5F5;">
<table style="width: 100%;">
    <tr>
        <td height="24px" style="font-size:24px;line-height:24px;">&#xA0;</td>
    </tr>
    <tr>
        <td valign="top">
            <table style="width: 100%; max-width: 800px; margin: 0 auto;">
                <tr>
                    <td>
                        <table align="center" style="width: 100%; background-color: #FFFFFF;">
                            <tr>
                                <td>
                                    <table style="width: 100%;">
                                        <tr>
                                            <td style="text-align: center; padding: 32px; border-bottom: 2px solid #ffc700;box-shadow: 0px 4px 10px 8px rgba(44, 44, 44, 0.08), 0px 4px 40px 20px rgba(136, 135, 135, 0.02);background-color: #f7f7f7; ">
                                                <?php if ( $img = get_option( 'woocommerce_email_header_image' ) ) : ?>
                                                    <img src="<?php echo esc_url( $img ) ?>" alt="<?php echo get_bloginfo( 'name', 'display' ) ?>" style="max-width: 152px; height: auto;">
                                                <?php endif; ?>
                                            </td>
                                        </tr>