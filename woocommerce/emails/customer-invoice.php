<?php
/**
 * Customer invoice email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-invoice.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates/Emails
 * @version 3.7.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Executes the e-mail header.
 *
 * @hooked WC_Emails::email_header() Output the email header
 */
do_action( 'woocommerce_email_header', $email_heading, $email ); ?>

<tr>
    <td style="padding: 38px 32px;">
        <?php /* translators: %s: Customer first name */ ?>
        <p><strong style="color: #5A5A5A;"><?php printf( esc_html__( 'Hello %s,', 'woocommerce' ), esc_html( $order->get_billing_first_name() ) ); ?></strong></p>
        <br>
        <p style="color: #6B6B6B;">
            <?php if( $order->has_status( 'pending' ) ) : ?>
                <?php
                printf(
                    wp_kses(
                    /* translators: %1$s Site title, %2$s Order pay link */
                        __('An order has been created for you on %1$s. Your invoice is below: %2$s', 'woocommerce'),
                        array(
                            'a' => array(
                                'href' => array(),
                            ),
                        )
                    ),
                    esc_html(get_bloginfo('name', 'display')),
                    //'<a href="' . esc_url($order->get_checkout_payment_url()) . '">' . esc_html__('Pay for this order', 'woocommerce') . '</a>'
                    '<a href="' . add_query_arg( ['order_no' => $order->get_id()], esc_url( get_permalink( get_page_by_path( 'payment-confirmation' ) ) ) ) . '">' . esc_html__('Payment Confirmation', 'woocommerce') . '</a>'
                ); // phpcs:ignore WordPress.XSS.EscapeOutput.OutputNotEscaped; ?>
            <?php else : ?>
                <?php printf( esc_html__( 'Here are the details of your order placed on %s:', 'woocommerce' ), esc_html( wc_format_datetime( $order->get_date_created() ) ) );; // phpcs:ignore WordPress.XSS.EscapeOutput.OutputNotEscaped ?>
            <?php endif; ?>
        </p>
    </td>
</tr>

<?php
/**
 * Hook for the woocommerce_email_order_details.
 *
 * @hooked WC_Emails::order_details() Shows the order details table.
 * @hooked WC_Structured_Data::generate_order_data() Generates structured data.
 * @hooked WC_Structured_Data::output_structured_data() Outputs structured data.
 * @since 2.5.0
 */
do_action( 'woocommerce_email_order_details', $order, $sent_to_admin, $plain_text, $email );

/**
 * Hook for the woocommerce_email_order_meta.
 *
 * @hooked WC_Emails::order_meta() Shows order meta data.
 */
do_action( 'woocommerce_email_order_meta', $order, $sent_to_admin, $plain_text, $email );

/**
 * Hook for woocommerce_email_customer_details.
 *
 * @hooked WC_Emails::customer_details() Shows customer details
 * @hooked WC_Emails::email_address() Shows email address
 */
do_action( 'woocommerce_email_customer_details', $order, $sent_to_admin, $plain_text, $email );

/**
 * Show user-defined additional content - this is set in each email's settings.
 */
if ( $additional_content ) {
    ?>
    <tr>
        <td style="padding: 32px">
            <p style="color: #6B6B6B;"><?php echo wp_kses_post( wpautop( wptexturize( $additional_content ) ) ); ?></p>
        </td>
    </tr>
    <?php
}

/**
 * Executes the email footer.
 *
 * @hooked WC_Emails::email_footer() Output the email footer
 */
do_action( 'woocommerce_email_footer', $email );
