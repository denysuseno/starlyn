<?php
/**
 * Email Order Items
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/email-order-items.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates/Emails
 * @version 3.7.0
 */

defined( 'ABSPATH' ) || exit;

$text_align  = is_rtl() ? 'right' : 'left';
$margin_side = is_rtl() ? 'left' : 'right';

$count  = count( $items );
$i      = 1;

foreach ( $items as $item_id => $item ) :
	$product       = $item->get_product();
	$sku           = '';
	$purchase_note = '';
	$image         = '';

	if ( ! apply_filters( 'woocommerce_order_item_visible', true, $item ) ) {
		continue;
	}

	if ( is_object( $product ) ) {
		$sku           = $product->get_sku();
		$purchase_note = $product->get_purchase_note();
		$image_attr    = array('styles' => 'height: auto; width: 100%;');
		$image         = $product->get_image( $image_size, $image_attr );
	}

	?>

    <!-- custom -->
    <tr class="<?php echo esc_attr( apply_filters( 'woocommerce_order_item_class', 'order_item', $item, $order ) ); ?>">
        <?php if( $show_image ) : ?>
            <td>
                <p style="max-width: 120px; margin-right: 12px;">
                    <?php echo wp_kses_post( apply_filters( 'woocommerce_order_item_thumbnail', $image, $item ) ); ?>
                </p>
            </td>
        <?php endif; ?>

        <?php
            $product_display_name   = $item->get_name();
            $product_sku            = $show_sku && $sku ? wp_kses_post( ' (#' . $sku . ')' ) : '';
            $qty                    = $item->get_quantity();
            $refunded_qty           = $order->get_qty_refunded_for_item( $item_id );
            if ( $refunded_qty ) {
                $qty_display = '<del>' . esc_html( $qty ) . '</del> <ins>' . esc_html( $qty - ( $refunded_qty * -1 ) ) . '</ins>';
            } else {
                $qty_display = esc_html( $qty );
            }
            $product_display_name   .= $product_sku;
            $product_display_name   .= ' x ' . $qty_display;
        ?>
        <td style="text-align: justify; margin-top: 15px;<?php echo $i != $count-1 ? '' : 'padding-bottom: 15px' ?>; <?php echo $i > 1 ? 'padding-top: 15px;' : ''; ?>"><p><?php echo wp_kses_post( apply_filters( 'woocommerce_order_item_name', $product_display_name, $item, false ) ) ?></p>
            <p style="display: flex;">
                <span style="margin-right: 12px; color: #666666; font-weight: normal;">Product Info:</span>
                <?php
                $weight      = $product->get_weight() ? $product->get_weight() * 1000 : '';
                $weight_unit = 'gr'; //get_option('woocommerce_weight_unit');
                ?>
                <span style="margin-right: 12px; color: #BDBDBD; font-weight: normal;">Berat<br><b style="color: #2B2B2B; font-weight: normal;"><?php echo sprintf( '%d%s', $weight, $weight_unit ) ?></b></span>
                <span style="border-right: 1px solid #FFC700;"></span>
                <?php
                $dimensions  = get_product_dimensions( $product );
                ?>
                <span style="margin-left: 12px; color: #BDBDBD; font-weight: normal;">Dimensi<br><b style="color: #2B2B2B; font-weight: normal;"><?php echo sprintf( '%d x %d x %d cm', $dimensions['length'], $dimensions['width'], $dimensions['height'] ) ?></b></span>
            </p>
        </td>
        <td style="text-align: right; color: #808080;vertical-align: center;"><?php echo wp_kses_post( $order->get_formatted_line_subtotal( $item ) ); ?></td>
    </tr>

    <?php
    $i++;
    ?>

    <?php if( $i <= $count ) : ?>
        <!-- light line -->
        <tr>
            <td colspan="3" style="border-bottom: 2px solid #FFC700; padding: 8px 0;"></td>
        </tr>
    <?php endif; ?>

    <!-- original -->

	<tr class="<?php echo esc_attr( apply_filters( 'woocommerce_order_item_class', 'order_item', $item, $order ) ); ?>" style="display: none">
		<td class="td" style="text-align:<?php echo esc_attr( $text_align ); ?>; vertical-align: middle; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif; word-wrap:break-word;">
		<?php

		// Show title/image etc.
		if ( $show_image ) {
			echo wp_kses_post( apply_filters( 'woocommerce_order_item_thumbnail', $image, $item ) );
		}

		// Product name.
		echo wp_kses_post( apply_filters( 'woocommerce_order_item_name', $item->get_name(), $item, false ) );

		// SKU.
		if ( $show_sku && $sku ) {
			echo wp_kses_post( ' (#' . $sku . ')' );
		}

		// allow other plugins to add additional product information here.
		do_action( 'woocommerce_order_item_meta_start', $item_id, $item, $order, $plain_text );

		wc_display_item_meta(
			$item,
			array(
				'label_before' => '<strong class="wc-item-meta-label" style="float: ' . esc_attr( $text_align ) . '; margin-' . esc_attr( $margin_side ) . ': .25em; clear: both">',
			)
		);

		// allow other plugins to add additional product information here.
		do_action( 'woocommerce_order_item_meta_end', $item_id, $item, $order, $plain_text );

		?>
		</td>
		<td class="td" style="text-align:<?php echo esc_attr( $text_align ); ?>; vertical-align:middle; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;">
			<?php
			$qty          = $item->get_quantity();
			$refunded_qty = $order->get_qty_refunded_for_item( $item_id );

			if ( $refunded_qty ) {
				$qty_display = '<del>' . esc_html( $qty ) . '</del> <ins>' . esc_html( $qty - ( $refunded_qty * -1 ) ) . '</ins>';
			} else {
				$qty_display = esc_html( $qty );
			}
			echo wp_kses_post( apply_filters( 'woocommerce_email_order_item_quantity', $qty_display, $item ) );
			?>
		</td>
		<td class="td" style="text-align:<?php echo esc_attr( $text_align ); ?>; vertical-align:middle; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;">
			<?php echo wp_kses_post( $order->get_formatted_line_subtotal( $item ) ); ?>
		</td>
	</tr>
	<?php

	if ( $show_purchase_note && $purchase_note ) {
		?>
		<tr style="display: none">
			<td colspan="3" style="text-align:<?php echo esc_attr( $text_align ); ?>; vertical-align:middle; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;">
				<?php
				echo wp_kses_post( wpautop( do_shortcode( $purchase_note ) ) );
				?>
			</td>
		</tr>
		<?php
	}
	?>

<?php endforeach; ?>
