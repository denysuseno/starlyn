<?php
/**
 * Customer Reset Password email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-reset-password.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates/Emails
 * @version 4.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$url = esc_url( add_query_arg( array( 'key' => $reset_key, 'id' => $user_id ), wc_get_endpoint_url( 'lost-password', '', wc_get_page_permalink( 'myaccount' ) ) ) );
?>

<?php do_action( 'woocommerce_email_header', $email_heading, $email ); ?>

<tr>
    <td style="padding: 38px 32px;">
        <p><strong style="color: #5A5A5A;"><?php printf( esc_html__( 'Hello %s,', 'woocommerce' ), esc_html( $user_login ) ); ?></strong></p>
        <br>
        <p style="color: #6B6B6B;">Please click the button below to reset your password</p>
        <p style="margin-top: 40px;">
            <a href="<?php echo $url ?>" style="font-weight: 500; text-align: center; transition: all .2s ease-out; border: 1px solid transparent; background: #808080; box-shadow: 0px 2px 10px rgba(0, 0, 0, 0.08); border-radius: 2px; color: #fff;padding: 10px 22px;text-decoration: none; text-transform: uppercase;">
                Reset Password
            </a>
        </p>
        <p style="margin-top: 40px; color: #6B6B6B;">Try to use this link below, if the button is not working:</p>
        <p>
            <?php echo $url; ?>
        </p>
    </td>
</tr>

<?php
/**
 * Show user-defined additional content - this is set in each email's settings.
 */
if ( $additional_content ) {
    ?>
    <tr>
        <td style="padding: 0 32px 32px 32px">
            <p style="color: #6B6B6B;"><?php echo wp_kses_post( wpautop( wptexturize( $additional_content ) ) ); ?></p>
        </td>
    </tr>
    <?php
}

do_action( 'woocommerce_email_footer', $email );
