<?php
/**
 * Email Footer
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/email-footer.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates/Emails
 * @version 3.7.0
 */

defined( 'ABSPATH' ) || exit;
?>
</table>
<br>
<p style="padding: 0 32px 32px; color: #6B6B6B; font-size: 14px;">Thank You, <br>Starlyn</p>
</td>
</tr>
<tr>
    <td style="padding: 24px 32px; background: #FFC700; box-shadow: 0px 0px 6px 6px rgba(105, 101, 101, 0.08), 0px 0px 30px 20px rgba(136, 135, 135, 0.02); border-radius: 4px;" >
        <table style="width: 100%">
            <tr>
                <td style="vertical-align: top;">
                    <p style="font-size: 14px; color: #ffffff; font-weight: 700; display: inline-block; margin-top: 3px;">
                        © 2020 PT Starlyn Indonesia</p>
                </td>
            </tr>
        </table>
    </td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
<tr>
    <td height="24px" style="font-size:24px;line-height:24px;">&#xA0;</td>
</tr>
</table>
<div style="display:none; white-space:nowrap; font:15px courier; line-height:0;"> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </div>
</body>
</html>