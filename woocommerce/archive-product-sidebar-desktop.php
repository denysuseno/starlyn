<?php
    $li_class = 'item';
?>
<div class="aside-container">
    <ul class="items">
        <li class="item <?php echo isset( $args['curr_cat']->name ) && $args['curr_cat']->name === 'product' ? 'item-current' : '' ?>"><a href="<?php echo wc_get_page_permalink( 'shop' ) ?>" class="link">All Products</a></li>
        <li class="item"><a href="<?php echo get_permalink( get_page_by_path( 'deals' ) ) ?>" class="link">Discount</a></li>
        <li class="item <?php echo isset( $_GET['orderby'] ) && $_GET['orderby'] == 'popularity' && isset( $_GET['f_by'] ) && $_GET['f_by'] == 'popular' ? 'item-current' : '' ?>"><a href="<?php echo add_query_arg(['orderby' => 'popularity', 'f_by' => 'popular'], wc_get_page_permalink( 'shop' ) ) ?>" class="link">Popular</a></li>

        <?php
        if( $args['categories'] ) :
            foreach ( $args['categories'] as $category ) :

                $class_active = isset( $args['curr_cat']->term_id ) && $args['curr_cat']->term_id === $category->term_id ? ' item-current' : '';
                echo sprintf( '<li class="%s"><a href="%s" class="link">%s</a></li>', $li_class . $class_active, get_category_link( $category->term_id ), $category->name );

            endforeach;
        endif;
        ?>
    </ul>
</div>