<?php
/**
 * Thankyou page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/thankyou.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.7.0
 */

defined( 'ABSPATH' ) || exit;
?>

<div class="checkout-complete">

	<?php
	if ( $order ) :
		do_action( 'woocommerce_before_thankyou', $order->get_id() );
		?>

		<?php if ( $order->has_status( 'failed' ) ) : ?>

            <div class="content">
                <div class="wrapper">

                <h2 class="title">Opps, sorry</h2>

                <p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed text"><?php esc_html_e( 'Unfortunately your order cannot be processed as the originating bank/merchant has declined your transaction. Please attempt your purchase again.', 'woocommerce' ); ?></p>

                <p class="woocommerce-notice woocommerce-notice--error woocommerce-thankyou-order-failed-actions text">
                    <!--<a href="<?php /*echo esc_url( $order->get_checkout_payment_url() ); */?>" class="button pay"><?php /*esc_html_e( 'Pay', 'woocommerce' ); */?></a>-->
                    <?php if ( is_user_logged_in() ) : ?>
                        <a href="<?php echo esc_url( wc_get_page_permalink( 'myaccount' ) ); ?>" class="button pay"><?php esc_html_e( 'My account', 'woocommerce' ); ?></a>
                    <?php endif; ?>
                </p>

                </div>
            </div>

		<?php else : ?>

        <div class="content">
            <div class="wrapper">

                <h2 class="title">Thank You</h2>

                <!--<p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received text"><?php /*echo apply_filters( 'woocommerce_thankyou_order_received_text', esc_html__( 'Thank you. Your order has been received.', 'woocommerce' ), $order ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped */?></p>-->
                <p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received text">You have succesfully made an order at starlyn.com with order number <strong><?= $order->get_order_number() ?></strong></p>

                <?php if( $order->get_payment_method() == 'bacs' && in_array( $order->get_status(), ['on-hold', 'pending'] ) ) : ?>
                    <?php
                    $bacsGateway = WC()->payment_gateways()->payment_gateways()['bacs'];
                    $accountDetails = get_option(
                        'woocommerce_bacs_accounts',
                        array(
                            array(
                                'account_name'   => $bacsGateway->get_option( 'account_name' ),
                                'account_number' => $bacsGateway->get_option( 'account_number' ),
                                'sort_code'      => $bacsGateway->get_option( 'sort_code' ),
                                'bank_name'      => $bacsGateway->get_option( 'bank_name' ),
                                'iban'           => $bacsGateway->get_option( 'iban' ),
                                'bic'            => $bacsGateway->get_option( 'bic' ),
                            ),
                        ));
                    ?>
                    <div class="payment">
                        <div class="bank-transfer">
                            <?php if( $accountDetails ) : ?>
                                <?php foreach ( $accountDetails as $accountDetail ) : ?>
                                    <div class="account">
                                        <p class="bank-name"><?= $accountDetail['bank_name'] ?></p>
                                        <p class="account-name"><?= $accountDetail['account_name'] ?></p>
                                        <p class="account-number"><?= $accountDetail['account_number'] ?></p>
                                    </div>
                                <?php endforeach; ?>
                            <?php endif; ?>

                            <div class="action">
                                <a href="<?php echo add_query_arg( 'order_no', $order->get_order_number(), get_permalink( get_page_by_path( 'payment-confirmation' ) ) ) ?>" class="button button-gray">Payment Confirmation</a>
                            </div>
                            <div class="notes">
                                <p>Dalam 24 jam kami tidak menerima konfirmasi pembayaran maka akan kami anggap order pembelian anda batal</p>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>

            </div>
        </div>

        <div class="detail">
            <div class="wrapper">
                <div class="inner">
                    <div class="order-summary">
                        <h3 class="heading">Order Summary</h3>
                        <div class="summary-detail">
                            <?php
                            $totalRows = $order->get_order_item_totals();
                            $orderTotal = $totalRows['order_total'];
                            unset( $totalRows['order_total'] );

                            foreach ( $totalRows as $key => $total ) {
                                ?>
                                <p class="row">
                                    <span class="text"><?php echo esc_html( $total['label'] ); ?></span>
                                    <span class="value <?= $key == 'discount' ? 'value-reduced' : ''; ?>"><?php echo ( 'payment_method' === $key ) ? esc_html( $total['value'] ) : wp_kses_post( $total['value'] ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></span>
                                </p>
                                <?php
                            }
                            ?>
                        </div>
                        <div class="summary-total">
                            <p class="row">
                                <span class="text">Total</span>
                                <span class="value"><?= $orderTotal['value'] ?></span>
                            </p>
                        </div>
                    </div>
                    <div class="order-shipping">
                        <div class="shipping-address">
                            <h3 class="heading">Shipping Address</h3>
                            <?php $show_shipping = ! wc_ship_to_billing_address_only() && $order->needs_shipping_address(); ?>
                            <div class="address">
                                <?php if( $show_shipping ) : ?>
                                    <?php wc_get_template( 'order/order-details-customer-shipping.php', array( 'order' => $order ) ); ?>
                                <?php else : ?>
                                    -
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="shipping-method">
                            <h3 class="heading">Shipping Methods</h3>
                            <?php foreach( $order->get_shipping_methods() as $shipping_method ) : ?>
                                <p><?= $shipping_method->get_name() ?></p>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

		<?php endif; ?>

        <!-- disabled -->
		<?php do_action( 'woocommerce_thankyou_' . $order->get_payment_method(), $order->get_id() ); ?>
        <!-- disabled -->
		<?php do_action( 'woocommerce_thankyou', $order->get_id() ); ?>

	<?php else : ?>

		<p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', esc_html__( 'Thank you. Your order has been received.', 'woocommerce' ), null ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></p>

	<?php endif; ?>

</div>
