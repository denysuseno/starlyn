<?php global $woocommerce; ?>

<div class="summary-detail">
    <!-- Items Total -->
    <p class="row">
        <span class="text">Items Total (<?php echo $woocommerce->cart->cart_contents_count ?>)</span>
        <span class="value"><?php wc_cart_totals_subtotal_html(); ?></span>
    </p>

    <!-- Sales Tax -->
    <?php if ( wc_tax_enabled() && ! WC()->cart->display_prices_including_tax() ) : ?>
        <?php if ( 'itemized' === get_option( 'woocommerce_tax_total_display' ) ) : ?>
            <?php foreach ( WC()->cart->get_tax_totals() as $code => $tax ) : // phpcs:ignore WordPress.WP.GlobalVariablesOverride.Prohibited ?>
                <p class="row tax-rate-<?php echo esc_attr( sanitize_title( $code ) ); ?>">
                    <span class="text">Sales Tax</span>
                    <span class="value"><?php echo wp_kses_post( $tax->formatted_amount ); ?></span>
                </p>
            <?php endforeach; ?>
        <?php else : ?>
            <p class="row">
                <span class="text"><?php echo esc_html( WC()->countries->tax_or_vat() ); ?></span>
                <span class="value"><?php wc_cart_totals_taxes_total_html(); ?></span>
            </p>
        <?php endif; ?>
    <?php endif; ?>

    <!-- Coupons -->
    <?php foreach ( WC()->cart->get_coupons() as $code => $coupon ) : ?>
        <p class="row coupon-<?php echo esc_attr( sanitize_title( $code ) ); ?>">
            <span class="text">Discount Voucher (<?php wc_cart_totals_coupon_label( $coupon ); ?>)</span>
            <span class="value value-reduced"><?php wc_cart_totals_coupon_html( $coupon ); ?></span>
        </p>
    <?php endforeach; ?>

    <!-- Total Shipping Fee -->
    <p class="row">
        <span class="text">Total Shipping Fee</span>
        <span class="value"><?php echo WC()->cart->get_cart_shipping_total() ?></span>
    </p>

    <?php if( WC()->cart->get_fee_total() ) : ?>
        <!-- Total Insurance Fee -->
        <!--<p class="row">
            <span class="text">Shipping Insurance</span>
            <span class="value"><?php /*echo wc_price( WC()->cart->get_fee_total() ) */?></span>
        </p>-->
    <?php endif; ?>

    <?php
        $insurance_fee = 0;
        $packaging_fee = 0;
    ?>

    <?php
        if( sizeof( WC()->cart->get_fees() ) > 0 ) :
            foreach ( WC()->cart->get_fees() as $fee_key => $fee ) :
                if( $fee_key == 'shipping-insurance' ) :
                    $insurance_fee = $fee->total;
                endif;
                if( $fee_key == 'wood-packaging' ) :
                    $packaging_fee = $fee->total;
                endif;
            endforeach;
        endif;
    ?>

    <?php if( $insurance_fee ) : ?>
        <p class="row">
            <span class="text">Shipping Insurance</span>
            <span class="value"><?php echo wc_price( $insurance_fee ) ?></span>
        </p>
    <?php endif ?>

    <?php if( $packaging_fee ) : ?>
        <p class="row">
            <span class="text">Wood Packaging</span>
            <span class="value"><?php echo wc_price( $packaging_fee ) ?></span>
        </p>
    <?php endif ?>
</div>

<?php
    $subtotal = WC()->cart->get_subtotal();
    $discount = WC()->cart->get_discount_total();
    $shipping = WC()->cart->get_shipping_total();
    $insurance= WC()->cart->get_fee_total();
?>
<?php $cart_value = number_format( (float) ( WC()->cart->get_subtotal() + $shipping + $insurance ) - $discount, wc_get_price_decimals(), '.', '' ); ?>

<div class="summary-total">
    <p class="row">
        <span class="text">Total</span>
        <span class="value"><?php echo wc_price( $cart_value ) ?></span>
    </p>
</div>