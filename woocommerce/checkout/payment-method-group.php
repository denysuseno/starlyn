<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}
?>

<?php if ( isset( $payment_method_group['payments'] ) && ! empty( $payment_method_group['payments'] ) ) : ?>

    <li class="item">
        <div class="label js-payment-expand">
            <span class="text"><?php echo $payment_method_group['pay_method_group_title'] ?></span>
            <div class="toggle">
                <svg class="icon icon-up" role="img"><use xlink:href="<?= library_url() ?>/images/svg-symbols.svg#icon-angle-up" /></svg>
                <svg class="icon icon-down" role="img"><use xlink:href="<?= library_url() ?>/images/svg-symbols.svg#icon-angle-down" /></svg>
            </div>
        </div>
        <div class="desc">
            <?php if( $payment_method_group['pay_method_group_description'] ) : ?>
                <p class="text"><?php echo $payment_method_group['pay_method_group_description'] ?></p>
            <?php endif; ?>
            <div class="payment-option">
                <ul class="payment-option-items">
                    <?php
                        foreach ( $payment_method_group['payments'] as $gateway ) {
                            wc_get_template('checkout/payment-method-custom.php', array('gateway' => $gateway, 'group' => $payment_method_group));
                        }
                    ?>
                </ul>
            </div>
        </div>
    </li>

<?php endif; ?>