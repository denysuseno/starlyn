<?php
/**
 * Review order table
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/review-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.8.0
 */

defined( 'ABSPATH' ) || exit;

$has_force_insurance    = 0; // flag apakah terdapat salah 1 item yg "wajib" asuransi.
$has_insurance          = 0; // flag apakah terdapat salah 1 item yg ingin diasuransikan.
$has_force_packaging    = 0; // flag apakah terdapat salah 1 item yg "wajib" packing kayu.
$has_packaging          = 0; // flag apakah terdapat salah 1 item yg ingin packing kayu.

$chosen_shipping_prefix = get_chosen_shipping_code_prefix();
?>
<div class="woocommerce-checkout-review-order-table">

    <div class="cart-products">
        <ul class="items">
            <?php
            do_action( 'woocommerce_review_order_before_cart_contents' );

            foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
                $_product = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );

                if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_checkout_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
                    $product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );

                    $dimensions = get_product_dimensions( $_product );

                    $force_wood_packaging       = get_field( 'force_wood_packaging', $_product->get_id() );
                    $force_shipping_insurance   = get_field( 'force_shipping_insurance', $_product->get_id() );

                    if( $force_shipping_insurance == 'yes' ) {
                        $has_force_insurance++;
                    }

                    if( ( isset( $cart_item['shipping_insurance'] ) && $cart_item['shipping_insurance'] == 1 ) || $force_shipping_insurance == 'yes' ) {
                        $has_insurance++;
                    }

                    if( $force_wood_packaging == 'yes' ) {
                        $has_force_packaging++;
                    }

                    if( ( isset( $cart_item['wood_packaging'] ) && $cart_item['wood_packaging'] == 1 ) || $force_wood_packaging == 'yes' ) {
                        $has_packaging++;
                    }
                    ?>
                    <li class="item <?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">
                        <div class="item-image">
                            <?php $thumbnailId = $_product->get_image_id(); $thumbnail = wp_get_attachment_image_url( $thumbnailId ); ?>
                            <a href="<?= $product_permalink ?>" class="link">
                                <img src="<?= $thumbnail ?>" alt="<?= $_product->get_name() ?>" class="image">
                            </a>
                        </div>
                        <div class="item-main">
                            <div class="detail">
                                <div class="meta">
                                    <div class="meta-info">
                                        <h5 class="title"><a href="<?= $product_permalink ?>" class="link"><?php echo apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $cart_item_key ) ?></a></h5>
                                        <div class="info">
                                            <p class="info-label">Product Info:</p>
                                            <div class="info-items">
                                                <div class="info-item">
                                                    <?php
                                                    $weight      = $_product->get_weight() ? $_product->get_weight() * 1000 : '';
                                                    $weight_unit = 'gr'; //get_option('woocommerce_weight_unit');
                                                    ?>
                                                    <span class="label">Berat</span>
                                                    <span class="value"><?php echo sprintf( '%d%s', $weight, $weight_unit ) ?></span>
                                                </div>
                                                <div class="info-item">
                                                    <?php if ( $_product->has_dimensions() && ! $_product->is_type('variable') ) : ?>
                                                        <span class="label">Dimensi</span>
                                                        <span class="value"><?php echo sprintf( '%dx%dx%d cm', $dimensions['length'], $dimensions['width'], $dimensions['height'] ) ?></span>
                                                    <?php elseif( $_product->has_dimensions() && $_product->is_type('variable') ) : ?>
                                                        <span class="label">Dimensi</span>
                                                        <?php echo esc_html( $dimensions ); ?>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php
                                        $insurance_rate = 0;
                                        if( $chosen_shipping_prefix == 'jne' ) {
                                            $insurance_rate = $cart_item['line_subtotal'] * JNE_INSURANCE_PERCENTAGE;
                                        } elseif( $chosen_shipping_prefix == 'sicepat' ) {
                                            $insurance_rate = $cart_item['line_subtotal'] * SICEPAT_INSURANCE_PERCENTAGE;
                                        }
                                    ?>
                                    <!-- FOR DEBUGGING ONLY -->
                                    <div class="<?php echo is_production() ? 'is-hidden' : 'is-hidden' ?>">
                                        <small>Insurance: <strong><?php echo wc_price( $insurance_rate ) ?></strong></small> <br>
                                    </div>
                                </div>
                            </div>
                            <div class="shipping is-hidden">
                                <div class="shipping-option">
                                    <ul class="shipping-option-items">
                                        <li class="shipping-option-item <?php echo $force_wood_packaging == 'yes' ? 'is-disabled' : '' ?>">
                                            <label class="label">
                                                <?php $wood_packaging_checked = ( isset( $cart_item['wood_packaging'] ) && $cart_item['wood_packaging'] == 1 ) || $force_wood_packaging == 'yes' ? 'checked' : '' ?>
                                                <?php echo sprintf( '<input type="checkbox" name="cart[%s][wood_packaging]" %s value="1" data-cart-id="%s" data-redirect-url="%s" class="cart-wood-packaging">', $cart_item_key, $wood_packaging_checked, $cart_item_key, wc_get_checkout_url() ) ?>
                                                <span class="box">
                                                    <svg class="icon" role="img"><use xlink:href="<?= library_url() ?>/images/svg-symbols.svg#icon-check" /></svg>
                                                </span>
                                                <span class="text">Packing kayu</span>
                                            </label>
                                        </li>
                                        <li class="shipping-option-item <?php echo $force_shipping_insurance == 'yes' ? 'is-disabled' : '' ?>">
                                            <label class="label">
                                                <?php $shipping_insurance_checked = ( isset( $cart_item['shipping_insurance'] ) && $cart_item['shipping_insurance'] == 1 ) || $force_shipping_insurance == 'yes' ? 'checked' : '' ?>
                                                <?php echo sprintf( '<input type="checkbox" name="cart[%s][shipping_insurance]" %s value="1" data-cart-id="%s" data-redirect-url="%s" class="cart-shipping-insurance">', $cart_item_key, $shipping_insurance_checked, $cart_item_key, wc_get_checkout_url() ) ?>
                                                <span class="box">
                                                    <svg class="icon" role="img"><use xlink:href="<?= library_url() ?>/images/svg-symbols.svg#icon-check" /></svg>
                                                </span>
                                                <span class="text">Asuransi</span>
                                            </label>
                                        </li>
                                    </ul>
                                    <?php if( $force_wood_packaging == 'yes' && $force_shipping_insurance == 'yes' ) : ?>
                                        <div class="shipping-option-notes">Pengiriman produk ini harus menggunakan packing kayu & asuransi</div>
                                    <?php elseif( $force_wood_packaging == 'yes' && $force_shipping_insurance == 'no' ) : ?>
                                        <div class="shipping-option-notes">Pengiriman produk ini harus menggunakan packing kayu</div>
                                    <?php elseif( $force_wood_packaging == 'no' && $force_shipping_insurance == 'yes' ) : ?>
                                        <div class="shipping-option-notes">Pengiriman produk ini harus menggunakan asuransi</div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                        <?php /*echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped */?>
                    </li>
                    <?php
                }
            }
            ?>
        </ul>
        <?php
        do_action( 'woocommerce_review_order_after_cart_contents' );
        wp_nonce_field('woocommerce-cart', 'woocommerce-cart-nonce');
        ?>
    </div>

    <?php if( in_array( $chosen_shipping_prefix, ['jne', 'sicepat'] ) ) : ?>
        <?php
            if( $chosen_shipping_prefix == 'jne' ) {
                $percentage = JNE_INSURANCE_PERCENTAGE * 100;
            } elseif( $chosen_shipping_prefix == 'sicepat' ) {
                $percentage = SICEPAT_INSURANCE_PERCENTAGE * 100;
            }
        ?>
        <div style="margin-top: 25px" class="shipping-insurance-flag-wrapper">
            <?php echo sprintf( '<input type="checkbox" name="shipping_insurance" %s id="shipping-insurance" value="1" class="shipping-insurance-flag" %s>', $has_insurance > 0 ? 'checked' : '',  $has_force_insurance > 0 ? 'disabled' : '' ) ?>
            <label for="shipping-insurance">
                <?php echo $has_force_insurance > 0 ? 'Wajib' : '' ?> Asuransi Pengiriman
            </label> <br>
            <small>Biaya ganti rugi senilai total harga barang dengan biaya sebesar <?php echo $percentage ?>% dari total harga barang dan berlaku pembulatan ke atas</small>
        </div>

        <div style="margin-top: 25px" class="shipping-packaging-flag-wrapper">
            <?php echo sprintf( '<input type="checkbox" name="shipping_packaging" %s id="shipping-packaging" value="1" class="shipping-packaging-flag" %s>', $has_packaging > 0 ? 'checked' : '',  $has_force_packaging > 0 ? 'disabled' : '' ) ?>
            <label for="shipping-packaging">
                <?php echo $has_force_packaging > 0 ? 'Wajib' : '' ?> Packing Kayu
            </label> <br>
        </div>
    <?php endif; ?>

    <?php if ( WC()->cart->needs_shipping() && WC()->cart->show_shipping() ) : ?>

        <?php do_action( 'woocommerce_review_order_before_shipping' ); ?>

        <div class="shipping-method">
            <h4 class="heading">Choose Shipping</h4>
            <div class="shipping-list">
                <?php wc_cart_totals_shipping_html(); ?>
            </div>
        </div>

    <?php endif; ?>

    <?php do_action( 'woocommerce_review_order_after_shipping' ); ?>

    <?php if( 1 == 2 ) : ?>

        <tr class="cart-subtotal">
            <th><?php esc_html_e( 'Subtotal', 'woocommerce' ); ?></th>
            <td><?php wc_cart_totals_subtotal_html(); ?></td>
        </tr>

        <?php foreach ( WC()->cart->get_coupons() as $code => $coupon ) : ?>
            <tr class="cart-discount coupon-<?php echo esc_attr( sanitize_title( $code ) ); ?>">
                <th><?php wc_cart_totals_coupon_label( $coupon ); ?></th>
                <td><?php wc_cart_totals_coupon_html( $coupon ); ?></td>
            </tr>
        <?php endforeach; ?>

        <?php foreach ( WC()->cart->get_fees() as $fee ) : ?>
            <tr class="fee">
                <th><?php echo esc_html( $fee->name ); ?></th>
                <td><?php wc_cart_totals_fee_html( $fee ); ?></td>
            </tr>
        <?php endforeach; ?>

        <?php if ( wc_tax_enabled() && ! WC()->cart->display_prices_including_tax() ) : ?>
            <?php if ( 'itemized' === get_option( 'woocommerce_tax_total_display' ) ) : ?>
                <?php foreach ( WC()->cart->get_tax_totals() as $code => $tax ) : // phpcs:ignore WordPress.WP.GlobalVariablesOverride.Prohibited ?>
                    <tr class="tax-rate tax-rate-<?php echo esc_attr( sanitize_title( $code ) ); ?>">
                        <th><?php echo esc_html( $tax->label ); ?></th>
                        <td><?php echo wp_kses_post( $tax->formatted_amount ); ?></td>
                    </tr>
                <?php endforeach; ?>
            <?php else : ?>
                <tr class="tax-total">
                    <th><?php echo esc_html( WC()->countries->tax_or_vat() ); ?></th>
                    <td><?php wc_cart_totals_taxes_total_html(); ?></td>
                </tr>
            <?php endif; ?>
        <?php endif; ?>

        <?php do_action( 'woocommerce_review_order_before_order_total' ); ?>

        <tr class="order-total woocommerce_review_order_before_order_total">
            <th><?php esc_html_e( 'Total', 'woocommerce' ); ?></th>
            <td><?php wc_cart_totals_order_total_html(); ?></td>
        </tr>

    <?php endif; ?>

    <?php do_action( 'woocommerce_review_order_after_order_total' ); ?>

</div>
