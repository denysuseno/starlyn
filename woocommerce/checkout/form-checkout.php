<?php
/**
 * Checkout Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-checkout.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

do_action( 'woocommerce_before_checkout_form', $checkout );

// If checkout registration is disabled and not logged in, the user cannot checkout.
if ( ! $checkout->is_registration_enabled() && $checkout->is_registration_required() && ! is_user_logged_in() ) {
    echo esc_html( apply_filters( 'woocommerce_checkout_must_be_logged_in_message', __( 'You must be logged in to checkout.', 'woocommerce' ) ) );
    return;
}
?>

    <div class="checkout-page">
        <div class="wrapper">

            <form name="checkout" method="post" class="checkout" action="<?php echo esc_url( wc_get_checkout_url() ); ?>" enctype="multipart/form-data">

                <div class="wrapper-inside">

                    <?php if ( $checkout->get_checkout_fields() ) : ?>

                        <?php do_action( 'woocommerce_checkout_before_customer_details' ); ?>

                        <div class="col2-set is-hidden" id="customer_details">
                            <div class="col-1">
                                <?php do_action( 'woocommerce_checkout_billing' ); ?>
                            </div>

                            <div class="col-2">
                                <?php do_action( 'woocommerce_checkout_shipping' ); ?>
                            </div>
                        </div>

                        <?php do_action( 'woocommerce_checkout_after_customer_details' ); ?>

                    <?php endif; ?>

                    <?php do_action( 'woocommerce_checkout_before_order_review_heading' ); ?>

                    <?php do_action( 'woocommerce_checkout_before_order_review' ); ?>

                    <div id="order_review" class="woocommerce-checkout-review-order">
                        <?php do_action( 'woocommerce_checkout_order_review' ); ?>
                    </div>

                    <?php do_action( 'woocommerce_checkout_after_order_review' ); ?>

                    <div class="checkout-page-main">

                        <?php if( wc_notice_count() ) : ?>
                            <div style="margin-bottom: 24px;">
                                <?php do_action( 'starlyn_checkout_output_all_notices' ) ?>
                            </div>
                        <?php endif; ?>

                        <div class="header">
                            <h3 class="title">Checkout</h3>
                        </div>
                        <div class="checkout-shipping-wrapper">
                            <div class="content">
                                <div class="shipping-address">
                                    <div class="shipping-address-header">
                                        <h4 class="title">Shipping Address</h4>
                                    </div>
                                    <div class="shipping-address-preview">
                                        <?php if( is_user_logged_in() && empty( is_wc_endpoint_url('order-received') ) ) : ?>

                                            <?php
                                                $myMainAddress = get_my_main_address_book( get_current_user_id() );
                                                $mainAddress = $myMainAddress && isset( $myMainAddress[0] ) && $myMainAddress[0] ? $myMainAddress[0] : [];
                                            ?>

                                            <?php if( $mainAddress ) : ?>
                                                <div class="address">
                                                    <p class="address-name"><span id="checkout-selected-address-name"><?= $mainAddress->name ?></span> (<span id="checkout-selected-address-salutation"><?= $mainAddress->address_name ?></span>) <span id="checkout-selected-address-is-main"><?= $mainAddress->as_default_billing_address ? '<span class="selected">Utama</span>' : '' ?></span></p>
                                                    <span id="checkout-selected-address-phone"><?= $mainAddress->phone ?></span> <br>
                                                    <span id="checkout-selected-address-address"><?= $mainAddress->address ?></span> <br>
                                                    <span id="checkout-selected-address-district"><?= $mainAddress->district ?></span>,
                                                    <span id="checkout-selected-address-city"><?= $mainAddress->regency ?></span>,
                                                    <span id="checkout-selected-address-postcode"><?= $mainAddress->postcode ?></span>
                                                </div>
                                            <?php else : ?>
                                                Anda belum memiliki alamat pengiriman. Mohon tambah data alamat terlebih dahulu.
                                            <?php endif; ?>

                                            <?php $myAddress = get_all_my_address_book(); ?>
                                            <div class="action">
                                                <?php if( ! $myAddress ) : ?>
                                                    <a href="#popup-add-new" class="button button-line js-popup-inline btn-checkout-choose-another-address">+ Add New Address</a>
                                                <?php else : ?>
                                                    <a href="#popup-address" class="button button-line js-popup-inline btn-checkout-choose-another-address">Choose another address</a>

                                                    <?php if( count( $myAddress ) < MAX_ADDRESS_BOOK ) : ?>
                                                        <a href="#popup-add-new" class="button button-line js-popup-inline">+ Add New Address</a>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </div>

                                        <?php else : ?>

                                            <?php if ( $checkout->get_checkout_fields() ) : ?>

                                                <div class="form-input-field">
                                                    <label class="label" for="billing_email_alias">Email <abbr class="required" title="required">*</abbr></label>
                                                    <input type="email" class="input" name="billing_email_alias" id="billing_email_alias">
                                                    <br>
                                                    <span class="hint-failed is-hidden"></span>
                                                    <span class="hint-login is-hidden">
                                                            <?php $url = esc_url( add_query_arg( ['redirect' => wc_get_checkout_url()], get_permalink( get_option('woocommerce_myaccount_page_id') ) ) ); ?>
                                                            <?php echo sprintf( 'Alamat e-mail yang digunakan sudah terdaftar, silahkan %s terlebih dahulu untuk melanjutkan transaksi.', '<a href="'.$url.'">Login</a>' ) ?>
                                                        </span>
                                                    <span class="hint-register is-hidden">Anda dapat memilih untuk mendaftar sebagai member setelah checkout.</span>
                                                </div>

                                                <br>

                                                <div class="checkout-guest-form form-address-book is-disabled">

                                                    <!-- Label -->
                                                    <div class="form-input-field">
                                                        <label class="label" for="billing_address_2_alias">Label <abbr class="required" title="required">*</abbr></label>
                                                        <input type="text" class="input" id="billing_address_2_alias" name="billing_address_2_alias" value="" required placeholder="Contoh: Rumah, Toko">
                                                    </div>

                                                    <!-- Nama -->
                                                    <div class="form-input-field">
                                                        <label class="label" for="billing_first_name_alias">Nama <abbr class="required" title="required">*</abbr></label>
                                                        <input type="text" class="input" id="billing_first_name_alias" name="billing_first_name_alias" value="" required>
                                                    </div>

                                                    <!-- Nomor telepon -->
                                                    <div class="form-input-field">
                                                        <label class="label" for="billing_phone_alias">Nomor telepon <abbr class="required" title="required">*</abbr></label>
                                                        <input type="text" class="input force-number" id="billing_phone_alias" name="billing_phone_alias" value="" required>
                                                    </div>

                                                    <!-- Alamat -->
                                                    <div class="form-input-field">
                                                        <label class="label" for="billing_address_1_alias">Alamat <abbr class="required" title="required">*</abbr></label>
                                                        <textarea class="input" id="billing_address_1_alias" name="billing_address_1_alias" value="" required></textarea>
                                                    </div>

                                                    <div class="form-input-fields">
                                                        <!-- Provinsi -->
                                                        <div class="form-input-field">
                                                            <?php
                                                            $province = get_province();
                                                            $regencyNonce = wp_create_nonce( 'get_regency_nonce' );
                                                            ?>
                                                            <label class="label" for="billing_state_alias">Provinsi <abbr class="required" title="required">*</abbr></label>
                                                            <select class="input province-ajax update_totals_on_change" id="billing_state_alias" name="billing_state_alias" data-url="<?= admin_url( 'admin-ajax.php?action=get_regency&nonce='.$regencyNonce ) ?>" required>
                                                                <option value="">- Pilih Provinsi -</option>
                                                                <?php if( $province ) : ?>
                                                                    <?php foreach( $province as $item ) : ?>
                                                                        <option value="<?= $item->province_name ?>"><?= $item->province_name ?></option>
                                                                    <?php endforeach; ?>
                                                                <?php endif; ?>
                                                            </select>
                                                        </div>

                                                        <!-- Kotamadya -->
                                                        <div class="form-input-field">
                                                            <?php $districtNonce = wp_create_nonce( 'get_district_nonce' ); ?>
                                                            <label class="label" for="billing_regency_alias">Kotamadya<span class="required">*</span></label>
                                                            <select class="input regency-ajax update_totals_on_change" id="billing_regency_alias" name="billing_regency_alias" data-val="" data-url="<?= admin_url( 'admin-ajax.php?action=get_district&nonce='.$districtNonce ) ?>" required>
                                                                <option value="">- Pilih Kabupaten / Kota -</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-input-fields">
                                                        <!-- Daerah -->
                                                        <div class="form-input-field">
                                                            <?php $postcodeNonce = wp_create_nonce( 'get_postcode_nonce' ); ?>
                                                            <label class="label" for="billing_district_alias">Kecamatan<span class="required">*</span></label>
                                                            <select class="input district-ajax update_totals_on_change" id="billing_district_alias" name="billing_district_alias" data-val="" data-url="<?= admin_url( 'admin-ajax.php?action=get_postcode&nonce='.$postcodeNonce ) ?>" required>
                                                                <option value="">- Pilih Kecamatan -</option>
                                                            </select>
                                                        </div>

                                                        <!-- Kode pos -->
                                                        <div class="form-input-field">
                                                            <label class="label" for="billing_postcode_alias">Kode Pos<span class="required">*</span></label>
                                                            <select class="input postcode-ajax" id="billing_postcode_alias" name="billing_postcode_alias" data-val="" required>
                                                                <option value="">- Pilih Kode Pos -</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <!-- Catatan -->
                                                    <div class="form-input-field">
                                                        <label class="label" for="order_comment_alias">Catatan</label>
                                                        <textarea class="input" id="order_comment_alias" name="order_comment_alias" value="" rows="1"></textarea>
                                                    </div>

                                                </div>

                                            <?php endif; ?>

                                        <?php endif; ?>

                                    </div>
                                </div>
                                <?php do_action( 'starlyn_checkout_shipping_review' ); ?>
                            </div>
                        </div>

                        <div class="checkout-payment-wrapper is-hidden">
                            <div class="content">
                                <?php /*woocommerce_checkout_payment() */?>
                                <!-- Custom checkout payment -->
                                <?php starlyn_checkout_payment() ?>
                            </div>
                        </div>
                    </div>

                    <div class="checkout-page-summary">
                        <?php checkout_page_summary_html(); ?>
                    </div>

                </div>

            </form>

        </div>
    </div>

<?php do_action( 'woocommerce_after_checkout_form', $checkout ); ?>
