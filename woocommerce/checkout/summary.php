<div class="inner">
    <div class="header">
        <h3 class="title">Order Summary</h3>
    </div>
    <div class="content">
        <div class="summary-shipping-wrapper">
            <div class="summary-shipping-wrapper-content">
                <?php summary_order_detail_shipping() ?>
            </div>
        </div>
        <div class="summary-payment-wrapper is-hidden">
            <div class="order-detail">
                <div class="summary-payment-wrapper-content">
                    <?php summary_order_detail_payment() ?>
                </div>
            </div>
            <div class="shipping-address">
                <h4 class="heading">Shipping Address</h4>
                <div class="address">
                    <p class="address-name"><span class="checkout-selected-address-name"></span> <span class="checkout-selected-address-salutation"></span> <span class="checkout-selected-address-is-main"></span></p>
                    <span class="checkout-selected-address-phone"></span> <br>
                    <span class="checkout-selected-address-address"></span> <br>
                    <span class="checkout-selected-address-district"></span>,
                    <span class="checkout-selected-address-city"></span>,
                    <span class="checkout-selected-address-postcode"></span>
                </div>
            </div>
            <div class="shipping-method">
                <h4 class="heading">Shipping Methods</h4>
                <div class="summary-payment-shipping-label-wrapper">
                    <?php checkout_page_summary_shipping_label() ?>
                </div>
                <!--<p>JNE regular 2-4 Days</p>-->
            </div>
        </div>
    </div>
    <div class="action">
        <div class="btn-payment-fragments-shipping">
            <div class="btn-payment-fragments-shipping-content is-disabled">
                <?php starlyn_button_payment_shipping() ?>
            </div>
        </div>
        <div class="btn-payment-fragments-payment is-hidden">
            <div class="btn-payment-fragments-payment-content">
                <?php starlyn_button_payment_payment() ?>
            </div>
        </div>
    </div>
</div>