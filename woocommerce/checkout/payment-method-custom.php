<?php
/**
 * Output a single payment method
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/payment-method.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce/Templates
 * @version     3.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>
<?php if( in_array( $group['pay_method_group_key'], ['virtual_account', 'e_wallet', 'payloan'] ) ) : ?>
    <?php if ( $gateway->has_fields() || $gateway->get_description() ) : ?>
        <?php $gateway->payment_fields(); ?>
    <?php endif; ?>
<?php else : ?>
    <li class="payment-option-item">
        <label class="item-label">
            <input id="payment_method_<?php echo esc_attr( $gateway->id ); ?>" type="radio" class="input-radio checkout-select-payment-method" name="payment_method" value="<?php echo esc_attr( $gateway->id ); ?>" <?php checked( $gateway->chosen, true ); ?> data-order_button_text="<?php echo esc_attr( $gateway->order_button_text ); ?>" />
            <span class="radio"></span>
            <span class="text">
                <!--<img src="<?/*= library_url() */?>/images/logo-kredivo-payment.png" alt="" class="logo">-->
                <div style="margin-right: 16px;">
                    <?php echo $gateway->get_icon(); ?>
                </div>
                <?php /*echo $gateway->get_title(); */?>
            </span>
        </label>
        <?php if ( $gateway->has_fields() || $gateway->get_description() ) : ?>
            <?php $gateway->payment_fields(); ?>
        <?php endif; ?>
    </li>
<?php endif; ?>