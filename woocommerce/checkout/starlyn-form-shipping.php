<div class="shipping-address">
    <div class="shipping-address-header">
        <h4 class="title">Shipping Address</h4>
    </div>
    <?php if( $hasAddress ) : ?>
        <div class="shipping-address-preview">
            <?php do_action( 'woocommerce_checkout_shipping' ); ?>
            <div class="action">
                <a href="#popup-address" class="button button-line js-popup-inline btn-checkout-choose-another-address">Choose another address</a>
            </div>
        </div>
    <?php else : ?>
        <p>Anda belum memiliki data alamat. Silakan tambah alamat terlebih dahulu.</p>
        <div class="action">
            <a href="#popup-add-new" class="button button-line js-popup-inline btn-checkout-choose-another-address">Add new address</a>
        </div>
    <?php endif; ?>
</div>
<div class="cart-products">
    <ul class="items">
        <?php foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) : ?>
            <?php $_product = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key ); ?>
            <?php
                $product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '', $cart_item, $cart_item_key );
                $force_wood_packaging       = get_field( 'force_wood_packaging', $_product->get_id() );
                $force_shipping_insurance   = get_field( 'force_shipping_insurance', $_product->get_id() );
            ?>
            <?php if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_checkout_cart_item_visible', true, $cart_item, $cart_item_key ) ) : ?>

                <li class="item">
                    <div class="item-image">
                        <?php $thumbnailId = $_product->get_image_id(); $thumbnail = wp_get_attachment_image_url( $thumbnailId ); ?>
                        <a href="<?= $product_permalink ?>" class="link">
                            <img src="<?= $thumbnail ?>" alt="<?= $_product->get_name() ?>" class="image">
                        </a>
                    </div>
                    <div class="item-main">
                        <div class="detail">
                            <div class="meta">
                                <div class="meta-info">
                                    <h5 class="title"><a href="<?= $product_permalink ?>" class="link"><?= $_product->get_name() ?></a></h5>
                                    <div class="info">
                                        <p class="info-label">Product Info:</p>
                                        <div class="info-items">
                                            <div class="info-item">
                                                <?php
                                                $weight      = $_product->get_weight() ? $_product->get_weight() * 1000 : '';
                                                $weight_unit = 'gr'; //get_option('woocommerce_weight_unit');
                                                ?>
                                                <span class="label">Berat</span>
                                                <span class="value"><?php echo sprintf( '%d%s', $weight, $weight_unit ) ?></span>
                                            </div>
                                            <div class="info-item">
                                                <?php if ( $_product->has_dimensions() && ! $_product->is_type('variable') ) : ?>
                                                    <?php
                                                    $length = ! empty( $_product->get_length() ) ? $_product->get_length() : '';
                                                    $width  = ! empty( $_product->get_width() ) ? $_product->get_width() : '';
                                                    $height = ! empty( $_product->get_height() ) ? $_product->get_height() : '';
                                                    ?>
                                                    <span class="label">Dimensi</span>
                                                    <span class="value"><?php echo sprintf( '%dx%dx%d cm', $length, $width, $height ) ?></span>
                                                <?php elseif( $_product->has_dimensions() && $_product->is_type('variable') ) : ?>
                                                    <span class="label">Dimensi</span>
                                                    <?php echo esc_html( wc_format_dimensions( $_product->get_dimensions( false ) ) ); ?>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="shipping">
                            <div class="shipping-option">
                                <ul class="shipping-option-items">
                                    <li class="shipping-option-item <?php echo $force_wood_packaging == 'yes' ? 'is-disabled' : '' ?>">
                                        <label class="label">
                                            <input type="checkbox" name="wood_packaging" <?php echo $force_wood_packaging == 'yes' ? 'checked' : '' ?>>
                                            <span class="box">
                                                    <svg class="icon" role="img"><use xlink:href="<?= library_url() ?>/images/svg-symbols.svg#icon-check" /></svg>
                                                </span>
                                            <span class="text">Packing kayu</span>
                                        </label>
                                    </li>
                                    <li class="shipping-option-item <?php echo $force_shipping_insurance == 'yes' ? 'is-disabled' : '' ?>">
                                        <label class="label">
                                            <input type="checkbox" name="shipping_insurance" <?php echo $force_shipping_insurance == 'yes' ? 'checked' : '' ?>>
                                            <span class="box">
                                                    <svg class="icon" role="img"><use xlink:href="<?= library_url() ?>/images/svg-symbols.svg#icon-check" /></svg>
                                                </span>
                                            <span class="text">Asuransi</span>
                                        </label>
                                    </li>
                                </ul>
                                <?php if( $force_wood_packaging == 'yes' && $force_shipping_insurance == 'yes' ) : ?>
                                    <div class="shipping-option-notes">Pengiriman produk ini harus menggunakan packing kayu & asuransi</div>
                                <?php elseif( $force_wood_packaging == 'yes' && $force_shipping_insurance == 'no' ) : ?>
                                    <div class="shipping-option-notes">Pengiriman produk ini harus menggunakan packing kayu</div>
                                <?php elseif( $force_wood_packaging == 'no' && $force_shipping_insurance == 'yes' ) : ?>
                                    <div class="shipping-option-notes">Pengiriman produk ini harus menggunakan asuransi</div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </li>

            <?php endif; ?>
        <?php endforeach; ?>
    </ul>
</div>
<?php if ( WC()->cart->needs_shipping() && WC()->cart->show_shipping() ) : ?>

    <?php do_action( 'woocommerce_review_order_before_shipping' ); ?>

    <div class="shipping-method">
        <h4 class="heading order_review_heading">Choose Shipping</h4>
        <div class="shipping-list is-loading">
            <?php wc_cart_totals_shipping_html(); ?>
        </div>
    </div>

    <?php do_action( 'woocommerce_review_order_after_shipping' ); ?>

<?php endif; ?>