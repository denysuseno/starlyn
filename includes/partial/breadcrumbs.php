<div class="container">
    <ul class="breadcrumbs-custom-path">
        <li><a href="<?= home_url() ?>">Home</a></li>
        <?php if(isset($childLink)) : ?>
            <li><a href="<?= $parentLink ?>"><?= $parentTitle ?></a></li>
            <li class="active"><?= $childTitle ?></li>
        <?php else : ?>
            <li class="active"><?= $title ?></li>
        <?php endif; ?>
    </ul>
</div>