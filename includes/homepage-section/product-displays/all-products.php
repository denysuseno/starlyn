<?php
// order by latest product.
$query_args = array(
    'posts_per_page'    => 12,
    'post_status'       => 'publish',
    'post_type'         => 'product',
    'wc_query'          => 'product_query',
    'orderby'           => 'date',
    'meta_key'          => '',
    'order'             => 'DESC',
    'paged'             => 1,
    's'                 => ''
);
$loop = new WP_Query( $query_args );
?>

<div aria-labelledby="all-product" role="tabpanel">

    <?php if ( $loop->have_posts() ) : ?>

        <?php woocommerce_product_loop_start(); ?>

        <?php
            while ( $loop->have_posts() ) {
                $loop->the_post();

                /**
                 * Hook: woocommerce_shop_loop.
                 */
                do_action( 'woocommerce_shop_loop' );

                wc_get_template_part( 'content', 'product' );
            }
            wp_reset_postdata();
        ?>

        <?php woocommerce_product_loop_end(); ?>

        <?php if( $loop->max_num_pages > 0 ) : ?>
            <div class="view-more">
                <a href="<?php echo wc_get_page_permalink( 'shop' ) ?>" class="link">View More</a>
            </div>
        <?php endif; ?>

    <?php else : ?>

        <p class="woocommerce-info"><?php esc_html_e( 'Belum ada produk.', 'woocommerce' ); ?></p>

    <?php endif; ?>

</div>
