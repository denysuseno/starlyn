<?php
$featured_promotions = get_field( 'featured_promotions', $post->ID );
$promotion_lists     = isset( $featured_promotions['promotion_list'] ) ? $featured_promotions['promotion_list'] : [];

if( $promotion_lists ) :
    ?>
    <section class="section featured-promotio">
        <div class="wrapper">
            <ul class="items">
                <?php foreach( $promotion_lists as $promotion ) : ?>
                    <?php
                    $promotion_img  = isset( $promotion['promotion_image'] ) ? $promotion['promotion_image'] : '';
                    $promotion_link = isset( $promotion['promotion_link'] ) ? $promotion['promotion_link'] : [];
                    $link_url       = isset( $promotion_link['url'] ) ? $promotion_link['url'] : '';
                    $link_target    = isset( $promotion_link['target'] ) ? $promotion_link['target'] : '';
                    ?>
                    <li class="item">
                        <?php if( $link_url ) : ?>
                            <a href="<?php echo $link_url ?>" class="link" target="<?php echo $link_target ?>">
                        <?php endif; ?>
                            <img src="<?php echo $promotion_img ?>" alt="Featured Promotion" class="image">
                        <?php if( $link_url ) : ?>
                            </a>
                        <?php endif; ?>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </section>
<?php endif; ?>