<?php
$banner             = get_field( 'banner', $post->ID );
$banner_image       = isset( $banner['banner_image'] ) ? $banner['banner_image'] : '';
$banner_link        = isset( $banner['banner_link'] ) ? $banner['banner_link'] : [];
$banner_url         = isset( $banner_link['url'] ) ? $banner_link['url'] : '';
$banner_url_target  = isset( $banner_link['target'] ) ? $banner_link['target'] : '';

if( $banner_image && $banner_url ) :
    ?>
    <section class="section home-banner">
        <a href="<?php echo $banner_url ?>" class="link" target="<?php echo $banner_url_target ?>">
            <img src="<?php echo $banner_image ?>" alt="Banner Image" class="image">
        </a>
    </section>
<?php endif; ?>