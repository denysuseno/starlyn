<?php
$popular_brands = get_field( 'popular_brands', $post->ID );
$brands         = isset( $popular_brands['brands'] ) ? $popular_brands['brands'] : [];

if( $brands ) :
    ?>
    <section class="section popular-brand">
        <div class="wrapper">
            <div class="section-header">
                <div class="heading">
                    <h2 class="title">POPULAR BRANDS</h2>
                </div>
                <div class="heading-alt">
                    <a href="<?php echo get_permalink( get_page_by_path( 'brands' ) ) ?>" class="link">View More</a>
                </div>
            </div>
            <div class="section-content">
                <div class="brand-list">
                    <ul class="items">
                        <?php foreach ( $brands as $brand_id ) : ?>
                            <?php
                            $brand_term = get_term( $brand_id );
                            $term_name  = $brand_term->name;
                            $term_link  = get_term_link( $brand_id );
                            $term_img   = get_field( 'brand_logo', 'brand_' . $brand_id );
                            ?>
                            <li class="item">
                                <a href="<?php echo $term_link ?>" class="link">
                                    <img src="<?php echo $term_img ? $term_img : '' ?>" alt="<?php echo $term_name ?>">
                                </a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>