<section class="section our-channel">
    <div class="wrapper">
        <div class="section-header">
            <div class="heading">
                <h2 class="title">Our Channels</h2>
            </div>
        </div>
        <div class="section-content">
            <div class="channels">
                <?php
                $youtube_channel_thumbnail_url      = get_channel_thumbnail_url( 'default' );
                $youtube_channel_name               = get_channel_name();
                $youtube_channel_subscribers_count  = get_channel_subscribers_count();
                $youtube_channel_url                = get_channel_url();
                $youtube_meta_options               = get_field( 'our_channel', $post->ID );
                $youtube_video_url                  = $youtube_meta_options['youtube_channel']['featured_video_url'];
                $youtube_video_iframe               = $youtube_meta_options['youtube_channel']['featured_video_iframe'];
                $youtube_video_content_title        = get_transient( 'home_video_content_title' );
                if( ! $youtube_video_content_title ) {
                    $youtube_video_content_title    = get_video_content_title( $youtube_video_url );
                    set_transient( 'home_video_content_title', $youtube_video_content_title, DAY_IN_SECONDS );
                }
                $youtube_video_viewer_count         = get_transient( 'home_video_viewer_count' );
                if( ! $youtube_video_viewer_count ) {
                    $youtube_video_viewer_count     = get_video_content_viewer_count( $youtube_video_url );
                    set_transient( 'home_video_viewer_count', $youtube_video_viewer_count, DAY_IN_SECONDS );
                }
                $youtube_video_published_date       = get_transient( 'home_video_published_date' );
                if( ! $youtube_video_published_date ) {
                    $youtube_video_published_date   = get_video_content_published_date( $youtube_video_url );
                    set_transient( 'home_video_published_date', $youtube_video_published_date, DAY_IN_SECONDS );
                }
                ?>
                <div class="channel channel-youtube">
                    <div class="container">
                        <div class="channel-header">
                            <div class="avatar">
                                <img src="<?php echo $youtube_channel_thumbnail_url ?>" alt="Channel Avatar Youtube" class="image">
                            </div>
                            <div class="meta">
                                <h5 class="title">
                                    <a href="<?php echo $youtube_channel_url ?>" class="link" target="_blank"><?php echo $youtube_channel_name ?></a>
                                </h5>
                                <p class="subs"><?php echo $youtube_channel_subscribers_count ?> subscribers</p>
                            </div>
                        </div>
                        <div class="channel-content">
                            <div class="channel-content-video">
                                <?php echo $youtube_video_iframe ?>
                            </div>
                            <div class="channel-content-meta">
                                <h4 class="title">
                                    <a href="<?php echo $youtube_video_url ?>" class="link" target="_blank"><?php echo $youtube_video_content_title ?></a>
                                </h4>
                                <p class="detail"><?php echo number_format( $youtube_video_viewer_count, 0, ',', '.' ) . ' views | ' . time_elapsed_string( $youtube_video_published_date ) ?></p>
                            </div>
                        </div>
                        <div class="channel-footer">
                            <span class="text">Follow Us on</span>
                            <img src="<?= library_url() ?>/images/logo-youtube.png" alt="Follow on Youtube" class="image">
                        </div>
                    </div>
                </div>

                <?php
                $our_channel                    = get_field( 'our_channel', $post->ID );
                $instagram_channel              = isset( $our_channel['instagram_channel'] ) ? $our_channel['instagram_channel'] : [];
                $instagram_access_token         = isset( $instagram_channel['access_token'] ) ? $instagram_channel['access_token'] : '';
                $instagram_channel_avatar       = isset( $instagram_channel['instagram_channel_avatar'] ) ? $instagram_channel['instagram_channel_avatar'] : '';
                $instagram_user_info            = get_instagram_user_info( $instagram_access_token );
                $instagram_latest_post          = get_latest_instagram_post( $instagram_access_token );
                $instagram_latest_post_media    = isset( $instagram_latest_post->media_url ) ? $instagram_latest_post->media_url : '';
                $instagram_username             = isset( $instagram_user_info->username ) ? $instagram_user_info->username : '';
                $instagram_permalink            = 'https://www.instagram.com/'.$instagram_username;
                ?>
                <div class="channel channel-instagram">
                    <div class="container">
                        <div class="channel-header">
                            <div class="avatar">
                                <img src="<?php echo $instagram_channel_avatar ?>" alt="Channel Avatar Instagram" class="image">
                            </div>
                            <div class="meta">
                                <h5 class="title">
                                    <a href="<?php echo $instagram_permalink ?>" class="link" target="_blank"><?php echo $instagram_username ?></a>
                                </h5>
                            </div>
                        </div>
                        <div class="channel-content">
                            <div class="channel-content-image">
                                <img src="<?php echo $instagram_latest_post_media ?>" alt="Instagram Channel Image" class="image">
                            </div>
                        </div>
                        <div class="channel-footer">
                            <span class="text">Follow Us on</span>
                            <img src="<?= library_url() ?>/images/logo-instagram.png" alt="Follow on Instagram" class="image">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>