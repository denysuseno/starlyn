<?php
$special_offers = get_field( 'special_offers', $post->ID );
$product_offers = isset( $special_offers['products'] ) ? $special_offers['products'] : [];

if( $product_offers ) :
    ?>
    <section class="section special-offer">
        <div class="wrapper">
            <div class="section-header">
                <div class="heading">
                    <h2 class="title">Special Offers</h2>
                </div>
                <!--<div class="heading-alt">
                    <p class="offer-slider-index">Page <span class="current"></span> of <span class="total"></span></p>
                </div>-->
            </div>
            <div class="section-content">
                <div class="special-offer-display js-special-offer-slider">
                    <div class="slider">
                        <ul class="items">
                            <?php foreach( $product_offers as $product_id ) : ?>
                                <?php
                                $_product       = wc_get_product( $product_id );
                                $product_link   = get_permalink( $_product->get_id() );
                                $product_img    = wp_get_attachment_image_url( get_post_thumbnail_id( $product_id ), 'original' );
                                ?>
                                <li class="item">
                                    <a href="<?php echo $product_link ?>" class="link">
                                        <div class="container">
                                            <div class="item-price">
                                                <p class="text">Hari ini hanya</p>
                                                <p class="value"><span class="highligth"><?php echo wc_price( $_product->get_price() ) ?></span>*</p>
                                            </div>
                                            <div class="item-image">
                                                <img src="<?php echo $product_img ?>" alt="<?php echo $_product->get_name() ?>" class="image">
                                            </div>
                                            <div class="item-terms">
                                                <p>*syarat dan ketentuan berlaku</p>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                        <span class="slider-nav slider-nav-prev">
                                            <img src="<?= library_url() ?>/images/icon-angle-left-slim.svg" alt="Icon angle left">
                                        </span>
                        <span class="slider-nav slider-nav-next">
                                            <img src="<?= library_url() ?>/images/icon-angle-right-slim.svg" alt="Icon angle right">
                                        </span>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>