<section class="section home-product js-tab" data-tab-deeplink>
    <div class="home-product-nav">
        <div class="wrapper">
            <div class="filter">
                <form class="form" method="get" action="<?php echo wc_get_page_permalink( 'shop' ) ?>">
                    <div class="filter-field filter-search">
                        <input type="text" class="input" name="s" placeholder="Search">
                    </div><!--
                    <div class="filter-field filter-sort">
                        <select class="input" name="product_sort">
                            <option value="">Sort</option>
                        </select>
                    </div>-->
                </form>
            </div>
            <div class="nav">
                <ul class="items" role="tablist">
                    <li class="item item-current homepage-product-nav">
                        <button id="all-product" class="link" role="tab">All Products</button>
                    </li>
                    <li class="item homepage-product-nav">
                        <button id="discount-product" class="link" role="tab">Discount</button>
                    </li>
                    <li class="item homepage-product-nav">
                        <button id="popular-product" class="link" role="tab">Popular</button>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="home-product-display">
        <div class="wrapper">

            <div style="margin-bottom: 16px">
                <?php echo wc_print_notices() ?>
            </div>

            <?php
                include 'product-displays/all-products.php';
                include 'product-displays/discount-products.php';
                include 'product-displays/popular-products.php'
            ?>

        </div>
    </div>
</section>