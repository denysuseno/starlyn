<?php
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 11 );

/**
 * Remove single product exceprt.
 */
//remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );

add_action('woocommerce_product_meta_start', 'starlyn_single_product_meta_start');
function starlyn_single_product_meta_start() {
    ?>
    <div class="info-label">Product Info:</div>
    <div class="info-list">
    <?php
}

add_action('woocommerce_product_meta_end', 'starlyn_single_product_meta_end');
function starlyn_single_product_meta_end() {
    ?>
    </div>
    <?php
}