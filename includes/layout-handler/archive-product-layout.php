<?php


/**
 * Remove all notices
 */
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_output_all_notices', 10 );

/**
 * Remove text product result count.
 */
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );

/**
 * Remove catalog ordering.
 */
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );

/**
 * Remove product open link.
 */
remove_action( 'woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open', 10 );

/**
 * Remove product close link.
 */
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_product_link_close', 5 );

/**
 * Remove product thumbnail & change to custom.
 */
remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10 );

/**
 * Remove default woo product title.
 */
remove_action( 'woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title', 10 );

add_action( 'woocommerce_before_shop_loop_item_title', 'custom_product_thumbnail', 10 );
function custom_product_thumbnail() {
    global $product;
    $link = apply_filters( 'woocommerce_loop_product_link', get_the_permalink(), $product );
    $thumbnail = get_the_post_thumbnail_url( $product->get_id() ) != "" ? get_the_post_thumbnail_url( $product->get_id() ) : wc_placeholder_img_src();
?>
    <div class="item-image">
        <a href="<?= esc_url( $link ) ?>" class="link">
            <img src="<?php echo $thumbnail ?>" alt="<?php echo $product->get_title() ?>" class="image">
        </a>
    </div>
    <div class="item-meta">
<?php
}

add_action( 'woocommerce_before_shop_loop_item_title', 'wishlist_action_remove', 9 );
function wishlist_action_remove() {
    if( is_page( 'wishlist' ) ) :
        global $product;
?>
        <div class="wishlist-action">
            <a href="javascript:void(0)" class="link btn-remove-wishlist" title="Remove" data-product-id="<?php echo $product->get_id() ?>" data-nonce="<?php echo wp_create_nonce('wishlist-nonce') ?>">
                <svg class="icon" role="img"><use xlink:href="<?= library_url() ?>/images/svg-symbols.svg#icon-trash" /></svg>
            </a>
        </div>
<?php
    endif;
}

add_action( 'woocommerce_shop_loop_item_title', 'custom_loop_product_title', 10 );
function custom_loop_product_title() {
    global $product;
    $link = apply_filters( 'woocommerce_loop_product_link', get_the_permalink(), $product );
?>
    <p class="title">
        <a href="<?= esc_url( $link ) ?>" class="link"><?= get_the_title() ?></a>
    </p>
<?php
}

remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5 );
add_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 11 );

remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );
add_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_add_to_cart', 12 );

add_action( 'woocommerce_after_shop_loop_item_title', 'woo_after_shop_loop_item_title', 15 );
function woo_after_shop_loop_item_title() {
    ?>
    </div><!--END of .item-meta-->
    <?php
}


add_action( 'starlyn_archive_aside_desktop', 'display_archive_aside_desktop' );
function display_archive_aside_desktop() {
    global $wp_query;

    $categories = sn_get_product_categories();

    $args = [
        'categories'    => $categories,
        'curr_cat'      => $wp_query->get_queried_object(),
    ];

    wc_get_template('archive-product-sidebar-desktop.php', $args );
}

add_action( 'starlyn_archive_aside_mobile', 'display_archive_aside_mobile' );
function display_archive_aside_mobile() {
    global $wp_query;

    $categories = sn_get_product_categories();

    $args = [
        'categories'    => $categories,
        'curr_cat'      => $wp_query->get_queried_object(),
    ];

    wc_get_template( 'archive-product-sidebar-mobile.php', $args );
}

add_action( 'woocommerce_before_shop_loop', 'archive_product_filter' );
function archive_product_filter() {
    ?>
    <div class="filter">
        <div class="filter-main">
            <div class="filter-nav">
                <a href="#" class="link js-product-nav">
                    Category
                    <svg class="icon" role="img"><use xlink:href="<?= library_url() ?>/images/svg-symbols.svg#icon-chevron-down" /></svg>
                </a>
            </div>
            <div class="filter-form">
                <form class="form woocommerce-ordering" method="get">
                    <div class="filter-field filter-search">
                        <input type="text" class="input" name="s" placeholder="Search" value="<?php echo isset( $_GET['s'] ) ? $_GET['s'] : '' ?>">
                    </div>
                    <div class="filter-field filter-sort">
                        <?php woocommerce_catalog_ordering() ?>
                    </div>
                </form>
            </div>
        </div>

        <!-- product nav mobile -->
        <?php do_action( 'starlyn_archive_aside_mobile' ); ?>
        <!-- end product nav mobile -->

    </div>
    <?php
}

add_filter( 'woocommerce_catalog_orderby', 'starlyn_catalog_orderby', 10, 1 );
function starlyn_catalog_orderby( $options ) {
    unset( $options['rating'] );
    unset( $options['date'] );

    $options['menu_order'] = __( 'Sort', 'woocommerce' );
    $options['popularity'] = __( 'Terlaris', 'woocommerce' );
    $options['price']      = __( 'Harga Terendah', 'woocommerce' );
    $options['price-desc'] = __( 'Harga Tertinggi', 'woocommerce' );

    return $options;
}

remove_filter( 'woocommerce_default_catalog_orderby', 'starlyn_default_catalog_orderby' );
add_action( 'starlyn_product_archives_output_all_notices', 'woocommerce_output_all_notices', 10 );