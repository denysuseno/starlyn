<?php

class Youtube_API {

    public $channel_id;
    public $api_key;
    public $base_api_url;
    public $data_part;

    public function __construct() {

        $youtube_options    = get_field( 'youtube_api', 'option' );
        $this->channel_id   = $youtube_options['youtube_channel_id'];
        $this->api_key      = $youtube_options['youtube_api_key'];
        $this->base_api_url = 'https://content.googleapis.com/youtube/v3';
        $this->data_part    = $youtube_options['youtube_data_part'];

    }

    public function get_channels_data_api() {

        if( empty( $this->channel_id ) || empty( $this->api_key ) || empty( $this->data_part ) ) {
            return;
        }

        // delete_transient( 'get_youtube_channels_data_api' );
        $response_json = get_transient( 'get_youtube_channels_data_api' );

        if( false !== $response_json ) {
            return $response_json;
        }

        $query = [
            'id'    => $this->channel_id, // Channel ID.
            'key'   => $this->api_key, // Your API Key.
            'part'  => $this->data_part, // snippet for getting thumbnails, statistic for getting subscriber count.
        ];

        $request_url = sprintf(
            '%s?%s',
            $this->base_api_url . '/channels',
            http_build_query( $query )
        );

        $response_json = json_decode( @file_get_contents( $request_url ) );

        set_transient( 'get_youtube_channels_data_api', $response_json, DAY_IN_SECONDS );

        return $response_json;

    }

    public function get_channel_thumbnails() {

        $data_api   = $this->get_channels_data_api();
        $thumbnails = isset( $data_api->items[0]->snippet->thumbnails ) ? $data_api->items[0]->snippet->thumbnails : '';

        return $thumbnails;

    }

    public function get_channel_thumbnail( $size = 'default' ) {

        $thumbnails = $this->get_channel_thumbnails();
        $thumbnail  = [];

        if( isset( $thumbnails->$size ) ) {
            $thumbnail = $thumbnails->$size;
        }

        return $thumbnail;

    }

    public function get_channel_thumbnail_url( $size = 'default' ) {

        $thumbnail      = $this->get_channel_thumbnail( $size );
        $thumbnail_url  = '';

        if( isset( $thumbnail->url ) ) {
            $thumbnail_url = $thumbnail->url;
        }

        return $thumbnail_url;

    }

    public function get_channel_name() {

        $data_api       = $this->get_channels_data_api();
        $channel_name   = isset( $data_api->items[0]->snippet->title ) ? $data_api->items[0]->snippet->title : '';

        return $channel_name;

    }

    public function get_channel_subscribers_count() {

        $data_api           = $this->get_channels_data_api();
        $subscribers_count  = isset( $data_api->items[0]->statistics->subscriberCount ) ? $data_api->items[0]->statistics->subscriberCount : 0;

        return $subscribers_count;

    }

    public function get_channel_unique_id() {

        $data_api           = $this->get_channels_data_api();
        $subscribers_count  = isset( $data_api->items[0]->id ) ? $data_api->items[0]->id : '';

        return $subscribers_count;

    }

    /**
     * source: https://gist.github.com/ghalusa/6c7f3a00fd2383e5ef33
     *
     * @param $video_url
     * @return mixed
     */
    public function get_video_id_from_url( $video_url ) {

        if( empty( $video_url ) ) {
            return;
        }

        // Here is a sample of the URLs this regex matches: (there can be more content after the given URL that will be ignored)

        // http://youtu.be/dQw4w9WgXcQ
        // http://www.youtube.com/embed/dQw4w9WgXcQ
        // http://www.youtube.com/watch?v=dQw4w9WgXcQ
        // http://www.youtube.com/?v=dQw4w9WgXcQ
        // http://www.youtube.com/v/dQw4w9WgXcQ
        // http://www.youtube.com/e/dQw4w9WgXcQ
        // http://www.youtube.com/user/username#p/u/11/dQw4w9WgXcQ
        // http://www.youtube.com/sandalsResorts#p/c/54B8C800269D7C1B/0/dQw4w9WgXcQ
        // http://www.youtube.com/watch?feature=player_embedded&v=dQw4w9WgXcQ
        // http://www.youtube.com/?feature=player_embedded&v=dQw4w9WgXcQ

        // It also works on the youtube-nocookie.com URL with the same above options.
        // It will also pull the ID from the URL in an embed code (both iframe and object tags)

        preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $video_url, $match);
        $youtube_id = isset( $match[1] ) ? $match[1] : '';

        return $youtube_id;

    }

    public function get_videos_data_api( $video_id ) {

        if( empty( $this->api_key ) ) {
            return;
        }

        // delete_transient( 'get_youtube_videos_data_api' );
        // $response_json = get_transient( 'get_youtube_videos_data_api' );

        /*if( false !== $response_json ) {
            return $response_json;
        }*/

        $query = [
            'id'    => $video_id, // Video ID.
            'key'   => $this->api_key, // Your API Key.
            'part'  => 'snippet,statistics',
        ];

        $request_url = sprintf(
            '%s?%s',
            $this->base_api_url . '/videos',
            http_build_query( $query )
        );

        $response_json = json_decode( @file_get_contents( $request_url ) );

        // set_transient( 'get_youtube_videos_data_api', $response_json, DAY_IN_SECONDS );

        return $response_json;

    }

    public function get_video_content_title( $video_url ) {

        $video_id   = $this->get_video_id_from_url( $video_url );
        $data_api   = $this->get_videos_data_api( $video_id );
        $video_title= isset( $data_api->items[0]->snippet->title ) ? $data_api->items[0]->snippet->title : '';

        return $video_title;

    }

    public function get_video_viewer_count( $video_url ) {

        $video_id       = $this->get_video_id_from_url( $video_url );
        $data_api       = $this->get_videos_data_api( $video_id );
        $viewer_count   = isset( $data_api->items[0]->statistics->viewCount ) ? $data_api->items[0]->statistics->viewCount : 0;

        return $viewer_count;

    }

    public function get_video_published_date( $video_url ) {

        $video_id       = $this->get_video_id_from_url( $video_url );
        $data_api       = $this->get_videos_data_api( $video_id );
        $viewer_count   = isset( $data_api->items[0]->snippet->publishedAt ) ? $data_api->items[0]->snippet->publishedAt : '';

        return $viewer_count;

    }

}

function get_channel_thumbnail_url( $size = 'default' ) {
    $youtube_api    = new Youtube_API();
    $thumbnail_url  = $youtube_api->get_channel_thumbnail_url( $size );

    return $thumbnail_url;
}

function get_channel_name() {
    $youtube_api    = new Youtube_API();
    $channel_name   = $youtube_api->get_channel_name();

    return $channel_name;
}

function get_channel_subscribers_count() {
    $youtube_api        = new Youtube_API();
    $subscribers_count  = $youtube_api->get_channel_subscribers_count();

    return $subscribers_count;
}

function get_channel_url() {
    $youtube_api        = new Youtube_API();
    $unique_channel_id  = $youtube_api->get_channel_unique_id();

    return 'https://www.youtube.com/channel/' . $unique_channel_id;
}

function get_video_id_from_url( $video_url ) {

    $youtube_api    = new Youtube_API();
    $video_id       = $youtube_api->get_video_id_from_url( $video_url );

    return $video_id;

}

function get_video_content_title( $video_url ) {

    if( ! $video_url ) {
        return;
    }

    $youtube_api    = new Youtube_API();
    $content_title  = $youtube_api->get_video_content_title( $video_url );

    return $content_title;

}

function get_video_content_viewer_count( $video_url ) {

    if( ! $video_url ) {
        return;
    }

    $youtube_api    = new Youtube_API();
    $viewer_count   = $youtube_api->get_video_viewer_count( $video_url );

    return $viewer_count;

}

function get_video_content_published_date( $video_url ) {

    $youtube_api    = new Youtube_API();
    $published_date = $youtube_api->get_video_published_date( $video_url );

    return $published_date;

}