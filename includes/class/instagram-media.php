<?php

/**
 * Class Instagram_Media
 */

class Instagram_Media {
    
    public function __construct() {

    }

    public function get_user_profile( $access_token ) {

        if( empty( $access_token ) ) {
            return;
        }

        // delete_transient( 'get_instagram_user_info' );
        $response = get_transient( 'get_instagram_user_info' );

        if( false !== $response ) {
            return $response;
        }

        $query = [
            'fields'        => 'id,username',
            'access_token'  => $access_token,
        ];

        $host = sprintf(
            '%s?%s',
            'https://graph.instagram.com/me',
            http_build_query( $query )
        );

        $headers    = array( 'Accept: application/json; Content-Type: application/json;' );

        $response   = $this->curl_execute( 'get', $host, $headers );

        set_transient( 'get_instagram_user_info', $response, DAY_IN_SECONDS );

        return $response;

    }

    public function get_user_media( $access_token ) {

        if( empty( $access_token ) ) {
            return;
        }

        // delete_transient( 'get_instagram_user_media' );
        $response = get_transient( 'get_instagram_user_media' );

        if( false !== $response ) {
            return $response;
        }

        $query = [
            'fields'        => 'id,media_url,permalink',
            'access_token'  => $access_token,
        ];

        $host = sprintf(
            '%s?%s',
            'https://graph.instagram.com/me/media',
            http_build_query( $query )
        );

        $headers    = array( 'Accept: application/json; Content-Type: application/json;' );

        $response   = $this->curl_execute( 'get', $host, $headers );

        set_transient( 'get_instagram_user_media', $response, DAY_IN_SECONDS );

        return $response;

    }

    public function curl_execute( $method, $host, $headers, $params = [] ) {

        switch ( $method ) {
            case 'get':
                return $this->request_get( $host, $headers );
                break;
            case 'post':
                return $this->request_post( $host, $headers, $params );
                break;
        }

    }

    public function request_get( $host, $headers ) {

        $ch = curl_init();

        curl_setopt( $ch, CURLOPT_URL, $host );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, TRUE );

        $response = curl_exec($ch);
        curl_close($ch);

        return json_decode( $response );

    }

    public function request_post( $host, $headers, $params ) {

        $ch = curl_init();

        curl_setopt( $ch, CURLOPT_URL, $host );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, TRUE );
        curl_setopt( $ch, CURLOPT_POST, TRUE );
        curl_setopt( $ch, CURLOPT_POSTFIELDS, http_build_query( $params ) );

        $response = curl_exec($ch);
        curl_close($ch);

        return json_decode( $response );

    }

}

function get_instagram_user_info( $access_token ) {

    $instagram_media        = new Instagram_Media();
    $user_info              = $instagram_media->get_user_profile( $access_token );

    return $user_info;

}

function get_instagram_user_media( $access_token ) {

    $instagram_media        = new Instagram_Media();
    $user_media             = $instagram_media->get_user_media( $access_token );

    return $user_media;

}

function get_latest_instagram_post( $access_token ) {

    $user_media             = get_instagram_user_media( $access_token );
    $latest_post            = (object) [];

    if( isset( $user_media->data[0] ) && $user_media->data[0] ) {
        $latest_post = $user_media->data[0];
    }

    return $latest_post;

}