<?php
$commenter    = wp_get_current_commenter();
$comment_form = array(
    /* translators: %s is product title */
    'title_reply'         => have_comments() ? esc_html__( 'Tambah Ulasan', 'woocommerce' ) : sprintf( esc_html__( 'Jadi yang pertama untuk mengulas produk &ldquo;%s&rdquo;', 'woocommerce' ), get_the_title() ),
    /* translators: %s is product title */
    'title_reply_to'      => esc_html__( 'Leave a Reply to %s', 'woocommerce' ),
    'title_reply_before'  => '<h4 class="heading">',
    'title_reply_after'   => '</h4> <span class="js-button-close js-popup-inline" data-popper-target="#popup-review-'.$product_id.'"><svg class="icon" role="img"><use xlink:href="'.library_url().'/images/svg-symbols.svg#icon-close" /></svg></span>',
    'comment_notes_after' => '',
    'label_submit'        => esc_html__( 'Submit', 'woocommerce' ),
    'logged_in_as'        => '',
    'comment_field'       => '',
    'class_form'            => 'form',
    'class_container'       => 'popup-inline-wrapper',
    'submit_field'          => '<div class="form-submit">%1$s %2$s</div>',
    'class_submit'          => 'button button-gray',
);

$name_email_required = (bool) get_option( 'require_name_email', 1 );
$fields              = array(
    'author' => array(
        'label'    => __( 'Name', 'woocommerce' ),
        'type'     => 'text',
        'value'    => $commenter['comment_author'],
        'required' => $name_email_required,
    ),
    'email'  => array(
        'label'    => __( 'Email', 'woocommerce' ),
        'type'     => 'email',
        'value'    => $commenter['comment_author_email'],
        'required' => $name_email_required,
    ),
);

$comment_form['fields'] = array();

foreach ( $fields as $key => $field ) {
    $field_html  = '<p class="comment-form-' . esc_attr( $key ) . '">';
    $field_html .= '<label for="' . esc_attr( $key ) . '">' . esc_html( $field['label'] );

    if ( $field['required'] ) {
        $field_html .= '&nbsp;<span class="required">*</span>';
    }

    $field_html .= '</label><input id="' . esc_attr( $key ) . '" name="' . esc_attr( $key ) . '" type="' . esc_attr( $field['type'] ) . '" value="' . esc_attr( $field['value'] ) . '" size="30" ' . ( $field['required'] ? 'required' : '' ) . ' /></p>';

    $comment_form['fields'][ $key ] = $field_html;
}

$account_page_url = wc_get_page_permalink( 'myaccount' );
if ( $account_page_url ) {
    /* translators: %s opening and closing link tags respectively */
    $comment_form['must_log_in'] = '<p class="must-log-in">' . sprintf( esc_html__( 'You must be %1$slogged in%2$s to post a review.', 'woocommerce' ), '<a href="' . esc_url( $account_page_url ) . '">', '</a>' ) . '</p>';
}

if ( wc_review_ratings_enabled() ) {
    ob_start();
    include '_comment-field.php';
    $comment_form['comment_field'] = ob_get_contents();
    ob_get_clean();
}

comment_form( apply_filters( 'woocommerce_product_review_comment_form_args', $comment_form ) );
?>