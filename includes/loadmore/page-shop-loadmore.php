<?php

define( 'PAGE_SHOP_PPP', 12 );

add_action( 'wp_enqueue_scripts', 'page_shop_loadmore_enqueue_scripts' );
function page_shop_loadmore_enqueue_scripts() {
    global $wp_query;

    wp_register_script( 'page-shop-loadmore-script', get_stylesheet_directory_uri() . '/library/js/loadmore/page-shop-loadmore.js', array('jquery'), '', true );
    wp_localize_script(
        'page-shop-loadmore-script','page_shop_loadmore_vars', [
            'ajaxurl'       => admin_url( 'admin-ajax.php' ),
            'posts'         => json_encode( $wp_query->query_vars ),
            'current_page'  => $wp_query->query_vars['paged'] ? $wp_query->query_vars['paged'] : 1,
            'max_page'      => $wp_query->max_num_pages
        ]
    );
    wp_enqueue_script( 'page-shop-loadmore-script' );
}

add_action('wp_ajax_nopriv_page_shop_more_post_ajax', 'page_shop_more_post_ajax');
add_action('wp_ajax_page_shop_more_post_ajax', 'page_shop_more_post_ajax');
function page_shop_more_post_ajax() {
    $posts_query                = json_decode( stripslashes( $_POST['query'] ), true );

    // prepare our arguments for the query
    $params                     = $posts_query; // query_posts() takes care of the necessary sanitization
    $params['paged']            = (int) $_POST['page'] + 1;
    $params['post_status']      = 'publish';

    if( $posts_query['orderby'] == 'popularity' ) {
        $params['orderby']      = 'meta_value_num';
        $params['meta_key']     = 'total_sales';
        $params['order']        = 'DESC';
    } elseif( $posts_query['orderby'] == 'price' ) {
        $params['orderby']      = 'meta_value_num';
        $params['meta_key']     = '_price';
        $params['order']        = $posts_query['order'];
    }

    // it is always better to use WP_Query but not here
    query_posts( $params );


    if( have_posts() ) {
        while( have_posts() ) {
            the_post();

            /**
             * Hook: woocommerce_shop_loop.
             */
            do_action( 'woocommerce_shop_loop' );

            wc_get_template_part( 'content', 'product' );
        }
    }
    die;
}