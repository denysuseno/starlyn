<?php

define( 'PAGE_DEALS_PPP', 12 );

add_action( 'wp_enqueue_scripts', 'page_deals_loadmore_enqueue_scripts' );
function page_deals_loadmore_enqueue_scripts() {
    if( ! is_page( 'deals' ) ) {
        return;
    }

    $ordering   = page_deals_ordering_args();
    $s          = isset( $_GET['search'] ) && ! empty( $_GET['search'] ) ? $_GET['search'] : '';
    $query_args = query_page_deals( $ordering['orderby'], $ordering['meta_key'], $ordering['order'], 1, $s );
    $loop       = new WP_Query( $query_args );

    wp_register_script( 'page-deals-loadmore-script', get_stylesheet_directory_uri() . '/library/js/loadmore/page-deals-loadmore.js', array('jquery'), '', true );
    wp_localize_script(
        'page-deals-loadmore-script','page_deals_loadmore_vars', [
            'ajaxurl'   => admin_url( 'admin-ajax.php' ),
            'orderby'   => $ordering['orderby'],
            'meta_key'  => $ordering['meta_key'],
            'order'     => $ordering['order'],
            's'         => $s,
            'max_page'  => $loop->max_num_pages
        ]
    );
    wp_enqueue_script( 'page-deals-loadmore-script' );
}

add_action('wp_ajax_nopriv_page_deals_more_post_ajax', 'page_deals_more_post_ajax');
add_action('wp_ajax_page_deals_more_post_ajax', 'page_deals_more_post_ajax');
function page_deals_more_post_ajax() {
    $orderby    = (isset($_POST['orderby'])) ? $_POST['orderby'] : 'date';
    $meta_key   = (isset($_POST['meta_key'])) ? $_POST['meta_key'] : '';
    $order      = (isset($_POST['order'])) ? $_POST['order'] : 'DESC';
    $s          = (isset($_POST['s'])) ? $_POST['s'] : '';
    $page       = (isset($_POST['page_number'])) ? $_POST['page_number'] : 1;

    $query_args = query_page_deals( $orderby, $meta_key, $order, $page, $s );
    $loop       = new WP_Query( $query_args );

    header("Content-Type: text/html");

    $out = '';

    if( $loop->have_posts() ) {
        ob_start();
        while( $loop->have_posts() ) {
            $loop->the_post();

            /**
             * Hook: woocommerce_shop_loop.
             */
            do_action( 'woocommerce_shop_loop' );

            wc_get_template_part( 'content', 'product' );
        }
        $out = ob_get_contents();
        ob_get_clean();
    }
    wp_reset_postdata();
    die( $out );
}

function page_deals_ordering_args() {
    $ordering_args  = WC()->query->get_catalog_ordering_args();
    $ordering       = [];

    switch ( $ordering_args['orderby'] ) {
        case 'menu_order title':
            $ordering['orderby']    = 'date';
            $ordering['meta_key']   = '';
            $ordering['order']      = 'DESC';
            break;
        case 'price':
            $ordering['orderby']    = 'meta_value_num';
            $ordering['meta_key']   = '_price';
            $ordering['order']      = $ordering_args['order'];
            break;
        case 'popularity':
            $ordering['orderby']    = 'meta_value_num';
            $ordering['meta_key']   = 'total_sales';
            $ordering['order']      = 'DESC';
            break;
    }

    return $ordering;
}

function query_page_deals( $orderby = 'date', $meta_key = '', $order = 'DESC', $page = 1, $s = '' ) {
    $query_args = array(
        'posts_per_page'    => (int) PAGE_DEALS_PPP,
        'post_status'       => 'publish',
        'post_type'         => 'product',
        'post__in'          => array_merge( array( 0 ), wc_get_product_ids_on_sale() ),
        'wc_query'          => 'product_query',
        'orderby'           => $orderby,
        'meta_key'          => $meta_key,
        'order'             => $order,
        'paged'             => (int) $page,
        's'                 => $s
    );

    return $query_args;
}