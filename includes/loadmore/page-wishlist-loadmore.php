<?php

define( 'PAGE_WISHLIST_PPP', 12 );

add_action( 'wp_enqueue_scripts', 'page_wishlist_loadmore_enqueue_scripts' );
function page_wishlist_loadmore_enqueue_scripts() {
    $query_args = query_page_wishlist( 1 );
    $loop       = new WP_Query( $query_args );

    wp_register_script( 'page-wishlist-loadmore-script', get_stylesheet_directory_uri() . '/library/js/loadmore/page-wishlist-loadmore.js', array('jquery'), '', true );
    wp_localize_script(
        'page-wishlist-loadmore-script','page_wishlist_loadmore_vars', [
            'ajaxurl'   => admin_url( 'admin-ajax.php' ),
            'max_page'  => $loop->max_num_pages
        ]
    );
    wp_enqueue_script( 'page-wishlist-loadmore-script' );
}

add_action('wp_ajax_nopriv_page_wishlist_more_post_ajax', 'page_wishlist_more_post_ajax');
add_action('wp_ajax_page_wishlist_more_post_ajax', 'page_wishlist_more_post_ajax');
function page_wishlist_more_post_ajax() {
    $page       = (isset($_POST['page_number'])) ? $_POST['page_number'] : 1;

    $query_args = query_page_wishlist( $page );
    $loop       = new WP_Query( $query_args );

    header("Content-Type: text/html");

    $out = '';

    if( $loop->have_posts() ) {
        ob_start();
        while( $loop->have_posts() ) {
            $loop->the_post();

            /**
             * Hook: woocommerce_shop_loop.
             */
            do_action( 'woocommerce_shop_loop' );

            wc_get_template_part( 'content', 'product' );
        }
        $out = ob_get_contents();
        ob_get_clean();
    }
    wp_reset_postdata();
    die( $out );
}

function query_page_wishlist( $page = 1, $ppp = PAGE_WISHLIST_PPP ) {
    $ids = get_my_wishist_product_ids();

    if( ! $ids ) {
        return [];
    }

    $query_args = array(
        'posts_per_page'    => (int) $ppp,
        'post_status'       => 'publish',
        'post_type'         => 'product',
        'post__in'          => $ids,
        'paged'             => (int) $page,
        'orderby'           => 'post__in'
    );

    return $query_args;
}