<?php
/* Template Name: Home */

global $post;
?>

<?php get_header(); ?>

    <div id="site-content" class="site-content site-content-home">
        <div class="site-content-inner">
            <main class="main" role="main">
                <div class="home-page">
                    <?php
                    include 'includes/homepage-section/banner.php';

                    include 'includes/homepage-section/products.php';

                    include 'includes/homepage-section/popular-brands.php';

                    include 'includes/homepage-section/product-offers.php';

                    include 'includes/homepage-section/featured-promotions.php';

                    include 'includes/homepage-section/our-channel.php';
                    ?>
                </div>
            </main>
        </div>
    </div>

<?php get_footer(); ?>