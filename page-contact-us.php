<?php
/* Template Name: Contact Us */

global $post;
$header         = get_field( 'header', $post->ID );
$content        = get_field( 'content', $post->ID );
$supports       = get_field( 'contact_support', $post->ID );
$address        = get_field( 'contact_address', $post->ID );
?>

<?php get_header(); ?>

<div id="site-content" class="site-content site-content-about">
    <div class="site-content-inner">
        <main class="main" role="main">
            <div class="page-single wrapper-about">
                <div class="contact">
                    <div class="contact-page">
                        <!--<section class="section contact-page-header">
                            <img src="<?php echo $header['header_image'] ?>" alt="Header Image" class="images">
                            <div class="page-text">
                                <p class="text"><?php echo $header['header_text'] ?></p>
                                <h3 class="sub-text"><?php echo $header['header_title'] ?></h3>
                            </div>
                        </section>-->
                        <div class="contact-page-content intro">
                            <p class="text-contact"><?php echo $content['content_text'] ?></p>
                            <section class="section page-content intro">
                                <div class="page-content-inner">
                                    <h2 class="text"><?php echo $content['content_title'] ?></h2>
                                    <div class="page-content-wrapper wrapper">
                                        <?php if( APP_ENV == 'local' ) : ?>
                                            <?php echo do_shortcode( '[contact-form-7 id="709" title="Contact Us" html_class="forms-contact"]' ) ?>
                                        <?php elseif( APP_ENV == 'dev' ) : ?>
                                            <?php echo do_shortcode( '[contact-form-7 id="259" title="Contact Us" html_class="forms-contact"]' ) ?>
                                        <?php else : ?>
                                            <?php echo do_shortcode( '[contact-form-7 id="259" title="Contact Us" html_class="forms-contact"]' ) ?>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </section>
                            <section class="section contact-section">
                                <?php if( $supports ) : ?>
                                    <div class="contact-desc">
                                        <div class="contact-desc-inner intro">
                                            <h3 class="text">Contact Our Support team</h3>
                                            <?php foreach ( $supports as $support ) : ?>
                                                <p class="title-contact"><?php echo $support['support_type'] ?> : <span><?php echo $support['support_value'] ?></span> </p>
                                            <?php endforeach; ?>
                                        </div>
                                    </div>
                                <?php endif; ?>
                                <div class="contact-address intro">
                                    <div class="contact-address-text">
                                        <h3 class="text"><?php echo $address['contact_text'] ?></h3>
                                        <p class="title-address"><?php echo $address['contact_address'] ?></p>
                                    </div>
                                    <div class="contact-address-map">
                                        <?php echo $address['contact_map_iframe'] ?>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
</div>

<?php get_footer(); ?>