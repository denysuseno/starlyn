<?php
/* Template Name: Checkout */
?>

<?php get_header();

$class = is_checkout() && !empty( is_wc_endpoint_url('order-received') ) ? 'checkout-complete' : 'content-checkout';
?>

<div id="site-content" class="site-content site-<?= $class ?>">
    <div class="site-content-inner">
        <main class="main" role="main">

            <?php while (have_posts()) : the_post(); ?>

                <?php the_content(); ?>

            <?php endwhile; ?>

        </main>
    </div>
</div>

<?php if( is_user_logged_in() && empty( is_wc_endpoint_url('order-received') ) ) : ?>

    <?php
        $myAddress      = get_all_my_address_book();
        $checkout       = WC()->checkout();
        $mainAddressID  = $checkout->get_value( 'billing_address_id' );
    ?>

    <div id="popup-address" class="popup-address popup-inline">
        <div class="popup-inline-wrapper popup-inline-wrapper-large">
            <div class="choose-header">
                <h3 class="text">Choose Another Address</h3>
                <?php if( count( $myAddress ) < MAX_ADDRESS_BOOK ) : ?>
                    <a href="#popup-add-new" class="button button-gray js-popup-inline">+ Add New Address</a>
                <?php endif; ?>
            </div>
            <span class="js-button-close js-popup-inline" data-popper-target="#popup-address"><svg class="icon" role="img"><use xlink:href="<?= library_url() ?>/images/svg-symbols.svg#icon-close" /></svg></span>
            <div class="choose-content">
                <?php if( $myAddress ) : ?>

                    <ul class="items">

                        <?php foreach ( $myAddress as $address ) : ?>

                            <li class="item">
                                <label class="container">
                                    <input type="radio" name="shipping_methods" class="checkout-select-address" <?= $mainAddressID == $address->id ? 'checked' : ''; ?>
                                           data-url="<?= admin_url( 'admin-ajax.php?action=ajax_checkout_select_address&nonce='.wp_create_nonce('checkout_select_address') ) ?>"
                                           data-address-id="<?= $address->id ?>"
                                           data-address-name="<?= $address->name ?>"
                                           data-address-address-name="<?= $address->address_name ?>"
                                           data-address-country="<?= $address->country ?>"
                                           data-address-province="<?= $address->province ?>"
                                           data-address-regency="<?= $address->regency ?>"
                                           data-address-district="<?= $address->district ?>"
                                           data-address-postcode="<?= $address->postcode ?>"
                                           data-address-address="<?= $address->address ?>"
                                           data-address-phone="<?= $address->phone ?>"
                                           data-address-is-main="<?= $address->as_default_billing_address ?>"
                                    >
                                    <div class="label">
                                        <span class="radio"></span>
                                        <div class="label-head">
                                            <?php if( $address->as_default_billing_address ) : ?>
                                                <span class="selected">Utama</span>
                                            <?php endif; ?>
                                            <p class="label-text name"><?= $address->name ?> </p>
                                            <p class="label-text address">
                                                <?= $address->address ?> <br>
                                                <?= $address->province ?>, <?= $address->regency ?>, <?= $address->district ?> <?= $address->postcode ?>
                                            </p>
                                            <p class="label-text number"><?= $address->phone ?></p>
                                        </div>
                                    </div>
                                </label>
                            </li>

                        <?php endforeach; ?>

                    </ul>

                <?php else : ?>

                    Anda belum memiliki data alamat.

                <?php endif; ?>
            </div>
        </div>
    </div>
    <div id="popup-add-new" class="popup-address popup-inline">
        <div class="popup-inline-wrapper popup-inline-wrapper-large">
            <div class="choose-header">
                <h3 class="text">Add New Address</h3>
            </div>
            <span class="js-button-close js-popup-inline" data-popper-target="#popup-add-new"><svg class="icon" role="img"><use xlink:href="<?= library_url() ?>/images/svg-symbols.svg#icon-close" /></svg></span>
            <div class="choose-content">
                <div class="account-main-content">
                    <div class="addressbook-form">
                        <form class="form form-address-book" method="post">
                            <input type="hidden" name="address_country" value="ID">
                            <?php $checkoutUrl = wc_get_checkout_url(); ?>
                            <input type="hidden" name="redirect_after_success" value="<?= $checkoutUrl ?>">
                            <input type="hidden" name="redirect_after_failed" value="<?= wc_get_account_endpoint_url( 'edit-address/shipping' ) ?>">
                            <div class="form-input">
                                <div class="form-input-field">
                                    <label class="label" for="address-label">Label<span class="required">*</span></label>
                                    <input type="text" class="input" id="address-label" name="address_address_name" required placeholder="Contoh: Rumah, Toko">
                                </div>
                                <div class="form-input-field">
                                    <label class="label" for="address-name">Nama<span class="required">*</span></label>
                                    <input type="text" class="input" id="address-name" name="address_name" required>
                                </div>
                                <div class="form-input-field">
                                    <label class="label" for="address-phone">Nomor telepon<span class="required">*</span></label>
                                    <input type="text" class="input force-number" id="address-phone" name="address_phone" required>
                                </div>
                                <div class="form-input-field">
                                    <label class="label" for="address-address">Alamat<span class="required">*</span></label>
                                    <textarea rows="5" class="input" id="address-address" name="address_address" required></textarea>
                                </div>
                                <div class="form-input-fields">
                                    <div class="form-input-field">
                                        <?php
                                            $province = get_province();
                                            $regencyNonce = wp_create_nonce( 'get_regency_nonce' );
                                        ?>
                                        <label class="label" for="address-province">Provinsi<span class="required">*</span></label>
                                        <select class="input province-ajax" id="address-province" name="address_province" data-url="<?= admin_url( 'admin-ajax.php?action=get_regency&nonce='.$regencyNonce ) ?>" required>
                                            <option value="">- Pilih Provinsi -</option>
                                            <?php if( $province ) : ?>
                                                <?php foreach( $province as $item ) : ?>
                                                    <option value="<?= $item->province_name ?>"><?= $item->province_name ?></option>
                                                <?php endforeach; ?>
                                            <?php endif; ?>
                                        </select>
                                    </div>
                                    <div class="form-input-field">
                                        <?php $districtNonce = wp_create_nonce( 'get_district_nonce' ); ?>
                                        <label class="label" for="address-regency">Kabupaten / Kota<span class="required">*</span></label>
                                        <select class="input regency-ajax" id="address-regency" name="address_regency" data-url="<?= admin_url( 'admin-ajax.php?action=get_district&nonce='.$districtNonce ) ?>" required>
                                            <option value="">- Pilih Kabupaten / Kota -</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-input-fields">
                                    <?php $postcodeNonce = wp_create_nonce( 'get_postcode_nonce' ); ?>
                                    <div class="form-input-field">
                                        <label class="label" for="address-district">Kecamatan<span class="required">*</span></label>
                                        <select class="input district-ajax" id="address-district" name="address_district" data-url="<?= admin_url( 'admin-ajax.php?action=get_postcode&nonce='.$postcodeNonce ) ?>" required>
                                            <option value="">- Pilih Kecamatan -</option>
                                        </select>
                                    </div>
                                    <div class="form-input-field">
                                        <label class="label" for="address-postcode">Kode Pos<span class="required">*</span></label>
                                        <select class="input postcode-ajax" id="address-postcode" name="address_postcode" required>
                                            <option value="">- Pilih Kode Pos -</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-input-field">
                                    <label class="label label-main-address">
                                        <input type="checkbox" name="as_default_billing_address" value="yes">
                                        <span class="checkbox">
                                            <svg class="icon" role="img"><use xlink:href="<?= library_url() ?>/images/svg-symbols.svg#icon-check" /></svg>
                                        </span>
                                        <span class="text">Set as my main address</span>
                                    </label>
                                </div>
                            </div>
                            <div class="form-submit">
                                <?php wp_nonce_field( 'save_address_book' ); ?>
                                <input type="hidden" name="action" value="save_address_book" />
                                <button class="button button-gray" type="submit" name="save_address_book">Save Address</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php endif; ?>

<?php get_footer(); ?>
