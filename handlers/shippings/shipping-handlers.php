<?php

define( 'SHIPPING_INSURANCE_FEE_NAME', 'Shipping Insurance' );
define( 'SHIPPING_PACKAGING_FEE_NAME', 'Wood Packaging' );

/**
 * Enqueue "Shipping Insurance Flag" JS file
 */
add_action( 'wp_enqueue_scripts', 'shipping_insurance_flag_enqueue_scripts' );
function shipping_insurance_flag_enqueue_scripts() {
    wp_register_script( 'shipping-insurance-flag-script', get_stylesheet_directory_uri() . '/library/js/shipping-insurance-flag.js', array('jquery'), '', true );
    wp_localize_script(
        'shipping-insurance-flag-script', 'shipping_insurance_vars', ['ajaxurl' => admin_url( 'admin-ajax.php' ), 'redirect_url' => wc_get_checkout_url()]
    );
    wp_enqueue_script( 'shipping-insurance-flag-script' );
}

add_action( 'wp_ajax_update_shipping_insurance_flag', 'update_shipping_insurance_flag' );
add_action( 'wp_ajax_nopriv_update_shipping_insurance_flag', 'update_shipping_insurance_flag' );
function update_shipping_insurance_flag() {
    // Do a nonce check
    if( ! isset( $_POST['security'] ) || ! wp_verify_nonce( $_POST['security'], 'woocommerce-cart' ) ) {
        wp_send_json( array( 'nonce_fail' => 1 ) );
        exit;
    }

    $chosen_shipping = get_chosen_shipping_code_prefix();
    $has_insurance   = 0;
    $insurance_items = [];

    foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {

        $_product = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );

        if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 ) {

            $force_insurance = get_field( 'force_shipping_insurance', $_product->get_id() );
            $insurance_items[ $cart_item_key ] = 0;

            if( in_array( $chosen_shipping, ['jne', 'sicepat'] ) && ( isset( $_POST['shipping_insurance_flag'] ) && $_POST['shipping_insurance_flag'] == 1 ) || $force_insurance == 'yes' ) {
                $insurance_items[ $cart_item_key ] = 1;
                $has_insurance++;
            }

        }

    }

    if( isset( $_POST['shipping_insurance_flag'] ) && $_POST['shipping_insurance_flag'] == 1 ) {

        if( $insurance_items ) {
            foreach( $insurance_items as $cart_item_key => $is_checked ) {
                WC()->cart->cart_contents[$cart_item_key]['shipping_insurance'] = 1;
                WC()->cart->set_session();
            }
        }

        wc_add_notice( 'Berhasil mengaktifkan asuransi pengiriman.', 'success' );
        wp_send_json( array( 'success' => 1 ) );
        exit;

    } else {

        $has_checked = 0;

        if( $insurance_items ) {
            foreach( $insurance_items as $cart_item_key => $is_checked ) {
                if( $is_checked ) {
                    $has_checked++;
                } else {
                    WC()->cart->cart_contents[$cart_item_key]['shipping_insurance'] = 0;
                    WC()->cart->set_session();
                }
            }
        }

        if( $has_checked == 0 ) {
            wc_add_notice( 'Asuransi pengiriman berhasil dinonaktifkan.', 'success' );
        } else {
            wc_add_notice( 'Asuransi pengiriman tidak dapat dinonaktifkan.', 'success' );
        }

        wp_send_json( array( 'success' => 1 ) );
        exit;

    }
}

/**
 * Add Shipping Insurance rate to service fee.
 */
add_action( 'woocommerce_cart_calculate_fees','wc_add_shipping_insurance_rate_fee', 10, 1 );
function wc_add_shipping_insurance_rate_fee( $cart ) {
    if( is_admin() && ! defined( 'DOING_AJAX' ) ) {
        return;
    }

    $fee_name           = SHIPPING_INSURANCE_FEE_NAME;
    $chosen_shipping    = get_chosen_shipping_code_prefix();
    $has_insurance      = 0;
    $insurance_cost     = 0;
    $insurance_items    = [];

    foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {

        $_product = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );

        if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 ) {

            $force_insurance = get_field( 'force_shipping_insurance', $_product->get_id() );
            $insurance_items[ $cart_item_key ] = 0;

            if( $chosen_shipping == 'jne' ) {
                $insurance_cost += $cart_item['line_subtotal'] * JNE_INSURANCE_PERCENTAGE;
            } else {
                $insurance_cost += $cart_item['line_subtotal'] * SICEPAT_INSURANCE_PERCENTAGE;
            }

            if( ( isset( $cart_item['shipping_insurance'] ) && $cart_item['shipping_insurance'] == 1 ) || $force_insurance == 'yes' ) {

                $insurance_items[ $cart_item_key ] = 1;
                $has_insurance++;

            }

        }

    }

    if( in_array( $chosen_shipping, ['jne', 'sicepat'] ) && $has_insurance > 0 ) {
        if( $insurance_items ) {
            foreach( $insurance_items as $cart_item_key => $is_checked ) {
                WC()->cart->cart_contents[$cart_item_key]['shipping_insurance'] = 1;
                WC()->cart->set_session();
            }
        }

        if( $chosen_shipping == 'jne' ) {
            $insurance_cost += JNE_INSURANCE_ADMIN_FEE;
        }

        // WC()->cart->add_fee( $fee_name, round_nearest_hundred_up( $insurance_cost ) );
        WC()->cart->add_fee( $fee_name, $insurance_cost );
    }
}

/**
 * Enqueue "Shipping Packaging Flag" JS file
 */
add_action( 'wp_enqueue_scripts', 'shipping_packaging_flag_enqueue_scripts' );
function shipping_packaging_flag_enqueue_scripts() {
    wp_register_script( 'shipping-packaging-flag-script', get_stylesheet_directory_uri() . '/library/js/shipping-packaging-flag.js', array('jquery'), '', true );
    wp_localize_script(
        'shipping-packaging-flag-script', 'shipping_packaging_vars', ['ajaxurl' => admin_url( 'admin-ajax.php' ), 'redirect_url' => wc_get_checkout_url()]
    );
    wp_enqueue_script( 'shipping-packaging-flag-script' );
}

add_action( 'wp_ajax_update_shipping_packaging_flag', 'update_shipping_packaging_flag' );
add_action( 'wp_ajax_nopriv_update_shipping_packaging_flag', 'update_shipping_packaging_flag' );
function update_shipping_packaging_flag() {
    // Do a nonce check
    if( ! isset( $_POST['security'] ) || ! wp_verify_nonce( $_POST['security'], 'woocommerce-cart' ) ) {
        wp_send_json( array( 'nonce_fail' => 1 ) );
        exit;
    }

    $chosen_shipping = get_chosen_shipping_code_prefix();
    $has_packaging   = 0;
    $packaging_items = [];

    foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {

        $_product = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );

        if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 ) {

            $force_packaging = get_field( 'force_wood_packaging', $_product->get_id() );
            $packaging_items[ $cart_item_key ] = 0;

            if( in_array( $chosen_shipping, ['jne', 'sicepat'] ) && ( isset( $_POST['shipping_packaging_flag'] ) && $_POST['shipping_packaging_flag'] == 1 ) || $force_packaging == 'yes' ) {
                $packaging_items[ $cart_item_key ] = 1;
                $has_packaging++;
            }

        }

    }

    if( isset( $_POST['shipping_packaging_flag'] ) && $_POST['shipping_packaging_flag'] == 1 ) {

        if( $packaging_items ) {
            foreach( $packaging_items as $cart_item_key => $is_checked ) {
                WC()->cart->cart_contents[$cart_item_key]['wood_packaging'] = 1;
                WC()->cart->set_session();
            }
        }

        wc_add_notice( 'Berhasil mengaktifkan packing kayu.', 'success' );
        wp_send_json( array( 'success' => 1 ) );
        exit;

    } else {

        $has_checked = 0;

        if( $packaging_items ) {
            foreach( $packaging_items as $cart_item_key => $is_checked ) {
                if( $is_checked ) {
                    $has_checked++;
                } else {
                    WC()->cart->cart_contents[$cart_item_key]['wood_packaging'] = 0;
                    WC()->cart->set_session();
                }
            }
        }

        if( $has_checked == 0 ) {
            wc_add_notice( 'Packing kayu berhasil dinonaktifkan.', 'success' );
        } else {
            wc_add_notice( 'Packing tidak dapat dinonaktifkan.', 'success' );
        }

        wp_send_json( array( 'success' => 1 ) );
        exit;

    }
}

/**
 * Add Shipping Packaging rate to service fee.
 */
add_action( 'woocommerce_cart_calculate_fees','wc_add_shipping_packaging_rate_fee', 10, 1 );
function wc_add_shipping_packaging_rate_fee( $cart ) {
    if( is_admin() && ! defined( 'DOING_AJAX' ) ) {
        return;
    }

    $fee_name           = SHIPPING_PACKAGING_FEE_NAME;
    $chosen_shipping    = get_chosen_shipping_code_prefix();
    $has_packaging      = 0;
    $packaging_cost     = 0;
    $packaging_items    = [];
    $actual_weight      = 0; // only for jne
    $volumetric_weight  = 0; // only for jne

    foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {

        $_product = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
        $dimensions = get_product_dimensions( $_product );

        if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 ) {

            $force_packaging = get_field( 'force_wood_packaging', $_product->get_id() );
            $packaging_items[ $cart_item_key ] = 0;

            if( $chosen_shipping == 'jne' ) {
                $_product           = $cart_item['data'];
                $actual_weight      += calculate_actual_weight( $_product, $cart_item['quantity'] );
                $volumetric_weight  += calculate_volumetric_weight( $_product, $cart_item['quantity'] );
                $packaging_cost += 0;
            } else {
                $packaging_cost += sicepat_calculate_packaging_cost( $dimensions['length'], $dimensions['width'], $dimensions['height'] );
            }

            if( ( isset( $cart_item['wood_packaging'] ) && $cart_item['wood_packaging'] == 1 ) || $force_packaging == 'yes' ) {

                $packaging_items[ $cart_item_key ] = 1;
                $has_packaging++;

            }

        }

    }

    // khusus shipping jne, perhitungan packing kayu adalah ongkir dengan 2x total berat
    if( $chosen_shipping == 'jne' ) {
        $shipping_id    = get_chosen_shipping_code_subfix();
        //$weight         = shipping_set_weight( $actual_weight, $volumetric_weight ) * 2;
        $weight         = shipping_set_weight( $actual_weight, $volumetric_weight );
        $province       = WC()->customer->get_billing_state();
        $combineCity    = WC()->customer->get_billing_city();
        $destination    = jne_get_destination_code( $province, $combineCity );

        $jne_options    = get_field( 'jne_shipping_group', 'option' );
        $origin         = ! isset( $jne_options['jne_origin_code'] ) ? '' : $jne_options['jne_origin_code'];

        $jne_api_username   = is_production() ? JNE_API_USERNAME : JNE_API_USERNAME_DEV;
        $jne_api_key        = is_production() ? JNE_API_KEY : JNE_API_KEY_DEV;

        $api_jne        = new JNE_Shipping_API( $jne_api_username, $jne_api_key );
        $rate_new       = 0; // total shipping dari 2x berat
        $rate_old       = WC()->session->get('cart_totals')['shipping_total']; // total shipping berat asli

        if( $api_jne && $destination && $weight ) {
            $api_response   = $api_jne->tariff( $origin, $destination, $weight );

            if( isset( $api_response->price ) ) {
                foreach ( $api_response->price as $result ) {
                    if( $result->service_code == strtoupper( $shipping_id ) ) {
                        $rate_new = $result->price;
                    }
                }
            }

            // Biaya ongkir dg 2x berat diambil biaya ongkir 1x dg berat
            $packaging_cost = $rate_new;
        }
    }

    if( in_array( $chosen_shipping, ['jne', 'sicepat'] ) && $has_packaging > 0 ) {
        if( $packaging_items ) {
            foreach( $packaging_items as $cart_item_key => $is_checked ) {
                WC()->cart->cart_contents[$cart_item_key]['wood_packaging'] = 1;
                WC()->cart->set_session();
            }
        }

        WC()->cart->add_fee( $fee_name, round_nearest_hundred_up( $packaging_cost ) );
    }
}



function calculate_actual_weight( $_product, $qty ) {
    $weight = $_product->get_weight() * $qty;

    return $weight;
}

function calculate_volumetric_weight( $_product, $qty ) {
    $dimensions = get_product_dimensions( $_product );
    $length     = isset( $dimensions['length'] ) ? $dimensions['length'] : 0;
    $width      = isset( $dimensions['width'] ) ? $dimensions['width'] : 0;
    $height     = isset( $dimensions['height'] ) ? $dimensions['height'] : 0;
    $weight     = ( $length * $width * $height ) / 6000;

    return $weight * $qty;
}

function shipping_set_weight( $actual_weight, $volumetric_weight ) {
    $weight     = max( $actual_weight, $volumetric_weight );

    return wc_get_weight( $weight, 'kg' );
}