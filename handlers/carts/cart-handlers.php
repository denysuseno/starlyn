<?php
/**
 * Update shipping options.
 */
add_filter( 'woocommerce_add_cart_item_data', 'starlyn_save_field_value_to_cart_data', 10, 3 );
function starlyn_save_field_value_to_cart_data( $cart_item_data, $product_id, $variation_id ) {
    $use_wood_packaging         = 0;
    $use_shipping_insurance     = 0;

    $product                    = wc_get_product( $product_id );
    $force_wood_packaging       = get_field( 'force_wood_packaging', $product->get_id() );
    $force_shipping_insurance   = get_field( 'force_shipping_insurance', $product->get_id() );

    if ( ( isset( $_POST['wood_packaging'] ) && ! empty( $_POST['wood_packaging'] ) ) || $force_wood_packaging == 'yes' ) {
        $use_wood_packaging = 1;
    }

    if ( ( isset( $_POST['shipping_insurance'] ) && ! empty( $_POST['shipping_insurance'] ) ) || $force_shipping_insurance == 'yes' ) {
        $use_shipping_insurance = 1;
    }

    $cart_item_data['wood_packaging']       = $use_wood_packaging;
    $cart_item_data['shipping_insurance']   = $use_shipping_insurance;

    return $cart_item_data;

}

/**
 * Display "Packing Kayu", "Asuransi" on the pages: Cart, Checkout, Order Received
 */
// not user currently
// add_filter( 'woocommerce_get_item_data', 'starlyn_display_field', 10, 2 );
function starlyn_display_field( $item_data, $cart_item ) {

    if ( isset( $cart_item['wood_packaging'] ) && ! empty( $cart_item['wood_packaging'] ) ) {
        $item_data[] = array(
            'key'     => 'Packing Kayu',
            'value'   => $cart_item['wood_packaging'],
            'display' => '', // in case you would like to display "value" in another way (for users)
        );
    }

    if ( isset( $cart_item['wood_packaging'] ) && ! empty( $cart_item['shipping_insurance'] ) ) {
        $item_data[] = array(
            'key'     => 'Asuransi',
            'value'   => $cart_item['shipping_insurance'],
            'display' => '', // in case you would like to display "value" in another way (for users)
        );
    }

    return $item_data;

}

/**
 * Add the field value to the Order Item meta
 */
// sementara tdk dipakai dlu.
add_action( 'woocommerce_checkout_create_order_line_item', 'starlyn_add_order_item_meta', 10, 4 );
function starlyn_add_order_item_meta( $item, $cart_item_key, $values, $order ) {
    if ( isset( $values['wood_packaging'] ) && ! empty( $values['wood_packaging'] ) ) {
        $item->add_meta_data( 'Packing Kayu', $values['wood_packaging'] == 1 ? 'Yes' : 'No' );
    }

    if ( isset( $values['shipping_insurance'] ) && ! empty( $values['shipping_insurance'] ) ) {
        // $subtotal_insurance = calculate_insurance_rate( $values );

        $item->add_meta_data( 'Asuransi', $values['shipping_insurance'] == 1 ? 'Yes' : 'No' );
        // $item->add_meta_data( '_shipping_insurance_rate', $subtotal_insurance );
    }

}

function calculate_insurance_rate( $cart_item ) {
    $chosen_shipping_prefix = get_chosen_shipping_code_prefix();
    $rate = 0;

    if( $chosen_shipping_prefix == 'jne' ) {
        $rate = $cart_item['line_subtotal'] * JNE_INSURANCE_PERCENTAGE;
    } elseif( $chosen_shipping_prefix == 'sicepat' ) {
        $rate = $cart_item['line_subtotal'] * SICEPAT_INSURANCE_PERCENTAGE;
    }

    return $rate;
}

/**
 * Enqueue our JS file
 */
add_action( 'wp_enqueue_scripts', 'update_cart_item_enqueue_scripts' );
function update_cart_item_enqueue_scripts() {
    wp_register_script( 'update-cart-item-script', get_stylesheet_directory_uri() . '/library/js/update-cart-item-ajax.js', array('jquery'), '', true );
    wp_localize_script(
        'update-cart-item-script', 'vars', ['ajaxurl' => admin_url( 'admin-ajax.php' )]
    );
    wp_enqueue_script( 'update-cart-item-script' );
}

// tidak dipakai, tp biarkan utk referensi project lain.
add_action( 'wp_ajax_update_cart_item', 'update_cart_item' );
add_action( 'wp_ajax_nopriv_update_cart_item', 'update_cart_item' );
function update_cart_item() {
    // Do a nonce check
    if( ! isset( $_POST['security'] ) || ! wp_verify_nonce( $_POST['security'], 'woocommerce-cart' ) ) {
        wp_send_json( array( 'nonce_fail' => 1 ) );
        exit;
    }

    // Save the notes to the cart meta
    $cart               = WC()->cart->cart_contents;
    $cart_id            = $_POST['cart_id'];
    $cart_item          = $cart[$cart_id];

    if( isset( $_POST['wood_packaging'] ) ) {
        $wood_packaging = $_POST['wood_packaging'];
        $cart_item['wood_packaging'] = $wood_packaging;
    }

    if( isset( $_POST['shipping_insurance'] ) ) {
        $shipping_insurance = $_POST['shipping_insurance'];
        $cart_item['shipping_insurance'] = $shipping_insurance;
    }

    WC()->cart->cart_contents[$cart_id] = $cart_item;
    WC()->cart->set_session();

    wc_add_notice( 'Cart updated.', 'success' );

    wp_send_json( array( 'success' => 1 ) );
    exit;
}