<?php

require_once('barcode.php');

// create order-barcode directory in uploads dir if not exists
function create_uploads_order_barcode_dir_if_not_exists( $upload_dir, $year, $month, $day ) {
    if( ! file_exists(  $upload_dir['basedir'].'/'.ORDER_BARCODE_DIR_NAME ) ) {
        mkdir( $upload_dir['basedir'].'/'.ORDER_BARCODE_DIR_NAME, 755, true );
    }

    if( ! file_exists(  $upload_dir['basedir'].'/'.ORDER_BARCODE_DIR_NAME.'/'.$year ) ) {
        mkdir( $upload_dir['basedir'].'/'.ORDER_BARCODE_DIR_NAME.'/'.$year, 755, true );
    }

    if( ! file_exists(  $upload_dir['basedir'].'/'.ORDER_BARCODE_DIR_NAME.'/'.$year.'/'.$month ) ) {
        mkdir( $upload_dir['basedir'].'/'.ORDER_BARCODE_DIR_NAME.'/'.$year.'/'.$month, 755, true );
    }

    if( ! file_exists(  $upload_dir['basedir'].'/'.ORDER_BARCODE_DIR_NAME.'/'.$year.'/'.$month.'/'.$day ) ) {
        mkdir( $upload_dir['basedir'].'/'.ORDER_BARCODE_DIR_NAME.'/'.$year.'/'.$month.'/'.$day, 755, true );
    }
}

// create waybill-barcode directory in uploads dir if not exists
function create_uploads_waybill_barcode_dir_if_not_exists( $upload_dir, $year, $month, $day ) {
    if( ! file_exists(  $upload_dir['basedir'].'/'.WAYBILL_BARCODE_DIR_NAME ) ) {
        mkdir( $upload_dir['basedir'].'/'.WAYBILL_BARCODE_DIR_NAME, 755, true );
    }

    if( ! file_exists(  $upload_dir['basedir'].'/'.WAYBILL_BARCODE_DIR_NAME.'/'.$year ) ) {
        mkdir( $upload_dir['basedir'].'/'.WAYBILL_BARCODE_DIR_NAME.'/'.$year, 755, true );
    }

    if( ! file_exists(  $upload_dir['basedir'].'/'.WAYBILL_BARCODE_DIR_NAME.'/'.$year.'/'.$month ) ) {
        mkdir( $upload_dir['basedir'].'/'.WAYBILL_BARCODE_DIR_NAME.'/'.$year.'/'.$month, 755, true );
    }

    if( ! file_exists(  $upload_dir['basedir'].'/'.WAYBILL_BARCODE_DIR_NAME.'/'.$year.'/'.$month.'/'.$day ) ) {
        mkdir( $upload_dir['basedir'].'/'.WAYBILL_BARCODE_DIR_NAME.'/'.$year.'/'.$month.'/'.$day, 755, true );
    }
}

// generate barcode order id when creating order.
add_action( 'woocommerce_checkout_order_processed', 'generate_barcode_order_number', 10, 3 );
function generate_barcode_order_number( $order_id, $posted_data, $order ) {
    $barcodeText    = $order_id;
    $barcodeSize    = '30';
    $barcodeType    = 'code39'; // code39 is used code which used in sicepat
    $barcodeDisplay = 'horizontal';
    $printText      = false;
    $upload_dir     = wp_get_upload_dir();
    $date           = date_i18n( 'Y-m-d H:i:s' );
    $year           = date_i18n( 'Y', strtotime( $date ) );
    $month          = date_i18n( 'm', strtotime( $date ) );
    $day            = date_i18n( 'd', strtotime( $date ) );
    $date_merge     = $year . '/' . $month . '/' . $day;
    $unique_id      = uniqid();
    $ext            = '.png';
    $filename       = sprintf( 'order-%d-%s%s', $order_id, $unique_id, $ext );
    $filename_full  = $date_merge.'/'.$filename;
    $filepath       = $upload_dir['basedir'].'/'.ORDER_BARCODE_DIR_NAME.'/'.$filename_full;

    create_uploads_order_barcode_dir_if_not_exists( $upload_dir, $year, $month, $day );

    // store order meta
    update_post_meta( $order_id, 'order_barcode_filename_full', $filename_full );
    update_post_meta( $order_id, 'order_barcode_filename', $filename );

    if( ! file_exists( $filepath ) ) {
        barcode( $filepath, $barcodeText, $barcodeSize, $barcodeDisplay, $barcodeType, $printText );

        chmod( $upload_dir['basedir'].'/'.ORDER_BARCODE_DIR_NAME.'/'.$year.'/'.$month.'/'.$day.'/'.$filename, 0664 );
        chown( $upload_dir['basedir'].'/'.ORDER_BARCODE_DIR_NAME.'/'.$year.'/'.$month.'/'.$day.'/'.$filename, USER_WEBSPACE );
        chgrp( $upload_dir['basedir'].'/'.ORDER_BARCODE_DIR_NAME.'/'.$year.'/'.$month.'/'.$day.'/'.$filename, USER_GROUP );
    }
}

// generate barcode awb no when update awb no
add_action( 'jne_submit_awb_success', 'generate_barcode_awb_number', 10, 2 );
add_action( 'sicepat_request_pickup_success', 'generate_barcode_awb_number', 10, 2 );
function generate_barcode_awb_number( $order_id, $awb_no ) {
    $barcodeText    = $awb_no;
    $barcodeSize    = '30';
    $barcodeType    = 'code39'; // code39 is used code which used in sicepat
    $barcodeDisplay = 'horizontal';
    $printText      = false;
    $upload_dir     = wp_get_upload_dir();
    $date           = date_i18n( 'Y-m-d H:i:s' );
    $year           = date_i18n( 'Y', strtotime( $date ) );
    $month          = date_i18n( 'm', strtotime( $date ) );
    $day            = date_i18n( 'd', strtotime( $date ) );
    $date_merge     = $year . '/' . $month . '/' . $day;
    $unique_id      = uniqid();
    $ext            = '.png';
    $filename       = sprintf( 'waybill-%d-%s-%s%s', $order_id, $awb_no, $unique_id, $ext );
    $filename_full  = $date_merge.'/'.$filename;
    $filepath       = $upload_dir['basedir'].'/'.WAYBILL_BARCODE_DIR_NAME.'/'.$filename_full;

    create_uploads_waybill_barcode_dir_if_not_exists( $upload_dir, $year, $month, $day );

    // store order meta
    update_post_meta( $order_id, 'waybill_barcode_filename_full', $filename_full );
    update_post_meta( $order_id, 'waybill_barcode_filename', $filename );

    if( ! file_exists( $filepath ) ) {
        barcode( $filepath, $barcodeText, $barcodeSize, $barcodeDisplay, $barcodeType, $printText );

        if( file_exists( $filepath ) ) {
            chmod( $filepath, 0664 );
            chown( $filepath, USER_WEBSPACE );
            chgrp( $filepath, USER_GROUP );
        }
    }
}

// pdf packing slips
add_action( 'wpo_wcpdf_after_document_label', 'generate_packing_slips_barcode', 10, 2 );
function generate_packing_slips_barcode( $type, WC_Order $order ) {
    if( $type != 'packing-slip' ) {
        return;
    }

    $order_id               = $order->get_order_number();
    $upload_dir             = wp_get_upload_dir();

    $order_barcode_text     = $order_id;
    $order_filename_full    = get_post_meta( $order_id, 'order_barcode_filename_full', true );
    $order_filepath         = $upload_dir['basedir'].'/'.ORDER_BARCODE_DIR_NAME.'/'.$order_filename_full; // use basedir

    $waybill_barcode_text   = get_post_meta( $order_id, '_awb_no', true );
    $waybill_filename_full  = get_post_meta( $order_id, 'waybill_barcode_filename_full', true );
    $waybill_filepath       = $upload_dir['basedir'].'/'.WAYBILL_BARCODE_DIR_NAME.'/'.$waybill_filename_full; // use basedir

    echo '<img class="barcode" alt="' . $order_barcode_text . '" src="'.$order_filepath.'"/>';
    echo '<img class="barcode" alt="' . $waybill_barcode_text . '" src="'.$waybill_filepath.'"/>';
}