<?php

add_action( 'woocommerce_init', 'save_data' );
function save_data() {

    if( is_user_logged_in() ) {

        if( isset( $_POST['action'] ) && 'save_address_book' == $_POST['action'] && !empty( $_POST['_wpnonce'] ) && wp_verify_nonce( $_POST['_wpnonce'], 'save_address_book' ) ) {

            $addrName       = isset( $_POST['address_name'] ) && $_POST['address_name'] ? $_POST['address_name'] : '';
            $addrPhone      = isset( $_POST['address_phone'] ) && $_POST['address_phone'] ? $_POST['address_phone'] : '';
            $addrLabel      = isset( $_POST['address_address_name'] ) && $_POST['address_address_name'] ? $_POST['address_address_name'] : '';
            $addrAddress    = isset( $_POST['address_address'] ) && $_POST['address_address'] ? $_POST['address_address'] : '';
            $addrProvince   = isset( $_POST['address_province'] ) && $_POST['address_province'] ? $_POST['address_province'] : '';
            $addrRegency    = isset( $_POST['address_regency'] ) && $_POST['address_regency'] ? $_POST['address_regency'] : '';
            $addrDistrict   = isset( $_POST['address_district'] ) && $_POST['address_district'] ? $_POST['address_district'] : '';
            $addrPostcode   = isset( $_POST['address_postcode'] ) && $_POST['address_postcode'] ? $_POST['address_postcode'] : '';
            $addrIsMain     = isset( $_POST['as_default_billing_address'] ) && $_POST['as_default_billing_address'] ? $_POST['as_default_billing_address'] : '';

            $haveMainAddress= get_my_main_address_book( get_current_user_id() );
            if( !$haveMainAddress ) {
                $_POST['as_default_billing_address'] = 'yes';
            }

            $redirectAfterSuccess   = isset( $_POST['redirect_after_success'] ) && $_POST['redirect_after_success'] ? $_POST['redirect_after_success'] : wc_get_account_endpoint_url( 'edit-address' );
            $redirectAfterFailed    = isset( $_POST['redirect_after_failed'] ) && $_POST['redirect_after_failed'] ? $_POST['redirect_after_failed'] : wc_get_account_endpoint_url( 'edit-address/shipping' );

            $url = add_query_arg( [
                'address_name'      => $addrName,
                'address_phone'     => $addrPhone,
                'address_label'     => $addrLabel,
                'address_address'   => $addrAddress,
                'address_province'  => $addrProvince,
                'address_regency'   => $addrRegency,
                'address_district'  => $addrDistrict,
                'address_postcode'  => $addrPostcode,
                'address_is_main'   => $addrIsMain,
            ],
                esc_url( $redirectAfterFailed )
            );

            $hasErrors = validate_address();

            if( ! $hasErrors ) {

                do_action('u_add_new_address_book');
                wc_add_notice(__('Address book added successfully.', 'woocommerce'));
                $url = esc_url( $redirectAfterSuccess );

            }

            wp_safe_redirect( $url );
            exit;

        }

        if( isset( $_POST['action'] ) && 'update_address_book' == $_POST['action'] && !empty( $_POST['_wpnonce'] ) && wp_verify_nonce( $_POST['_wpnonce'], 'update_address_book' ) ) {

            $url = add_query_arg( [
                'address' => isset( $_POST['address_id'] ) && $_POST['address_id'] ? $_POST['address_id'] : ''
            ],
                wc_get_account_endpoint_url( 'edit-address/shipping' )
            );

            $hasErrors = validate_address();

            $haveMainAddress= get_my_main_address_book( get_current_user_id(), $_POST['address_id'] );
            if( !$haveMainAddress ) {
                $_POST['as_default_billing_address'] = 'yes';
            }

            if( ! $hasErrors ) {

                WC_woocommerce_addressbook::update_addressbook($_POST['address_id']);
                wc_add_notice(__('Address book changed successfully.', 'woocommerce'));
                $url = esc_url( wc_get_account_endpoint_url( 'edit-address' ) );

            }

            wp_safe_redirect( $url );
            exit;

        }

    }

}

function validate_address() {

    $hasError = false;

    // address name
    if( empty( $_POST['address_name'] ) ) {
        $hasError = true;
        wc_add_notice( sprintf( '%s wajib diisi', ( 'Nama' ) ) );
    }

    // phone
    if( empty( $_POST['address_phone'] ) ) {
        $hasError = true;
        wc_add_notice( sprintf( '%s wajib diisi', ( 'Nomor Telepon' ) ) );
    }

    // address
    if( empty( $_POST['address_address'] ) ) {
        $hasError = true;
        wc_add_notice( sprintf( '%s wajib diisi', ( 'Alamat' ) ) );
    }

    // province
    if( empty( $_POST['address_province'] ) ) {
        $hasError = true;
        wc_add_notice( sprintf( '%s wajib diisi', ( 'Provinsi' ) ) );
    }

    // regency
    if( empty( $_POST['address_regency'] ) ) {
        $hasError = true;
        wc_add_notice( sprintf( '%s wajib diisi', ( 'Kotamadya' ) ) );
    }

    // district
    if( empty( $_POST['address_district'] ) ) {
        $hasError = true;
        wc_add_notice( sprintf( '%s wajib diisi', ( 'Daerah' ) ) );
    }

    return $hasError;

}

/**
 * Ajax
 */
add_action("wp_ajax_get_regency", "get_regency");
add_action("wp_ajax_nopriv_get_regency", "get_regency");
function get_regency() {

    if ( !wp_verify_nonce( $_REQUEST['nonce'], "get_regency_nonce")) {
        exit("Failed when validating nonce");
    }

    $province = $_REQUEST['province'];

    $data = get_regency_by_province( $province );

    $regency = [];

    if( $data ) {
        foreach ( $data as $item ) {
            $regency_name = trim( $item->regency_name );
            $regency[$regency_name] = $regency_name;
        }
    }

    echo json_encode( $regency ); exit;

}

add_action("wp_ajax_get_district", "get_district");
add_action("wp_ajax_nopriv_get_district", "get_district");
function get_district() {

    if ( !wp_verify_nonce( $_REQUEST['nonce'], "get_district_nonce")) {
        exit("Failed when validating nonce");
    }

    $regency = $_REQUEST['regency'];

    $data = get_district_by_regency( $regency );

    $district = [];

    if( $data ) {
        foreach ( $data as $item ) {
            if( $item->postcode != 0 || $item->postcode != '' ) {
                $district[$item->district_name] = [
                    'district_name' => $item->district_name,
                    'postcode'      => $item->postcode
                ];
            }
        }
    }

    echo json_encode( $district ); exit;

}

add_action("wp_ajax_get_postcode", "get_postcode");
add_action("wp_ajax_nopriv_get_postcode", "get_postcode");
function get_postcode() {

    if ( !wp_verify_nonce( $_REQUEST['nonce'], "get_postcode_nonce")) {
        exit("Failed when validating nonce");
    }

    $regency = $_REQUEST['regency'];
    $district = $_REQUEST['district'];

    $data = get_postcodes( $regency, $district );

    $postcodes = [];

    if( $data ) {
        foreach ( $data as $item ) {
            $postcodes[$item->postcode] = $item->postcode;
        }
    }

    echo json_encode( $postcodes ); exit;

}

add_action("wp_ajax_ajax_delete_address_book", "ajax_delete_address_book");
add_action("wp_ajax_nopriv_ajax_delete_address_book", "ajax_delete_address_book");
function ajax_delete_address_book() {

    if ( !wp_verify_nonce( $_REQUEST['nonce'], "ajax_delete_address_book")) {
        exit("Failed when validating nonce");
    }

    $id = $_REQUEST['id'];

    $address = get_address_by_id( $id );

    if( $address->as_default_billing_address ) {
        wc_add_notice(__('Cannot delete main address.', 'woocommerce'));
        echo true; exit;
    }

    delete_address_by_id( $id );

    do_action( 'u_delete_address_book' );
    wc_add_notice(__('Address book deleted successfully.', 'woocommerce'));

    echo true; exit;
}

add_action("wp_ajax_set_main_address_book", "set_main_address_book");
add_action("wp_ajax_nopriv_set_main_address_book", "set_main_address_book");
function set_main_address_book() {

    if ( !wp_verify_nonce( $_REQUEST['nonce'], "ajax_set_main_address_book")) {
        exit("Failed when validating nonce");
    }

    $id = $_POST['address_id'];

    WC_woocommerce_addressbook::update_addressbook($id);

    wc_add_notice(__('Set main address successfully.', 'woocommerce'));

    echo true; exit;
}

add_action("wp_ajax_get_main_address_book", "get_main_address_book");
add_action("wp_ajax_nopriv_get_main_address_book", "get_main_address_book");
function get_main_address_book() {
    if ( !wp_verify_nonce( $_REQUEST['nonce'], "ajax_get_main_address_book_nonce")) {
        exit("Failed when validating nonce");
    }

    $user_id = isset( $_GET['user_id'] ) && $_GET['user_id'] ? $_GET['user_id'] : null;

    $data = get_my_main_address_book( $user_id );

    if( $data && isset( $data[0] ) ) {
        echo json_encode( $data[0] ); exit;
    }

    echo null; exit;
}