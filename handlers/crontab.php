<?php
// wp_clear_scheduled_hook('get_delivered_order_date');

/**
 * Add cutom recurrence cron.
 */
add_filter( 'cron_schedules', 'my_cron_schedules' );
function my_cron_schedules( $schedules ) {

    if( ! isset( $schedules['1min'] ) ) {
        $schedules['1min'] = [
            'interval'  => 60,
            'display'   => __( 'Once every 1 minutes' ),
        ];
    }

    if( ! isset( $schedules['5min'] ) ) {
        $schedules['5min'] = [
            'interval'  => 5 * 60,
            'display'   => __( 'Once every 5 minutes' ),
        ];
    }

    if( ! isset( $schedules['30min'] ) ) {
        $schedules['30min'] = [
            'interval'  => 30 * 60,
            'display'   => __( 'Once every 30 minutes' ),
        ];
    }

    return $schedules;

}

if( ! wp_next_scheduled( 'get_delivered_order_date' ) ) {

    wp_schedule_event( time(), 'daily', 'get_delivered_order_date' );

}

/**
 * Get delivered date from tracking order.
 */
add_action( 'get_delivered_order_date', 'get_delivered_order_date_func' );
function get_delivered_order_date_func() {
    error_log( 'get_delivered_order_date_func is running..' );

    // send email for checking wheter cron is working
    wp_mail( 'd.suseno@icubic.co.id', 'CRON TEST', 'CRON IS RUNNING' );

    global $wpdb;

    // get shipped order ids
    $order_ids  = get_order_shipped_ids();
    error_log( 'Shipped Order Ids: ' . json_encode( $order_ids ) );

    // get awb no from shipped orders
    $order_awb  = get_orders_awb_no( $order_ids );
    error_log( 'Shipped Order Awb\'s: ' . json_encode( $order_awb ) );

    // tracking
    if( $order_awb ) {
        foreach( $order_awb as $order_id => $awb_no ) {
            $shipping_service   = get_post_meta( $order_id, '_shipping_service', true );
            $delivered_date     = null;

            if( $shipping_service == 'jne' ) {
                error_log( 'shipping service jne cron tracking is running..' );
                $tracking = jne_tracking( $awb_no );

                if( $tracking->is_success && $tracking->last_status && isset( $tracking->last_status['desc'] ) && $tracking->last_status['desc'] && stripos( $tracking->last_status['desc'], 'DELIVERED' ) !== false ) {
                    error_log( 'delivered date jne is running..' );
                    $delivered_date = date_i18n( 'Y-m-d H:i', strtotime( $tracking->last_status['date'] ) );
                }
            } elseif( $shipping_service == 'sicepat' ) {
                error_log( 'shipping service sicepat cron tracking is running..' );
                $tracking = sicepat_tracking( $awb_no );

                if( $tracking->is_success && $tracking->last_status->status == 'DELIVERED' ) {
                    error_log( 'delivered date sicepat is running..' );
                    $delivered_date = date_i18n( 'Y-m-d H:i', strtotime( $tracking->last_status->date_time ) );
                }
            }

            if( $delivered_date ) {
                update_post_meta( $order_id, '_delivered_at', $delivered_date );

                $delivered_dt   = new DateTime( $delivered_date );
                $now_dt         = new DateTime( date( 'Y-m-d H:i' ) );
                $date_diff      = $delivered_dt->diff( $now_dt );
                $diff_day       = $date_diff->format( '%r%a' );

                error_log( 'AWB ' . $awb_no . ' delivered date : ' . $delivered_date );

                if( abs( $diff_day ) >= 2 ) {
                    error_log( 'update status from Shipped to Completed' );
                    $order = new WC_Order( $order_id );
                    $order->update_status( 'completed', 'Autocompleted order' );
                }
            }
        }
    }

}

function get_order_shipped_ids() {

    global $wpdb;

    // only shipped order
    $post_status    = implode( "','", array( 'wc-shipped' ) );

    // setup query
    $query          = "SELECT id FROM $wpdb->posts WHERE post_type = 'shop_order' AND post_status IN ('{$post_status}') ";

    // get results
    $results        = $wpdb->get_results( $query, ARRAY_A );

    // get ids from results
    $ids            = array_unique( wp_list_pluck( $results, 'id' ) );
    $ids            = array_map( 'intval', $ids );

    return $ids;

}

function get_orders_awb_no( $order_ids ) {

    if( ! $order_ids ) {
        return;
    }

    $awb_no = [];
    foreach( $order_ids as $order_id ) {
        $awb = get_post_meta( $order_id, '_awb_no', true );

        if( $awb ) {
            $awb_no[ $order_id ] = $awb;
        }
    }

    return $awb_no;

}

//get_delivered_order_date_func();
add_action( 'woocommerce_after_register_post_type', 'testing' );
function testing() {
    //get_delivered_order_date_func();
}