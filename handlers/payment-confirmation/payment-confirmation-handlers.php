<?php

define( 'COOKIE_LIMIT', 30 );
define( 'PAYMENT_CONFIRMATION_MAX_UPLOAD_SIZE', 5242880 );

/**
 * Handle Payment Confirmation form
 */
add_action( 'init', 'pay_confirm_form_action_hook');
function pay_confirm_form_action_hook() {

    if( isset( $_POST['action'] ) && $_POST['action'] == 'payment_confirmation' ) {

        if ( isset( $_POST['payment_confirmation_nonce'] ) &&
            wp_verify_nonce( $_POST['payment_confirmation_nonce'], 'payment_confirmation_action' ) ) {

            $order_no       = sanitize_text_field( $_POST['order_no'] );
            $payment_date   = sanitize_text_field( $_POST['payment_date'] );
            $account_name   = sanitize_text_field( $_POST['account_name'] );
            $bank           = sanitize_text_field( $_POST['bank'] );
            $notes          = sanitize_text_field( $_POST['notes'] );
            $file           = $_FILES['attachment_file'];

            $order          = wc_get_order( $order_no );

            $errors         = do_validate_all( $order, $order_no, $payment_date, $account_name, $bank, $file );

            // buat validasi:
            // 1. check apakah no order tsb sudah pernah lakukan konfirmasi pemabayaran.
            // 2. order status harus pending atau on-hold, selain itu tdk bisa lakukan konfirmasi pembayaran.

            if( $errors['has_error'] ) {
                WC()->session->set( 'pc_order_no', $order_no );
                WC()->session->set( 'pc_payment_date', $payment_date );
                WC()->session->set( 'pc_account_name', $account_name );
                WC()->session->set( 'pc_bank', $bank );
                WC()->session->set( 'pc_notes', $notes );

                $msg = implode(", ", $errors['messages']);
                notice_and_redirect( $msg, 'error' );
            } else {
                // safe to process
                $args = [
                    'payment_date'          => $payment_date,
                    'account_name'          => $account_name,
                    'bank'                  => $bank,
                    'notes'                 => $notes,
                    'has_submit_pay_confirm'=> true,
                ];

                sn_update_order_meta( $order_no, $args );
                sn_upload_file_acf( $order_no, $file );

                WC()->session->__unset( 'pc_order_no' );
                WC()->session->__unset( 'pc_payment_date' );
                WC()->session->__unset( 'pc_account_name' );
                WC()->session->__unset( 'pc_bank' );
                WC()->session->__unset( 'pc_notes' );

                do_action( 'send_payment_confirm_mail_to_admin', $order_no );

                // update order status to Verification Needed
                $order->update_status( 'payment-verify', 'Customer submit payment confirmation.' );

                notice_and_redirect('Terima kasih telah melakukan pembayaran. Bukti transfer telah kami terima, dan akan dilakukan verifikasi lebih lanjut untuk order Anda dalam 1x24 jam di Hari kerja.', 'error' );
            }
            
        }

    }

}

add_action( 'send_payment_confirm_mail_to_admin', 'payment_confirm_mail', 10, 1 );
function payment_confirm_mail( $order_no ) {
    $to                 = get_option( 'admin_email' );
    $subject            = sprintf( 'Payment Confirmation #%d', $order_no );
    $headers            = array('From: '.get_option( 'blogname' ).' <'.get_option( 'admin_email' ).'>');

    $attachment         = get_field( 'payment_confirm_attachment', $order_no, true );
    $order_detail_url   = sprintf( '%s/wp-admin/post.php?post=%d&action=edit', home_url() ,$order_no );

    $message            = sprintf( '<p>Hello Admin, you have a new payment confirmation for order <strong>#%d</strong></p>', $order_no );
    $message            .= '<p>Click the link below for more details:</p>';
    $message            .= sprintf( '<p><a href="%s">%s</a></p>', $order_detail_url, $order_detail_url );

    add_filter( 'wp_mail_content_type', 'payment_confirm_mail_content_type' );

    wp_mail( $to, $subject, $message, $headers, array( $attachment ));

    // Reset content-type to avoid conflicts -- https://core.trac.wordpress.org/ticket/23578
    remove_filter( 'wp_mail_content_type', 'payment_confirm_mail_content_type' );
}

function payment_confirm_mail_content_type() {
    return 'text/html';
}

function do_validate_all( $order, $order_no, $payment_date, $account_name, $bank, $file ) {

    $has_error      = false;
    $error_message  = [];

    // validate order no
    if( empty( $order_no ) || is_null( $order_no ) || '' == $order_no ) {
        $has_error       = true;
        $error_message[] = 'Nomor Order harus diisi';
    }

    // validate payment date
    if( empty( $payment_date ) || is_null( $payment_date ) || '' == $payment_date ) {
        $has_error       = true;
        $error_message[] = 'Tanggal pembayaran harus diisi';
    }

    // validate account name
    if( empty( $account_name ) || is_null( $account_name ) || '' == $account_name ) {
        $has_error       = true;
        $error_message[] = 'Nama pemilik rekening harus diisi';
    }

    // validate bank
    if( empty( $bank ) || is_null( $bank ) || '' == $bank ) {
        $has_error       = true;
        $error_message[] = 'Bank harus diisi';
    }

    // validate order exists
    if( ! $order ) {
        $has_error       = true;
        $error_message[] = 'Nomor Order tidak ditemukan';
    }

    // validate file upload
    switch ( $file['error'] ) {
        case 1:
            $has_error = true;
            $error_message[] = 'Gagal menggungah file. Ukuran file tidak boleh melebihi 5 MB';
            break;
        case 2:
            $has_error = true;
            $error_message[] = 'Ukuran file harus kurang dari 5 MB';
            break;
        case 3:
            $has_error = true;
            $error_message[] = 'The uploaded file was only partially uploaded';
            break;
        case 4:
            $has_error = true;
            $error_message[] = 'No file was uploaded';
            break;
    }

    // validate file extensions
    $allowed_ext = ['pdf', 'jpeg', 'jpg', 'png'];
    $filename    = $file['name'];
    $file_info   = pathinfo( $filename );

    if ( isset( $file_info['extension'] ) && ! in_array( $file_info['extension'], $allowed_ext ) ) {
        $has_error = true;
        $error_message[] = 'Tipe file harus ' . implode( ', ', $allowed_ext );
    }

    // validate max file size
    if( $file['size'] > PAYMENT_CONFIRMATION_MAX_UPLOAD_SIZE ) {
        $has_error = true;
        $error_message[] = 'Ukuran file terlalu besar. Ukuran file tidak boleh lebih dari 5 MB';
    }

    if( $order ) {
        // check if has submit payment confirmation
        $has_submitted = get_field( 'has_submit_pay_confirm', $order_no );
        if( $has_submitted ) {
            $has_error = true;
            $error_message[] = 'Anda sudah pernah melakukan konfirmasi pembayaran sebelumnya';
        }

        // check order status
        if( ! in_array( $order->get_status(), ['pending', 'on-hold'] ) ) {
            $has_error = true;
            $error_message[] = 'Anda hanya dapat melakukan konfirmasi pembayaran untuk pesanan yang masih <i>Pending/On-Hold</i> saja';
        }
    }


    $result = [
        'has_error' => $has_error,
        'messages'  => $error_message
    ];

    return $result;

}

function sn_update_order_meta( $order_no, $args ) {
    if( $order_no && $args ) {
        foreach( $args as $key => $value ) {
            update_post_meta( $order_no, $key, $value ); // store to order
        }
    }
}

function sn_upload_file_acf( $order_no, $files ) {
    $attach_id = get_image_attach_id( $order_no, $files );

    // Saving image
    update_field('payment_confirm_attachment', $attach_id, $order_no );
}

function get_image_attach_id( $order_no, $files ) {

    // filename format : payment-confirm-{order-no}-{uniqid}-{original-filename}
    $fname          = sprintf( 'payment-confirm-%d-%s-%s', $order_no, uniqid(), $files['name'] );
    $upload         = ! empty( $files['name'] ) && !empty( $files['tmp_name'] ) ? wp_upload_bits( $fname, null, file_get_contents( $files['tmp_name'] ) ) : [];
    $filename       = $upload['file'];
    $wp_filetype    = wp_check_filetype( $filename, null );

    $attachment = array(
        'post_mime_type' => $wp_filetype['type'],
        'post_title'     => time() . '_' . $files['name'],
        'post_content'   => '',
        'post_status'    => 'inherit'
    );

    $attach_id = wp_insert_attachment( $attachment, $filename, $order_no );

    require_once( ABSPATH . 'wp-admin/includes/image.php' );

    $attach_data = wp_generate_attachment_metadata( $attach_id, $filename );

    wp_update_attachment_metadata( $attach_id, $attach_data );

    /*// move to uploads directory
    if( isset( $files['name'] ) && isset( $files['tmp_name'] ) ) {
        $path           = time() . '_' . $files['name'];
        $tmp            = $files['tmp_name'];
        $upload_dir     = wp_upload_dir();
        $destination    = $upload_dir['basedir'].'/payment-confirmation/'.$path;

        if( $path ) {
            move_uploaded_file( $tmp, $destination );
        }
    }*/

    return $attach_id;
}

function notice_and_redirect( $msg, $type ) {
    wc_add_notice( $msg, $type );
    $link = get_permalink( get_page_by_path( 'payment-confirmation' ) );
    wp_safe_redirect( $link );
}