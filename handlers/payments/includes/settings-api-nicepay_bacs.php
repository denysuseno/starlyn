<?php
add_filter( 'woocommerce_settings_api_form_fields_bacs', 'add_wc_settings_payments_field_bacs', 10, 1);
function add_wc_settings_payments_field_bacs( $fields ) {
    $options = get_payment_method_groups_pluck();
    $fields['group'] = array(
        'title'       => __( 'Group', 'woocommerce' ),
        'type'        => 'select',
        'description' => __( 'Select payment group', 'woocommerce' ),
        'default'     => '',
        'desc_tip'    => true,
        'options'     => $options
    );

    return $fields;
}

add_filter( 'woocommerce_bacs_icon', 'custom_bacs_icon' );
function custom_bacs_icon() {
    $logo = get_field( 'payment_bank_transfer_logo', 'option' );

    return $logo;
}