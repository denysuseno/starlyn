<?php
include 'includes/settings-api-nicepay_bacs.php';

function starlyn_checkout_payment() {
    if ( WC()->cart->needs_payment() ) {
        $available_gateways = WC()->payment_gateways()->get_available_payment_gateways();
        WC()->payment_gateways()->set_current_gateway( $available_gateways );
    } else {
        $available_gateways = array();
    }

    $payment_method_groups = get_active_payment_method_groups();

    if ( ! empty( $available_gateways ) ) {
        foreach ( $available_gateways as $gateway ) {
            if( isset( $gateway->settings['group'] ) && isset( $payment_method_groups[ $gateway->settings['group'] ] ) ) {
                $payment_method_groups[ $gateway->settings['group'] ]['payments'][] = $gateway;
            }
        }
    }

    wc_get_template(
        'checkout/payment-custom.php',
        array(
            'checkout'              => WC()->checkout(),
            'payment_method_groups' => $payment_method_groups,
            'available_gateways'    => $available_gateways,
            'order_button_text'     => apply_filters( 'woocommerce_order_button_text', __( 'Place order', 'woocommerce' ) ),
        )
    );
}

function get_payment_method_groups_option() {
    $pay_method_groups      = get_field( 'pay_method_groups', 'option' );
    $payment_method_groups  = [];

    if( $pay_method_groups ) {
        foreach ( $pay_method_groups as $group ) {
            $payment_method_groups[ $group['pay_method_group_key'] ] = $group;
        }
    }

    return $payment_method_groups;
}

function get_active_payment_method_groups() {
    $pay_method_groups      = get_payment_method_groups_option();
    $active_payment_groups  = [];

    if( $pay_method_groups ) {
        foreach ( $pay_method_groups as $group_key => $group ) {
            if( $group['pay_method_group_status'] == 'active' ) {
                $active_payment_groups[ $group_key ] = $group;
            }
        }
    }

    return $active_payment_groups;
}

function get_payment_method_groups_pluck() {
    $groups     = get_payment_method_groups_option();
    $options    = ['' => '- Select Group -'];

    if( $groups ) {
        foreach ( $groups as $group_key => $group ) {
            $options[ $group_key ] = $group['pay_method_group_title'];
        }
    }

    return $options;
}

function get_shipping_name_by_id( $shipping_id ) {
    $packages = WC()->shipping->get_packages();

    foreach ( $packages as $i => $package ) {
        if ( isset( $package['rates'] ) && isset( $package['rates'][ $shipping_id ] ) ) {
            $rate = $package['rates'][ $shipping_id ];
            /* @var $rate WC_Shipping_Rate */
            return $rate->get_label();
        }
    }

    return '';
}

function payment_has_editable_fee() {
    return [
        //'bacs',
        'nicepay_cc',
        'nicepay_vav2',
        'nicepay_ewalletv2',
        'nicepay_payloan'
    ];
}

/**
 * Add a standard $ value surcharge to all transactions in cart / checkout
 */
add_action( 'woocommerce_cart_calculate_fees','wc_add_service_fee', 20 );
function wc_add_service_fee() {
    if( is_admin() && ! defined( 'DOING_AJAX' ) ) {
        return;
    }

    $service_name       = 'Service Fee';
    $fee                = 0;
    $choosen_gateway_id = WC()->session->get('chosen_payment_method');

    if( empty( $choosen_gateway_id ) || is_null( $choosen_gateway_id ) || $choosen_gateway_id == '' ) {
        return;
    }

    $gateway            = WC()->payment_gateways()->payment_gateways()[$choosen_gateway_id];

    if( $gateway && $gateway->enabled == 'yes' && in_array( $gateway->id, payment_has_editable_fee() ) ) {
        // Fee dihitung dari subtotal item + ongkos kirim.
        $item_subtotal  = WC()->cart->get_subtotal();
        $shipping_total = WC()->cart->get_shipping_total();
        $subtotal       = $item_subtotal + $shipping_total;

        if( $gateway->id == 'nicepay_vav2' ) {
            $fee = calculate_va_fee( $gateway, $subtotal );
        } elseif( $gateway->id == 'nicepay_ewalletv2' ) {
            $fee = calculate_e_wallet_fee( $gateway, $subtotal );
        } elseif( $gateway->id == 'nicepay_payloan' ) {
            $fee = calculate_payloan_fee( $gateway, $subtotal );
        } else {
            $fee = $gateway->calculate_service_fee( $subtotal );
        }

    }

    $service_fee = round_hundred( $fee );
    WC()->cart->add_fee( $service_name, $service_fee );
}

function calculate_va_fee( $gateway, $subtotal ) {
    $service_fee = 0;

    if( isset( $_POST['post_data'] ) ) {
        parse_str( $_POST['post_data'], $post_data );
    } else {
        $post_data = $_POST;
    }

    if( isset( $post_data['bankCdVAV2'] ) ) {
        if( $post_data['bankCdVAV2'] == 'CENA' ) {
            $service_fee = $gateway->calculate_service_fee_CENA( $subtotal );
        } elseif( $post_data['bankCdVAV2'] == 'BMRI' ) {
            $service_fee = $gateway->calculate_service_fee_BMRI( $subtotal );
        } elseif( $post_data['bankCdVAV2'] == 'BNIN' ) {
            $service_fee = $gateway->calculate_service_fee_BNIN( $subtotal );
        }
    }

    return $service_fee;
}

function calculate_e_wallet_fee( $gateway, $subtotal ) {
    $service_fee = 0;

    if( isset( $_POST['post_data'] ) ) {
        parse_str( $_POST['post_data'], $post_data );
    } else {
        $post_data = $_POST;
    }

    if( isset( $post_data['mitraCdEWalletV2'] ) ) {
        if( $post_data['mitraCdEWalletV2'] == 'OVOE' ) {
            $service_fee = $gateway->calculate_service_fee_OVO( $subtotal );
        } elseif( $post_data['mitraCdEWalletV2'] == 'LINK' ) {
            $service_fee = $gateway->calculate_service_fee_LinkAja( $subtotal );
        }
    }

    return $service_fee;
}

function calculate_payloan_fee( $gateway, $subtotal ) {
    $service_fee = 0;

    if( isset( $_POST['post_data'] ) ) {
        parse_str( $_POST['post_data'], $post_data );
    } else {
        $post_data = $_POST;
    }

    if( isset( $post_data['mitraCdPayLoan'] ) ) {
        if( $post_data['mitraCdPayLoan'] == 'KDVI' ) {
            $service_fee = $gateway->calculate_service_fee_Kredivo( $subtotal );
        } elseif( $post_data['mitraCdPayLoan'] == 'AKLP' ) {
            $service_fee = $gateway->calculate_service_fee_Akulaku( $subtotal );
        }
    }

    return $service_fee;
}

function round_hundred( $nominal ) {
    if( !$nominal || $nominal == 0 ) {
        return 0;
    }

    $nominal = round( $nominal, 0,PHP_ROUND_HALF_UP );

    /**
     * Get hundred value from nominal
     */
    $hundred = substr($nominal, -3);

    /**
     * If hundred > 500 then Round to Rp. 1000
     * Elseif hundred <= 500 then Round to Rp. 500
     */
    if( $hundred > 500 ) {
        $result = $nominal + (1000 - $hundred);
    } elseif( $hundred == 0 ) {
        $result = $nominal;
    } else {
        $result = ($nominal - $hundred) + 500;
    }

    return (int) $result;
}

// sort cart fees
add_filter( 'woocommerce_sort_fees_callback', 'remove_woocommerce_sort_fees_callback', 10, 2 );
function remove_woocommerce_sort_fees_callback( $a, $b ){
    return 1;
}