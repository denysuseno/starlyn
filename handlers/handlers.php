<?php

require_once('woocommerce-address-book.php');
require_once('carts/cart-handlers.php');
require_once('shippings/shipping-handlers.php');
require_once('payments/payment-handlers.php');
require_once('payment-confirmation/payment-confirmation-handlers.php');
require_once('crontab.php');
require_once('barcode-generator/barcode-handler.php');