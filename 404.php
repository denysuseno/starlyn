<?php get_header(); ?>

<!-- 404-->
<div id="site-content" class="site-content site-content-about">
    <div class="site-content-inner">
        <main class="main" role="main">
            <div class="page-single wrapper-about">
                <!-- edit here -->
                <div class="page-not-found intro">
                    <img src="<?= library_url() ?>/images/image-404.png" alt="404 Image" class="image">
                </div>

            </div>
        </main>
    </div>
</div>

<?php get_footer(); ?>
