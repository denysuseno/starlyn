<?php
/* Template Name: My Account */
?>

<?php get_header(); ?>

<?php
$pageType = is_account_page() && is_user_logged_in() ? 'account' : 'about';
?>

<div id="site-content" class="site-content site-content-<?= $pageType ?>">
    <div class="site-content-inner">
        <main class="main" role="main">

            <?php while (have_posts()) : the_post(); ?>

                <?php the_content(); ?>

            <?php endwhile; ?>

        </main>
    </div>
</div>


<?php get_footer(); ?>
