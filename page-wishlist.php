<?php
/* Template Name: Wishlist */

$query_args = query_page_wishlist( 1 );
$loop       = new WP_Query( $query_args );
?>

<?php get_header(); ?>

    <div id="site-content" class="site-content site-content-product">
        <div class="site-content-inner">
            <main class="main" role="main">
                <div class="wishlist-page">
                    <div class="wrapper">
                        <div class="wishlist-page-header">
                            <h3 class="title">YOUR WISHLIST</h3>
                        </div>
                        <div class="wishlist-page-content">
                            <?php if ( woocommerce_product_loop() && $loop->have_posts() ) : ?>

                                <?php
                                /**
                                 * Hook: woocommerce_before_shop_loop.
                                 *
                                 * @hooked woocommerce_output_all_notices - 10
                                 * @hooked woocommerce_result_count - 20
                                 * @hooked woocommerce_catalog_ordering - 30
                                 */
                                // do_action( 'woocommerce_before_shop_loop' );
                                ?>

                                <div class="notices" style="margin-bottom: 32px">
                                    <?php do_action( 'starlyn_product_archives_output_all_notices' ); ?>
                                </div>

                                <?php woocommerce_product_loop_start(); ?>

                                <?php
                                if ( $loop->have_posts() ) {
                                    while ( $loop->have_posts() ) {
                                        $loop->the_post();

                                        /**
                                         * Hook: woocommerce_shop_loop.
                                         */
                                        do_action( 'woocommerce_shop_loop' );

                                        wc_get_template_part( 'content', 'product' );
                                    }
                                }
                                wp_reset_postdata();
                                ?>

                                <?php woocommerce_product_loop_end(); ?>

                                <?php
                                /**
                                 * Hook: woocommerce_after_shop_loop.
                                 *
                                 * @hooked woocommerce_pagination - 10
                                 */
                                // do_action( 'woocommerce_after_shop_loop' );
                                ?>

                                <?php if( $loop->max_num_pages > 1 ) : ?>
                                    <div class="view-more">
                                        <a href="javascript:void(0)" id="more-posts-wishlist" class="link">View More</a>
                                    </div>
                                <?php endif; ?>

                            <?php else : ?>

                                <p>No wishlist found.</p>

                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </main>
        </div>
    </div>

<?php get_footer(); ?>