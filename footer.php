<footer id="site-footer" class="site-footer" role="contentinfo">
    <div class="site-footer-top">
        <div class="wrapper">
            <div class="logo">
                <img src="<?= library_url() ?>/images/logo-starlyn-alt.svg" alt="Footer logo" class="logo-image">
            </div>
            <div class="nav">
                <ul class="items">
                    <li class="item"><a href="<?= get_permalink( get_page_by_path( 'about-us' ) ) ?>">About Us</a></li>
                    <li class="item"><a href="<?= get_permalink( get_page_by_path( 'contact-us' ) ) ?>">Contact Us</a></li>
                    <li class="item"><a href="<?= get_permalink( get_page_by_path( 'payment-confirmation' ) ) ?>">Payment Confirmation</a></li>
                </ul>
            </div>
            <div class="social">
                <?php
                $social_media = get_field( 'social_media', 'option' );
                ?>

                <?php if( $social_media ) : ?>
                    <ul class="items">
                        <?php foreach( $social_media as $sosmed ) : ?>
                            <li class="item">
                                <?php if( $sosmed['social_media_link'] ) : ?>
                                    <a href="<?php echo $sosmed['social_media_link'] ?>" target="_blank">
                                <?php endif; ?>
                                        <img src="<?php echo $sosmed['social_media_icon'] ?>" alt="Logo <?php echo $sosmed['social_media_platform'] ?>">
                                    <!--<svg class="icon" role="img"><use xlink:href="<?/*= library_url() */?>/images/svg-symbols.svg#icon-facebook" /></svg>-->
                                <?php if( $sosmed['social_media_link'] ) : ?>
                                    </a>
                                <?php endif; ?>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <div class="site-footer-bottom">
        <?php
            $payment_channels = get_field( 'payment_method_channel', 'option' );
        ?>
        <div class="wrapper">
            <div class="payment-logo">
                <?php if( $payment_channels ) : ?>
                    <ul class="items">
                        <?php foreach( $payment_channels as $payment_channel ) : ?>
                            <li class="item">
                                <img src="<?php echo $payment_channel['payment_logo'] ?>" alt="Logo <?php echo $payment_channel['payment_name'] ?>" class="image">
                            </li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>
            </div>
            <div class="site-attribution">
                <div class="copyright">
                    <div class="copyright-text">Copyright © 2020 Starlyn.com</div>
                    <div class="copyright-nav">
                        <ul class="items">
                            <li class="item"><a href="<?= get_privacy_policy_url() ?>">Privacy Policy</a></li>
                            <li class="item"><a href="<?= get_permalink( get_page_by_path( 'terms-condition' ) ) ?>">Terms of Use</a></li>
                        </ul>
                    </div>
                </div>
                <div class="author">
                    <p class="author-text">Designed by <a href="<?php echo esc_url( 'icubic.co.id' ) ?>" target="_blank">ICUBIC</a></p>
                    <p class="mobile-copyright">Copyright © 2020 Starlyn.com</p>
                </div>
            </div>
        </div>
    </div>
</footer>
</div> <!-- #site-container -->

<!-- Tommy 04.02.20 -->
<?php $floatingWA = get_field('floating_whatsapp_number', 'option'); ?>
<div class="floating-whatsapp">
    <div class="floating-whatsapp-popup">
        <p>Halo, silahkan hubungi kami untuk info lebih lengkap</p>
        <a target="_blank" href="https://wa.me/<?= $floatingWA ?>" class="button button-primary">Chat Sekarang</a>
    </div>
    <div class="floating-whatsapp-icon js-whatsapp-toggle">
        <svg class="icon icon-whatsapp" role="img"><use xlink:href="<?= library_url() ?>/images/svg-symbols.svg#icon-whatsapp-big"></use></svg>
        <svg class="icon icon-cross" role="img"><use xlink:href="<?= library_url() ?>/images/svg-symbols.svg#icon-cross-big"></use></svg>
    </div>
</div>
<!-- End Tommy 04.02.20 -->

<script type="text/javascript">
    function showPopupLogin() {
        let El = $('.popup-login'), siteHeader = $('#site-header');
        if (El.length > 0) {
            $('html,body').animate({scrollTop: siteHeader.offset().top}, 'slow');
            El.addClass('is-toggled');
        }
    }
</script>

<?php wp_footer(); ?>
</body>
</html> <!-- haunter owned! -->