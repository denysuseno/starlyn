<?php
/* Template Name: Cart */
?>

<?php get_header(); ?>

<div id="site-content" class="site-content site-content-cart">
    <div class="site-content-inner">
        <main class="main" role="main">

            <?php while (have_posts()) : the_post(); ?>

                <?php the_content(); ?>

            <?php endwhile; ?>

        </main>
    </div>
</div>

<?php get_footer(); ?>
