<?php
/* Template Name: About Us */

global $post;
$header         = get_field( 'header', $post->ID );
$intro_image    = get_field( 'intro_image', $post->ID );
$content_image  = get_field( 'content_image', $post->ID );
$subtext_intro  = get_field( 'subtext_intro', $post->ID );
?>

<?php get_header(); ?>

<div id="site-content" class="site-content site-content-about">
    <div class="site-content-inner">
        <main class="main" role="main">
            <div class="page-single wrapper-about">
                <div class="about">
                    <section class="section about-header">
                        <div class="about-header-text intro">
                            <p class="header"><?php echo $header['header_text'] ?></p>
                            <h3 class="title"><?php echo $header['header_title'] ?></h3>
                        </div>
                    </section>
                    <section class="section about-content">
                        <div class="about-content-wrapper intro">
                            <div class="about-content-header">
                                <img src="<?php echo $intro_image ?>" alt="About Intro" class="logo">
                            </div>
                            <div class="about-content-page">
                                <p class="text"><?php the_content() ?></p>
                            </div>
                        </div>
                        <div class="about-img">
                            <div class="about-img-content">
                                <img src="<?php echo $content_image['image_1'] ?>" alt="About Content Image 1" class="images">
                            </div>
                            <div class="about-img-content">
                                <img src="<?php echo $content_image['image_2'] ?>" alt="About Content Image 2" class="images">
                            </div>
                        </div>
                        <p class="subs-text intro"><?php echo $subtext_intro ?></p>
                    </section>
                </div>
            </div>
        </main>
    </div>
</div>

<?php get_footer(); ?>
