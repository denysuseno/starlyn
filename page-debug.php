<?php
/* Template Name: Debug */
$user = wp_get_current_user();
if( $user->user_login != 'icubic' ) {
    echo 'You are not Developer!!';
    return;
}
?>

<?php get_header(); ?>

<div id="site-content" class="site-content site-content-deal">
    <div class="site-content-inner">
        <main class="main" role="main">
            <div class="deals-page">
                <div class="deals-page-content">
                    <div class="wrapper">
                        <div class="header">
                            <h2 class="title">Debugging</h2>
                            <p class="subtitle"></p>
                        </div>
                        <div class="content">

                            <?php
                                v(wc_get_product_ids_on_sale());
                            ?>

                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
</div>

<?php get_footer(); ?>
