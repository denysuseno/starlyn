<?php

use WPO\WC\PDF_Invoices\Documents\Order_Document;
use WPO\WC\PDF_Invoices\Documents\Order_Document_Methods;

require_once('library/walker.php');
require_once('library/admin.php');
require_once('handlers/handlers.php');
require_once('includes/includes.php');
require_once(ABSPATH . 'vendor/autoload.php');

define( 'SICEPAT_INSURANCE_PERCENTAGE', 0.0025 );
define( 'JNE_INSURANCE_PERCENTAGE', 0.002 );
define( 'JNE_INSURANCE_ADMIN_FEE', 5000 );
define( 'ORDER_BARCODE_DIR_NAME', 'order-barcode' );
define( 'WAYBILL_BARCODE_DIR_NAME', 'waybill-barcode' );

/* SET CONTENT WIDTH */
if ( ! isset( $content_width ) ) {
	$content_width = 800;
}

function v($v, $die = null){

    echo '<pre>';
    var_dump($v);
    echo '</pre>';

    if($die) die();

}

function library_url() {
    return get_template_directory_uri() . '/library';
}

/* THUMBNAIL SIZE OPTIONS */
add_image_size('walker-thumb-300', 300, 300, true);

/* COMMENT LAYOUT */
function walker_comment($comment, $args, $depth) {
	$GLOBALS['comment'] = $comment;

	if ('pingback' == $comment->comment_type || 'trackback' == $comment->comment_type) : ?>

	<li id="comment-<?php comment_ID(); ?>" <?php comment_class(); ?>>
		<div class="comment-body">
			<?php _e('Pingback:', 'walker'); ?> <?php comment_author_link(); ?> <?php edit_comment_link(__('Edit', 'walker'), '<span class="edit-link">', '</span>'); ?>
		</div>

	<?php else : ?>

	<li id="comment-<?php comment_ID(); ?>" <?php comment_class(empty($args['has_children']) ? '' : 'parent'); ?>>
		<article id="div-comment-<?php comment_ID(); ?>" class="comment-body">
			<footer class="comment-meta">
				<div class="comment-author vcard">
					<?php if (0 != $args['avatar_size']) echo get_avatar($comment, $args['avatar_size']); ?>
					<?php printf(__('%s <span class="says">says:</span>', 'walker'), sprintf('<cite class="fn">%s</cite>', get_comment_author_link())); ?>
				</div>

				<div class="comment-metadata">
					<a href="<?php echo esc_url(get_comment_link($comment->comment_ID)); ?>">
						<time datetime="<?php comment_time('c'); ?>">
							<?php printf(_x('%1$s at %2$s', '1: date, 2: time', 'walker'), get_comment_date(), get_comment_time()); ?>
						</time>
					</a>
					<?php edit_comment_link(__('Edit', 'walker'), '<span class="edit-link">', '</span>'); ?>
				</div>

				<?php if ('0' == $comment->comment_approved) : ?>
				<p class="comment-awaiting-moderation"><?php _e('Your comment is awaiting moderation.', 'walker'); ?></p>
				<?php endif; ?>
			</footer>

			<div class="comment-content">
				<?php comment_text(); ?>
			</div>

			<?php
				comment_reply_link(array_merge($args, array(
					'add_below' => 'div-comment',
					'depth'     => $depth,
					'max_depth' => $args['max_depth'],
					'before'    => '<div class="reply">',
					'after'     => '</div>',
				)));
			?>
		</article>

	<?php
	endif;
}

/* SEARCH FORM LAYOUT */
function walker_wpsearch($form) {
	$form =
		'<form role="search" method="get" id="search-form" class="search-form" action="' . home_url('/') . '" >
			<label class="screen-reader-text" for="s">' . __('Search for:', 'walkertheme') . '</label>
			<input type="text" value="' . get_search_query() . '" name="s" id="s" placeholder="'.esc_attr__('Search the Site...','walkertheme').'" />
			<input type="submit" id="searchsubmit" value="'. esc_attr__('Search') .'" />
		</form>';
	return $form;
}

/* WOOCOMMERCE SUPPORT */
add_theme_support('woocommerce');

add_filter( 'woocommerce_enqueue_styles', '__return_false' );

remove_action('woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
remove_action('woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);
remove_action('woocommerce_sidebar', 'woocommerce_get_sidebar', 10);

add_action('woocommerce_before_main_content', 'walker_wrapper_start', 10);
add_action('woocommerce_after_main_content', 'walker_wrapper_end', 10);
add_action('woocommerce_walker_sidebar', 'woocommerce_get_sidebar', 10);

function walker_wrapper_start() { ?>
    <div id="site-content" class="site-content <?= is_shop() ? 'site-content-product' : '' ?>">
        <div class="site-content-inner">
            <main class="main" role="main">
                <div class="<?= is_shop() ? 'product-page' : 'product-single' ?>">
                    <div class="wrapper">
<?php
}

function walker_wrapper_end() { ?>
                    </div>
                </div>
            </main>
        </div>
    </div>
<?php
}

add_filter('woocommerce_single_product_image_gallery_classes', 'starlyn_single_product_image_classes', 10, 1);
function starlyn_single_product_image_classes( $class ) {
    $class[] = 'gallery';

    return $class;
}

/**
 * Change woocommerce Shop Page title.
 */
add_filter('woocommerce_page_title', 'custom_woo_page_title', 10, 1);
function custom_woo_page_title( $pageTitle ) {
    if( $pageTitle == 'Shop' ) {
        return 'ALL PRODUCTS';
    }
}

/**
 * Remove breadcrumbs on shop and single product page.
 */
add_action('template_redirect', 'remove_shop_breadcrumbs' );
function remove_shop_breadcrumbs(){

    if( is_shop() || is_product() )
        remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0);

}

add_filter('nav_menu_css_class', 'nav_menu_css_class', 10, 2);
function nav_menu_css_class($classes, $menu_item){
    if( in_array('current_page_item', $classes) ){
        $classes[] = 'active';
    }

    $classes[] = 'vanguard-nav-item';
    return $classes;
}


add_filter('nav_menu_link_attributes', 'nav_menu_link_attributes');
function nav_menu_link_attributes($atts){
    $atts['class'] = 'vanguard-nav-link';
    return $atts;
}

add_action('nav_menu_submenu_css_class', 'custom_submenu_css_class');
function custom_submenu_css_class() {
    return array('vanguard-menu', 'vanguard-navbar-dropdown');
}

function woocommerce_output_related_products() {
	woocommerce_related_products(4, 4);
}

// add custom menu here
add_filter('wp_nav_menu_items','wp_nav_menu_items_custom', 10, 2);
function wp_nav_menu_items_custom($items, $args) {
    $oldItems = $items;

    if( $args && isset($args->menu_id) && $args->menu_id == 'items-header' ) {
        $catArgs = [
            'orderby'    => 'id',
            'order'      => 'asc',
            'hide_empty' => false,
        ];

        $productCategories = get_terms( 'product_cat', $catArgs );

        $activeShopLink = is_shop() || is_product_category() || is_product() ? 'active' : '';

        ob_start();
    ?>

        <li class="vanguard-nav-item <?= is_shop() ? 'active' : '' ?>"><a class="vanguard-nav-link" href="<?= get_permalink(get_page_by_path('shop')) ?>">Products</a></li>

    <?php

        $newItems = ob_get_contents();

        ob_end_clean();

        $newItems .= $oldItems;

        $items = $newItems;
    }

    return $items;

}

function get_breadcrumbs( $args ) {
    wc_get_template('includes/partial/breadcrumbs.php', $args);
}

function get_custom_product_attributes( $product ) {
    $productAttributes = $product->get_attributes();
    $productAttributeSlug = [];
    if( $productAttributes ) {
        foreach ( $productAttributes as $slug => $attribute ) {
            $productAttributeName = wc_attribute_label($slug, $product);
            $productAttributeSlug[$slug] = $productAttributeName;
        }
    }

    $attributeValues = [];
    if( $productAttributeSlug ) {
        foreach ( $productAttributeSlug as $slug => $attributeTitle ) {
            $attributeValues[$attributeTitle] = get_terms($slug);
        }
    }

    return $attributeValues;
}

function getCustomProductTags( $product ) {
    $output = '';

    $terms = $product->wc_get_product_tag_list();

    if( count($terms) > 0 ) {
        $tags = [];

        foreach( $terms as $term ) {
            $termId   = $term->term_id;
            $termName = $term->name;
            $termSlug = $term->slug;
            $termLink = get_term_link( $term, 'product_tag' );

            $tags     = '<a href="'.$termLink.'">'.$termName.'</a>';
        }

        if( $tags ) {
            $output = implode(',', $tags);
        }
    }

    return $output;
}

/**
 * Change number of products that are displayed per page (shop page)
 */
add_filter( 'loop_shop_per_page', 'new_loop_shop_per_page', 20 );
function new_loop_shop_per_page( $cols ) {
    // $cols contains the current number of products per page based on the value stored on Options -> Reading
    // Return the number of products you wanna show per page.
    $cols = 12;
    return $cols;
}

function wpbeginner_numeric_posts_nav() {
    /*if( is_singular() )
        return;*/

    global $wp_query;

    /** Stop execution if there's only 1 page */
    if( $wp_query->max_num_pages <= 1 )
        return;

    $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
    $max   = intval( $wp_query->max_num_pages );

    /** Add current page to the array */
    if ( $paged >= 1 )
        $links[] = $paged;

    /** Add the pages around the current page to the array */
    if ( $paged >= 3 ) {
        $links[] = $paged - 1;
        $links[] = $paged - 2;
    }

    if ( ( $paged + 2 ) <= $max ) {
        $links[] = $paged + 2;
        $links[] = $paged + 1;
    }

    echo '<ul class="pagination">' . "\n";

    /** Previous Post Link */
    if ( get_previous_posts_link() )
        printf( '<li class="page-item page-item-control"><span class="page-link">%s</span></li>' . "\n", get_previous_posts_link('Sebelumnya') );

    /** Link to first page, plus ellipses if necessary */
    if ( ! in_array( 1, $links ) ) {
        $class = 1 == $paged ? 'active' : '';

        printf( '<li class="page-item %s"><a href="%s"><span class="page-link">%s</span></a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );

        if ( ! in_array( 2, $links ) )
            echo '<li class="page-item">…</li>';
    }

    /** Link to current page, plus 2 pages in either direction if necessary */
    sort( $links );
    foreach ( (array) $links as $link ) {
        $class = $paged == $link ? 'active' : '';
        printf( '<li class="page-item %s"><a href="%s"><span class="page-link">%s</span></a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
    }

    /** Link to last page, plus ellipses if necessary */
    if ( ! in_array( $max, $links ) ) {
        if ( ! in_array( $max - 1, $links ) )
            echo '<li class="page-item">…</li>' . "\n";

        $class = $paged == $max ? 'active' : '';
        printf( '<li class="page-item %s"><a href="%s"><span class="page-link">%s</span></a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
    }

    /** Next Post Link */
    if ( get_next_posts_link() )
        printf( '<li class="page-item page-item-control"><span class="page-link">%s</span></li>' . "\n", get_next_posts_link('Selanjutnya') );

    /*echo '</ul>' . "\n";

    echo '<ul class="pagination">' . "\n";

    echo '<li class="page-item page-item-control disabled"><a class="page-link" href="#" aria-label="Previous"><span class="fa fa-angle-double-left" aria-hidden="true"></span> Sebelumnya</a></li>' . "\n";
    echo '<li class="page-item active"><span class="page-link">1</span></li>' . "\n";
    echo '<li class="page-item"><a class="page-link" href="#">2</a></li>' . "\n";
    echo '<li class="page-item"><a class="page-link" href="#">3</a></li>' . "\n";
    echo '<li class="page-item page-item-control"><a class="page-link" href="#" aria-label="Next">Selanjutnya <span class="fa fa-angle-double-right" aria-hidden="true"></span></a></li>' . "\n";

    echo '</ul>' . "\n";*/

}

// function to display number of posts.

function getPostViews($postID){
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0 View";
    }
    return $count.' Views';
}

// function to count views.
function setPostViews($postID) {
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}

function postContent() {
    global $post;

    return apply_filters('the_content', get_post_field('post_content', $post->id));
}

add_action('pre_get_posts', 'posts_per_tag_archive_page');
function posts_per_tag_archive_page( $query ){
    if ( $query->is_main_query() && $query->is_tag() && ! is_admin() ) {
        $query->set( 'posts_per_page', 1 );
    }
}

// Change posts per page in the design category
add_action( 'pre_get_posts', 'posts_per_category_archive_page' );
function posts_per_category_archive_page( $query ) {
    if( $query->is_main_query() && is_category() && ! is_admin() ) {
        $query->set( 'posts_per_page', 9 );
    }
}

add_action( 'init', 'form_action_hook');
function form_action_hook() {
    if ( isset($_POST['bot_trap']) && empty($_POST['bot_trap']) ) { // HANDLE FOR BOT
        if ( isset($_POST['action']) && $_POST['action'] == 'contact_us' ) {
            if ( isset( $_POST['contact_us_nonce'] ) &&
                wp_verify_nonce( $_POST['contact_us_nonce'], 'contact_us_action' ) ) {

                $args = [
                    'post_title'    => sanitize_text_field($_POST['name']) . ' - ' . sanitize_text_field($_POST['subject']),
                    'subject'       => sanitize_text_field($_POST['subject']),
                    'name'          => sanitize_text_field($_POST['name']),
                    'phone'         => sanitize_text_field($_POST['phone']),
                    'post_content'  => sanitize_text_field($_POST['message']),
                    'post_type'     => sanitize_text_field($_POST['post_type']),
                    'post_status'   => 'pending',
                ];

                $cptId = wp_insert_post($args);

                if( $cptId ) {
                    foreach ( $args as $key => $value ) {
                        update_post_meta($cptId, $key, $value);
                    }

                    $redirectTo = sanitize_text_field($_POST['redirect_url']);

                    wc_add_notice( 'Terima kasih' );
                    wp_redirect( $redirectTo );
                    exit;
                }
            }
        }
    }
}

function siteHeaderHtml() {
    siteHeaderTopHtml();
    siteHeaderMainHtml();
}

function siteHeaderTopHtml() {
    locate_template( 'site-header-top.php', true, true );
}

function siteHeaderMainHtml() {
    locate_template( 'site-header-main.php', true, true );
}

remove_action('woocommerce_before_customer_login_form', 'woocommerce_output_all_notices', 10);
add_action('woocommerce_customer_register_form_message', 'custom_register_form_notification', 11);
function custom_register_form_notification() {
    wc_print_notices();
}

remove_action('woocommerce_before_lost_password_form', 'woocommerce_output_all_notices', 10);
add_action('woocommerce_lost_password_form_message', 'custom_lost_password_notification', 11);
function custom_lost_password_notification() {
    wc_print_notices();
}

remove_action('woocommerce_before_reset_password_form', 'woocommerce_output_all_notices', 10);
add_action('woocommerce_reset_password_form_message', 'custom_reset_password_notification', 11);
function custom_reset_password_notification() {
    wc_print_notices();
}

add_filter('woocommerce_account_menu_items', 'starlyn_my_account_menu_link', 40);
function starlyn_my_account_menu_link( $menuLinks ) {
    $menuLinks['edit-address'] = 'Alamat'; // Rename menu Address to Alamat

    unset($menuLinks['downloads']); // Remove menu Downloads
    unset($menuLinks['orders']); // Remove menu Orders
    unset($menuLinks['edit-account']); // Remove menu Account Details
    unset($menuLinks['customer-logout']); // Remove menu Logout

    $transaction = ['transaction' => 'Transaksi']; // Add new menu called Transaksi
    $review = ['product-review' => 'Ulasan Produk']; // Add new menu called Ulasan Produk

    // Sorting menu
    //$menuLinks = array_slice( $menuLinks, 0, 2, true ) + $review + array_slice( $menuLinks, 2, null, true );
    $menuLinks = array_slice( $menuLinks, 0, 1, true ) + $transaction + $review + array_slice( $menuLinks, 1, null, true );

    return $menuLinks;
}

add_filter('woocommerce_account_menu_item_classes', 'starlyn_account_menu_item_classes', 10, 2);
function starlyn_account_menu_item_classes($classes, $endpoint) {
    global $wp;

    $classes = [
        'item',
        'item--'.$endpoint,
    ];

    // Set current item class.
    $current = isset( $wp->query_vars[ $endpoint ] );
    if ( 'dashboard' === $endpoint && ( isset( $wp->query_vars['page'] ) || empty( $wp->query_vars ) ) ) {
        $current = true; // Dashboard is not an endpoint, so needs a custom check.
    } elseif ( 'orders' === $endpoint && isset( $wp->query_vars['view-order'] ) ) {
        $current = true; // When looking at individual order, highlight Orders list item (to signify where in the menu the user currently is).
    } elseif ( 'payment-methods' === $endpoint && isset( $wp->query_vars['add-payment-method'] ) ) {
        $current = true;
    } elseif ( 'product-review' === $endpoint && isset( $wp->query_vars['add-product-review'] ) ) {
        $current = true;
    }

    if ( $current ) {
        $classes[] = 'item-current';
    }

    return $classes;
}

add_action( 'init', 'starlyn_add_endpoint' );
function starlyn_add_endpoint() {
    add_rewrite_endpoint( 'product-review', EP_PAGES );
    add_rewrite_endpoint( 'dispute-center', EP_PAGES );
}

/*
 * Content for the new page in My Account, woocommerce_account_{ENDPOINT NAME}_endpoint
 */
add_action( 'woocommerce_account_dispute-center_endpoint', 'starlyn_my_account_endpoint_dispute_center_content' );
function starlyn_my_account_endpoint_dispute_center_content() {

    // disabled sementara
    return;

    $args = [];
    wc_get_template('myaccount/dispute-center.php', $args);

}

/*
 * Content for the new page in My Account, woocommerce_account_{ENDPOINT NAME}_endpoint
 */
add_action( 'woocommerce_account_product-review_endpoint', 'starlyn_my_account_endpoint_product_review_content' );
function starlyn_my_account_endpoint_product_review_content() {

    $args = [];
    wc_get_template('myaccount/product-review.php', $args);

}

remove_action( 'woocommerce_register_form', 'wc_registration_privacy_policy_text', 20 );

add_action( 'woocommerce_register_post', 'starlyn_register_validate_fields', 10, 3 );
function starlyn_register_validate_fields( $username, $email, $errors ) {
    if ( empty( $_POST['confirm_password'] ) ) {
        $errors->add( 'confirm_password_error', 'Please confirm your password' );
    }
}

/**
 * Add the code below to your theme's functions.php file
 * to add a confirm password field on the register form under My Accounts.
 */
add_filter('woocommerce_registration_errors', 'woocommerce_registration_errors_validation', 11, 3);
function woocommerce_registration_errors_validation($reg_errors, $sanitized_user_login, $user_email) {
    global $woocommerce;
    extract( $_POST );
    if ( strcmp( $password, $confirm_password ) !== 0 ) {
        return new WP_Error( 'registration-error', __( 'Passwords do not match.', 'woocommerce' ) );
    }
    return $reg_errors;
}

//add_action('wp_loaded', 'custom_lost_password_action');
function custom_lost_password_action() {
    $email = isset($_POST['user_login']) ? $_POST['user_login'] : '';

    if ( isset( $_POST['wc_reset_password'], $_POST['user_login'] ) ) {
        $nonce_value = wc_get_var( $_REQUEST['woocommerce-lost-password-nonce'], wc_get_var( $_REQUEST['_wpnonce'], '' ) ); // @codingStandardsIgnoreLine.

        if ( ! wp_verify_nonce( $nonce_value, 'lost_password' ) ) {
            return;
        }

        $success = WC_Shortcode_My_Account::retrieve_password();

        // If successful, redirect to my account with query arg set.
        if ( $success ) {
            wp_redirect( add_query_arg( ['reset-link-sent' => 'true', 'email' => $email], wc_get_account_endpoint_url( 'lost-password' ) ) );
            exit;
        }
    }
}

remove_action( 'woocommerce_account_content', 'woocommerce_output_all_notices', 5 );

add_action('woocommerce_my_account_notification', 'wc_my_account_notification');
function wc_my_account_notification() {
    echo '<div class="my-account-notification" style="margin-bottom: 32px;">';
    wc_print_notices();
    echo '</div>';
}

/**
 * Remove required field requirement in My Account Edit form
 */
add_filter('woocommerce_save_account_details_required_fields', 'remove_required_fields');
function remove_required_fields( $required_fields ) {
    unset($required_fields['account_first_name']);
    unset($required_fields['account_last_name']);

    if( !isset($_POST['change_password_page']) ) {
        $required_fields['account_phone'] = __('Nomor Telepon', 'woocommerce');
    }

    return $required_fields;
}

/**
 * Redirect back to /my-account/edit-account page after save account details.
 */
add_action('woocommerce_save_account_details', 'wc_save_account_redirect', 12, 1);
function wc_save_account_redirect( $user_id ) {

    if( isset($_POST['account_phone']) ) {
        update_user_meta( $user_id, 'account_phone', sanitize_text_field( $_POST['account_phone'] ) );
    }

    if( isset($_POST['account_birthdate']) ) {
        update_user_meta( $user_id, 'account_birthdate', sanitize_text_field( $_POST['account_birthdate'] ) );
    }

    if( isset($_POST['account_gender']) ) {
        update_user_meta( $user_id, 'account_gender', sanitize_text_field( $_POST['account_gender'] ) );
    }

    if( isset($_POST['change_password_page']) ) {
        wp_safe_redirect( add_query_arg( ['change_password' => true], wc_get_endpoint_url( 'edit-account') ) );
        exit;
    } else {
        wp_safe_redirect( wc_get_endpoint_url( 'edit-account') );
        exit;
    }

}

add_action('woocommerce_save_account_details_errors', 'validate_save_account_details', 10, 2);
function validate_save_account_details($errors, $user) {

    global $wpdb;

    $tableName = $wpdb->prefix . 'usermeta';

    if( isset($_POST['account_phone']) ) {
        $newPhone           = sanitize_text_field( $_POST['account_phone'] );
        $existingUser       = $wpdb->get_results("SELECT * FROM $tableName WHERE user_id != $user->ID AND meta_key='account_phone' AND meta_value='".$newPhone."'");
        $existingUsedId     = isset($existingUser[0]->user_id) && $existingUser[0]->user_id ? $existingUser[0]->user_id : null;
        $existingUsedPhone  = isset($existingUser[0]->meta_value) && $existingUser[0]->meta_value ? $existingUser[0]->meta_value : null;

        if( $existingUsedPhone && $existingUsedId !== get_current_user_id() ) {
            wc_add_notice(__( 'This phone number is already registered.', 'woocommerce' ), 'error');
        }
    }

    if( isset($_POST['change_password_page']) ) {
        if( empty($_POST['password_current']) ) {
            wc_add_notice(__( 'Password lama harus diisi!', 'woocommerce' ), 'error');
        }

        if( empty($_POST['password_1']) ) {
            wc_add_notice(__( 'Password baru harus diisi!', 'woocommerce' ), 'error');
        }

        if( empty($_POST['password_2']) ) {
            wc_add_notice(__( 'Konfirmasi password baru harus diisi!', 'woocommerce' ), 'error');
        }
    }

    return $errors;

}

add_action('woocommerce_before_add_to_cart_form', 'starlyn_before_add_to_cart_form');
function starlyn_before_add_to_cart_form() {
?>
    <div class="stock">
<?php
}

add_action('woocommerce_after_add_to_cart_form', 'starlyn_after_add_to_cart_form');
function starlyn_after_add_to_cart_form() {
    global $product;
    $badges_stock = get_the_terms( $product->get_id(), 'badges_stock' );
?>
    </div>
    <div class="stock-tags">
        <?php if( $badges_stock ) : ?>
            <ul class="items">
                <?php foreach ( $badges_stock as $badge ) : ?>
                    <li class="item">
                        <span class="check">
                            <svg class="icon" role="img"><use xlink:href="<?= library_url() ?>/images/svg-symbols.svg#icon-check" /></svg>
                        </span>
                        <span class="text"><?php echo $badge->name ?></span>
                    </li>
                <?php endforeach; ?>
            </ul>
        <?php endif; ?>
    </div>
<?php
}

add_filter('woocommerce_quantity_input_classes', 'woo_quantity_input_classes', 10, 1);
function woo_quantity_input_classes( $classes ) {
    $classes[] = 'input';

    return $classes;
}

add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_action', 45 );
function woocommerce_template_single_action() {
    global $product;
?>
    <div class="action">
        <div class="action-cart">
            <a href="javascript:void(0)" onclick="javascript:document.getElementById('btn-add-to-cart').click();" class="button button-primary <?= ! $product->is_in_stock() ? 'is-disabled' : '' ?>">
                <svg class="icon" role="img"><use xlink:href="<?= library_url() ?>/images/svg-symbols.svg#icon-cart" /></svg>
                <span class="text">Add to Cart</span>
            </a>
        </div>
        <div class="action-wishlist">
            <?php if( is_user_logged_in() ) : ?>
                <?php
                $in_wishlist = is_in_wishlist( $product->get_id() );
                $is_loved    = $in_wishlist ? 'is-loved' : '';
                ?>
                <!--<a href="#" class="button button-wishlist is-loved">-->
                <a href="javascript:void(0)" class="button button-wishlist btn-toggle-wishlist <?php echo $is_loved ?>" data-product-id="<?php echo $product->get_id() ?>" data-nonce="<?php echo wp_create_nonce('wishlist-nonce') ?>">
                    <svg class="icon icon-love" role="img"><use xlink:href="<?= library_url() ?>/images/svg-symbols.svg#icon-love" /></svg>
                    <svg class="icon icon-loved" role="img"><use xlink:href="<?= library_url() ?>/images/svg-symbols.svg#icon-love-filled" /></svg>
                </a>
            <?php else : ?>
                <a href="<?php echo get_permalink(get_option('woocommerce_myaccount_page_id')) ?>" class="button button-wishlist">
                    <svg class="icon icon-love" role="img"><use xlink:href="<?= library_url() ?>/images/svg-symbols.svg#icon-love" /></svg>
                    <svg class="icon icon-loved" role="img"><use xlink:href="<?= library_url() ?>/images/svg-symbols.svg#icon-love-filled" /></svg>
                </a>
            <?php endif; ?>
        </div>
    </div>
<?php
}

add_action( 'woocommerce_single_product_summary', 'single_product_breadcrumbs', 1 );
function single_product_breadcrumbs() {
    global $product;
?>
    <div class="breadcrumb">
        <a href="<?= home_url() ?>">Home</a> / <a href="<?= wc_get_page_permalink( 'shop' ) ?>">Products</a> / <span><?= $product->get_name() ?></span>
    </div>
<?php
}

/**
 * Remove content product sale flash.
 */
remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash', 10 );

/**
 * Remove single product sale flash.
 */
remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash', 10 );

/**
 * Custom Price Html.
 *
 * @param $priceAmt
 * @param $regularPrice
 * @param $salePrice
 *
 * @return string
 */
function customPriceHtml( $priceAmt, $regularPrice, $salePrice ) {
    if( ! $regularPrice ) {
        return;
    }

    $htmlPrice = '';

    // if product is sale
    if( ( $priceAmt == $salePrice ) && ( $salePrice != 0 ) ) {

        $percentSave = 100 - ( ( $salePrice / $regularPrice ) * 100 );

        $htmlPrice .= "<span class='value'>".wc_price( $salePrice )."</span>";
        $htmlPrice .= "<div class='discount'>";
        $htmlPrice .= "<span class='highligth'>Save ".floor( $percentSave )."% </span> <span class='text'>was ".wc_price( $regularPrice )."</span>";
        $htmlPrice .= "</div>";

    }

    // in sale but free
    elseif( ( $priceAmt  == $salePrice ) && ( $salePrice == 0 ) ) {

        $percentSave = 100 - ( ( $salePrice / $regularPrice ) * 100 );

        $htmlPrice .= "<span class='value'>Free</span>";
        $htmlPrice .= "<div class='discount'>";
        $htmlPrice .= "<span class='highligth'>Save ".floor( $percentSave )."% </span> <span class='text'>was ".wc_price( $regularPrice )."</span>";
        $htmlPrice .= "</div>";

    }

    // not in sale
    elseif( ( $priceAmt == $regularPrice ) && ( $regularPrice != 0 ) ) {

        $htmlPrice .= "<span class='value'>".wc_price( $regularPrice )."</span>";

    }

    // for free product
    elseif ( ( $priceAmt == $regularPrice ) && ( $regularPrice == 0 ) ) {

        $htmlPrice .= "<span class='value'>Free</span>";

    }

    return $htmlPrice;
}

add_filter('woocommerce_get_price_html', 'my_simple_product_price_html', 100, 2);
function my_simple_product_price_html( $price, $product ) {
    $regularPrice   = $product->get_regular_price();
    $salePrice      = $product->get_sale_price();
    $priceAmt       = $product->get_price();

    return customPriceHtml( $priceAmt, $regularPrice, $salePrice );
}

add_action('woocommerce_product_thumbnails_main_slider', 'woo_product_thumbnails_main_slider');
function woo_product_thumbnails_main_slider() {
    $args = [];
    wc_get_template('single-product/product-thumbnails-main.php', $args);
}

remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );

/**
 * Remove product tab Description header
 */
add_filter( 'woocommerce_product_description_heading', 'remove_product_tab_description_header' );
function remove_product_tab_description_header( $header ) {
    $header = '';

    return $header;
}

/**
 * Custom product tab
 */
add_filter( 'woocommerce_product_tabs', 'starlyn_custom_product_tabs' );
function starlyn_custom_product_tabs( $tabs ) {
    unset( $tabs['additional_information'] );

    $tabs['recommendation'] = [
        'title'    => __( 'Recommendation', 'woocommerce' ),
        'priority' => 40,
        'callback' => 'woocommerce_product_recommendation_tab',
    ];

    $tabs['video'] = [
        'title'    => __( 'Video', 'woocommerce' ),
        'priority' => 50,
        'callback' => 'woocommerce_product_video_tab',
    ];

    return $tabs;
}

if( ! function_exists( 'woocommerce_product_recommendation_tab' ) ) {
    function woocommerce_product_recommendation_tab() {
        wc_get_template( 'single-product/tabs/recommendation.php' );
    }
}

if( ! function_exists( 'woocommerce_product_video_tab' ) ) {
    function woocommerce_product_video_tab() {
        wc_get_template( 'single-product/tabs/video.php' );
    }
}

/**
 * Remove product tab Additional Information header
 */
add_filter( 'woocommerce_product_additional_information_heading', 'remove_product_tab_additional_information_header' );
function remove_product_tab_additional_information_header( $header ) {
    $header = '';

    return $header;
}

/**
 * Customize comment class.
 */
add_filter( 'comment_class', 'woo_comment_class', 10, 1 );
function woo_comment_class( $classes ) {
    $classes[] = 'item';

    return $classes;
}

/**
 * Remove product review meta.
 */
remove_action( 'woocommerce_review_meta', 'woocommerce_review_display_meta', 10 );

/**
 * Custom product review content.
 */
remove_action( 'woocommerce_review_comment_text', 'woocommerce_review_display_comment_text', 10 );
add_action( 'woocommerce_review_comment_text', 'woo_review_display_comment_text', 10 );

function woo_review_display_comment_text() {
    comment_text();
}

add_action( 'woocommerce_before_cart', 'woo_before_cart' );
function woo_before_cart() {
?>
    <div class="cart-page">
        <div class="wrapper">
<?php
}

add_action( 'woocommerce_after_cart', 'woo_after_cart' );
function woo_after_cart() {
?>
        </div>
    </div>
<?php
}

/**
 * Remove cart cross sells.
 */
remove_action( 'woocommerce_cart_collaterals', 'woocommerce_cross_sell_display' );

/**
 * Remove cart notice wrapper.
 */
remove_action( 'woocommerce_before_cart', 'woocommerce_output_all_notices', 10 );

/**
 * Custom cart notice wrapper.
 */
add_action( 'woo_cart_output_all_notices', 'woocommerce_output_all_notices', 10 );

/**
 * Customize my account order column.
 */
add_filter( 'woocommerce_my_account_my_orders_columns', 'woo_my_account_order_columns', 10, 1 );
function woo_my_account_order_columns( $columns ) {
    unset( $columns['order-number'] );
    unset( $columns['order-date'] );
    unset( $columns['order-status'] );
    unset( $columns['order-total'] );
    unset( $columns['order-actions'] );

    $columns['order-number']    = __( 'Order#', 'woocommerce' );
    $columns['order-date']      = __( 'Tanggal', 'woocommerce' );
    $columns['order-ship-to']   = __( 'Dikirim ke', 'woocommerce' );
    $columns['order-total']     = __( 'Total Belanja', 'woocommerce' );
    $columns['order-status']    = __( 'Status', 'woocommerce' );
    $columns['order-actions']   = '&nbsp;';
    $columns['order-action-pay']= '&nbsp;';

    return $columns;
}

add_filter( 'woocommerce_my_account_my_orders_query', 'my_account_orders_custom_query', 10, 1 );
function my_account_orders_custom_query( $args ) {
    // filter by order number
    if( isset( $_GET['order_no'] ) && $_GET['order_no'] ) {
        $args['post__in'] = array( $_GET['order_no'] );
    }

    // filter by date created from
    if( isset( $_GET['from'] ) && $_GET['from'] ) {
        $args['date_created'] = '>=' . $_GET['from'];
    }

    // filter by date created to
    if( isset( $_GET['to'] ) && $_GET['to'] ) {
        $args['date_created'] = '<=' . $_GET['to'];
    }

    // filter by date created from - to
    if( ( isset( $_GET['from'] ) && $_GET['from'] ) && ( isset( $_GET['to'] ) && $_GET['to'] ) ) {
        $args['date_created'] = $_GET['from'] . '...' . $_GET['to'];
    }

    // set posts per page = 10
    $args['posts_per_page'] = 10;

    return $args;
}

/**
 * Customize order details order in my account view order.
 */
add_filter('woocommerce_get_order_item_totals', 'woo_get_order_item_totals', 10, 3);
function woo_get_order_item_totals( $total_rows, $order, $tax_display ) {
    $item_count = $order->get_item_count() - $order->get_item_count_refunded();

    if( isset( $total_rows['cart_subtotal'] ) ) {
        $total_rows['cart_subtotal']['label'] = sprintf('Items Total (%1$s)', $item_count); // rename Subtotal row
    }

    if( isset( $total_rows['tax'] ) ) {
        $total_rows['tax']['label'] = 'Sales Tax'; // rename Tax row
    }

    if( isset( $total_rows['discount'] ) ) {
        $total_rows['discount']['label'] = 'Discount Voucher'; // rename Discount row
    }

    if( isset( $total_rows['shipping'] ) ) {
        $total_rows['shipping']['label'] = 'Total Shipping Fee'; // rename Discount row
    }

    unset( $total_rows['payment_method'] ); // remove Pay Method row

    return $total_rows;
}

/**
 * Remove coupon form in checkout page.
 */
//remove_action( 'woocommerce_before_checkout_form', 'woocommerce_checkout_coupon_form', 10 );
remove_action( 'woocommerce_before_checkout_form', 'woocommerce_output_all_notices', 10 );
remove_action( 'woocommerce_before_checkout_form_cart_notices', 'woocommerce_output_all_notices', 10 );
add_action( 'starlyn_checkout_output_all_notices', 'woocommerce_output_all_notices', 10 );

/**
 * Add opening div in checkout page.
 */
//add_action( 'woocommerce_before_checkout_form', 'starlyn_opening_div_in_checkout_page', 5 );
function starlyn_opening_div_in_checkout_page() {
?>
    <div class="checkout-page">
        <div class="wrapper">
<?php
}

/**
 * Add closing div in checkout page.
 */
//add_action( 'woocommerce_after_checkout_form', 'starlyn_closing_div_in_checkout_page', 10 );
function starlyn_closing_div_in_checkout_page() {
?>
        </div>
    </div>
<?php
}

/**
 * Remove payment options in checkout shipping page.
 */
//remove_action( 'woocommerce_checkout_order_review', 'woocommerce_checkout_payment', 20 );

/**
 * Make payment options in checkout payment.
 */
//add_action( 'starlyn_checkout_payment_list', 'woocommerce_checkout_payment', 20 );

//add_action('starlyn_select_payment_method_page', 'sn_select_payment_method_page');
function sn_select_payment_method_page() {
    wc_get_template( 'checkout/starlyn-form-payments.php' );
}

//add_action('starlyn_select_shipping_method_page', 'sn_select_shipping_method_page');
function sn_select_shipping_method_page() {
    if( !is_user_logged_in() ) {
        return;
    }

    $myAddress = get_all_my_address_book();

    $args = [
        'hasAddress' => count( $myAddress ) ? true : false
    ];

    wc_get_template( 'checkout/starlyn-form-shipping.php', $args );
}

add_filter('woocommerce_order_button_text', 'change_order_button_text', 10, 1);
function change_order_button_text( $text ) {
    $text = 'Payment';

    return $text;
}

// Remove bacs on thank you page
add_action('init', 'remove_bacs_from_thank_you_page', 100);
function remove_bacs_from_thank_you_page() {
    if( ! function_exists( 'WC' ) ) {
        return;
    }

    // get all available payment gateways
    $availableGateways = WC()->payment_gateways()->get_available_payment_gateways();

    // get the bacs gateway class
    $gateway = isset( $availableGateways['bacs'] ) ? $availableGateways['bacs'] : false;

    if( false === $gateway ) {
        return;
    }

    // Remove the action, which places the BACS details on the thank you page
    remove_action( 'woocommerce_thankyou_bacs', array( $gateway, 'thankyou_page' ) );
}

// Remove thankyou page order details
remove_action( 'woocommerce_thankyou', 'woocommerce_order_details_table', 10 );

add_filter('woocommerce_states', 'custom_wc_states', 10, 1);
function custom_wc_states( $states ) {
    $provinces = get_province();

    if( $provinces ) {
        $statesID = [];

        foreach ( $provinces as $province ) {
            $statesID[$province->province_name] = $province->province_name;
        }

        $states['ID'] = $statesID;
    }

    return $states;
}

add_filter( 'woocommerce_default_address_fields', 'custom_default_address_field', 999, 1 );
function custom_default_address_field( $fields ) {
    // Alamat 1
    $fields['address_1']['label'] = 'Alamat';
    $fields['address_1']['priority'] = 4;
    $fields['address_1']['placeholder'] = null;

    // Alamat 2
    $fields['address_2']['label'] = 'Label';
    $fields['address_2']['required'] = true;
    $fields['address_2']['placeholder'] = null;
    $fields['address_2']['priority'] = 3;

    // Provinsi
    $fields['state']['priority'] = 5;

    // Kotamadya
    $fields['city']['label'] = 'Kotamadya';
    $fields['city']['priority'] = 6;
    $fields['city']['class'] = [
        'update_totals_on_change'
    ];

    // Kode pos
    $fields['postcode']['label'] = 'Kode pos';
    $fields['postcode']['priority'] = 8;

    return $fields;
}

add_filter( 'woocommerce_checkout_fields', 'custom_checkout_validation', 10, 1 );
function custom_checkout_validation( $fields ) {
    $fields['billing']['billing_district'] = [
        'label'     => 'Daerah',
        'required'  => true,
        'class'     => [
            'form-row-first',
            'update_totals_on_change'
        ],
        'priority'  => 7
    ];

    $fields['billing']['billing_email']['priority'] = 0;
    $fields['billing']['billing_email']['label'] = 'Email';

    $fields['billing']['billing_first_name']['priority'] = 1;
    $fields['billing']['billing_first_name']['label'] = 'Nama';

    $fields['billing']['billing_phone']['label'] = 'Nomor telepon';
    $fields['billing']['billing_phone']['priority'] = 2;

    $fields['billing']['billing_country']['class'] = [
        'is-hidden',
        'form-row-wide',
        'address-field',
        'update_totals_on_change',
    ];

    $fields['billing']['billing_state']['label'] = 'Provinsi';

    $fields['billing']['billing_country']['class'] = ['is-hidden'];

    $fields['order']['order_comments']['label'] = 'Catatan';
    $fields['order']['order_comments']['placeholder'] = null;
    $fields['order']['order_comments']['priority'] = 9;

    unset( $fields['billing']['billing_last_name'] );
    unset( $fields['billing']['billing_company'] );

    return $fields;

}

add_filter('woocommerce_get_country_locale', 'wpse_120741_wc_change_state_label_locale');
function wpse_120741_wc_change_state_label_locale($locale){
    // uncomment /** like this /**/ to show the locales
    /**
    echo '<pre>';
    print_r($locale);
    echo '</pre>';
    /**/
    $locale['ID']['state']['label'] = __('Provinsi', 'woocommerce');

    return $locale;
}

add_filter('woocommerce_no_shipping_available_html', 'custom_no_shipping_available_html', 10, 1);
function custom_no_shipping_available_html( $html ) {
    $html = __( 'Maaf, belum ada metode pengiriman yang tersedia untuk alamat yang Anda pilih saat ini.', 'woocommerce' );

    return $html;
}

add_filter('woocommerce_shipping_may_be_available_html', 'custom_shipping_may_be_available_html', 10, 1);
function custom_shipping_may_be_available_html( $html ) {
    if( is_user_logged_in() ) {
        $html = __('Silakan pilih alamat untuk menampilkan opsi pengiriman.', 'woocommerce');
    } else {
        $html = __('Silakan lengkapi alamat untuk menampilkan opsi pengiriman.', 'woocommerce');
    }

    return $html;
}

//add_filter('sicepat_active_services', 'add_sicepat_active_services', 10, 1);
function add_sicepat_active_services( $active_services ) {
    $active_services['CARGO'] = 'CARGO';
    return $active_services;
}

/**
 * Save the custom field at shipping calculator.
 */
add_action('woocommerce_calculated_shipping', 'my_custom_shipping_calculator_field');
function my_custom_shipping_calculator_field()
{

    $area = isset($_REQUEST['billing_district']) ? $_REQUEST['billing_district'] : '';

    if ($area) {
        WC()->customer->__set('billing_district', $area);
    }

}

remove_action( 'woocommerce_before_checkout_form', 'woocommerce_checkout_coupon_form', 10 );
remove_action( 'woocommerce_checkout_order_review', 'woocommerce_order_review', 10 );
remove_action( 'woocommerce_checkout_order_review', 'woocommerce_checkout_payment', 20 );

add_action( 'starlyn_checkout_shipping_review', 'woocommerce_order_review', 10 );

function checkout_page_summary_html() {
    wc_get_template( 'checkout/summary.php' );
}

function summary_order_detail_shipping() {
    wc_get_template( 'checkout/summary-order-detail-shipping.php' );
}

function summary_order_detail_payment() {
    wc_get_template( 'checkout/summary-order-detail-payment.php' );
}

function checkout_page_summary_shipping_label() {
    $shippingLabel = get_shipping_name_by_id( WC()->session->get( 'chosen_shipping_methods' )[0] );

    $args = [
        'shipping_label' => $shippingLabel
    ];

    wc_get_template( 'checkout/summary-shipping-label.php', $args );
}

add_filter( 'woocommerce_update_order_review_fragments', 'checkout_summary_fragments', 10, 1 );
function checkout_summary_fragments( $array ) {
    if( ! is_checkout() ) {
        return;
    }

    ob_start();
    echo '<div class="summary-shipping-wrapper-content">';
    summary_order_detail_shipping();
    echo '</div>';
    $summaryShipping = ob_get_clean();
    $array['.summary-shipping-wrapper-content'] = $summaryShipping;

    ob_start();
    echo '<div class="summary-payment-wrapper-content">';
    summary_order_detail_payment();;
    echo '</div>';
    $summaryPayment = ob_get_clean();
    $array['.summary-payment-wrapper-content'] = $summaryPayment;

    ob_start();
    echo '<div class="summary-payment-shipping-label-wrapper">';
    checkout_page_summary_shipping_label();
    echo '</div>';
    $summaryShippingLabel = ob_get_clean();
    $array['.summary-payment-shipping-label-wrapper'] = $summaryShippingLabel;

    ob_start();
    echo '<div class="btn-payment-fragments-shipping-content">';
    starlyn_button_payment_shipping();
    echo '</div>';
    $btnPaymentShippingFragments = ob_get_clean();
    $array['.btn-payment-fragments-shipping-content'] = $btnPaymentShippingFragments;

    ob_start();
    echo '<div class="btn-payment-fragments-payment-content">';
    starlyn_button_payment_payment();
    echo '</div>';
    $btnPaymentFragments = ob_get_clean();
    $array['.btn-payment-fragments-payment-content'] = $btnPaymentFragments;

    return $array;
}

function starlyn_button_payment_shipping() {
    $choosenShipping = WC()->session->get('chosen_shipping_methods')[0];

    if( $choosenShipping ) {
        echo '<a href="javascript:void(0)" class="button button-primary show_payment_method_wrapping">Payment</a>';
    } else {
        echo '<a href="javascript:void(0)" class="button button-primary is-disabled">Payment</a>';
        echo '<p style="margin-top: 10px">Silahkan pilih metode pengiriman terlebih dahulu.</p>';
    }
}

function starlyn_button_payment_payment() {
?>
    <a href="javascript:void(0)" class="button button-primary btn-checkout-place-order">
        <svg class="icon" role="img"><use xlink:href="<?= library_url() ?>/images/svg-symbols.svg#icon-secured"/></svg>
        Proceed Payment
    </a>
<?php
}

function is_production() {
    $bool = false;
    if( in_array( APP_ENV, ['prod', 'production'] ) ) {
        $bool = true;
    }
    return $bool;
}

add_action( 'template_redirect', 'define_default_payment_gateway' );
function define_default_payment_gateway(){
    if( is_checkout() && ! is_wc_endpoint_url() ) {
        // HERE define the default payment gateway ID
        $default_payment_id = 'bacs';

        WC()->session->set( 'chosen_payment_method', $default_payment_id );
    }
}

// add_action('woocommerce_checkout_order_processed', 'wc_debug', 10, 3);
function wc_debug($order_id, $posted_data, $order) {
    error_log( '=================' );
    error_log( 'order id: ' . $order_id );
    error_log( 'posted_data: ' . $posted_data['shipping_method'][0] );
    error_log( 'session: ' . WC()->session->get('chosen_shipping_methods')[0] );
    error_log( $posted_data['shipping_method'][0] === WC()->session->get('chosen_shipping_methods')[0] );
    error_log( '=================' );
    //v('stop',1);
}

function create_error_log( $title, $log, $encode = false ) {
    $error_log = $title . ':';
    if( $encode ) {
        $error_log .= json_encode( $log );
    } else {
        $error_log .= $log;
    }
    error_log( $error_log );
}

function sn_get_product_categories() {
    $orderby    = 'id';
    $order      = 'asc';
    $hideEmpty  = false;
    $args       = array(
        'orderby'    => $orderby,
        'order'      => $order,
        'hide_empty' => $hideEmpty,
    );

    $categories = get_terms( 'product_cat', $args );

    return $categories;
}

/**
 * Change the strength requirement on the woocommerce password
 *
 * Strength Settings
 * 4 = Strong
 * 3 = Medium (default)
 * 2 = Also Weak but a little stronger
 * 1 = Password should be at least Weak
 * 0 = Very Weak / Anything
 */
add_filter( 'woocommerce_min_password_strength', 'starlyn_change_password_strength' );
function starlyn_change_password_strength( $strength ) {
    return 0;
}

function sicepat_get_destination_code( $province, $combine_city ) {
    $array_city = explode(', ', $combine_city );
    $regency    = isset( $array_city[0] ) && $array_city[0] ? $array_city[0] : '';
    $district   = isset( $array_city[1] ) && $array_city[1] ? $array_city[1] : '';

    $dest_code  = get_sicepat_code( $province, $regency, $district );

    return $dest_code;
}

function get_sicepat_code( $province, $regency, $district ) {
    global $wpdb;
    $table_name = $wpdb->prefix . "district";
    $code = $wpdb->get_row( "SELECT sicepat_code FROM $table_name WHERE province_name='$province' AND regency_name='$regency' AND district_name='$district'" );
    if( $code ) {
        return $code->sicepat_code;
    }
    return null;
}

function jne_get_destination_code( $province, $combine_city ) {
    $array_city = explode(', ', $combine_city );
    $regency    = isset( $array_city[0] ) && $array_city[0] ? $array_city[0] : '';
    $district   = isset( $array_city[1] ) && $array_city[1] ? $array_city[1] : '';

    $dest_code  = get_jne_code( $province, $regency, $district );

    return $dest_code;
}

function get_jne_code( $province, $regency, $district ) {
    global $wpdb;
    $table_name = $wpdb->prefix . "district";
    $code = $wpdb->get_row( "SELECT jne_code FROM $table_name WHERE province_name='$province' AND regency_name='$regency' AND district_name='$district'" );
    if( $code ) {
        return $code->jne_code;
    }
    return null;
}

add_action('woocommerce_checkout_order_processed', 'save_shipping_sub_service_meta', 10, 3);
function save_shipping_sub_service_meta( $order_id, $posted_data, $order ) {
    // get shipping sub service
    $chosen_methods         = WC()->session->get( 'chosen_shipping_methods' );
    $chosen_shipping        = $chosen_methods[0];
    $chosen_shipping_array  = explode( '_', $chosen_shipping );
    $sub_service_pos        = count($chosen_shipping_array)-1;
    $shipping_sub_service   = isset( $chosen_shipping_array[ $sub_service_pos ] ) ? strtoupper( $chosen_shipping_array[ $sub_service_pos ] ) : '';

    $shipping_service       = null;
    $destination_code       = null;
    $origin_code            = null;

    if( in_array( 'sicepat', $chosen_shipping_array ) ) {
        $shipping_service   = 'sicepat';
        $destination_code   = sicepat_get_destination_code( $order->get_billing_state(), $order->get_billing_city() );

        // get SiCepat shipping origin code.
        $sicepat_options    = get_field( 'sicepat_shipping_group', 'option' );
        $origin_code        = ! isset( $sicepat_options['sicepat_origin_code'] ) ? '' : $sicepat_options['sicepat_origin_code'];
    } elseif( in_array( 'jne', $chosen_shipping_array ) ) {
        $shipping_service   = 'jne';
        $destination_code   = jne_get_destination_code( $order->get_billing_state(), $order->get_billing_city() );

        // get JNE shipping origin code.
        $jne_options        = get_field( 'jne_shipping_group', 'option' );
        $origin_code        = ! isset( $jne_options['jne_origin_code'] ) ? '' : $jne_options['jne_origin_code'];
    } else {
        $shipping_service   = 'starlyn_logistic';
        $shipping_sub_service= $chosen_shipping;
    }

    // update order meta
    update_post_meta( $order_id, '_shipping_service', $shipping_service );
    update_post_meta( $order_id, '_shipping_sub_service', $shipping_sub_service );
    update_post_meta( $order_id, '_shipping_origin_code', $origin_code );
    update_post_meta( $order_id, '_shipping_destination_code', $destination_code );
}

add_action( 'init', 'myStartSession', 1 );
add_action( 'wp_logout', 'myEndSession' );
add_action( 'wp_login', 'myEndSession' );

function myStartSession() {
    if( !session_id() ) {
        session_start();
    }
}

function myEndSession() {
    session_destroy();
}

add_action( 'admin_notices', 'sample_admin_notice__success', 10 );
function sample_admin_notice__success() {
    if( ! isset( $_SESSION['notice_success'] ) )
        return;
    ?>
    <div class="notice notice-success is-dismissible">
        <p><?php _e( $_SESSION['notice_success'], 'woocommerce' ); ?></p>
    </div>
    <?php
    unset( $_SESSION['notice_success'] );
}

add_action( 'admin_notices', 'sample_admin_notice__error', 10 );
function sample_admin_notice__error() {
    if( ! isset( $_SESSION['notice_error'] ) )
        return;
    ?>
    <div class="notice notice-error is-dismissible">
        <p><?php _e( $_SESSION['notice_error'], 'woocommerce' ); ?></p>
    </div>
    <?php
    unset( $_SESSION['notice_error'] );
}

/**
 * This function will register a new custom order status: Shipped.
 */
add_action( 'init', 'register_shipped_order_status' );
function register_shipped_order_status() {
    register_post_status(
        'wc-shipped',
        array(
            'label'                     => 'Shipped',
            'public'                    => true,
            'exclude_from_search'       => false,
            'show_in_admin_all_list'    => true,
            'show_in_admin_status_list' => true,
            'label_count'               => _n_noop( 'Shipped (%s)', 'Shipped (%s)' ),
        )
    );
}

/**
 * Add the custom order status to the list of order statuses.
 * Possible values are, e.g. wc-pending, wc-processing, wc-on-hold, wc-completed.
 */
add_filter( 'wc_order_statuses', 'add_shipped_to_order_statuses' );
function add_shipped_to_order_statuses( $order_statuses ) {
    $new_order_statuses = [];

    // add new order status after processing
    foreach ( $order_statuses as $key => $status ) {

        $new_order_statuses[ $key ] = $status;

        if( 'wc-processing' === $key ) {
            $new_order_statuses['wc-shipped'] = 'Shipped';
        }

    }

    return $new_order_statuses;
}

/**
 * This function will register a new custom order status: Verification Needed.
 *
 * https://www.businessbloomer.com/woocommerce-set-custom-order-status-for-new-orders/
 */
add_filter( 'woocommerce_register_shop_order_post_statuses', 'starlyn_register_custom_order_status' );
function starlyn_register_custom_order_status( $order_statuses ){
    // Status must start with "wc-"
    $order_statuses['wc-payment-verify'] = array(
        'label'                     => _x( 'Verification Needed', 'Order status', 'starlyn' ),
        'public'                    => false,
        'exclude_from_search'       => false,
        'show_in_admin_all_list'    => true,
        'show_in_admin_status_list' => true,
        'label_count'               => _n_noop( 'Verification Needed <span class="count">(%s)</span>', 'Verification Needed <span class="count">(%s)</span>', 'starlyn' ),
    );
    return $order_statuses;
}

/**
 * Add the custom order status to the list of order statuses.
 * Possible values are, e.g. wc-pending, wc-processing, wc-on-hold, wc-completed.
 */
add_filter( 'wc_order_statuses', 'add_payment_verify_to_order_statuses' );
function add_payment_verify_to_order_statuses( $order_statuses ) {
    $new_order_statuses = [];

    // add new order status after processing
    foreach ( $order_statuses as $key => $status ) {

        $new_order_statuses[ $key ] = $status;

        if( 'wc-pending' === $key ) {
            $new_order_statuses['wc-payment-verify'] = _x( 'Verification Needed', 'Order status', 'starlyn' );
        }

    }

    return $new_order_statuses;
}

/**
 * Enqueue SiCepat Tracking JS
 */
add_action( 'wp_enqueue_scripts', 'sicepat_tracking_enqueue_scripts' );
function sicepat_tracking_enqueue_scripts() {
    wp_register_script( 'sicepat-tracking-script', get_stylesheet_directory_uri() . '/library/js/tracking/sicepat-tracking-ajax.js', array('jquery'), '', true );
    wp_localize_script(
        'sicepat-tracking-script', 'vars', ['ajaxurl' => admin_url( 'admin-ajax.php' )]
    );
    wp_enqueue_script( 'sicepat-tracking-script' );
}

/**
 * SiCepat Tracking Ajax Process.
 */
add_action( 'wp_ajax_sicepat_tracking_ajax', 'sicepat_tracking_ajax' );
add_action( 'wp_ajax_nopriv_sicepat_tracking_ajax', 'sicepat_tracking_ajax' );
function sicepat_tracking_ajax() {
    // Do a nonce check
    if( ! isset( $_POST['security'] ) || ! wp_verify_nonce( $_POST['security'], 'sicepat-tracking-nonce' ) ) {
        wp_send_json( array( 'nonce_fail' => 1 ) );
        exit;
    }

    $awb_no = $_POST['awb_no'];

    //---------//
    // process //
    //---------//

    // delete_transient( 'sicepat_tracking_transient' );
    $transient = get_transient( 'sicepat_tracking_transient' );

    if( ! empty( $transient ) ) {
        $args = $transient;
    } else {
        $args = sicepat_tracking( $awb_no );
        set_transient( 'sicepat_tracking_transient', $args, HOUR_IN_SECONDS );
    }

    if( $args->is_success ) {
        ob_start();
        wc_get_template( 'myaccount/tracking/popup-tracking-sicepat.php', $args );
        $response_html = ob_get_clean();

        wp_send_json( array( 'success' => 1, 'response_html' => $response_html ) );
        exit;
    } else {
        wp_send_json( array( 'success' => 0, 'errors' => $args->errors ) );
        exit;
    }
}

/**
 * Enqueue JNE Tracking JS
 */
add_action( 'wp_enqueue_scripts', 'jne_tracking_enqueue_scripts' );
function jne_tracking_enqueue_scripts() {
    wp_register_script( 'jne-tracking-script', get_stylesheet_directory_uri() . '/library/js/tracking/jne-tracking-ajax.js', array('jquery'), '', true );
    wp_localize_script(
        'jne-tracking-script', 'vars', ['ajaxurl' => admin_url( 'admin-ajax.php' )]
    );
    wp_enqueue_script( 'jne-tracking-script' );
}



/**
 * SiCepat Tracking Ajax Process.
 */
add_action( 'wp_ajax_jne_tracking_ajax', 'jne_tracking_ajax' );
add_action( 'wp_ajax_nopriv_jne_tracking_ajax', 'jne_tracking_ajax' );
function jne_tracking_ajax() {
    // Do a nonce check
    if( ! isset( $_POST['security'] ) || ! wp_verify_nonce( $_POST['security'], 'jne-tracking-nonce' ) ) {
        wp_send_json( array( 'nonce_fail' => 1 ) );
        exit;
    }

    $awb_no = $_POST['awb_no'];

    //---------//
    // process //
    //---------//

    // delete_transient( 'jne_tracking_transient' );
    $transient = get_transient( 'jne_tracking_transient' );

    if( ! empty( $transient ) ) {
        $args = $transient;
    } else {
        $args = jne_tracking( $awb_no );
        set_transient( 'jne_tracking_transient', $args, HOUR_IN_SECONDS );
    }

    if( $args->is_success ) {
        ob_start();
        wc_get_template( 'myaccount/tracking/popup-tracking-jne.php', $args );
        $response_html = ob_get_clean();

        wp_send_json( array( 'success' => 1, 'response_html' => $response_html ) );
        exit;
    } else {
        wp_send_json( array( 'success' => 0, 'errors' => $args->errors ) );
        exit;
    }
}

function get_chosen_shipping_code_prefix() {
    $chosen_methods         = WC()->session->get( 'chosen_shipping_methods' );
    $chosen_shipping        = isset( $chosen_methods[0] ) ? $chosen_methods[0] : '';
    $chosen_shipping_arr    = $chosen_shipping != '' ? explode( '_', $chosen_shipping ) : [];
    $chosen_shipping        = isset( $chosen_shipping_arr[0] ) ? $chosen_shipping_arr[0] : '';

    return $chosen_shipping;
}

function get_chosen_shipping_code_subfix() {
    $chosen_methods         = WC()->session->get( 'chosen_shipping_methods' );
    $chosen_shipping        = isset( $chosen_methods[0] ) ? $chosen_methods[0] : '';
    $chosen_shipping_arr    = $chosen_shipping != '' ? explode( '_', $chosen_shipping ) : [];
    $count                  = count( $chosen_shipping_arr ) - 1;
    $chosen_shipping        = isset( $chosen_shipping_arr[$count] ) ? $chosen_shipping_arr[$count] : '';

    return $chosen_shipping;
}

function round_nearest_hundred_up( $number ) {
    return ceil( $number / 100 ) * 100;
}

function get_product_dimensions( $_product ) {
    if ( $_product->has_dimensions() && ! $_product->is_type('variable') ) {
        return [
            'length' => ! empty( $_product->get_length() ) ? round( $_product->get_length(), 0, PHP_ROUND_HALF_UP ) : 0,
            'width'  => ! empty( $_product->get_width() ) ? round( $_product->get_width(), 0, PHP_ROUND_HALF_UP ) : 0,
            'height' => ! empty( $_product->get_height() ) ? round( $_product->get_height(), 0, PHP_ROUND_HALF_UP ) : 0,
        ];
    } else {
        return wc_format_dimensions( $_product->get_dimensions( false ) );
    }
}

add_filter( 'upload_mimes', 'my_custom_mime_types' );
function my_custom_mime_types( $mimes ) {
    // New allowed mime types.
    $mimes['svg'] = 'image/svg+xml';
    $mimes['svgz'] = 'image/svg+xml';

    return $mimes;
}

add_filter( 'woocommerce_variable_price_html', 'custom_variable_price_html' );
function custom_variable_price_html( $price ) {
?>
    <?php echo is_product() ? '' : '<div class="price">' ?>
        <span class="value">
            <?php echo $price ?>
        </span>
    <?php echo is_product() ? '' : '</div>' ?>
<?php
}

add_action( 'woocommerce_before_add_to_cart_quantity', 'add_style_before_add_to_cart_quantity' );
function add_style_before_add_to_cart_quantity() {
?>
    <div class="product-single-stock">
        <p class="label">Amount</p>

        <div class="input">
            <div class="quantity-input js-qty-input">
                <svg class="icon minus" role="img">
                    <use xlink:href="<?= library_url() ?>/images/svg-symbols.svg#icon-minus" />
                </svg>
<?php
}

add_action( 'woocommerce_after_add_to_cart_quantity', 'add_style_after_add_to_cart_quantity' );
function add_style_after_add_to_cart_quantity() {
?>
                <svg class="icon plus" role="img">
                    <use xlink:href="<?= library_url() ?>/images/svg-symbols.svg#icon-plus" />
                </svg>
            </div> <!-- end of .quantity-input js-qty-input -->
        </div> <!-- end of .input -->
    </div> <!-- end of .product-single-stock -->
<?php
}

// Product Review -->
add_action( 'woocommerce_custom_product_review_form', 'wc_custom_product_review_form' );
function wc_custom_product_review_form() {
    global $product;
    $args = [
        'product_id' => $product->get_id()
    ];
    wc_get_template(  'includes/product-reviews/popup-product-review.php', $args );
}

add_filter( 'comment_post_redirect', 'redirect_after_comment', 10, 2 );
function redirect_after_comment( $location, $comment ){
    $post_id = $comment->comment_post_ID;

    // product-only comment redirects
    if( 'product' == get_post_type( $post_id ) ) {
        wc_add_notice( 'Terima kasih telah memberi ulasan.', 'success' );
        $location = $_SERVER["HTTP_REFERER"]; //add_query_arg( ['tab' => 'tab-reviews'], get_permalink( $post_id ) );
    }
    return $location;
}

add_filter( 'woocommerce_my_account_my_orders_review_query', 'my_account_orders_review_custom_query', 10, 1 );
function my_account_orders_review_custom_query( $args ) {
    // filter by order number
    if( isset( $_GET['order_no'] ) && $_GET['order_no'] ) {
        $args['post__in'] = array( $_GET['order_no'] );
    }

    // filter by date created from
    if( isset( $_GET['from'] ) && $_GET['from'] ) {
        $args['date_created'] = '>=' . $_GET['from'];
    }

    // filter by date created to
    if( isset( $_GET['to'] ) && $_GET['to'] ) {
        $args['date_created'] = '<=' . $_GET['to'];
    }

    // filter by date created from - to
    if( ( isset( $_GET['from'] ) && $_GET['from'] ) && ( isset( $_GET['to'] ) && $_GET['to'] ) ) {
        $args['date_created'] = $_GET['from'] . '...' . $_GET['to'];
    }

    return $args;
}

add_action( 'comment_post', 'add_product_review_commentmeta' );
function add_product_review_commentmeta( $comment_id ) {
    $comment = get_comment( $comment_id );
    $type = $comment->comment_type;
    if( is_user_logged_in() && $type == 'review' ) {
        $user_id  = get_current_user_id();
        $location = get_user_meta( $user_id, 'billing_state', true );
        add_comment_meta( $comment_id, 'location', $location );

        if( isset( $_POST['review_key'] ) && $_POST['review_key'] ) {
            add_comment_meta( $comment_id, 'review_key', $_POST['review_key'] );
        }
    }
}
// End of Product Review

// redirect to login page after failed login.
add_action( 'wp_login_failed', 'custom_login_failed', 10, 2 );
function custom_login_failed( $username, $error ) {
    $referrer = $_SERVER['HTTP_REFERER'];

    $error_code = [];
    if( isset( $error->errors ) && $error->errors ) {
        foreach ( $error->errors as $err_code => $error_messages ) {
            $error_code[ $err_code ] = $err_code;
        }
    }

    if ( $referrer && ! strstr( $referrer, 'wp-login' ) && ! strstr( $referrer,'wp-admin' ) ) {
        wp_redirect( add_query_arg( ['login' =>'failed', 'error_code' => implode(',', $error_code), 'user_email' => $username ], get_permalink( get_option('woocommerce_myaccount_page_id') ) ) );
        exit;
    }
}

// Register Verification
// prevents the user from logging in automatically after registering their account
add_filter( 'woocommerce_registration_redirect', 'wc_registration_redirect' );
function wc_registration_redirect( $redirect_to ) {
    wp_logout();
    $location = add_query_arg( ['n' => ''], get_permalink( get_page_by_path( 'verify' ) ) );
    wp_redirect( $location ); // redirects to a confirmation message
    exit;
}

// when a user registers, sends them an email to verify their account
add_action( 'user_register', 'my_user_register', 10, 1 );
function my_user_register( $user_id ) {
    $user_info  = get_userdata( $user_id );
    $code       = md5( time() );
    $string     = ['id' => $user_id, 'code' => $code];

    update_user_meta( $user_id, 'is_activated', 0 );
    update_user_meta( $user_id, 'activationcode', $code );

    $url        = get_site_url() . '/verify/?p=' . base64_encode( serialize( $string ) );

    ob_start();
    wc_get_template(
        'emails/customer-account-confirmation.php',
            array(
                'url' => $url
            )
        );
    $html = ob_get_contents();
    ob_get_clean();

    wc_mail( $user_info->user_email, __( 'Activate your account' ), $html );
}

// when the user logs in, checks whether their email is verified
add_filter( 'wp_authenticate_user', 'wp_authenticate_user', 10, 1 );
function wp_authenticate_user( $userdata ) {
    $has_activation_status = get_user_meta( $userdata->ID, 'is_activated', false );

    // checks if this is an older account without activation status; skips the rest of the function if it is
    if( $has_activation_status ) {
        $is_activated = get_user_meta( $userdata->ID, 'is_activated', true );
        if( ! $is_activated ) {
            // my_user_register( $userdata->ID ); // resends the activation mail if the account is not activated
            $userdata = new WP_Error(
                'register_confirmation_error',
                __( '<strong>Error:</strong> Your account has to be activated before you can login. Please click the link in the activation email that has been sent to you.<br /> If you do not receive the activation email within a few minutes, check your spam folder or <a href="/verify/?u='.$userdata->ID.'">click here to resend it</a>.' )
            );
        }
    }

    return $userdata;

}

// handles all this verification stuff
add_action( 'init', 'register_verification_init' );
function register_verification_init() {
    // If accessed via an authentification link
    if( isset( $_GET['p'] ) && ! empty( $_GET['p'] ) ) {
        $data           = @unserialize( base64_decode( $_GET['p'] ) );
        $code           = get_user_meta( $data['id'], 'activationcode', true );
        $is_activated   = get_user_meta( $data['id'], 'is_activated', true ); // checks if the account has already been activated. We're doing this to prevent someone from logging in with an outdated confirmation link

        if( $is_activated ) {
            wc_add_notice( __( 'This account has already been activated. Please log in with your username and password.' ), 'error' );
        } else {
            // checks whether the decoded code given is the same as the one in the data base
            if( $code == $data['code'] ) {
                update_user_meta( $data['id'], 'is_activated', 1 );

                $user_id = $data['id'];
                $user    = get_user_by( 'id', $user_id );

                if( $user ) {
                    //wp_set_current_user( $user_id, $user->user_login );
                    //wp_set_auth_cookie( $user_id );
                    //do_action( 'wp_login', $user->user_login, $user );
                }

                // sent new email account to customer
                $class_emails = new WC_Emails();
                $class_emails->customer_new_account( $user_id );

                wc_add_notice( __( '<strong>Success:</strong> Your account has been activated! You have been logged in and can now use the site to its full extent.' ), 'notice' );
            } else {
                $location = add_query_arg( ['u' => $data['id']], get_permalink( get_page_by_path( 'verify' ) ) );
                wc_add_notice( __( '<strong>Error:</strong> Account activation failed. Please try again in a few minutes or <a href="'.$location.'">resend the activation email</a>.<br />Please note that any activation links previously sent lose their validity as soon as a new activation email gets sent.<br />If the verification fails repeatedly, please contact our administrator.' ), 'error' );
            }
        }

    }

    // resend email verification
    if( isset( $_GET['u'] ) && ! empty( $_GET['u'] ) ) {
        my_user_register( $_GET['u'] );
        wc_add_notice( __( 'Your activation email has been resent. Please check your email and your spam folder.' ), 'notice' );
    }

    // If account has been freshly created
    if( isset( $_GET['n'] ) ) {
        wc_add_notice( __( 'Thank you for creating your account. You will need to confirm your email address in order to activate your account. An email containing the activation link has been sent to your email address. If the email does not arrive within a few minutes, check your spam folder.' ), 'notice' );
    }
}

add_action( 'woocommerce_email', 'disable_account_creation_email' );
function disable_account_creation_email( $email_class ) {
    remove_action( 'woocommerce_created_customer_notification', array( $email_class, 'customer_new_account' ), 10, 3 );
}

// End of Register Verification

add_action( 'woocommerce_customer_reset_password', 'email_success_reset_password', 11, 1 );
function email_success_reset_password( $user ) {
    ob_start();
    wc_get_template(
        'emails/customer-reset-password-success.php',
        array(
            'user_login' => $user->user_login
        )
    );
    $html = ob_get_contents();
    ob_get_clean();

    wc_mail( $user->user_email, __( 'Reset Password Success' ), $html );
}

add_action( 'wp_ajax_check_existing_user', 'check_existing_user' );
add_action( 'wp_ajax_nopriv_check_existing_user', 'check_existing_user' );
function check_existing_user() {
    // Do a nonce check
    if( ! isset( $_REQUEST['nonce'] ) || ! wp_verify_nonce( $_REQUEST['nonce'], 'ajax_check_existing_user_nonce' ) ) {
        wp_send_json( array( 'success' => 0, 'message' => 'nonce fail' ) );
        exit;
    }

    if( ! isset( $_POST['user_email'] ) ) {
        wp_send_json( array( 'success' => 0, 'message' => 'Email harus diisi' ) );
        exit;
    }

    $email = $_POST['user_email'];

    if ( ! filter_var( $email, FILTER_VALIDATE_EMAIL ) ) {
        wp_send_json( array( 'success' => 0, 'message' => 'Format email salah' ) );
        exit;
    }

    $user = get_user_by( 'email', $email );

    $is_exists = false;
    if( $user ) {
        $is_exists = true;
    }

    wp_send_json( array( 'success' => 1, 'message' => 'Success', 'is_exists' => $is_exists ) );
    exit;
}

// Gravatar
function get_avatar_img_url( $size = 160 ) {
    if( ! is_user_logged_in() ) {
        return;
    }

    $user       = wp_get_current_user();
    $user_email = $user->user_email;

    $url        = 'http://gravatar.com/avatar/' . md5( $user_email );
    $url        = add_query_arg( array(
        's' => $size,
    ), $url );

    return esc_url_raw( $url );
}

// force expand dashboard menu
// add_filter('admin_head', 'make_menu_unfolded');
function make_menu_unfolded() {
    print '<script>jQuery(document).ready(function(){jQuery("body").removeClass("folded")})</script>';
}

// call number in each page
function call_support() {
    $value = get_field( 'call_support', 'option' );

    return $value;
}

function urlToBase64( $path ){
    $arrContextOptions = array(
        "ssl" => array(
            "verify_peer"       => false,
            "verify_peer_name"  => false,
        ),
    );

    $type = pathinfo( $path, PATHINFO_EXTENSION );
    $data = @file_get_contents( $path,false, stream_context_create( $arrContextOptions ) );

    return  'data:image/' . $type . ';base64,' . base64_encode( $data );
}

// change woocommerce invoice pdf page size to A6
add_filter( 'wpo_wcpdf_paper_format', 'wcpdf_a6_packing_slips', 10, 2 );
function wcpdf_a6_packing_slips( $paper_format, $template_type ) {
    if( $template_type == 'packing-slip' ) {
        $paper_format = 'a6';
    }

    return $paper_format;
}

// custom wcpdf header logo
add_filter( 'wpo_wcpdf_header_logo_img_element', 'packing_slip_header_logo_custom', 10, 3 );
function packing_slip_header_logo_custom( $img_element, $attachment, Order_Document $document ) {
    if ($document->get_header_logo_id()) {
        $attachment_id = $document->get_header_logo_id();
        $company = $document->get_shop_name();
        if( $attachment_id ) {
            $attachment = wp_get_attachment_image_src( $attachment_id, 'full', false );
            $attachment_path = get_attached_file( $attachment_id );
            if ( empty( $attachment ) || empty( $attachment_path ) ) {
                return;
            }

            $attachment_src = $attachment[0];
            $attachment_width = $attachment[1];
            $attachment_height = $attachment[2];

            if ( apply_filters('wpo_wcpdf_use_path', true) && file_exists($attachment_path) ) {
                $src = $attachment_path;
            } else {
                $src = $attachment_src;
            }

            // original
            // $img_element = sprintf('<img src="%1$s" alt="%4$s" />', $src, $attachment_width, $attachment_height, esc_attr( $company ) );

            // custom : add style width 100px
            $img_element = sprintf('<img src="%1$s" alt="%4$s" style="width: 100px" />', $src, $attachment_width, $attachment_height, esc_attr( $company ) );

            echo $img_element;
        }
    }
}

add_action( 'wpo_wcpdf_shipping_partner_logo', 'wc_wcpdf_shipping_partner_logo', 10, 1 );
function wc_wcpdf_shipping_partner_logo( WC_Order $order ) {
    $service = get_post_meta( $order->get_order_number(), '_shipping_service', true );

    if( ! in_array( $service, ['jne', 'sicepat'] ) ) {
        return;
    }

    $option_logo= get_field( 'packing_slips', 'option' );
    $src        = '';
    $alt        = '';

    if( $service == 'jne' ) {
        $src = isset( $option_logo['logo_jne'] ) ? $option_logo['logo_jne'] : '';
        $alt = 'JNE Logo';
    } elseif( $service == 'sicepat' ) {
        $src = isset( $option_logo['logo_sicepat'] ) ? $option_logo['logo_sicepat'] : '';
        $alt = 'Sicepat Logo';
    }

    echo sprintf( '<img src="%s" alt="%s" style="width: 60px;">', urlToBase64( $src ), $alt );
}

add_action( 'wpo_wcpdf_generate_barcode_waybill', 'wcpdf_generate_barcode_waybill', 10, 1 );
function wcpdf_generate_barcode_waybill( WC_Order $order ) {
    $order_id       = $order->get_order_number();
    $upload_dir     = wp_get_upload_dir();

    $barcode_text   = get_post_meta( $order_id, '_awb_no', true );
    $filename_full  = get_post_meta( $order_id, 'waybill_barcode_filename_full', true );
    $filepath       = $upload_dir['basedir'].'/'.WAYBILL_BARCODE_DIR_NAME.'/'.$filename_full; // use basedir

    //echo '<img class="barcode" alt="' . $barcode_text . '" src="'.$filepath.'" style="width: 320px; margin-bottom: 4px;"/>';
    $html           = sprintf( '<img class="barcode" alt="%s" src="%s" style="width: 320px; margin-bottom: 4px;"/>', $barcode_text, $filepath );
    $html           .= sprintf( '<p style="margin-top: 0; margin-bottom: 0; font-size: 12px;">%s</p>', $barcode_text );

    echo $html;
}

add_action( 'wpo_wcpdf_generate_barcode_order', 'wcpdf_generate_barcode_order_no', 10, 1 );
function wcpdf_generate_barcode_order_no( WC_Order $order ) {
    $order_id       = $order->get_order_number();
    $upload_dir     = wp_get_upload_dir();

    $barcode_text   = $order_id;
    $filename_full  = get_post_meta( $order_id, 'order_barcode_filename_full', true );
    $filepath       = $upload_dir['basedir'].'/'.ORDER_BARCODE_DIR_NAME.'/'.$filename_full; // use basedir

    //echo '<img class="barcode" alt="' . $barcode_text . '" src="'.$filepath.'" style="width: 120px; margin-bottom: 4px;"/>';
    $html           = sprintf( '<img class="barcode" alt="%s" src="%s" style="width: 130px; margin-bottom: 4px;"/>', $barcode_text, $filepath );
    $html           .= sprintf( '<p style="margin: 0; font-size: 10px;">%s</p>', $barcode_text );

    echo $html;
}

add_action( 'wp_wcpd_display_shipping_sub_service', 'wcpd_display_shipping_sub_service', 10, 1 );
function wcpd_display_shipping_sub_service( WC_Order $order ) {
    $sub_service = get_post_meta( $order->get_order_number(), '_shipping_sub_service', true );

    echo $sub_service;
}

add_action( 'wpo_wcpdf_shipping_insurance_rate', 'wcpdf_shipping_insurance_rate', 10, 1 );
function wcpdf_shipping_insurance_rate( $order ) {
    $insurance_rate = get_shipping_insurance_rate( $order );

    echo wc_price( $insurance_rate );
}

function get_shipping_insurance_rate( WC_Order $order ) {
    $insurance_rate = 0;

    if( sizeof( $order->get_items( 'fee' ) ) > 0 ) {
        foreach( $order->get_items( 'fee' ) as $item_id => $item_fee ) {
            $fee_name = $item_fee->get_name();
            if( $fee_name == SHIPPING_INSURANCE_FEE_NAME ) {
                $insurance_rate = $item_fee->get_total();
                break;
            }
        }
    }

    return $insurance_rate;
}

add_action( 'wpo_wcpdf_shipping_total_weight', 'wcpdf_shipping_total_weight', 10, 1 );
function wcpdf_shipping_total_weight( $order ) {
    $total_weight = get_order_shipping_total_weight( $order, true );

    echo $total_weight;
}

function get_order_shipping_total_weight( WC_Order $order, $show_unit = false ) {
    $actual_weight      = 0;
    $volumetric_weight  = 0;

    if( sizeof( $order->get_items() ) > 0 ) {
        foreach ( $order->get_items() as $item_id => $item ) {
            $_product               = $item->get_product();
            $actual_weight          += calculate_actual_weight( $_product, $item->get_quantity() );
            $volumetric_weight      += calculate_volumetric_weight( $_product, $item->get_quantity() );
        }
    }

    $weight       = shipping_set_weight( $actual_weight, $volumetric_weight );
    $total_weight = wc_get_weight( $weight, 'kg' );

    if( ! $show_unit ) {
        return number_format( $total_weight, 2 );
    }

    return number_format( $total_weight, 2 ) . ' KG';
}

add_action( 'wpo_wcpdf_shipping_total_quantity', 'wcpdf_shipping_total_quantity', 10, 1 );
function wcpdf_shipping_total_quantity( $order ) {
    $total_quantity = get_order_shipping_total_quantity( $order, true );

    echo $total_quantity;
}

function get_order_shipping_total_quantity( WC_Order $order, $show_unit = false ) {
    $total_quantity = 0;

    if( sizeof( $order->get_items() ) > 0 ) {
        foreach ( $order->get_items() as $item_id => $item ) {
            $total_quantity += $item->get_quantity();
        }
    }

    if( ! $show_unit ) {
        return $total_quantity;
    }

    return $total_quantity . ' Unit';
}

// cek apakah sebuah order menggunakan packing kayu
function is_order_has_wood_packaging( $order_id ) {
    $order              = wc_get_order( $order_id );
    $has_wood_packaging = false;

    if( sizeof( $order->get_items( 'fee' ) ) > 0 ) {
        foreach( $order->get_items( 'fee' ) as $item_id => $item_fee ) {
            $fee_name = $item_fee->get_name();
            if( $fee_name == SHIPPING_PACKAGING_FEE_NAME ) {
                $has_wood_packaging = true;
                break;
            }
        }
    }

    return $has_wood_packaging;
}

add_filter( 'wpo_wcpdf_shipping_notes', 'wcpdf_shipping_notes', 10, 2 );
function wcpdf_shipping_notes( $shipping_notes, Order_Document_Methods $order_document_methods ) {
    $has_wood_packaging = is_order_has_wood_packaging( $order_document_methods->order_id );

    $additional_notes = 'Packing Kayu' . '</br>';
    if( $has_wood_packaging ) {
        return $additional_notes . $shipping_notes;
    }

    return $shipping_notes;
}

function time_elapsed_string($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
}

// set on-hold order to pending-payment
add_action( 'woocommerce_thankyou_bacs', 'update_status_order_to_pending_payment', 10, 1 );
function update_status_order_to_pending_payment( $order_id ) {
    if( ! $order_id ) {
        return;
    }

    remove_action( 'woocommerce_order_status_pending', 'wc_maybe_increase_stock_levels' );

    // Get an instance of the WC_Order object
    $order = new WC_Order( $order_id );

    if( ( get_post_meta( $order->get_id(), '_payment_method', true) == 'bacs' ) && ( 'on-hold' == $order->get_status() ) ) {
        $order->update_status('pending', "Change status BACS from on-hold to pending payment");
    } else {
        return;
    }

    add_action( 'woocommerce_order_status_pending', 'wc_maybe_increase_stock_levels' );
}

function get_payment_verify_to_processing_email_html( $order, $mailer, $heading = false ) {

    $template = 'emails/customer-processing-order.php';

    return wc_get_template_html( $template, array(
        'order'              => $order,
        'email_heading'      => $heading,
        'additional_content' => '',
        'sent_to_admin'      => false,
        'plain_text'         => false,
        'email'              => $mailer
    ) );

}

// Send order processing to customer when admin update manually order status from payment verify to processing
add_action( 'woocommerce_order_status_changed', 'order_status_payment_verify_to_processing_notification', 10, 3 );
function order_status_payment_verify_to_processing_notification( $order_id, $from, $to ) {
    if( $from == 'payment-verify' && $to == 'processing' ) {
        // get order
        $order      = wc_get_order( $order_id );

        // load the mailer class
        $mailer     = WC()->mailer();

        //format the email
        $recipient  = $order->get_billing_email();
        $subject    = __('Thank You for Your Payment!', 'starlyn' );
        $content    = get_payment_verify_to_processing_email_html( $order, $mailer, $subject );
        $headers    = "Content-Type: text/html\r\n";

        //send the email through wordpress
        $mailer->send( $recipient, $subject, $content, $headers );
    }
}

// Styling order status label
add_action( 'admin_head', 'styling_admin_order_list' );
function styling_admin_order_list() {
    global $pagenow, $post;

    if( $pagenow != 'edit.php') return; // Exit
    if ( $post ) {
        if( get_post_type($post->ID) != 'shop_order' ) return; // Exit
        // HERE we set your custom status
        //$order_status = 'Pay Confirmation'; // <==== HERE
        ?>
        <style>
            .order-status.status-<?php echo 'payment-verify' ?> {
                background: #d7f8a7;
                color: #0c942b;
            }
        </style>
        <?php
    }
}

// delete product video transient where updating product.
add_action( 'woocommerce_update_product', 'refresh_product_youtube_transient', 10, 1 );
function refresh_product_youtube_transient( $product_id ) {
    delete_transient( 'product_video_content_title' );
    delete_transient( 'product_video_viewer_count' );
    delete_transient( 'wc_products_onsale' ); // delete transient wc_get_product_ids_on_sale()
}

// remove pagination in archive products
remove_action( 'woocommerce_after_shop_loop', 'woocommerce_pagination', 10 );

add_filter('woocommerce_default_catalog_orderby', 'misha_default_catalog_orderby');
function misha_default_catalog_orderby( $sort_by ) {
    return 'popularity'; // by the number of sales
}

add_action( 'woocommerce_before_single_product', 'move_variations_single_price', 1 );
function move_variations_single_price(){
    global $product, $post;
    if ( $product->is_type( 'variable' ) ) {
        add_action( 'woocommerce_single_product_summary', 'replace_variation_single_price', 10 );
    }
}

// Replace the Variable Price range by the chosen variation price
function replace_variation_single_price() {
?>
    <style>
        .woocommerce-variation-price {
            display: none;
        }
    </style>
    <script>
        jQuery(document).ready(function($) {
            var priceselector = '.product .meta .price';
            var originalprice = $(priceselector).html();

            $( document ).on('show_variation', function() {
                let variant_price = $('.single_variation .woocommerce-variation-price span.price').html();
                $(priceselector).html( variant_price );
            });
            $( document ).on('hide_variation', function() {
                $(priceselector).html(originalprice);
            });
        });
    </script>
<?php
}

// Add space after price
add_filter('woocommerce_currency_symbol', 'starlyn_wc_add_space_currency', 10, 2);
function starlyn_wc_add_space_currency( $currency_symbol, $currency ) {
    if ('IDR' == $currency) {
        $currency_symbol .= ' ';
    }

    return $currency_symbol;
}
?>