<div class="site-header-main">
    <div class="wrapper">
        <div class="mobile">
            <a href="#" class="mobile-toggle js-mobile-menu-open">
                <svg class="icon" role="img"><use xlink:href="<?= library_url() ?>/images/svg-symbols.svg#icon-hamburger" /></svg>
            </a>
        </div>
        <div class="logo">
            <a class="link" href="<?= home_url() ?>" rel="nofollow">
                <img src="<?= library_url() ?>/images/logo-starlyn.svg" alt="Logo Starlyn" class="logo-image">
            </a>
        </div>
        <div class="nav">
            <div class="nav-menu">
                <ul class="items">
                    <li class="item <!-- @nav-product -->"><a href="<?= wc_get_page_permalink( 'shop' ) ?>" class="link">Products</a></li>
                    <li class="item <!-- @nav-deals -->"><a href="<?= get_permalink( get_page_by_path('deals') ) ?>" class="link">Deals</a></li>
                    <li class="item <!-- @nav-brands -->">
                        <?php
                            // get taxonomy brand.
                            $brands = get_terms([
                                'taxonomy'      => 'brand',
                                'hide_empty'    => false,
                                'orderby'       => 'name',
                            ]);
                        ?>
                        <a href="<?php echo get_permalink( get_page_by_path( 'brands' ) ) ?>" class="link">Brands</a>
                        <ul class="sub-items">
                            <li class="sub-item <!-- @nav-brands -->"><a href="<?php echo get_permalink( get_page_by_path( 'brands' ) ) ?>" class="link">All Brands</a></li>
                            <?php if( $brands ) : ?>
                                <?php foreach ( $brands as $brand ) : ?>
                                    <?php $display_in_menu = get_field( 'display_in_menu', $brand ); ?>
                                    <?php if( $display_in_menu == 'yes' ) : ?>
                                        <li class="sub-item"><a href="<?php echo get_term_link( $brand ) ?>" class="link"><?php echo $brand->name ?></a></li>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="nav-cart">
                <a href="<?= wc_get_cart_url() ?>" class="nav-cart-link">
                    <svg class="icon" role="img"><use xlink:href="<?= library_url() ?>/images/svg-symbols.svg#icon-cart" /></svg>
                    <span class="text">Cart</span>

                    <?php if( WC()->cart->get_cart_contents_count() ) : ?>
                        <span class="counter"><?php echo WC()->cart->get_cart_contents_count() ?></span>
                    <?php endif; ?>
                </a>
            </div>
        </div>
    </div>
</div>