<!doctype html>
<html class="no-js" lang="en">
    <head>
        <?php if( is_production() ) : ?>
            <!-- Global site tag (gtag.js) - Google Analytics -->
            <script async src="https://www.googletagmanager.com/gtag/js?id=UA-185935291-1"></script>
            <script>
                window.dataLayer = window.dataLayer || [];
                function gtag(){dataLayer.push(arguments);}
                gtag('js', new Date());

                gtag('config', 'UA-185935291-1');
            </script>
        <?php endif; ?>

        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="format-detection" content="telephone=no">
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

        <!-- handled by yoast seo -->
        <!--<title><?php wp_title('|', true, 'right') ?></title>-->

        <meta name="google-site-verification" content="t3bBP_MJepbYKhLF7J2qeZ3ovsTTt-LxTjDgZjce8b8" />

        <link rel="author" href="humans.txt" />

        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Mulish:ital,wght@0,400;0,600;0,700;1,400&display=swap" rel="stylesheet">

        <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
        <link rel="manifest" href="/manifest.json">
        <meta name="theme-color" content="#ffffff">

        <!-- Hide recaptcha v3 floating on right bottom page -->
        <style type="text/css">
            .grecaptcha-badge { visibility: hidden; }
        </style>

        <?php wp_head(); ?>
    </head>

    <body class="site-loading">
    <div id="site-container" class="site-container">
        <?php siteHeaderTopHtml(); ?>
        <header id="site-header" class="site-header" role="banner">
            <?php siteHeaderMainHtml(); ?>
        </header>

        <!-- mobile menu -->
        <div id="mobile-menu" class="mobile-menu">
            <div class="mobile-menu-background"></div>
            <div class="mobile-menu-container">
                <div class="inner">
                    <div class="header">
                        <span class="text">Menu</span>
                        <span class="js-mobile-menu-close" title="Close">
								<svg class="icon" role="img"><use xlink:href="<?= library_url() ?>/images/svg-symbols.svg#icon-close" /></svg>
							</span>
                    </div>
                    <div class="nav">
                        <ul class="items">
                            <li class="item <!-- @nav-product -->"><a href="<?= wc_get_page_permalink( 'shop' ) ?>" class="link">Products</a></li>
                            <li class="item <!-- @nav-deals -->"><a href="<?= get_permalink( get_page_by_path('deals') ) ?>" class="link">Deals</a></li>
                            <li class="item <!-- @nav-brands -->">
                                <?php
                                    // get taxonomy brand.
                                    $brands = get_terms([
                                        'taxonomy'      => 'brand',
                                        'hide_empty'    => false,
                                        'orderby'       => 'name',
                                    ]);
                                ?>
                                <a href="<?php echo get_permalink( get_page_by_path( 'brands' ) ) ?>" class="link">
                                    <div class="toggle-container">
                                        <span class="text">Brands</span>
                                        <span class="js-item-toggle">
                                            <svg class="icon icon-plus" role="img"><use xlink:href="<?= library_url() ?>/images/svg-symbols.svg#icon-plus" /></svg>
                                            <svg class="icon icon-minus" role="img"><use xlink:href="<?= library_url() ?>/images/svg-symbols.svg#icon-minus" /></svg>
                                        </span>
                                    </div>
                                </a>
                                <ul class="sub-items">
                                    <li class="sub-item <!-- @nav-brands -->"><a href="<?php echo get_permalink( get_page_by_path( 'brands' ) ) ?>" class="link">All Brands</a></li>
                                    <?php if( $brands ) : ?>
                                        <?php foreach ( $brands as $brand ) : ?>
                                        <?php $display_in_menu = get_field( 'display_in_menu', $brand ); ?>
                                            <?php if( $display_in_menu == 'yes' ) : ?>
                                                <li class="sub-item"><a href="<?php echo get_term_link( $brand ) ?>" class="link"><?php echo $brand->name ?></a></li>
                                            <?php endif; ?>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
