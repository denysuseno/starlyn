<?php get_header(); ?>

<!--======= banner start=======-->
<div class="tp-banner-container">
    <div class="tp-banner">
        <ul>
            <li data-slotamount="7" data-transition="3dcurtain-horizontal" data-masterspeed="1000" data-saveperformance="on"> <img alt="" src="<?= library_url() ?>/images/background/slider-1.jpg" data-lazyload="<?= library_url() ?>/images/background/slider-2.jpg">
                <div class="caption lft large-title tp-resizeme slidertext1" data-x="center" data-y="120" data-speed="600" data-start="1600">Lorem Ispum</div>
                <div class="caption lft large-title tp-resizeme slidertext2" data-x="center" data-y="210" data-speed="600" data-start="2200">Consectetur Adipiscing</div>
                <div class="caption lfb large-title tp-resizeme slidertext3" data-x="center" data-y="270" data-speed="600" data-start="2800">Lorem ipsum dolor sit amet, consectetur adipiscing elit ac eros nec<br/>
                    dui convallis posuere. Duis imperdiet convallis maecenas<br/>
                    convallis sem id tristique lobortis.</div>
                <div class="caption lfb large-title tp-resizeme slidertext4" data-x="center" data-y="340" data-speed="600" data-start="3500"><a href="#">Contact Us</a></div>
            </li>
            <li data-slotamount="7" data-transition="slotzoom-horizontal" data-masterspeed="1000" data-saveperformance="on"> <img alt="" src="<?= library_url() ?>/images/background/slider-1.jpg" data-lazyload="<?= library_url() ?>/images/background/slider-1.jpg">
                <div class="caption lft large-title tp-resizeme slidertext1" data-x="center" data-y="120" data-speed="600" data-start="1600">Lorem Ispum</div>
                <div class="caption lft large-title tp-resizeme slidertext2" data-x="center" data-y="210" data-speed="600" data-start="2200">Consectetur Adipiscing</div>
                <div class="caption lfb large-title tp-resizeme slidertext3" data-x="center" data-y="270" data-speed="600" data-start="2800">Lorem ipsum dolor sit amet, consectetur adipiscing elit ac eros nec<br/>
                    dui convallis posuere. Duis imperdiet convallis maecenas<br/>
                    convallis sem id tristique lobortis.</div>
                <div class="caption lfb large-title tp-resizeme slidertext4" data-x="center" data-y="340" data-speed="600" data-start="3500"><a href="#">Contact Us</a></div>
            </li>
        </ul>
    </div>
</div>
<!--======= banner end =======-->
<!--======= welcome-section start=======-->
<section class="welcome-section section section-lg">
    <!--container start-->
    <div class="container">
        <!--section-title start-->
        <div class="row text-center">
            <div class="col-md-12">
                <div class="main-heading">
                    <h2 class="section-title-dash">About <span class="section-title-dash-color"> Us</span><span class="section-title-dash-bottom">&nbsp;</span> </h2>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since.</p>
                </div>
            </div>
            <!-- Col end -->
        </div>
        <!--section-title end-->
        <!--row start-->
        <div class="row">
            <!--col start-->
            <div class="col-lg-6">
                <p class="welcom_para">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                <div class="about-list">
                    <h3> <i class="fa fa-quote-right"></i> Flexible Automated service </h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>
                </div>
                <div class="about-list">
                    <h3> <i class="fa fa-quote-right icon-color"></i> Awesome Clients Management </h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>
                </div>
                <div class="about-list">
                    <h3> <i class="fa fa-quote-right"></i> Flexible Automated service </h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore.</p>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="about-box-layout2">
                    <div class="item-img">
                        <div class="main-img"> <img src="<?= library_url() ?>/images/about1.jpg" alt="about"> </div>
                        <div class="sub-img"> <img src="<?= library_url() ?>/images/main-gallery.jpg" alt="about">
                            <div class="overlay-gallery">
                                <div class="icon-holder">
                                    <div class="icon"> <a class="video-play mfp-iframe xs-video wow fadeInUp animated" data-lightgallery="item" title="Interrio Gallery" href="https://www.youtube.com/watch?v=Vn_FGpZJqUs" data-wow-delay="600ms"><img src="<?= library_url() ?>/images/play-btn.png" alt="Play Button"></a> </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--row end-->
    </div>
    <!--container end-->
</section>
<!--======= welcome-section end =======-->
<!--======= Services-section start=======-->
<section class="section section-lg bg-gray-100">
    <div class="container">
        <!--section-title start-->
        <div class="row text-center">
            <div class="col-md-12">
                <div class="main-heading wow fadeInLeft">
                    <h2 class="section-title-dash">Our <span class="section-title-dash-color"> Services</span><span class="section-title-dash-bottom">&nbsp;</span> </h2>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since.</p>
                </div>

            </div>
            <!-- Col end -->
        </div>
        <!--section-title end-->
        <div class="row row-xs justify-content-center">
            <div class="owl-carousel owl-style-3" data-items="1" data-sm-items="1" data-lg-items="3" data-margin="30" data-autoplay="true" data-dots="true" data-animation-in="fadeIn" data-animation-out="fadeOut" data-pagination-class=".dots-custom">
                <!-- Services Creative-->
                <div class="service_single_content text-center wow fadeInUp" data-wow-delay="0.2s">
                    <div class="service_img"> <img src="<?= library_url() ?>/images/offer/offer-1.jpg" alt=""> </div>
                    <div class="signle-services-icon-wrapper"> <i class="fi"><i class="linearicons-gradient2"></i></i></div>
                    <div class="service_content">
                        <h6 class="d-blue bold">Quick & Efficiency</h6>
                        <p>Lorem ipsum dolor sit amet, consectetur posuere adipiscing elit. Nulla neque quam, maxi ut accumsan ut, posuere sit Lorem ipsum</p>
                        <a class="ttm-btn ttm-btn-size-sm" href="single-service.html">MORE ABOUT<i class="ti ti-angle-double-right"></i></a> </div>
                </div>
                <!-- Services Creative-->
                <div class="service_single_content text-center wow fadeInUp" data-wow-delay="0.2s">
                    <div class="service_img"> <img src="<?= library_url() ?>/images/offer/offer-2.jpg" alt=""> </div>
                    <div class="signle-services-icon-wrapper"> <i class="fi"><i class="linearicons-color-sampler"></i></i></div>
                    <div class="service_content">
                        <h6 class="d-blue bold">Easy To Edit</h6>
                        <p>Lorem ipsum dolor sit amet, consectetur posuere sit posuere neque adipiscing elit. Nulla neque quam, maxi ut accumsan ut, posuere sit Lorem ipsum</p>
                        <a class="ttm-btn ttm-btn-size-sm" href="single-service.html">MORE ABOUT<i class="ti ti-angle-double-right"></i></a> </div>
                </div>
                <!-- Services Creative-->
                <div class="service_single_content text-center wow fadeInUp" data-wow-delay="0.2s">
                    <div class="service_img"> <img src="<?= library_url() ?>/images/offer/offer-3.jpg" alt=""> </div>
                    <div class="signle-services-icon-wrapper"> <i class="fi"><i class="linearicons-texture"></i></i></div>
                    <div class="service_content">
                        <h6 class="d-blue bold">Clean Code</h6>
                        <p>Lorem ipsum dolor sit amet, consectetur posuere adipiscing elit. Nulla neque quam, maxi ut accumsan ut, posuere sit Lorem ipsum</p>
                        <a class="ttm-btn ttm-btn-size-sm" href="single-service.html">MORE ABOUT<i class="ti ti-angle-double-right"></i></a> </div>
                </div>
                <!-- Services Creative-->
                <div class="service_single_content text-center wow fadeInDown" data-wow-delay="0.2s">
                    <div class="service_img"> <img src="<?= library_url() ?>/images/offer/offer-4.jpg" alt=""> </div>
                    <div class="signle-services-icon-wrapper"> <i class="fi"><i class="linearicons-shield-check"></i></i></div>
                    <div class="service_content">
                        <h6 class="d-blue bold">Global Support</h6>
                        <p>Lorem ipsum dolor sit amet, consectetur posuere sit posuere neque adipiscing elit. Nulla neque quam, maxi ut accumsan ut, posuere sit Lorem ipsum</p>
                        <a class="ttm-btn ttm-btn-size-sm" href="single-service.html">MORE ABOUT<i class="ti ti-angle-double-right"></i></a> </div>
                </div>
                <!-- Services Creative-->
                <div class="service_single_content text-center wow fadeInDown" data-wow-delay="0.2s">
                    <div class="service_img"> <img src="<?= library_url() ?>/images/offer/offer-5.jpg" alt=""> </div>
                    <div class="signle-services-icon-wrapper"> <i class="fi"><i class="linearicons-broom"></i></i></div>
                    <div class="service_content">
                        <h6 class="d-blue bold">Great Contribution</h6>
                        <p>Lorem ipsum dolor sit amet, consectetur posuere adipiscing elit. Nulla neque quam, maxi ut accumsan ut, posuere sit Lorem ipsum</p>
                        <a class="ttm-btn ttm-btn-size-sm" href="single-service.html">MORE ABOUT<i class="ti ti-angle-double-right"></i></a> </div>
                </div>
                <!-- Services Creative-->
                <div class="service_single_content text-center wow fadeInDown" data-wow-delay="0.2s">
                    <div class="service_img"> <img src="<?= library_url() ?>/images/offer/offer-6.jpg" alt=""> </div>
                    <div class="signle-services-icon-wrapper"> <i class="fi"><i class="linearicons-bubbles"></i></i></div>
                    <div class="service_content">
                        <h6 class="d-blue bold">Hi-Profile Employee</h6>
                        <p>Lorem ipsum dolor sit amet, consectetur posuere sit posuere neque adipiscing elit. Nulla neque quam, maxi ut accumsan ut, posuere sit Lorem ipsum</p>
                        <a class="ttm-btn ttm-btn-size-sm" href="single-service.html">MORE ABOUT<i class="ti ti-angle-double-right"></i></a> </div>
                </div>
            </div>

        </div>
    </div>
</section>
<!--======= Services end =======-->
<!--======= experience-section start=======-->
<section class="section experienceWrap">
    <div class="container-fluid container-inset-0">
        <div class="row no-gutters">
            <div class="col-md-4 col-lg-5 col-xl-6 box-transform-wrap box-transform-1">
                <div class="box-transform" style="background-image: url(<?= library_url() ?>/images/background/looking-bg.jpg);"></div>
            </div>
            <div class="col-md-8 col-lg-7 col-xl-6 bg-image-3">
                <div class="tabs-custom tabs-custom-3" id="tabs-12">
                    <div class="tab-content tab-content-4 section-lg">
                        <div class="tab-pane fade show active" id="tabs-12-1">
                            <div class="oh-desktop">
                                <div class="main-heading wow fadeInLeft">
                                    <h2 class="section-title-dash"><span class="section-title-dash-color"> 24 years of</span>experience <span class="section-title-dash-bottom">&nbsp;</span> </h2>
                                </div>
                            </div>
                            <p class="wow fadeInRight">Lorem Ipsum is simply dummy text of the printing and typesetting the industry's standard dummy text ever since the 1500s.</p>
                            <div class="button-wrap oh-desktop"><a class="button button-icon button-icon-right button-primary wow slideInUp" href="about-us.html"><span class="icon fa fa-chevron-right"></span>Read more</a></div>
                        </div>
                        <div class="tab-pane fade" id="tabs-12-2">
                            <div class="oh-desktop">
                                <div class="main-heading wow fadeInLeft">
                                    <h2 class="section-title-dash"><span class="section-title-dash-color"> 24 years of</span>Missions <span class="section-title-dash-bottom">&nbsp;</span> </h2>
                                </div>
                            </div>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting the industry's standard dummy text ever since the 1500s.</p>
                            <a class="button button-icon button-icon-right button-primary" href="#"><span class="icon fa fa-chevron-right"></span>Read more</a> </div>
                        <div class="tab-pane fade" id="tabs-12-3">
                            <div class="oh-desktop">
                                <div class="main-heading wow fadeInLeft">
                                    <h2 class="section-title-dash"><span class="section-title-dash-color"> 24 years of</span>Goals <span class="section-title-dash-bottom">&nbsp;</span> </h2>
                                </div>
                            </div>
                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting the industry's standard dummy text ever since the 1500s.</p>
                            <a class="button button-icon button-icon-right button-primary" href="#"><span class="icon fa fa-chevron-right"></span>Read more</a> </div>
                    </div>
                    <ul class="nav nav-tabs-3">
                        <li class="nav-item-3" role="presentation"><a class="nav-link-3 active" href="#tabs-12-1" data-toggle="tab"><span>About us</span></a></li>
                        <li class="nav-item-3" role="presentation"><a class="nav-link-3" href="#tabs-12-2" data-toggle="tab"><span>Mission & vision</span></a></li>
                        <li class="nav-item-3" role="presentation"><a class="nav-link-3" href="#tabs-12-3" data-toggle="tab"><span>Our Goals</span></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!--======= experience-section end =======-->
<!--======= what-we-offer start=======-->
<section class="section section-lg bg-default text-center what-we-offer">
    <div class="container">
        <div class="group-xl group-sm-justify">
            <div class="main-heading wow fadeInLeft">
                <h2 class="section-title-dash">Our <span class="section-title-dash-color"> Facts</span><span class="section-title-dash-bottom">&nbsp;</span> </h2>
            </div>
            <div class="dots-custom"></div>
        </div>
        <div class="owl-carousel owl-style-3" data-items="1" data-sm-items="1" data-lg-items="3" data-margin="30" data-autoplay="true" data-dots="true" data-animation-in="fadeIn" data-animation-out="fadeOut" data-pagination-class=".dots-custom">
            <!-- Services Creative-->


            <div class="image-effect-one hover-shadow">
                <img src="<?= library_url() ?>/images/offer/offer-1.jpg" alt="">
                <div class="figcaption bg-dark">
                    <h4>General Builder</h4>
                    <p>Est notare quam littera gothica, quam nunc putamus parum claram.</p>
                    <a href="#"><i class="link-plus bg-primary"></i></a>
                </div>

            </div>

            <div class="image-effect-one hover-shadow">
                <img src="<?= library_url() ?>/images/offer/offer-2.jpg" alt="">
                <div class="figcaption bg-dark">
                    <h4>General Builder</h4>
                    <p>Est notare quam littera gothica, quam nunc putamus parum claram.</p>
                    <a href="#"><i class="link-plus bg-primary"></i></a>
                </div>

            </div>

            <div class="image-effect-one hover-shadow">
                <img src="<?= library_url() ?>/images/offer/offer-3.jpg" alt="">
                <div class="figcaption bg-dark">
                    <h4>General Builder</h4>
                    <p>Est notare quam littera gothica, quam nunc putamus parum claram.</p>
                    <a href="#"><i class="link-plus bg-primary"></i></a>
                </div>

            </div>


            <div class="image-effect-one hover-shadow">
                <img src="<?= library_url() ?>/images/offer/offer-4.jpg" alt="">
                <div class="figcaption bg-dark">
                    <h4>General Builder</h4>
                    <p>Est notare quam littera gothica, quam nunc putamus parum claram.</p>
                    <a href="#"><i class="link-plus bg-primary"></i></a>
                </div>

            </div>


            <div class="image-effect-one hover-shadow">
                <img src="<?= library_url() ?>/images/offer/offer-5.jpg" alt="">
                <div class="figcaption bg-dark">
                    <h4>General Builder</h4>
                    <p>Est notare quam littera gothica, quam nunc putamus parum claram.</p>
                    <a href="#"><i class="link-plus bg-primary"></i></a>
                </div>

            </div>


            <div class="image-effect-one hover-shadow">
                <img src="<?= library_url() ?>/images/offer/offer-6.jpg" alt="">
                <div class="figcaption bg-dark">
                    <h4>General Builder</h4>
                    <p>Est notare quam littera gothica, quam nunc putamus parum claram.</p>
                    <a href="#"><i class="link-plus bg-primary"></i></a>
                </div>

            </div>
        </div>
    </div>
</section>
<!--======= what-we-offer end =======-->
<!--======= CTA start=======-->
<section class="section parallax-container" data-parallax-img="<?= library_url() ?>/images/background/hd-2.jpg">
    <div class="parallax-content section-lg context-dark text-md-left">
        <div class="container">
            <div class="row row-30 justify-content-center align-items-center">
                <div class="col-sm-10 col-md-7 col-lg-6 col-xl-9">
                    <h5 class="font-weight-normal text-transform-none oh-desktop"><span class="d-inline-block wow slideInUp"><span class="text-primary">-20%</span> Lorem Ipsum is simply dumming and typesetting industry.</span></h5>
                </div>
                <div class="col-md-5 col-lg-4 col-xl-3 text-md-right oh-desktop"><a class="button button-outline button-icon button-icon-right wow slideInDown" href="#"><span class="icon fa fa-chevron-right"></span>learn More</a></div>
            </div>
        </div>
    </div>
</section>
<!--======= CTA end =======-->
<!--======= Gallery start=======-->
<section class="section section-lg bg-default isotope-wrap">
    <div class="container">
        <!--section-title start-->
        <div class="row text-center">
            <div class="col-md-12">
                <div class="main-heading wow fadeInLeft">
                    <h2 class="section-title-dash">Our <span class="section-title-dash-color"> Project</span><span class="section-title-dash-bottom">&nbsp;</span> </h2>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since.</p>
                </div>
            </div>
            <!-- Col end -->
        </div>
        <!--section-title end-->
        <div class="isotope-filters isotope-filters-horizontal">
            <button class="isotope-filters-toggle button button-sm button-icon button-icon-right button-blue-8" data-custom-toggle=".gallery-filters-list" data-custom-toggle-disable-on-blur="true" data-custom-toggle-hide-on-blur="true"><span class="icon mdi mdi-chevron-down"></span>Filter</button>
            <ul class="isotope-filters-list gallery-filters-list">
                <li><a class="active" href="#" data-isotope-filter="*">All Projects</a></li>
                <li><a href="#" data-isotope-filter="Type 1">Branding</a></li>
                <li><a href="#" data-isotope-filter="Type 2">Marketing</a></li>
                <li><a href="#" data-isotope-filter="Type 3">Promotion</a></li>
                <li><a href="#" data-isotope-filter="Type 4">Marketing</a></li>
            </ul>
        </div>
    </div>
    <div class="container-fluid container-inset-0">
        <div class="row row-10 row-desktop-8 gutters-8 isotope hoverdir" data-lightgallery="group">
            <div class="col-sm-6 col-lg-3 isotope-item" data-filter="Type 1">
                <!-- Thumbnail Modern-->
                <article class="thumbnail thumbnail-modern block-1 hoverdir-item" data-hoverdir-target=".thumbnail-modern-caption">
                    <div class="thumbnail-modern-figure"><img src="<?= library_url() ?>/images/gallery/01.jpg" alt="" /></div>
                    <div class="thumbnail-modern-caption">
                        <h6 class="thumbnail-modern-title"><a href="project-page.html">Lorem Ipsum is simply</a></h6>
                        <div class="link-wrapp"> <a class="thumbnail-modern-badge" href="<?= library_url() ?>/images/gallery/01.jpg" title="Business Financing-1" data-lightgallery="item"><i class="fa fa-search"></i></a> <a class="thumbnail-modern-badge" href="project-page.html"><i class="fa fa-link"></i></a> </div>
                    </div>
                </article>
            </div>
            <div class="col-sm-6 col-lg-3 isotope-item" data-filter="Type 2">
                <!-- Thumbnail Modern-->
                <article class="thumbnail thumbnail-modern block-1 hoverdir-item" data-hoverdir-target=".thumbnail-modern-caption">
                    <div class="thumbnail-modern-figure"><img src="<?= library_url() ?>/images/gallery/02.jpg" alt="" /></div>
                    <div class="thumbnail-modern-caption">
                        <h6 class="thumbnail-modern-title"><a href="project-page.html">Lorem Ipsum is simply</a></h6>
                        <div class="link-wrapp"> <a class="thumbnail-modern-badge" href="<?= library_url() ?>/images/gallery/02.jpg" title="Business Financing-2" data-lightgallery="item"><i class="fa fa-search"></i></a> <a class="thumbnail-modern-badge" href="project-page.html"><i class="fa fa-link"></i></a> </div>
                    </div>
                </article>
            </div>
            <div class="col-sm-6 col-lg-3 isotope-item" data-filter="Type 3">
                <!-- Thumbnail Modern-->
                <article class="thumbnail thumbnail-modern block-1 hoverdir-item" data-hoverdir-target=".thumbnail-modern-caption">
                    <div class="thumbnail-modern-figure"><img src="<?= library_url() ?>/images/gallery/03.jpg" alt="" /></div>
                    <div class="thumbnail-modern-caption">
                        <h6 class="thumbnail-modern-title"><a href="project-page.html">Lorem Ipsum is simply</a></h6>
                        <div class="link-wrapp"> <a class="thumbnail-modern-badge" href="<?= library_url() ?>/images/gallery/03.jpg" title="Business Financing-3" data-lightgallery="item"><i class="fa fa-search"></i></a> <a class="thumbnail-modern-badge" href="project-page.html"><i class="fa fa-link"></i></a> </div>
                    </div>
                </article>
            </div>
            <div class="col-sm-6 col-lg-3 isotope-item" data-filter="Type 4">
                <!-- Thumbnail Modern-->
                <article class="thumbnail thumbnail-modern block-1 hoverdir-item" data-hoverdir-target=".thumbnail-modern-caption">
                    <div class="thumbnail-modern-figure"><img src="<?= library_url() ?>/images/gallery/04.jpg" alt="" /></div>
                    <div class="thumbnail-modern-caption">
                        <h6 class="thumbnail-modern-title"><a href="project-page.html">Lorem Ipsum is simply</a></h6>
                        <div class="link-wrapp"> <a class="thumbnail-modern-badge" href="<?= library_url() ?>/images/gallery/04.jpg" title="Business Financing-4" data-lightgallery="item"><i class="fa fa-search"></i></a> <a class="thumbnail-modern-badge" href="project-page.html"><i class="fa fa-link"></i></a> </div>
                    </div>
                </article>
            </div>
            <div class="col-sm-6 col-lg-3 isotope-item" data-filter="Type 1">
                <!-- Thumbnail Modern-->
                <article class="thumbnail thumbnail-modern block-1 hoverdir-item" data-hoverdir-target=".thumbnail-modern-caption">
                    <div class="thumbnail-modern-figure"><img src="<?= library_url() ?>/images/gallery/05.jpg" alt="" /></div>
                    <div class="thumbnail-modern-caption">
                        <h6 class="thumbnail-modern-title"><a href="project-page.html">Lorem Ipsum is simply</a></h6>
                        <div class="link-wrapp"> <a class="thumbnail-modern-badge" href="<?= library_url() ?>/images/gallery/05.jpg" title="Business Financing-5" data-lightgallery="item"><i class="fa fa-search"></i></a> <a class="thumbnail-modern-badge" href="project-page.html"><i class="fa fa-link"></i></a> </div>
                    </div>
                </article>
            </div>
            <div class="col-sm-6 col-lg-3 isotope-item" data-filter="Type 2">
                <!-- Thumbnail Modern-->
                <article class="thumbnail thumbnail-modern block-1 hoverdir-item" data-hoverdir-target=".thumbnail-modern-caption">
                    <div class="thumbnail-modern-figure"><img src="<?= library_url() ?>/images/gallery/06.jpg" alt="" /></div>
                    <div class="thumbnail-modern-caption">
                        <h6 class="thumbnail-modern-title"><a href="project-page.html">Lorem Ipsum is simply</a></h6>
                        <div class="link-wrapp"> <a class="thumbnail-modern-badge" href="<?= library_url() ?>/images/gallery/06.jpg" title="Business Financing-6" data-lightgallery="item"><i class="fa fa-search"></i></a> <a class="thumbnail-modern-badge" href="project-page.html"><i class="fa fa-link"></i></a> </div>
                    </div>
                </article>
            </div>
            <div class="col-sm-6 col-lg-3 isotope-item" data-filter="Type 3">
                <!-- Thumbnail Modern-->
                <article class="thumbnail thumbnail-modern block-1 hoverdir-item" data-hoverdir-target=".thumbnail-modern-caption">
                    <div class="thumbnail-modern-figure"><img src="<?= library_url() ?>/images/gallery/07.jpg" alt="" /></div>
                    <div class="thumbnail-modern-caption">
                        <h6 class="thumbnail-modern-title"><a href="project-page.html">Lorem Ipsum is simply</a></h6>
                        <div class="link-wrapp"> <a class="thumbnail-modern-badge" href="<?= library_url() ?>/images/gallery/07.jpg" title="Business Financing-7" data-lightgallery="item"><i class="fa fa-search"></i></a> <a class="thumbnail-modern-badge" href="project-page.html"><i class="fa fa-link"></i></a> </div>
                    </div>
                </article>
            </div>
            <div class="col-sm-6 col-lg-3 isotope-item" data-filter="Type 4">
                <!-- Thumbnail Modern-->
                <article class="thumbnail thumbnail-modern block-1 hoverdir-item" data-hoverdir-target=".thumbnail-modern-caption">
                    <div class="thumbnail-modern-figure"><img src="<?= library_url() ?>/images/gallery/08.jpg" alt="" /></div>
                    <div class="thumbnail-modern-caption">
                        <h6 class="thumbnail-modern-title"><a href="project-page.html">Lorem Ipsum is simply</a></h6>
                        <div class="link-wrapp"> <a class="thumbnail-modern-badge" href="<?= library_url() ?>/images/gallery/08.jpg" title="Business Financing-8" data-lightgallery="item"><i class="fa fa-search"></i></a> <a class="thumbnail-modern-badge" href="project-page.html"><i class="fa fa-link"></i></a> </div>
                    </div>
                </article>
            </div>
        </div>
    </div>
</section>
<!--======= Gallery end =======-->
<!--======= testimonialWrap start=======-->
<section class="section swiper-container swiper-slider swiper-slider-8 testimonialWrap" data-loop="true" data-autoplay="5000" data-simulate-touch="false" data-slide-effect="fade">
    <div class="swiper-wrapper text-left">
        <div class="swiper-slide context-dark">
            <div class="swiper-slide-caption section-lg">
                <div class="container">
                    <div class="row justify-content-center justify-content-md-between">
                        <div class="col-5 d-none d-md-block position-static">
                            <div class="quote-classic-figure"><img src="<?= library_url() ?>/images/about/about-1.jpg" alt="" /> </div>
                        </div>
                        <div class="col-sm-11 col-md-7 col-xl-6">
                            <div class="inset-left-xl-70">
                                <div class="main-heading wow fadeInLeft">
                                    <h2 class="section-title-dash">What <span class="section-title-dash-color">clients</span> say <span class="section-title-dash-bottom">&nbsp;</span> </h2>
                                </div>

                                <!-- Quote Classic-->
                                <article class="quote-classic quote-classic-2 quote-classic-4" data-caption-animate="fadeInLeft" data-caption-delay="0">
                                    <div class="quote-classic-text">
                                        <h6 class="q">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has industry's dummy.</h6>
                                    </div>
                                    <p class="quote-classic-author">John Doe, Regular client</p>
                                </article>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="swiper-slide context-dark">
            <div class="swiper-slide-caption section-lg">
                <div class="container">
                    <div class="row justify-content-center justify-content-md-between">
                        <div class="col-5 d-none d-md-block position-static">
                            <div class="quote-classic-figure"><img src="<?= library_url() ?>/images/about/about-2.jpg" alt="" /> </div>
                        </div>
                        <div class="col-sm-11 col-md-7 col-xl-6">
                            <div class="inset-left-xl-70">
                                <div class="main-heading wow fadeInLeft">
                                    <h2 class="section-title-dash">What <span class="section-title-dash-color">clients</span> say <span class="section-title-dash-bottom">&nbsp;</span> </h2>
                                </div>

                                <!-- Quote Classic-->
                                <article class="quote-classic quote-classic-2 quote-classic-4" data-caption-animate="fadeInLeft" data-caption-delay="0">
                                    <div class="quote-classic-text">
                                        <h6 class="q">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy.</h6>
                                    </div>
                                    <p class="quote-classic-author">John Doe, Regular client</p>
                                </article>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="swiper-slide context-dark">
            <div class="swiper-slide-caption section-lg">
                <div class="container">
                    <div class="row justify-content-center justify-content-md-between">
                        <div class="col-5 d-none d-md-block position-static">
                            <div class="quote-classic-figure"><img src="<?= library_url() ?>/images/about/about-1.jpg" alt="" /> </div>
                        </div>
                        <div class="col-sm-11 col-md-7 col-xl-6">
                            <div class="inset-left-xl-70">
                                <div class="main-heading wow fadeInLeft">
                                    <h2 class="section-title-dash">What <span class="section-title-dash-color">clients</span> say <span class="section-title-dash-bottom">&nbsp;</span> </h2>
                                </div>

                                <!-- Quote Classic-->
                                <article class="quote-classic quote-classic-2 quote-classic-4" data-caption-animate="fadeInLeft" data-caption-delay="0">
                                    <div class="quote-classic-text">
                                        <h6 class="q">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has industry's dummy.</h6>
                                    </div>
                                    <p class="quote-classic-author">John Doe, Regular client</p>
                                </article>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="swiper-slide context-dark">
            <div class="swiper-slide-caption section-lg">
                <div class="container">
                    <div class="row justify-content-center justify-content-md-between">
                        <div class="col-5 d-none d-md-block position-static">
                            <div class="quote-classic-figure"><img src="<?= library_url() ?>/images/about/about-2.jpg" alt="" /> </div>
                        </div>
                        <div class="col-sm-11 col-md-7 col-xl-6">
                            <div class="inset-left-xl-70">
                                <div class="main-heading wow fadeInLeft">
                                    <h2 class="section-title-dash">What <span class="section-title-dash-color">clients</span> say <span class="section-title-dash-bottom">&nbsp;</span> </h2>
                                </div>

                                <!-- Quote Classic-->
                                <article class="quote-classic quote-classic-2 quote-classic-4" data-caption-animate="fadeInLeft" data-caption-delay="0">
                                    <div class="quote-classic-text">
                                        <h6 class="q">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy.</h6>
                                    </div>
                                    <p class="quote-classic-author">John Doe, Regular client</p>
                                </article>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--======= testimonialWrap end =======-->
<!--======= faqwrap start=======-->
<section class="section section-lg bg-default text-md-left faqwrap">
    <div class="container">
        <div class="row row-xxl row-50 justify-content-lg-between">
            <div class="col-md-6">
                <div class="main-heading wow fadeInLeft">
                    <h2 class="section-title-dash">F <span class="section-title-dash-color">A</span> Q<span class="section-title-dash-bottom">&nbsp;</span> </h2>
                </div>
                <div class="card-group-classic" id="accordion6" role="tablist" aria-multiselectable="false">
                    <article class="card card-custom card-classic card-classic-2 wow fadeInLeft">
                        <div class="card-header" role="tab">
                            <h6 class="card-title"><a id="accordion6-card-head-ckebbdjx" data-toggle="collapse" data-parent="#accordion6" href="#accordion6-card-body-nqsdivvf" aria-controls="accordion6-card-body-nqsdivvf" aria-expanded="true" role="button">Lorem Ipsum is simply printing. <span class="card-arrow"></span> </a></h6>
                        </div>
                        <div class="collapse show" id="accordion6-card-body-nqsdivvf" aria-labelledby="accordion6-card-head-ckebbdjx" data-parent="#accordion6" role="tabpanel">
                            <div class="card-body">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer essentially unchanged. </p>
                            </div>
                        </div>
                    </article>
                    <article class="card card-custom card-classic card-classic-2 wow fadeInLeft" data-wow-delay=".05s">
                        <div class="card-header" role="tab">
                            <h6 class="card-title"><a class="collapsed" id="accordion6-card-head-sxpeyhxv" data-toggle="collapse" data-parent="#accordion6" href="#accordion6-card-body-pclffkth" aria-controls="accordion6-card-body-pclffkth" aria-expanded="false" role="button">Lorem Ipsum is simply printing. <span class="card-arrow"></span> </a></h6>
                        </div>
                        <div class="collapse" id="accordion6-card-body-pclffkth" aria-labelledby="accordion6-card-head-sxpeyhxv" data-parent="#accordion6" role="tabpanel">
                            <div class="card-body">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer essentially unchanged. </p>
                            </div>
                        </div>
                    </article>
                    <article class="card card-custom card-classic card-classic-2 wow fadeInLeft" data-wow-delay=".1s">
                        <div class="card-header" role="tab">
                            <h6 class="card-title"><a class="collapsed" id="accordion6-card-head-iqdknafw" data-toggle="collapse" data-parent="#accordion6" href="#accordion6-card-body-nwgfdwng" aria-controls="accordion6-card-body-nwgfdwng" aria-expanded="false" role="button">Lorem Ipsum is simply printing. <span class="card-arrow"></span> </a></h6>
                        </div>
                        <div class="collapse" id="accordion6-card-body-nwgfdwng" aria-labelledby="accordion6-card-head-iqdknafw" data-parent="#accordion6" role="tabpanel">
                            <div class="card-body">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer essentially unchanged. </p>
                            </div>
                        </div>
                    </article>
                    <article class="card card-custom card-classic card-classic-2 wow fadeInLeft" data-wow-delay=".15s">
                        <div class="card-header" role="tab">
                            <h6 class="card-title"><a class="collapsed" id="accordion6-card-head-miiayuqb" data-toggle="collapse" data-parent="#accordion6" href="#accordion6-card-body-gqqphbuh" aria-controls="accordion6-card-body-gqqphbuh" aria-expanded="false" role="button">Lorem Ipsum is simply printing. <span class="card-arrow"></span> </a></h6>
                        </div>
                        <div class="collapse" id="accordion6-card-body-gqqphbuh" aria-labelledby="accordion6-card-head-miiayuqb" data-parent="#accordion6" role="tabpanel">
                            <div class="card-body">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer essentially unchanged. </p>
                            </div>
                        </div>
                    </article>
                </div>
            </div>
            <div class="col-md-6 col-lg-5">
                <article class="team-classic">
                    <div class="unit flex-column flex-sm-row align-items-start align-items-sm-center">
                        <div class="unit-left"><a class="team-classic-figure-3" href="#"><img src="<?= library_url() ?>/images/testimonial/client1.jpg" alt="" /></a></div>
                        <div class="unit-body">
                            <h5 class="team-classic-title-2">Contact Us! We'll get in touch as soon as possible.</h5>
                            <div class="team-classic-name"><a href="#">John Doe</a>, <span>Manager</span></div>
                        </div>
                    </div>
                </article>
                <form class="vanguard-form vanguard-mailform" data-form-output="form-output-global" data-form-type="contact" method="post" action="#">
                    <div class="form-wrap wow fadeInRight">
                        <input class="form-input" id="contact-name-6" type="text" name="name" data-constraints="@Required">
                        <label class="form-label" for="contact-name-6">Your Name*</label>
                    </div>
                    <div class="form-wrap wow fadeInRight" data-wow-delay=".05s">
                        <input class="form-input" id="contact-email-6" type="email" name="email" data-constraints="@Email @Required">
                        <label class="form-label" for="contact-email-6">Your E-mail*</label>
                    </div>
                    <div class="form-wrap wow fadeInRight" data-wow-delay=".15s">
                        <select class="form-input" data-minimum-results-for-search="Infinity" data-constraints="@Required">
                            <option value="1">Select a Service</option>
                            <option value="2">Construction Services</option>
                            <option value="3">Construction Services</option>
                            <option value="4">Construction Services</option>
                        </select>
                    </div>
                    <div class="form-wrap wow fadeInRight" data-wow-delay=".2s">
                        <input class="form-input" id="contact-phone-6" type="text" name="phone" data-constraints="@Numeric">
                        <label class="form-label" for="contact-phone-6">Phone</label>
                    </div>
                    <button class="button button-icon button-icon-right button-blue-8 wow fadeInRight" type="submit" data-wow-delay=".25s"><span class="icon fa fa-chevron-right"></span>Get in touch</button>
                </form>
            </div>
        </div>
    </div>
</section>
<!--======= faqwrap end =======-->
<!--======= Counter start=======-->
<section class="section counterWrapp section-lg">
    <div class="parallax-content context-dark">
        <div class="container">
            <!--section-title start-->
            <div class="row text-center">
                <div class="col-md-12">
                    <div class="main-heading wow fadeInLeft">
                        <h2 class="section-title-dash">Our <span class="section-title-dash-color"> Counter</span><span class="section-title-dash-bottom">&nbsp;</span> </h2>
                    </div>
                </div>
                <!-- Col end -->
            </div>
            <!--section-title end-->
            <div class="counter-modern-wrap">
                <div class="counter-modern">
                    <h3 class="counter-modern-number"><span class="counter">101</span><span class="symbol">+</span> </h3>
                    <div class="counter-modern-decor"></div>
                    <p class="counter-modern-title">Projects</p>
                </div>
                <div class="counter-modern">
                    <h3 class="counter-modern-number"><span class="counter">53</span> </h3>
                    <div class="counter-modern-decor"></div>
                    <p class="counter-modern-title">Awards</p>
                </div>
                <div class="counter-modern">
                    <h3 class="counter-modern-number"><span class="counter">246</span><span class="symbol">+</span> </h3>
                    <div class="counter-modern-decor"></div>
                    <p class="counter-modern-title">People</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!--======= Counter end =======-->
<!--======= Clients start=======-->
<!--<section class="section section-lg bg-default text-center">
    <div class="container">
        <div class="owl-carousel" data-items="1" data-sm-items="2" data-md-items="3" data-lg-items="5" data-margin="30" data-autoplay="true" data-dots="true" data-animation-in="fadeIn" data-animation-out="fadeOut"> <a class="clients-creative" href="#"><img src="<?/*= library_url() */?>/images/clients/1.jpg" alt="" /></a> <a class="clients-creative" href="#"><img src="<?/*= library_url() */?>/images/clients/2.jpg" alt="" /></a> <a class="clients-creative" href="#"><img src="<?/*= library_url() */?>/images/clients/3.jpg" alt="" /></a> <a class="clients-creative" href="#"><img src="<?/*= library_url() */?>/images/clients/4.jpg" alt="" /></a> <a class="clients-creative" href="#"><img src="<?/*= library_url() */?>/images/clients/5.jpg" alt="" /></a> <a class="clients-creative" href="#"><img src="<?/*= library_url() */?>/images/clients/6.jpg" alt="" /></a> <a class="clients-creative" href="#"><img src="<?/*= library_url() */?>/images/clients/7.jpg" alt="" /></a> <a class="clients-creative" href="#"><img src="<?/*= library_url() */?>/images/clients/8.jpg" alt="" /></a> </div>
    </div>
</section>-->
<!--======= Clients end =======-->

<?php get_footer(); ?>
