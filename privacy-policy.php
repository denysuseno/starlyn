<?php
/* Template Name: Privacy Policy */

global $post;
?>

<?php get_header(); ?>

<div id="site-content" class="site-content site-content-about">
    <div class="site-content-inner">
        <main class="main" role="main">
            <div class="page-single wrapper-about">
                <div class="terms">
                    <section class="section terms-header intro">
                        <h3 class="text">Privacy Policy</h3>
                    </section>
                    <section class="section terms-content intro">
                        <?php the_content() ?>
                    </section>
                </div>
            </div>
        </main>
    </div>
</div>

<?php get_footer(); ?>
