<div class="site-header-top">
    <div class="wrapper">
        <div class="nav">
            <ul class="items">
                <li class="item">
                    <?php if( is_user_logged_in() ) : ?>
                        <a href="<?php echo get_permalink( get_page_by_path('wishlist') ) ?>" class="link"><svg class="icon" role="img"><use xlink:href="<?= library_url() ?>/images/svg-symbols.svg#icon-love" /></svg> Wishlist</a>
                    <?php else : ?>
                        <a href="<?php echo esc_url( get_permalink( get_option('woocommerce_myaccount_page_id') ) ) ?>" class="link"><svg class="icon" role="img"><use xlink:href="<?= library_url() ?>/images/svg-symbols.svg#icon-love" /></svg> Wishlist</a>
                    <?php endif; ?>
                </li>
                <li class="item">
                    <?php if( !is_user_logged_in()) : ?>
                        <a href="#" class="link js-popup-login"><svg class="icon" role="img"><use xlink:href="<?= library_url() ?>/images/svg-symbols.svg#icon-user" /></svg> Account</a>

                        <!-- popup login -->
                        <div id="popup-login" class="popup-login">
                            <div class="popup-login-main">
                                <form class="woocommerce-form woocommerce-form-login login form" method="post">
                                    <div class="form-input">
                                        <div class="label">
                                            <label for="username" class="label-text"><?php esc_html_e( 'Email:', 'woocommerce' ); ?></label>
                                        </div>
                                        <input type="text" class="woocommerce-Input woocommerce-Input--text input-text input" name="username" id="username" autocomplete="username" required value="<?php echo ( ! empty( $_POST['username'] ) ) ? esc_attr( wp_unslash( $_POST['username'] ) ) : ''; ?>" /><?php // @codingStandardsIgnoreLine ?>
                                    </div>
                                    <div class="form-input">
                                        <div class="label">
                                            <span class="label-text"><?php esc_html_e( 'Password:', 'woocommerce' ); ?></span>
                                            <div class="remember">
                                                <input class="woocommerce-form__input woocommerce-form__input-checkbox" name="rememberme" type="checkbox" id="rememberme" value="forever" style="margin-right: 4px" />
                                                <label for="rememberme" class="remember-label">Ingat saya</label>
                                            </div>
                                        </div>
                                        <input class="woocommerce-Input woocommerce-Input--text input-text input" type="password" name="password" id="password" required autocomplete="current-password" />
                                    </div>
                                    <div class="form-action">
                                        <input type="hidden" name="redirect" value="<?= get_permalink( get_option('woocommerce_myaccount_page_id') ) ?>">
                                        <?php wp_nonce_field( 'woocommerce-login', 'woocommerce-login-nonce' ); ?>
                                        <button type="submit" class="woocommerce-button woocommerce-form-login__submit button button-gray" name="login" value="<?php esc_attr_e( 'Log in', 'woocommerce' ); ?>"><?php esc_html_e( 'Login', 'woocommerce' ); ?></button>
                                        <a href="<?php echo esc_url( wp_lostpassword_url() ); ?>" class="forgot-password"><?php esc_html_e( 'Forgot your password?', 'woocommerce' ); ?></a>
                                    </div>
                                    <div class="social-login">
                                        <ul class="social-login-items">
                                            <?php if( getFacebookLoginUrl() != "" ) : ?>
                                                <li class="social-login-item">
                                                    <a href="<?= getFacebookLoginUrl() ?>" class="button button-primary facebook">
                                                        <svg class="icon" role="img"><use xlink:href="<?= library_url() ?>/images/svg-symbols.svg#icon-facebook" /></svg>
                                                        <span class="text">Log in with Facebook</span>
                                                    </a>
                                                </li>
                                            <?php endif; ?>

                                            <?php if( getGoogleLoginUrl() != "" ) : ?>
                                                <li class="social-login-item">
                                                    <a href="<?= getGoogleLoginUrl() ?>" class="button button-primary googleplus">
                                                        <svg class="icon" role="img"><use xlink:href="<?= library_url() ?>/images/svg-symbols.svg#icon-google-plus" /></svg>
                                                        <span class="text">Log in with Google</span>
                                                    </a>
                                                </li>
                                            <?php endif; ?>
                                        </ul>
                                    </div>
                                </form>
                            </div>
                            <div class="popup-login-footer">
                                <p>Don’t have an account? <a href="<?php echo esc_url( add_query_arg( ['register' => 1], get_permalink( get_option('woocommerce_myaccount_page_id') ) ) ) ?>" class="signup-link">Sign up</a></p>
                            </div>
                        </div>
                        <!-- end popup login -->
                    <?php else : ?>
                        <?php $currentUser = wp_get_current_user(); ?>
                        <a href="<?= get_permalink( get_option('woocommerce_myaccount_page_id') ) ?>" class="link"><svg class="icon" role="img"><use xlink:href="<?= library_url() ?>/images/svg-symbols.svg#icon-user" /></svg> <?= $currentUser->display_name ?></a>
                    <?php endif; ?>

                </li>
            </ul>
        </div>
    </div>
</div>