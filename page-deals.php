<?php
/* Template Name: Deals */

global $post;
$hero       = get_field( 'hero', $post->ID );
$header     = get_field( 'header', $post->ID );

$ordering   = page_deals_ordering_args();
$s          = isset( $_GET['search'] ) && ! empty( $_GET['search'] ) ? $_GET['search'] : '';
$query_args = query_page_deals( $ordering['orderby'], $ordering['meta_key'], $ordering['order'], 1, $s );
$loop       = new WP_Query( $query_args );
?>

<?php get_header(); ?>

    <div id="site-content" class="site-content site-content-deal">
        <div class="site-content-inner">
            <main class="main" role="main">
                <div class="deals-page">
                    <!--<div class="deals-page-hero">
                        <div class="wrapper">
                            <h4 class="text"><?php echo $hero['hero_text'] ?></h4>
                            <h1 class="title"><?php echo $hero['hero_title'] ?></h1>
                            <h1 class="subtitle"><?php echo $hero['hero_subtitle'] ?></h1>
                        </div>
                    </div>-->
                    <div class="deals-page-content">
                        <div class="wrapper">
                            <div class="header">
                                <h2 class="title"><?php echo $header['header_title'] ?></h2>
                                <p class="subtitle"><?php echo $header['header_subtitle'] ?></p>
                            </div>
                            <div class="content">

                                <?php if ( $loop->have_posts() ) : ?>

                                    <?php
                                    /**
                                     * Hook: woocommerce_before_shop_loop.
                                     *
                                     * @hooked woocommerce_output_all_notices - 10
                                     * @hooked woocommerce_result_count - 20
                                     * @hooked woocommerce_catalog_ordering - 30
                                     */
                                    // do_action( 'woocommerce_before_shop_loop' );
                                    ?>

                                    <div class="notices" style="margin-bottom: 32px">
                                        <?php do_action( 'starlyn_product_archives_output_all_notices' ); ?>
                                    </div>

                                    <div class="filter">
                                        <form class="form woocommerce-ordering">
                                            <div class="filter-field filter-search">
                                                <input type="text" class="input" name="search" placeholder="Search" value="<?php echo isset( $_GET['search'] ) ? $_GET['search'] : '' ?>">
                                            </div>
                                            <div class="filter-field filter-sort">
                                                <?php woocommerce_catalog_ordering() ?>
                                            </div>
                                        </form>
                                    </div>

                                    <?php woocommerce_product_loop_start(); ?>

                                        <?php
                                            while ( $loop->have_posts() ) {
                                                $loop->the_post();

                                                /**
                                                 * Hook: woocommerce_shop_loop.
                                                 */
                                                do_action( 'woocommerce_shop_loop' );

                                                wc_get_template_part( 'content', 'product' );
                                            }
                                            wp_reset_postdata();
                                        ?>

                                    <?php woocommerce_product_loop_end(); ?>

                                    <?php
                                    /**
                                     * Hook: woocommerce_after_shop_loop.
                                     *
                                     * @hooked woocommerce_pagination - 10
                                     */
                                    // do_action( 'woocommerce_after_shop_loop' );
                                    ?>

                                    <?php if( $loop->max_num_pages > 1 ) : ?>
                                        <div class="view-more">
                                            <a href="javascript:void(0)" id="more-posts-deals" class="link">View More</a>
                                        </div>
                                    <?php endif; ?>

                                <?php else : ?>

                                    <?php
                                    /**
                                     * Hook: woocommerce_no_products_found.
                                     *
                                     * @hooked wc_no_products_found - 10
                                     */
                                    do_action( 'woocommerce_no_products_found' );
                                    ?>

                                <?php endif; ?>

                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>
    </div>

<?php get_footer(); ?>
