<?php
/* Template Name: Payment Confirmation */

$order_no       = WC()->session->get( 'pc_order_no' );
if( isset( $_GET['order_no'] ) && $_GET['order_no'] ) {
    $order_no = $_GET['order_no'];
}
$payment_date   = WC()->session->get( 'pc_payment_date' );
$account_name   = WC()->session->get( 'pc_account_name' );
$bank           = WC()->session->get( 'pc_bank' );
$notes          = WC()->session->get( 'pc_notes' );
?>

<?php get_header(); ?>

    <div id="site-content" class="site-content">
        <div class="site-content-inner">
            <main class="main" role="main">
                <div class="payment-confirmation">
                    <div class="wrapper">
                        <div class="header">
                            <h2 class="title">Payment Confirmation</h2>
                            <p class="subtitle">Please fill up the form below to complete your payment confirmation</p>
                        </div>

                        <?php wc_print_notices(); ?>

                        <form class="form" action="<?php echo get_permalink() ?>" method="post" enctype="multipart/form-data">
                            <input type="hidden" name="action" value="payment_confirmation">
                            <?php wp_nonce_field( 'payment_confirmation_action', 'payment_confirmation_nonce' ); ?>
                            <div class="form-input">
                                <div class="form-input-field">
                                    <label class="label" for="order-no">Order No.</label>
                                    <input type="text" name="order_no" class="input" id="order-no" value="<?php echo $order_no ?>" required>
                                </div>
                                <div class="form-input-field">
                                    <label class="label" for="payment-date">Payment Date</label>
                                    <input type="date" name="payment_date" class="input" id="payment-date" value="<?php echo $payment_date ?>" required>
                                </div>
                                <div class="form-input-field">
                                    <label class="label" for="account-name">Account Name</label>
                                    <input type="text" name="account_name" class="input" id="account-name" value="<?php echo $account_name ?>" required>
                                </div>
                                <div class="form-input-field">
                                    <label class="label" for="bank">Bank</label>
                                    <input type="text" name="bank" class="input" id="bank" value="<?php echo $bank ?>" required>
                                </div>
                                <div class="form-input-field">
                                    <label class="label" for="notes">Notes</label>
                                    <textarea name="notes" class="input" id="notes"><?php echo $notes ?></textarea>
                                </div>
                                <div class="form-input-field file">
                                    <label class="label" for="file">Transfer File</label>
                                    <input name="attachment_file" type="file" id="file" required>
                                </div>
                            </div>
                            <div class="form-submit">
                                <button class="button button-gray">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </main>
        </div>
    </div>

<?php get_footer(); ?>