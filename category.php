<?php get_header(); ?>

<!-- Breadcrumbs -->
<section class="bg-gray-7">
    <div class="breadcrumbs-custom box-transform-wrap context-dark">
        <div class="container">
            <h3 class="breadcrumbs-custom-title"><?php single_cat_title() ?></h3>
            <div class="breadcrumbs-custom-decor"></div>
        </div>
        <div class="box-transform" style="background-image: url(<?= library_url() ?>/images/background/background-7.jpg);"></div>
    </div>
    <div class="container">
        <ul class="breadcrumbs-custom-path">
            <li><a href="<?= home_url() ?>">Home</a></li>
            <li><a href="<?= get_permalink( get_page_by_title('News') ) ?>">News</a></li>
            <li class="active">Kategori: <?php single_cat_title() ?></li>
        </ul>
    </div>
</section>
<!-- Grid Blog-->
<section class="section section-lg bg-default">
    <div class="container">
        <div class="row row-40 row-lg-60 justify-content-center">

            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <div class="col-sm-6 col-lg-4 order-lg-1">
                    <!-- Post Classic-->
                    <article class="post post-classic block-3">
                        <div class="post-classic-figure"><img src="<?php the_post_thumbnail_url(); ?>" alt="<?php the_title() ?>" />
                        </div>
                        <div class="post-classic-content">
                            <p class="post-classic-title"><a href="<?php the_permalink() ?>"><?php the_title() ?></a>
                            </p>
                            <p><?php the_excerpt() ?></p>
                        </div>
                        <time class="post-classic-time" datetime="<?= get_the_date('Y-m-d') ?>"><?= get_the_date('d F Y') ?></time>
                    </article>
                </div>
            <?php endwhile; ?>
            <?php else : ?>
                Tidak ada artikel
            <?php endif; ?>

        </div>
        <div class="pagination-wrap">
            <!-- Bootstrap Pagination-->
            <nav aria-label="Page navigation">
                <?php wpbeginner_numeric_posts_nav() ?>
            </nav>
        </div>
    </div>
</section>

<?php get_footer(); ?>